/**
 * A place for classes related to Njord's Content Managemnt type system.
 * For now it's just going to hold a hand built bogus class but in the future it will be replaced with a more robust CM system
 * which will allow for internationalization.
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.cm;
/**
 * The intent here is to make Njord support plugins for what it supports. Plugin architecture will need a class to define how to get a list of objects (Both local and remote) into the navigation tree. How to fetch and save them to both local and remote filesystems and a definition of the editor to use. I need to figure out how to list the classes in a directory and then execute each of those. I need to create an interface which plugins must support so that I know what methods to invoke.
 * Maybe I can document the plugin architecture here for a while.
 * 
 * 
 * Plugins need to support
 * 
 * getting an editor for an object. 
 * Getting the Class for the object they support the editing of? IE I should have one for an iRule one for an iApp template.
 * The object should provide an editor
 * public void newObjectFromDialog()  should take the object type as an argument. Thus the objects need to implement an interface which provides they return a name and a way to get an editor.
 * The objects themselves need to implement the load and save functionality. 
 * 
 * 
 * I think I need to read up on the eclipse plugin architecture and emulate some (but definately not all) of their functionality.
 * http://www.eclipse.org/articles/Article-Plug-in-architecture/plugin_architecture.html
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.plugin;
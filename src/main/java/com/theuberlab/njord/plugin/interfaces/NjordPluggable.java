package com.theuberlab.njord.plugin.interfaces;

import java.util.List;
import java.util.Map;

//TODO: I need to update NjordPluginManager to scan for jar files. Then I need to define the format of jar files. This is so that the plugins can include their own include files, etc.

/**
 * <code>NjordPluggable</code> defines an interface which would allow new plugins to be developed for Njord to manage objects of different types than those it comes with.
 * 
 * @author Aaron Forster
 *
 */
public interface NjordPluggable {
//    public String shortName;
    // Should probably have some variables
    //shortName
    //fullName
    //fullPath (which is probably redundant with the above
    //editor(theRsyntaxTextEditor)
    //isDirty
    /**
     * Returns the name of the plugin.
     * 
     * @return
     */
    abstract String getPluginName();
    
    /**
     * returns the category like iRule, iApps Template, Virtual Server, etc. This should return a name which does not end in an s. The S will be added for list objects.
     * 
     * @return
     */
    abstract String getObjectCategory();
    
    //TODO: I should set a max characters limit here.
    /**
     * returns a short form of the category name suitable for the navigation tree. This should return a name which does not end in an s. The S will be added for list objects.
     * Should be a static method for this category.
     * 
     * @return
     */
    abstract String getObjectCategoryShortName();
    
    /**
     * Returns a map of archetypes (templates) Suitable for the plugins editor with the form of Map<String archetypeName, object archtype>. For example getObjectArchetypes() for iRules returns a map of iRules.
     * @return
     */
    abstract Map getObjectArchitypes();
    
    //TODO: Figure out what type this is to return. Probably string for now.
    /**
     * The list of items to render in the list. I need to define a format for these.
     * This actually is going to need to be a specific object type which I could return a full path and a short name from. Short name to use in the list and full path for getting and saving.
     * Should be a static method.
     * 
     * @return
     */
    abstract List<NjordPluggable> getList();
    
    //TODO: Figure out what type this is to return. Probably string for now.
    /**
     * Same as getList but for local objects. getLocalList() should somehow be optional.
     * Should be a static method.
     * 
     * @return
     */
    abstract List<NjordPluggable> getLocalList();
    
    //TODO: Currently of type org.fife.ui.rsyntaxtextarea.TextEditorPane but that should be wrapped in some Njord class. Perhaps this should be the empty editor and getObject returns the object I can stuff into it?
    /**
     * Should return the object within an editor that I can use to "Edit" this object given the full name (full path) of an object.
     * 
     * @param objectFullName
     * @return
     */
    abstract org.fife.ui.rsyntaxtextarea.TextEditorPane getEditor();
    
    /**
     * Writes the object both local or remote. This means there should be some interface requirements for objecte definition which define clean/dirty as well as get/save methods. Well actually if the object itself defines the save method this one isn't neccesary. OMG confusing
     * 
     * @param objectName
     * @return
     */
    abstract boolean saveObject(NjordPluggable object);
    
    /**
     * Tells us if we should bother looking for local versions. Remote will always be supported since it is the default.
     * 
     * @return
     */
    abstract boolean getIsLocalSupported();
    
    /*
     * Short name would be just a display name sans any path portioon. IE /common/myRuleName would return myRuleName.
     */
    abstract String getShortName();
    
    /**
     * Full name would include any path portion /common/stuff
     * @return
     */
    abstract String getFullName();
    
    /**
     * Needs to return the clean/dirty status of the object.
     * 
     * @return
     */
    abstract boolean getIsDirty();
    
    /**
     * On a per object basis tells us if the object is local or remote.
     * 
     * @return
     */
    abstract boolean getIsLocal();
}
/**
 * 
 */
/**
 * This is where plugin specific interfaces will be defined.
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.plugin.interfaces;
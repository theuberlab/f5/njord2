package com.theuberlab.njord.plugin.plugins;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fife.ui.rsyntaxtextarea.TextEditorPane;

import com.theuberlab.njord.plugin.interfaces.NjordPluggable;

/**
 * <code>NjordiRulePlugin</code>
 * Hrm... I can't force static methods w/ an interface. Perhaps when instantiated blank it's more of a 'factory' type object?  
 * Then how do I specify the difference. I think I may need a fetch() method which tells it to fetch the object from wherever it is (remote or local)
 * Which also means I probably needa getIsFecthed() to make sure such a thing has occurred. Or perhaps getEditor should just do that?
 * 
 * @author Aaron Forster
 *
 */
public class NjordiRulePlugin implements NjordPluggable {
    protected String fullCategoryName = "iRules";
    protected String shortCategoryName = "iRules";
    protected Map<String, String> archetypes = new HashMap<String, String>();
    /**
     * Holds the line separator string for the os we're running on.
     */
    public String nl = System.getProperty("line.separator");
    
    /**
     * 
     */
    public NjordiRulePlugin() {
        archetypes.put("blank", "");
        String iRuleText = "#Sample grabbed from https://devcentral.f5.com/wiki/iRules.HtmlCommentScrubber.ashx for testing purposes" + nl 
                + "when HTTP_REQUEST {" + nl
                + "# Don't allow data to be chunked" + nl
                + " if { [HTTP::version] eq \"1.1\" } {" + nl
                + "     if { [HTTP::header is_keepalive] } {" + nl
                + "         HTTP::header replace \"Connection\" \"Keep-Alive\"" + nl
                + "     }" + nl
                + "     HTTP::version \"1.0\"" + nl
                + " }" + nl
                + "}" + nl
                + "when HTTP_RESPONSE {" + nl
                + " if { [HTTP::header exists \"Content-Length\"] && [HTTP::header \"Content-Length\"] < 1000000} {" + nl
                + "     set content_length [HTTP::header \"Content-Length\"]" + nl
                + " } else {" + nl
                + "     set content_length 1000000" + nl
                + " }" + nl
                + " if { $content_length > 0 } {" + nl
                + "     HTTP::collect $content_length" + nl
                + " }" + nl
                + "}" + nl
                + "when HTTP_RESPONSE_DATA {" + nl
                + " # Find the HTML comments" + nl
                + "" + nl
                + " set indices [regexp -all -inline -indices {<!--(?:[^-]|--|[^-]-[^-])*?--\\s*?>} [HTTP::payload]]" + nl
                + " # Replace the comments with spaces in the response" + nl
                + " #log local0. \"Indices: $indices\"" + nl
                + "" + nl
                + " foreach idx $indices {" + nl
                + "     set start [lindex $idx 0]" + nl
                + "     set len [expr {[lindex $idx 1] - $start + 1}]" + nl
                + "     log local0. \"Start: $start, Len: $len\"" + nl
                + "     HTTP::payload replace $start $len [string repeat \" \" $len]" + nl
                + " }" + nl
                + "}" + nl;
        archetypes.put("HTML Scrubber", iRuleText);
        
    }
    
    /**
     * Get's the plugin name.
     */
    public String getPluginName() {
        return shortCategoryName;
    }
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getObjectCategory()
     */
    @Override
    public String getObjectCategory() {
        return fullCategoryName;
    }
    
    public String getArchitypeText(String archetypeName) {
        return archetypes.get(archetypeName);
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getObjectCategoryShortName()
     */
    @Override
    public String getObjectCategoryShortName() {
        return shortCategoryName;
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getList()
     */
    @Override
    public List<NjordPluggable> getList() {
        // TODO Auto-generated method stub
        return null;
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getLocalList()
     */
    @Override
    public List<NjordPluggable> getLocalList() {
        // TODO Auto-generated method stub
        return null;
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getEditor()
     */
    @Override
    public TextEditorPane getEditor() {
        // TODO Auto-generated method stub
        return null;
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#saveObject(com.theuberlab.njord.plugin.NjordPluggable)
     */
    @Override
    public boolean saveObject(NjordPluggable object) {
        // TODO Auto-generated method stub
        return false;
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getIsLocalSupported()
     */
    @Override
    public boolean getIsLocalSupported() {
        // TODO Auto-generated method stub
        return false;
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getShortName()
     */
    @Override
    public String getShortName() {
        // TODO Auto-generated method stub
        return null;
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getFullName()
     */
    @Override
    public String getFullName() {
        // TODO Auto-generated method stub
        return null;
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getIsDirty()
     */
    @Override
    public boolean getIsDirty() {
        // TODO Auto-generated method stub
        return false;
    }
    
    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.NjordPluggable#getIsLocal()
     */
    @Override
    public boolean getIsLocal() {
        // TODO Auto-generated method stub
        return false;
    }
    // ###############################################################
    // VARIABLES
    // ###############################################################

    /* (non-Javadoc)
     * @see com.theuberlab.njord.plugin.interfaces.NjordPluggable#getObjectArchitypes()
     */
    @Override
    public Map getObjectArchitypes() {
        return archetypes;
    }
    
    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################
    
    // ###############################################################
    // GETTERS AND SETTERS
    // ###############################################################
    
    // ###############################################################
    // METHODS
    // ###############################################################
    
}

/**
 * This is where all default plugins will be found.
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.plugin.plugins;
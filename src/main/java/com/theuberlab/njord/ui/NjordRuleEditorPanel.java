package com.theuberlab.njord.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;

import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.SyntaxScheme;
import org.fife.ui.rsyntaxtextarea.TextEditorPane;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.parser.TaskTagParser;
import org.fife.ui.rtextarea.RTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javadocking.dockable.DraggableContent;
import com.javadocking.drag.DragListener;
import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.interfaces.NjordEditorPanel;
import com.theuberlab.njord.main.Njord;
import com.theuberlab.njord.main.NjordConcierge;
import com.theuberlab.njord.util.NjordCompletionProvider;
import com.theuberlab.njord.util.NjordDocumentListener;
import com.theuberlab.njord.util.NjordFileLocation;
import com.theuberlab.njord.util.iRuleTootipParser;
import com.theuberlab.njord.util.iRulesTokenMaker;

/**
 * <code>NjordTextPanel</code> Currently only takes some text and creates a panel which contains that text. Not very useful.
 * 
 * @author Aaron Forster
 *
 */
public class NjordRuleEditorPanel extends JPanel implements DraggableContent, NjordEditorPanel {
	// ###############################################################
	// VARIABLES
	// ###############################################################
	/** Serial Version ID */
	private static final long serialVersionUID = 1L;
    /**
     * The short name of this node. I.E. myiRule. We have to store this because the layout manager is going to call getName()
     */
    public String name;
    /**
     * The full name including the path prefix for v10 systems. I.E. /Common/myiRule
     */
    public String fullName;
	/**
	 * What created us.
	 */
	protected NjordConcierge concierge = null;
	/**
     * Holds the line separator string for the os we're running on.
     */
    public String nl = System.getProperty("line.separator");
    /** What to prefix log messages with */
    public String logPrefix = this.getClass().getName() + ": ";
    /**
     * Holds our logger factory for SLF4J.
     */
    public final Logger log = LoggerFactory.getLogger(Njord.class);
    protected NjordFileLocation fileLoc;
    protected TextEditorPane editorPane;
    protected RTextScrollPane paneScrollPane;
    protected AutoCompletion autoCompletion;
    
    
    /* 
     * #######################################################################
     * Editor colors
     * #######################################################################
     */
    
    //TODO: Add a way to update these? Maybe just pull them from the prefs file and you have to edit that by hand if you want to edit them.
    //These are good enough for now.
    /**
     * The color to use for highlighting annotations
     */
    public Color annotationColor = Color.decode("#646464");
    /**
     * The color to use for highlighting variables
     */
    public Color variableColor = Color.decode("#0000C0");
//  public Color identifyerColor = Color.decode("#880080");
    /**
     * The color to use for highlighting comments
     */
    public Color commentColor = Color.decode("#3F7F5F"); // Leave this the same green for now but may override with this green from eclipse
    /**
     * The color to use for highlighting functions
     */
    public Color functionColor = Color.decode("#880080");
    /**
     * The color to use for highlighting regular expressions
     */
    public Color regexColor = Color.decode("#2A00FF");
    /**
     * The color to use for highlighting tcl built in commands
     */
    public Color reservedWordColor = Color.decode("#0033FF"); // TCL built in functions (not entirely accurate)
    /**
     * The color to use for highlighting iRules added commands
     */
    public Color reservedWord2Color = Color.decode("#000099"); // Irules stuff
    /**
     * The color to use for highlighting operators
     */
    public Color operatorColor = new Color(880080);
    /**
     * The color to use for highlighting quoted text
     */
    public Color doublequoteColor = Color.decode("#1a5a9a");
    /**
     * The color to use for highlighting backquoted text
     */
    public Color backquoteColor = Color.decode("#4a2a3a");
    /**
     * The color to use for highlighting brackets
     */
    public Color bracketColor = Color.decode("#990099");
    
    /**
     * The RSyntaxtTextArea pane.
     */
    private TextEditorPane ruleEditor = null;
//	/**
//	 * Default Empty constructor. Not in use
//	 */
//	public NjordTextPanel() {
//		// TODO Auto-generated constructor stub
//	}

	
    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################
    /*
     * Currently the only constructor that should be used.
     * 
     */
    public NjordRuleEditorPanel(NjordConcierge concierge, NjordFileLocation iRuleLocation, String name) {
//        super(new FlowLayout());
        super(new BorderLayout());
        this.concierge = concierge;
        this.fileLoc = iRuleLocation;
        this.name = name;
        
        // Register with the concierge
        concierge.registerEditorPanel(this);
        
        // ruleEditor = new TextEditorPane(0, true, iRuleLocation, "ISO-8859-1");
//        TextEditorPane 
        editorPane = getEditorForRule(iRuleLocation);
        paneScrollPane = new RTextScrollPane(editorPane);
//        paneScrollPane.setLineNumbersEnabled(owner.getPreferences().getShwLineNumbers());
        
//        JScrollPane paneScrollPane = new JScrollPane(editorPane);
        paneScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        paneScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        paneScrollPane.setPreferredSize(new Dimension(500, 350));
        
        setPreferredSize(new Dimension(500, 350));
        setMinimumSize(new Dimension(80,80));
        setBorder(BorderFactory.createLineBorder(Color.lightGray));
        
        editorPane.setCaretPosition(0);
        this.add(paneScrollPane, BorderLayout.CENTER);    
        
        // This makes it so that when you click in an editor which has been torn off Njord sets the last active editor
        editorPane.setFocusable(true);
        FocusRequester focusRequester = new FocusRequester(this, editorPane);
        editorPane.addMouseListener(focusRequester);
        editorPane.addFocusListener(focusRequester);
        
        // Now set everything that we're getting from the preferences file.
        updateViewSettingsFromPrefs();
    }
    
    // TODO: I think this might need to be moved to the main Njord class but I am no longer sure.
    // This bit here will trigger if someone clicks within an editor itself or on the title bar of an editor that has been externalized.
    // It does _not_ trigger if a docked tab is selected. That is done via the overridden stateChanged(ChangeEvent e) within the WorkspaceComponentFactory
    // private class inside of the main Njord class.
    private class FocusRequester extends MouseAdapter implements FocusListener {
        Component component;
        NjordRuleEditorPanel ownerPanel = null;
        
        public FocusRequester(NjordRuleEditorPanel ownerPanel, Component component) {
            this(component);
            this.ownerPanel = ownerPanel;
//            this.component = component;
        }
            public FocusRequester(Component component) {
                this.component = component;
            }
            public void mouseClicked(MouseEvent e) {
                System.out.println("requestFocusInWindow for window [" + name + "]");
                component.requestFocusInWindow();
            }
            public void focusGained(FocusEvent arg0) {
                System.out.println("focusGained for window [" + name + "]");
                // Unfortunately this returns the FocusRequester class. Unclear how to set it to the actual editor
                concierge.getOwner().setActiveEditorPane(ownerPanel);
            }
            public void focusLost(FocusEvent arg0) {
                System.out.println("focusLost for window [" + name + "]");
            }
        }
	// ###############################################################
	// GETTERS AND SETTERS
	// ###############################################################
    
    public String getName() {
//        return fileLoc.getFileName();
        return name;
    }
    
    public String getFullName() {
        return fileLoc.getFileFullPath();
    }
    
    public boolean getIsDirty() {
        return editorPane.isDirty();
    }
    
    public boolean getIsLocal() {
        return fileLoc.isLocal();
    }
    
    public boolean getExists() {
        return fileLoc.exists;
    }
    
    public String toString() {
        return this.getName();
    }
    
    //TODO: add a method (and the supporting infrastructure) to find out what the type is.
    
    
	// ###############################################################
	// METHODS
	// ###############################################################
	
	/**
     * Currently new iRule is going to use null for iRuleLocation and I'll need to create a 'deploy to bigip' function.
     * 
     * @param iRuleLocation Expects mostly a NjordFileLocation but should be generic enough to handle any. 
     * @param iRuleName
     * @return A TextEditorPane which contains an iRule.
     */
    @SuppressWarnings("static-access")
    public TextEditorPane getEditorForRule(NjordFileLocation iRuleLocation) {
        log.debug("Running getEditorForRule(FileLocation iRuleLocation[{}], String iRuleName [{}])", iRuleLocation.getFileFullPath(), 
                iRuleLocation.getFileName());
        String iRuleName = iRuleLocation.getFileName();
        
//      How do to this so one should go if there's a remote location and another if it's local
//      
//      if we are handed a NjordFileLocation then it's remote and open an editor with that. If not then open an editor with a default local location?
//      Let's cheat w/ the new iRule dialog again and if we have local rules selected we do a new local irule and if we have remote we do a remote?
//      Also select remote if we have nothing selected but we _do_ have a valid connection to the bigip?
//      No because I have to make it so we can select non-leaf nodes first. 
//      new iRule just has to be expanded to handle both local and remote and the local/remote selection can be set automatically based on what's selected.
//      Actually for RIGHT NOW le'ts just add a new local iRule button.
//      
//      So then new local iRule I suppose I should use a default location handler for local windows? Then just give it a path in your home directory?
//              
//      
//              
//              Ok here's what I'm going to do.
//              
//              NjordFileLocation is going to be modified to handle both local and remote Files.
//              If it's Local then we are going to use some other output stream. If it's remote we will use the njordoutputstream. How will we handle migrating local to remote?
//              Maybe a new deploy to bigip button that actually just gets the contents of the text editor, issues a create irule on the bigip with them then re-fetches the list of iRules from the server. I love this plan.
//              Then if the file is 'local' all actions like getOutputStream return some output stream suitable for 'local file system' or whatever and if remote uses the other.
//              
//              So then my todos right now are create this method have new iRule use a null for file location if there's no valid connection which would just let you create the editor and such. Then I can get this subroutine working and tested and deal with the other outputstream after.     
                
        //For the methods that take a default encoding value "If this value is null, the system default encoding is used."
        
        
        try {
            ruleEditor = new TextEditorPane(0, true, iRuleLocation, "ISO-8859-1");
            
        } catch (IOException e) {
            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
            exceptionHandler.processException();
        }
        
        ruleEditor.setName(iRuleName);
        ruleEditor.discardAllEdits(); //This prevents undo from 'undoing' the loading of the editor with an iRule. Should be unnessesary now that I'm using FileLocation.
//        ruleEditor.getDocument().addDocumentListener(new NjordDocumentListener(this));
        ruleEditor.getDocument().addDocumentListener(new NjordDocumentListener(concierge));
        
        //Syntax Highlighting items
        ruleEditor.setSyntaxEditingStyle("SYNTAX_STYLE_IRULES");
//        ruleEditor.setCodeFoldingEnabled(true);
//        ruleEditor.setCodeFoldingEnabled(owner.getPreferences().getCodeFoldingEnabled());
        ruleEditor.setAntiAliasingEnabled(true);
        // Show line numbers is up above.
//        ruleEditor.setLineWrap(owner.getPreferences().getLineWrapEnabled());
//        ruleEditor.setAutoscrolls(owner.getPreferences().getAutoScroll());
//        ruleEditor.setPaintTabLines(owner.getPreferences().getPaintTabLines());
//        ruleEditor.setWhitespaceVisible(owner.getPreferences().getVisibleWhiteSpace());
//        ruleEditor.setEOLMarkersVisible(owner.getPreferences().getEOLMarkersVisible());
        
        

        //Colors pulled out of the eclipse gui
//      Variables blue             00 00 192 0000C0
//      keywords  purpleish        127 00 85  7F0055
//      Operators solid black      00 00 00  000000
//      Strings  also blue         42 00 255  2A00FF
//      Annotations grey           100 100 100 646464   (I think this is stuff like @Test)
//      comments greenish          63  127  95  3F7F5F
//      todo tag blue grey         127 159 191  7F9FBF
        
        //Some of my own
        // A couple of slightly different but good blues
        //0000CC 0000FF    how about 000066 and  0033ff
        //Some good purples
        // 660066 990099
        
        SyntaxScheme scheme = ruleEditor.getSyntaxScheme();
        
        log.debug(logPrefix + "Vars currently have " + scheme.getStyle(Token.VARIABLE).foreground);
        
        scheme.getStyle(Token.ANNOTATION).foreground = annotationColor;
        scheme.getStyle(Token.VARIABLE).foreground = variableColor;
//      scheme.getStyle(Token.IDENTIFIER).foreground = identifyerColor; //I think 'identifyer' is all the other words.
        scheme.getStyle(Token.FUNCTION).foreground = functionColor;
        scheme.getStyle(Token.REGEX).foreground = regexColor;
        scheme.getStyle(Token.COMMENT_EOL).foreground = commentColor;
//      scheme.getStyle(Token.COMMENT_MULTILINE).foreground = commentColor; // I don't think there's a multi-line comment defined yet
        scheme.getStyle(Token.RESERVED_WORD_2).foreground = reservedWord2Color;
        scheme.getStyle(Token.RESERVED_WORD).foreground = reservedWordColor;
        scheme.getStyle(Token.OPERATOR).foreground = operatorColor;
//      scheme.getStyle(Token.LITERAL_STRING_DOUBLE_QUOTE).foreground = doublequoteColor;
        scheme.getStyle(Token.LITERAL_BACKQUOTE).foreground = backquoteColor;
        scheme.getStyle(Token.SEPARATOR).foreground = bracketColor;
        
        // A CompletionProvider is what knows of all possible completions, and
        // analyzes the contents of the text area at the caret position to
        // determine what completion choices should be presented. Most
        // instances of CompletionProvider (such as DefaultCompletionProvider)
        // are designed so that they can be shared among multiple text
        // components.
        NjordCompletionProvider provider = createCompletionProvider();
        provider.setAutoActivationRules(true, ":_");
        
        //Trying to get hyperlinks working the below isn't doing it. Taking a look at the JavaScriptTokenMaker it has functionality to
        //  identify URLs. From the comments that hyperlinks only work if the language supports it I'm going to guess that hyperlinks
        //  means if the full http://etc.etcimator.com is in the code. not what I'm looking for where you can have anything be a hyperlink
        //  I'm going to have to extend it. I'll make the thing set tooltip text for them and then figure out how to modify the hyperlink
        //  listener so that it works. Or even better just modify a double click listener. Ooh, that's good.
//        ruleEditor.setLinkScanningMask(InputEvent.ALT_DOWN_MASK);
//        ruleEditor.addHyperlinkListener(this);// addHyperlinkListener(l);
        
        // An AutoCompletion acts as a "middle-man" between a text component
        // and a CompletionProvider. It manages any options associated with
        // the auto-completion (the popup trigger key, whether to display a
        // documentation window along with completion choices, etc.). Unlike
        // CompletionProviders, instances of AutoCompletion cannot be shared
        // among multiple text components.
        
        autoCompletion = new AutoCompletion(provider);
        autoCompletion.setAutoActivationEnabled(concierge.getPreferences().getAutoActivationEnabled());
        autoCompletion.setAutoActivationDelay(concierge.getPreferences().getAutoActivationDelay());
        autoCompletion.install(ruleEditor);
        
        
        iRulesTokenMaker ruleMaker = new iRulesTokenMaker(concierge);
        ((RSyntaxDocument)ruleEditor.getDocument()).setSyntaxStyle(ruleMaker);
        
        //Gotta add a parser here I think to catch the words and make them links?
        iRuleTootipParser parser = new iRuleTootipParser(concierge, ruleMaker);
        ruleEditor.addParser(parser);
        
        //Let's see if setting this true or false fixes the odd word wrap== new line thing
        ruleEditor.setWrapStyleWord(true);
        
        JFileChooser fc = new JFileChooser();  
        FileSystemView fv = fc.getFileSystemView();  
        File documentsDir = fv.getDefaultDirectory();
        String templatesDirPath = documentsDir.getAbsolutePath() + "/Njord/Templates/";
        ruleEditor.setTemplateDirectory(templatesDirPath);
        ruleEditor.setTemplatesEnabled(true);
        
        
        // Add an item to the popup menu that opens the file whose name is
        // specified at the current caret position.
        JPopupMenu popup = ruleEditor.getPopupMenu();
        popup.addSeparator();
        popup.add(new JMenuItem(new SearchAction()));
        
        
        return ruleEditor;
        

//        tm.addHighlightedIdentifier("customKeyword", <span class="posthilit">Token</span>.RESERVED_WORD);
//        JavaTokenMaker tm = new JavaTokenMaker();
//        tm.addHighlightedIdentifier("customKeyword", <span class="posthilit">Token</span>.RESERVED_WORD);
//        ((RSyntaxDocument)textArea.getDocument()).setSyntaxStyle(tm);
        
//        public void addHighlightedIdentifier(String word, int tokenType);
//        public boolean removeHighlightedIdentifier(String word);
//        public void clearAddedHighlightedIdentifiers();
        
        
        
    }
	
    /**
     * Create a simple provider that adds some iRules Completions.
     * 
     * @return The completion provider.
     */
    private NjordCompletionProvider createCompletionProvider() {
        // A DefaultCompletionProvider is the simplest concrete implementation
        // of CompletionProvider. This provider has no understanding of
        // language semantics. It simply checks the text entered up to the
        // caret position for a match against known completions. This is all
        // that is needed in the majority of cases.
        NjordCompletionProvider provider = new NjordCompletionProvider();

        String eventsPath; // CLIENT_ACCEPTED, CACHE_REQUEST, etc.
        String operatorsPath;
        String statementsPath; // drop, pool and more
        String functionsPath; // findstr, class and others
        String commandsPath; // HTTP::return etc etc
        String tclCommandsPath; // Built in tcl commands
        
        eventsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_EVENTS");
        operatorsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_OPERATORS");
        statementsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_STATEMENTS"); // drop, pool and more
        functionsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_FUNCTIONS"); // findstr, class and others
        commandsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_COMMANDS"); // HTTP::return etc etc
        tclCommandsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_TCL"); // Built in tcl commands
        
        
        BufferedReader reader = null;
        
        // Now go through the above list in order and add the contents of each file to the completion provider.
        List<String> keyWordsLists = new ArrayList<String>(Arrays.asList(eventsPath, operatorsPath, statementsPath, functionsPath,
                commandsPath, tclCommandsPath));
        
        for (String keyWordList : keyWordsLists) {
            boolean append = false;
            if (keyWordList.matches(eventsPath)) {
                append = true;
            }
            try {
                reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(keyWordList)));
                String str;
                while ((str = reader.readLine()) != null) {
                    if (append == true) { // If this is the events list then let's append a space and an open curly brace.
                        str = str + " {";
                    }
                    provider.addCompletion(new BasicCompletion(provider, str));
                }
            } catch (IOException e) {
                TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
                exceptionHandler.processException();
            } finally {
                try {
                    if (reader !=null) { 
                        reader.close();  
                    }
                } catch (IOException e) {
                    TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
                    exceptionHandler.processException();
                }
            }
        }
        // These are some sample completions. I will add more useful ones in the future as well as the ability to add your own.
//      provider.addCompletion(new ShorthandCompletion(provider, "dbl0",
//              "if {($DEBUG > 0 )} { log local0.debug \"", "if {($DEBUG > 0 )} { log local0.debug \""));
//      provider.addCompletion(new ShorthandCompletion(provider, "dbl1",
//              "if {($DEBUG > 1 )} { log local0.debug \"", "if {($DEBUG > 1 )} { log local0.debug \""));
        
        return provider;
    }
    
	/**
	 * Implementations of DraggableContent. Adds a mouse listener to the containg NjordRuleEditorPane so you can drag the larger window.
	 */
	public void addDragListener(DragListener dragListener) {
		addMouseListener(dragListener);
		addMouseMotionListener(dragListener);
//		editorPane.addMouseListener(dragListener);
//		editorPane.addMouseMotionListener(dragListener);
	}
	
	/**
	 * Sets the text within the editor. Used when creating new rules from templates.
	 * 
	 * @param text
	 */
	public void setText(String text) {
	    editorPane.setText(text);
	    
	}
	
	/**
	 * Removes edits from the undo history. Mainly useful when using setText to set the content of the editor.
	 */
	public void discardAllEdits() {
	    editorPane.discardAllEdits();
	}
	
	/**
	 * Save the content.
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException {
	    editorPane.save();
	    // if we make it to here then the save was successful. Which means that if it didn't before the object now exists.
	    fileLoc.exists = true;
	}
	
	/**
	 * Not yet implemented.
	 * 
	 */
	public void deleteObject() {
	    
	}
	
	/**
	 * Undo.
	 */
	public void undoLastAction() {
	    editorPane.undoLastAction();
	}
	
	/**
	 * Redo.
	 */
	public void redoLastAction() {
	    editorPane.redoLastAction();
	}
	
	/**
     * A temporary class until I fix it. Returns the contained RTextArea. I need to figure out what the searchengine needs to do so I can implement those and no longer use this.
     * @return
     */
    public RTextArea getTextArea() {
        return editorPane;
    }
    
    /**
     * Cut selected text to clipboard.
     */
    public void cut() {
        editorPane.cut();
    }
    
    /**
     * Copy selected text to clipboard.
     */
    public void copy() {
        editorPane.copy();
    }
    
    /**
     * Paste text from clipboard.
     */
    public void paste() {
        editorPane.paste();
    }
    
    /**
     * Paste text from clipboard.
     */
    public void copyAsRichText() {
        editorPane.copyAsRtf();
    }
    
    
    public int getLineStartOffset(int line) throws BadLocationException {
        return editorPane.getLineStartOffset(line);
    }
    
    
    // This bit here isn't going to work.
//    public void deleteItems() {
//      RTextArea.getAction(RTextArea.DELETE_ACTION);
////      menu.add(deleteItem);
//    }
    
    public void selectAll() {
        editorPane.selectAll();
    }
    
    public void setCaretPosition(int position) {
        editorPane.setCaretPosition(position);
    }
    
    public ComponentOrientation getComponentOrientation() {
        return editorPane.getComponentOrientation();
    }
    
    public void removeParser(TaskTagParser taskParser){
        editorPane.removeParser(taskParser);    
    }
    
    public void addParser(TaskTagParser taskParser){
        editorPane.addParser(taskParser);    
    }
    
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        editorPane.addPropertyChangeListener(propertyName, listener);
    }
    
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        editorPane.removePropertyChangeListener(propertyName, listener);
    }
    
    /**
     * Set a bunch of editor preferences.
     */
    public void updateViewSettingsFromPrefs() {
        //Some stuff to add to the editor
        // Try setEncoding
        //setTemplateDirectory(dir)
        //setTemplatesEnabled(enabled)
        //setIconGroup(group)
        //setMarkOccurrences(markOccurrences) gotta update my search to include that
        
        paneScrollPane.setLineNumbersEnabled(concierge.getPreferences().getShwLineNumbers());
        editorPane.setCodeFoldingEnabled(concierge.getPreferences().getCodeFoldingEnabled());
        editorPane.setLineWrap(concierge.getPreferences().getLineWrapEnabled());
        editorPane.setAutoscrolls(concierge.getPreferences().getAutoScroll());
        editorPane.setPaintTabLines(concierge.getPreferences().getPaintTabLines());
        editorPane.setWhitespaceVisible(concierge.getPreferences().getVisibleWhiteSpace());
        editorPane.setEOLMarkersVisible(concierge.getPreferences().getEOLMarkersVisible());
        autoCompletion.setAutoActivationEnabled(concierge.getPreferences().getAutoActivationEnabled());
        autoCompletion.setAutoActivationDelay(concierge.getPreferences().getAutoActivationDelay());
    }
    
    //TODO: this needs to move somewhere's else.
    /**
     * An action that gets the filename at the current caret position and tries
     * to open that file. If there is a selection, it uses the selected text as
     * the filename.
     */
    private class SearchAction extends TextAction {

       public SearchAction() {
          super("Find");
       }

       public void actionPerformed(ActionEvent e) {

          JTextComponent tc = getTextComponent(e);
          String textAtCarrot = null;
          
          // Get the name of the file to load. If there is a selection, use
          // that as the file name, otherwise, scan for a filename around
          // the caret.
          try {
             int selStart = tc.getSelectionStart();
             int selEnd = tc.getSelectionEnd();
             if (selStart != selEnd) {
                textAtCarrot = tc.getText(selStart, selEnd - selStart);
             } else {
                textAtCarrot = getFilenameAtCaret(tc);
             }
          } catch (BadLocationException ble) {
             ble.printStackTrace();
             UIManager.getLookAndFeel().provideErrorFeedback(tc);
             return;
          }
          log.debug("Found text element [{}]", textAtCarrot);
          
          //TODO: This has to change
          concierge.getOwner().doSearch(textAtCarrot);
//          loadFile(new File(filename));

       }
       
       // This was the start of me trying to figure out how to make a search wrap. I think I can set the carrot position to the
       // beginning of the file and then keep searching. I'll need to do it intelligently with some flip/flop when I run past the
       // end so I can avoid an infinite loop.
       public void setCarrotPosition(int pos) {
           ruleEditor.setCaretPosition(pos);
       }
       
       /**
        * Gets the filename that the caret is sitting on. Note that this is a
        * somewhat naive implementation and assumes filenames do not contain
        * whitespace or other "funny" characters, but it will catch most common
        * filenames.
        * 
        * @param tc
        *           The text component to look at.
        * @return The filename at the caret position.
        * @throws BadLocationException
        *            Shouldn't actually happen.
        */
       public String getFilenameAtCaret(JTextComponent tc) throws BadLocationException {
          int caret = tc.getCaretPosition();
          int start = caret;
          Document doc = tc.getDocument();
          while (start > 0) {
             char ch = doc.getText(start - 1, 1).charAt(0);
             if (isFilenameChar(ch)) {
                start--;
             } else {
                break;
             }
          }
          int end = caret;
          while (end < doc.getLength()) {
             char ch = doc.getText(end, 1).charAt(0);
             if (isFilenameChar(ch)) {
                end++;
             } else {
                break;
             }
          }
          return doc.getText(start, end - start);
       }

       public boolean isFilenameChar(char ch) {
          return Character.isLetterOrDigit(ch) || ch == ':' || ch == '.' || ch == '_'
                || ch == File.separatorChar;
       }

    }
    
}
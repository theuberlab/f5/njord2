/*
 * 10/21/2009
 *
 * AbstractParserNoticeWindow.java - Base class for dockable windows that
 * display parser notices.
 * Copyright (C) 2009 Robert Futrell
 * http://fifesoft.com/rtext
 * Licensed under a modified BSD license.
 * See the included license file for details.
 */
package com.theuberlab.njord.ui;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.BadLocationException;

import com.theuberlab.njord.main.Njord;
import com.theuberlab.njord.main.NjordConcierge;


/**
 * Grabbed from rtext the rsyntaxtextarea main project.
 * Base class for dockable windows containing parser notices.
 *
 * @author Robert Futrell
 * @version 1.0
 */
public abstract class AbstractParserNoticeWindow extends JPanel {
	private NjordConcierge concierge;
	private JTable table;

    /**
     * The "primary" component in this docked window.
     */
//    private Component primaryComponent;
    
    
	public AbstractParserNoticeWindow(NjordConcierge concierge) {
		this.concierge = concierge;
	}
	
	public AbstractParserNoticeWindow(LayoutManager layout) {
        super(layout);
    }
	 
	protected JTable createTable(TableModel model) {
		table = new JTable() {
			/**
			 * Overridden to ensure the table completely fills the JViewport it
			 * is sitting in.  Note in Java 6 this could be taken care of by the
			 * method JTable#setFillsViewportHeight(boolean).
			 * 1.6: Remove this and replace it with the method call.
			 */
			public boolean getScrollableTracksViewportHeight() {
				Component parent = getParent();
				return parent instanceof JViewport ?
					parent.getHeight()>getPreferredSize().height : false;
			}
		};
//		setPrimaryComponent(table);
		fixTableModel(model);
		table.addMouseListener(new TableMouseListener());
		Dimension size = table.getPreferredScrollableViewportSize();
		size.height = 200;
		table.setPreferredScrollableViewportSize(size);
//		UIUtil.fixJTableRendererOrientations(table);
//		UIUtil.possiblyFixGridColor(table);
		return table;
	}


	private void fixTableModel(TableModel model) {

		JTableHeader old = table.getTableHeader();
		table.setTableHeader(new JTableHeader(old.getColumnModel()));

		/*
		 * From FileExplorerTableModel
		 * 
		 * A table model that simulates the functionality found in the table used in
		 * Windows' "details view" in Windows explorer.  This is a WIP, and currently is
		 * pretty much just a copy of <code>FileExplorerTableModel.java</code> found in
		 * java.sun.com's tutorial section.<p>
		 * This model currently allows the user to sort by column, and colors the cells
		 * of elements in sorted-by columns slightly darker than normal, to signify that
		 * the table is sorted by that row.  Future enhancements include a right-click
		 * popup menu for the table header that allows you to add or remove columns.<p>
		 *
		 * NOTE: If you use this table model in an application that allows the user
		 * to change the LaF at runtime, you will get NullPointerExceptions when the
		 * user changes from the Windows LaF to another LaF, such as Metal.  This is
		 * due to Sun bug 6429812.  This bug is still open as of 6u18.  You'll have to
		 * implement a workaround for when the LaF changes if you want to use this
		 * class in an application that allows runtime LaF changes.
		 * See <a href="http://bugs.sun.com/view_bug.do?bug_id=6429812">6429812</a>
		 * for more information.  As an alternative, you can probably use Swing's
		 * built-in table sorting support, if you only support Java 6 and up.
		 * 
		 * 
		 * Skipping for now, going to try w/ Swing's built in sorting.
		 */
//		FileExplorerTableModel model2 = new FileExplorerTableModel(model,
//				table.getTableHeader());
		
		
		
		
//		model2.setColumnComparator(Integer.class, new Comparator() {	
//			public int compare(Object o1, Object o2) {
//				Integer int1 = (Integer)o1;
//				Integer int2 = (Integer)o2;
//				return int1.compareTo(int2);
//			}
//		});

		IconTableCellRenderer itcr = new IconTableCellRenderer();
		ComponentOrientation o = ComponentOrientation.getOrientation(getLocale());
		itcr.applyComponentOrientation(o);
		table.setDefaultRenderer(Icon.class, itcr);

//		table.setModel(model2);
		
		
		MyTableModel tableModel = new MyTableModel();
		
		table.setModel(tableModel);
		
		TableColumnModel tcm = table.getColumnModel();
		tcm.getColumn(0).setPreferredWidth(32);
		tcm.getColumn(0).setWidth(32);
		tcm.getColumn(1).setPreferredWidth(200);
		tcm.getColumn(1).setWidth(200);
		tcm.getColumn(2).setPreferredWidth(48);
		tcm.getColumn(2).setWidth(48);
		tcm.getColumn(3).setPreferredWidth(800);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

	}


//	/**
//	 * Returns the parent RText application.
//	 *
//	 * @return The parent RText instance.
//	 */
//	protected RText getRText() {
//		return concierge;
//	}


	/**
	 * Overridden to work around Java bug 6429812.  As of Java 6 (!), changing
	 * LaF from Windows to something else (e.g. Metal) at runtime with a custom
	 * delegating TableCellRenderer on a JTableHeader can cause NPE's.  Then,
	 * the UI never repaints from then on.  Seems to happen only when the table
	 * is actually visible during the LaF change.  See
	 * <a href="http://bugs.sun.com/view_bug.do?bug_id=6429812">6429812</a>.
	 */
	public void updateUI() {

		/*
		 * NOTE: This is silly, but it's what it took to get a LaF change to
		 * occur without throwing an NPE because of the JRE bug.  No doubt there
		 * is a better way to handle this.  What we do is:
		 *
		 * 1. Before updating the UI, reset the JTableHeader's default renderer
		 *    to what it was originally.  This prevents the NPE from the JRE
		 *    bug, since we're no longer using the renderer with a cached
		 *    Windows-specific TableCellRenderer (when Windows LaF is enabled).
		 * 2. Update the UI, like normal.
		 * 3. After the update, we must explicitly re-set the JTableHeader as
		 *    the column view in the enclosing JScrollPane.  This is done by
		 *    default the first time you add a JTable to a JScrollPane, but
		 *    since we gave the JTable a new header as a workaround for the JRE
		 *    bug, we must explicitly tell the JScrollPane about it as well.
		 */

		TableModel model = null;
		if (table!=null) {
			model = ((MyTableModel)table.getModel()).getTableModel();
			TableCellRenderer r = table.getTableHeader().getDefaultRenderer();
//			if (r instanceof SortableHeaderRenderer) { // Always true
//				SortableHeaderRenderer shr = (SortableHeaderRenderer)r;
//				table.getTableHeader().setDefaultRenderer(
//											shr.getDelegateRenderer());
//			}
		}

		super.updateUI();

		if (table!=null) {
			JScrollPane sp = (JScrollPane)table.getParent().getParent();
			fixTableModel(model);
			sp.setColumnHeaderView(table.getTableHeader());
			sp.revalidate();
			sp.repaint();
		}

	}


	/**
	 * Basic model for tables displaying parser notices.
	 */
	protected abstract class ParserNoticeTableModel extends DefaultTableModel {

		public ParserNoticeTableModel(String lastColHeader) {
			String[] colHeaders = new String[] {
					"",
//					concierge.getString("ParserNoticeList.File"),
//					concierge.getString("ParserNoticeList.Line"),
					"TODO",
                    "TEXT",
					lastColHeader, };
			setColumnIdentifiers(colHeaders);
		}

		protected abstract void addNoticesImpl(NjordRuleEditorPanel textArea,
												List notices);

		public Class getColumnClass(int col) {
			Class clazz = null;
			switch (col) {
				case 0:
					clazz = Icon.class;
					break;
				case 1:
					clazz = TextAreaWrapper.class;
					break;
				case 2:
					clazz = Integer.class;
					break;
				default:
					clazz = super.getColumnClass(col);
			}
			return clazz;
		}

		public void addRow(Object[] data) {
			if (data[1] instanceof NjordRuleEditorPanel) {
				data[1] = new TextAreaWrapper((NjordRuleEditorPanel)data[1]);
			}
			super.addRow(data);
		}

		public boolean isCellEditable(int row, int col) {
			return false;
		}

		public void update(NjordRuleEditorPanel textArea, List notices) {
			//setRowCount(0);
			for (int i=0; i<getRowCount(); i++) {
				TextAreaWrapper wrapper = (TextAreaWrapper)getValueAt(i, 1);
				NjordRuleEditorPanel textArea2 = wrapper.textArea;
				if (textArea2==textArea) {
					removeRow(i);
					i--;
				}
			}
			if (notices!=null) {
				addNoticesImpl(textArea, notices);
			}
		}

	}


	/**
	 * A table cell renderer for icons.
	 */
	private static class IconTableCellRenderer extends DefaultTableCellRenderer{

		static final Border b = BorderFactory.createEmptyBorder(0, 5, 0, 5);

		public Component getTableCellRendererComponent(JTable table,
			Object value, boolean selected, boolean focus, int row, int col) {
			super.getTableCellRendererComponent(table, value, selected, focus,
					row, col);
			setText(null);
			setIcon((Icon)value);
			setBorder(b);
			return this;
		}

	}


	/**
	 * A wrapper around text areas that overrides toString() appropriately, so
	 * we don't have to create two separate custom renderers, one for Substance
	 * and another for all other LookAndFeels.  Substance sucks.
	 */
	private static class TextAreaWrapper implements Comparable {

		private NjordRuleEditorPanel textArea;

		public TextAreaWrapper(NjordRuleEditorPanel textArea) {
			this.textArea = textArea;
		}

		public int compareTo(Object o) {
			return toString().compareTo(o.toString());
		}

		public String toString() {
			return textArea.getName();
		}

	}


	private class TableMouseListener extends MouseAdapter {

		public void mouseClicked(MouseEvent e) {

			if (e.getButton()==MouseEvent.BUTTON1 && e.getClickCount()==2) {
				int row = table.rowAtPoint(e.getPoint());
				if (row>-1) {
					// Get values from model since columns are re-orderable.
					TableModel model = table.getModel();
					TextAreaWrapper wrapper = (TextAreaWrapper)model.
							getValueAt(row, 1);
					NjordRuleEditorPanel textArea = wrapper.textArea;
					//TODO: Don't do this.
					Njord mainWindow = concierge.getOwner();
//					if (mainWindow.setActiveEditorPane(textArea)) {
					mainWindow.setActiveEditorPane(textArea);
					
					
						Integer i = (Integer)model.getValueAt(row, 2);
						int line = i.intValue() - 1; // 0-based
						try {
							textArea.setCaretPosition(
								textArea.getLineStartOffset(line));
						} catch (BadLocationException ble) {
							UIManager.getLookAndFeel().
										provideErrorFeedback(textArea);
						}
						textArea.requestFocusInWindow();
					
				}
			}

		}

	}

//    public void setPrimaryComponent(Component primary) {
//        this.primaryComponent = primary;
//    }
    
    private class MyTableModel extends AbstractTableModel {
        private String[] columnNames = { "Task", "Task data", "Column three", "Column four"}; 
//        private Object[][] data;
        
        
        Object[][] data = {
                {"Kathy", "Smith",
                 "Snowboarding", new Integer(5), new Boolean(false)},
                {"John", "Doe",
                 "Rowing", new Integer(3), new Boolean(true)},
                {"Sue", "Black",
                 "Knitting", new Integer(2), new Boolean(false)},
                {"Jane", "White",
                 "Speed reading", new Integer(20), new Boolean(true)},
                {"Joe", "Brown",
                 "Pool", new Integer(10), new Boolean(false)}
                };
        protected TableModel tableModel;

        
        
//        private Row[] viewToModel;
        private int[] modelToView;

        private JTableHeader tableHeader;
//        private MouseHandler mouseListener;
        private TableModelListener tableModelListener;
        private Map columnComparators = new HashMap();
        private List sortingColumns = new ArrayList();

        private JTable table;
        
        
        
        
        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        /*
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col < 2) {
                return false;
            } else {
                return true;
            }
        }

        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            data[row][col] = value;
            fireTableCellUpdated(row, col);
        }
        
        /**
         * Sets the table model to use.
         *
         * @param tableModel The table model to use.
         * @see #getTableModel
         */
        public void setTableModel(TableModel tableModel) {

            // Remove the table model listener from the old table model and add it
            // to the new table model.
            if (this.tableModel!=null)
                this.tableModel.removeTableModelListener(tableModelListener);
            this.tableModel = tableModel;
            if (this.tableModel!=null)
                this.tableModel.addTableModelListener(tableModelListener);

            // Housekeeping.
//            clearSortingState();
            fireTableStructureChanged();

        }
        
        /**
         * Returns the table model being used.
         *
         * @return The table model being used.
         * @see #setTableModel
         */
        public TableModel getTableModel() {
            return tableModel;
        }
    }
    
    
}
package com.theuberlab.njord.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerListModel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import com.theuberlab.njord.main.Njord;

// Currently this code will produce a bunch of different dialogs. I need to add one that will produce the prefs dialog I am going for.

public class NewiRuleDialog extends JDialog implements PropertyChangeListener {
    private Njord owner;

//    private String magicWord;
    private JOptionPane optionPane; // Do I still need this?
    
//    // Do I still need these?
//    private String btnString1 = "Enter";
//    private String btnString2 = "Cancel";
    
	public JFrame frame;
	public JTextField txtObjectName;
	public JTextField ConnPreffsHostTextField;
	public JTextField ConnPreffsPortTextField;
	public JTextField ConnPreffsUserTextField;
	public JPanel panel_1;
    
	String[] objectArchetypes = null;
	
	protected JSpinner spnrCreatIn = null;
	protected JLabel lblObjectTemplate = null;
	protected JList lstObjectArchetype = null;
	
	
	protected SpinnerListModel spinnerListM = null;
	
	/**
	 * Holds the list of rule architypes.
	 */
	ArrayList ArchetypesListList = null;
	/**
	 * holds the possible destinations for an object.
	 */
	ArrayList destinationsList = null;
	
	public JComboBox cmboDestinations = null;
	protected DefaultComboBoxModel destinationsComboboxModel = null;
	protected DefaultComboBoxModel archetypesComboboxModel = null;
	        
    //TODO: Modify this method so that it taks your text and validates it.
    /**
     * NOT YET IMPLEMENTED, currently just returns the text you feed it.
     * Returns null if the typed string was invalid;
     * otherwise, returns the string as the user entered it.
     */
    public String getValidatedText(String text) {
        return text;
    }

    /** Creates the reusable dialog. */
    public NewiRuleDialog(Frame aFrame, Njord parent) {
        super(aFrame, true);
        owner = parent;
        
        setTitle("New");
        
        
        
        
//        objectArchetypes = { "Blank", "Select pool based on URI"};
        

//        textField1 = new JTextField(10);
//        textField2 = new JTextField(10);

//        //Create an array of the text and components to be displayed.
//        String msgString1 = "What was Dr. SEUSS's real last name?";
//        String msgString2 = "(The answer is \"" + magicWord
//                  + "\".)";
//        String msgString3 = "Some more text";
//        String msgString4 = "nuthin";
//        
//        Object[] array1 = {msgString1, msgString2, textField1};
//        Object[] array2 = {msgString2, msgString3, textField2};


        //Create an array specifying the number of dialog buttons
        //and their text. this makes btnString1 oktext and btnString2 cancel text
//        Object[] options = {btnString1, btnString2};

        
        
//        JTextField xField = new JTextField(5);
//        JTextField yField = new JTextField(5);
        
        
        
//        myPanel = new JPanel();
//        myPanel.add(new JLabel("x:"));
//        myPanel.add(xField);
//        myPanel.add(Box.createHorizontalStrut(15)); // a spacer
//        myPanel.add(new JLabel("y:"));
//        myPanel.add(yField);
  
        ////////////////////////////////////              Panel Stuff

        frame = new JFrame();
		frame.setBounds(100, 100, 531, 528);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		panel_1 = new JPanel();
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new BorderLayout(10, 10));
		
		Component verticalStrut = Box.createVerticalStrut(20);
		panel_1.add(verticalStrut, BorderLayout.NORTH);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut, BorderLayout.WEST);
		
		JPanel panel = new JPanel();
		panel_1.add(panel);
		panel.setLayout(new FormLayout(new ColumnSpec[] {
		        FormFactory.RELATED_GAP_COLSPEC,
		        FormFactory.DEFAULT_COLSPEC,
		        FormFactory.RELATED_GAP_COLSPEC,
		        ColumnSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_COLSPEC,
		        FormFactory.DEFAULT_COLSPEC,
		        FormFactory.RELATED_GAP_COLSPEC,
		        ColumnSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_COLSPEC,
		        FormFactory.DEFAULT_COLSPEC,
		        FormFactory.RELATED_GAP_COLSPEC,},
		    new RowSpec[] {
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        RowSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_ROWSPEC,
		        RowSpec.decode("default:grow"),}));
		
		//#####################################################################################################################################################
		// THIS IS WHERE I AM
		//#####################################################################################################################################################
		
		// I need to create the dialog. I need a name (perhaps w/ a partition or just include that in the path of the name?
		// iRuler has templates which I like, I'd like to add that.
		// It also has a dialog which allows you to select some events and it will create a rule w/ said events, let's put that off.
		// I _should_ be figuring out if we're remote or local from where you hit "New" but once you've done so there should be a pulldown for where to create. It should have local and then each of the bigips. 
		
		
		
		
		JLabel lblObjectName = new JLabel("Rule Name:");
		lblObjectName.setToolTipText("A unique human readable name that identifies this bigip. No spaces or special characters.");
		panel.add(lblObjectName, "4, 4");
		
		txtObjectName = new JTextField();
		txtObjectName.setText("newiRule");
        panel.add(txtObjectName, "8, 4, fill, default");
        txtObjectName.setColumns(10);
		
//		iIPAddress 
//        iPort 
//        iUserName 
//        iPassword
        
        JLabel lblCreateIn = new JLabel("Create in:");
        lblCreateIn.setToolTipText("IP Address or DNS name");
        panel.add(lblCreateIn, "4, 6");
        
     // Doing this here temporarily.
//        String[] DestinationsList = { "local" };
//        destinationsList = new ArrayList<String>(Arrays.asList(DestinationsList));
//        spinnerListM = new SpinnerListModel(destinationsList);
        
//      jSpinner.setModel(new javax.swing.SpinnerListModel(stringList))
        
//        spnrCreatIn = new JSpinner(spinnerListM);
//        panel.add(spnrCreatIn, "8, 6, fill, fill");
        
        destinationsComboboxModel = new DefaultComboBoxModel();
        destinationsComboboxModel.addElement("Local");
//        model.addElement("Strewberry");
//        model.addElement("Vanilla");
        cmboDestinations = new JComboBox(destinationsComboboxModel);
        panel.add(cmboDestinations, "8, 6, fill, fill");
//        panel.add(cmboDestinations);
        
//		ConnPreffsHostTextField = new JTextField();
//		ConnPreffsHostTextField.setText("192.168.215.251");
//		panel.add(ConnPreffsHostTextField, "8, 6, fill, default");
//		ConnPreffsHostTextField.setColumns(10);
		
		lblObjectTemplate = new JLabel("Rule Archetype:");
		lblObjectTemplate.setToolTipText("Select a starting point");
		panel.add(lblObjectTemplate, "4, 8");
		
		// Doing this here temporarily.
//        String[] objectArchetypes = { "Blank", "Select pool based on URI" };
        String[] objectArchetypes = { };
        ArchetypesListList = new ArrayList<String>(Arrays.asList(objectArchetypes));
        archetypesComboboxModel = new DefaultComboBoxModel();
        
//		lstObjectArchetype = new JList(objectArchetypes);
		lstObjectArchetype = new JList(archetypesComboboxModel);
		lstObjectArchetype.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstObjectArchetype.setSelectedValue("Blank", true);
//		panel.add(lstObjectArchetype, "8, 8, 1, 7, fill, fill");
        
		JScrollPane listScroller = new JScrollPane(lstObjectArchetype);
        listScroller.setPreferredSize(new Dimension(250, 80));
        panel.add(listScroller, "8, 8, 1, 7, fill, fill");
        
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_1, BorderLayout.EAST);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		panel_1.add(verticalStrut_1, BorderLayout.SOUTH);
        
        
//        optionPaneTwo = new JOptionPane(
        //Create the JOptionPane.
		// If i need this bit then try adding a message as the first part.
//		optionPane = new JOptionPane(JOptionPane.QUESTION_MESSAGE,
//                JOptionPane.YES_NO_OPTION,
//                null,
//                options,
//                options[0]);
                
//		optionPane = new JOptionPane(array1,
//                                    JOptionPane.QUESTION_MESSAGE,
//                                    JOptionPane.YES_NO_OPTION,
//                                    null,
//                                    options,
//                                    options[0]);

        //Make this dialog display it.
        //setContentPane(optionPane); // The normal generic one
//        setContentPane(optionPane); // Testing the real quick multi option one
        
        //Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(
                                        JOptionPane.CLOSED_OPTION));
            }
        });
        
        //Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent ce) {
                txtObjectName.requestFocusInWindow();
            }
        });
        
//        //Register an event handler that puts the text into the option pane.
//        ConnPreffsBigIPName.addActionListener(this);

        //Register an event handler that reacts to option pane state changes.
//        optionPane.addPropertyChangeListener(this);
    }

//    /** This method handles events for the text field. */
//    public void actionPerformed(ActionEvent e) {
//        optionPane.setValue(btnString1);
//    }

    /** This method reacts to state changes in the option pane. */
    public void propertyChange(PropertyChangeEvent e) {
        String prop = e.getPropertyName();

        if (isVisible()
         && (e.getSource() == optionPane)
         && (JOptionPane.VALUE_PROPERTY.equals(prop) ||
             JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
            Object value = optionPane.getValue();

            if (value == JOptionPane.UNINITIALIZED_VALUE) {
                //ignore reset
                return;
            }

            //Reset the JOptionPane's value.
            //If you don't do this, then if the user
            //presses the same button next time, no
            //property change event will be fired.
            optionPane.setValue(
                    JOptionPane.UNINITIALIZED_VALUE);

//            if (btnString1.equals(value)) {
//                    typedText = textField1.getText();
//                String ucText = typedText.toUpperCase();
//                if (magicWord.equals(ucText)) {
//                    //we're done; clear and dismiss the dialog
//                    clearAndHide();
//                } else {
//                    //text was invalid
//                    textField1.selectAll();
//                    JOptionPane.showMessageDialog(
//                                    BIGIPSettingsDialog.this,
//                                    "Sorry, \"" + typedText + "\" "
//                                    + "isn't a valid response.\n"
//                                    + "Please enter "
//                                    + magicWord + ".",
//                                    "Try again",
//                                    JOptionPane.ERROR_MESSAGE);
//                    typedText = null;
//                    textField1.requestFocusInWindow();
//                }
//            } else { //user closed dialog or clicked cancel
//                owner.tuNotify("It's OK.");
//                typedText = null;
//                clearAndHide();
//            }
        }
    }
    
//    public void addToSpinnerList (String newString) {
//        destinationsList.add(newString);
//        spnrCreatIn.updateUI();
//    }
    
    public void addToDestinations (String newDest) {
        destinationsComboboxModel.addElement(newDest);
    }
    
//    /**
//     * NOT YET IMPLEMENTED.
//     * 
//     * @param newSpinnerList
//     */
//    public void replaceSpinnerList (List<String> newSpinnerList) {
////        destinationsList.
//    }
    
    public void addToListboxList (String newString) {
        archetypesComboboxModel.addElement(newString);
//        ArchetypesListList.add(newString);
    }
    
    /**
     * 
     * @return
     */
    public String getSpinnerValue() {
//        int index=0;
//        String returnString = "";
//        destinationsComboboxModel.getSelectedItem();
//        destinationsList.size();
//        for(Object listValue : destinationsList) {
//          if(listValue.equals(spnrCreatIn.getValue())) {
//              returnString = (String) destinationsList.get(index);
//          }
//          
////              return index;
//          index++;
//        }
        return destinationsComboboxModel.getSelectedItem().toString();
//        return returnString;
    }
    
    /**
     * 
     * @return
     */
    public String getListboxValue() {
//        int index=0;
//        String returnString = "";
//        destinationsList.size();
//        for(Object listValue : destinationsList) {
//          if(listValue.equals(spnrCreatIn.getValue())) {
//              returnString = (String) destinationsList.get(index);
//          }
//          
////              return index;
//          index++;
//        }
//        return returnString;
        return archetypesComboboxModel.getSelectedItem().toString();
    }
    
    public void setSelectedDestination(String destination) {
        destinationsComboboxModel.setSelectedItem(destination);
//      cmboDestinations
    }
    
    public String getSelectedArchetypeText() {
//        return lstObjectArchetype.getSelectedValue().toString();
        String archetypeText = lstObjectArchetype.getSelectedValue().toString();
        return archetypeText;
                
//        return (String) lstObjectArchetype.getSelectedValuesList().get(0);
    }
    /**
     * NOT YET IMPLEMENTED.
     * 
     * @param newSpinnerList
     */
    public void replaceListBoxList (List<String> newSpinnerList) {
        
    }
    
    /** This method clears the dialog and hides it. */
    public void clearAndHide() {
//        textField1.setText(null);
        setVisible(false);
    }
}

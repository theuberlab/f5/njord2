package com.theuberlab.njord.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import com.theuberlab.njord.main.Njord;


/**
 * <code>FlexibleHiddenTextDialog</code> Provides a dialog box which prompts the user for input. Provides options which cause the input to be masked.
 * Uses a JPasswordField instead of a JTextField for flexibility. 
 * Also allows a show/hide checkbox to be displayed or hidden.
 * 
 * @author Aaron Forster
 *
 */
public class FlexibleTextDialog extends JDialog implements PropertyChangeListener, ActionListener {
    private Njord owner;
    private JOptionPane optionPane;
    
	private JFrame frame;
	private JPasswordField pwdHiddenTextField;
	private JCheckBox chkbxShowHideText;
	public JPanel panel_1;
	
	/** 
	 * Creates the reusable dialog to prompt the user for input which needs to be obfuscated on entry. 
	 * Note, if hideText is used the method getHiddenInputText() should be used to retrieve the text instead of getInputText() to avoid
	 * converting the provided text into a String which would be stored in memory and potentially read by other applications. 
	 * 
	 * @param parent What has called us.
	 * @param aFrame The parent frame.
	 * @param title The title for the dialog.
	 * @param message The text to display in the message box.
	 * @param inputName The name to give the input field. May be null.
	 * @param inputTootip A tooltip to set on the input field. May be null.
	 * @param hideText If set to true uses a JPasswordField instead of a JTextBox. If 
     * @param showHideOption If set to true displays a checkbox which allows the user to temporarily un-obfuscate the hidden text.
	 */
    public FlexibleTextDialog(Njord parent, Frame aFrame, String message, String inputName, String inputTootip, boolean hideText, boolean showHideOption) {
        super(aFrame, true);
        owner = parent;
        
//        setTitle(title);
        
        frame = new JFrame();
		frame.setBounds(100, 100, 300, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		panel_1 = new JPanel();
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new BorderLayout(10, 10));
		
		Component verticalStrut = Box.createVerticalStrut(20);
		panel_1.add(verticalStrut, BorderLayout.NORTH);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut, BorderLayout.WEST);
		
		JPanel panel = new JPanel();
		panel_1.add(panel);
		panel.setLayout(new FormLayout(new ColumnSpec[] {
		        FormFactory.RELATED_GAP_COLSPEC,
		        FormFactory.DEFAULT_COLSPEC,
		        FormFactory.RELATED_GAP_COLSPEC,
		        ColumnSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_COLSPEC,
		        FormFactory.DEFAULT_COLSPEC,
		        FormFactory.RELATED_GAP_COLSPEC,},
		    new RowSpec[] {
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        RowSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        RowSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_ROWSPEC,
		        RowSpec.decode("default:grow"),}));
		
		JLabel lblObjectName = new JLabel(message);
		panel.add(lblObjectName, "4, 2");
		
		// I need a jlabel here which would be optional to use inputName
		
		
		// We'll use a JPasswordField instead of a JTextBoxField for simplicity. however if 
		pwdHiddenTextField = new JPasswordField();
//		pwdHiddenTextField.setText(inputName);
		
		pwdHiddenTextField.setToolTipText(inputTootip);
		panel.add(pwdHiddenTextField, "4, 6, fill, fill");
		if (hideText) {
		    System.out.println("Using hidden text");
		    pwdHiddenTextField.setEchoChar('*');
		} else {
		    System.out.println("Not Using hidden text");
		    pwdHiddenTextField.setEchoChar((char) 0);
		}
		
		chkbxShowHideText = new JCheckBox("Show");
        chkbxShowHideText.addActionListener(this);
        panel.add(chkbxShowHideText, "6, 6");
        
	    chkbxShowHideText.setVisible(showHideOption);
	    chkbxShowHideText.setSelected(!hideText);
	    
	    if (hideText) {
	        chkbxShowHideText.setActionCommand("Show");
	    } else {
	        chkbxShowHideText.setActionCommand("Hide");
	    }
	    
//		String title, String message, String inputName, String inputTootip, boolean hideText, boolean showHideOption) {
		
//		txtObjectName = new JTextField();
//		txtObjectName.setText("newiRule");
//        panel.add(txtObjectName, "8, 4, fill, default");
//        txtObjectName.setColumns(10);
		
//		iIPAddress 
//        iPort 
//        iUserName 
//        iPassword
//        
//        JLabel lblCreateIn = new JLabel("Create in:");
//        lblCreateIn.setToolTipText("IP Address or DNS name");
//        panel.add(lblCreateIn, "4, 6");
//        
//     // Doing this here temporarily.
////        String[] DestinationsList = { "local" };
////        destinationsList = new ArrayList<String>(Arrays.asList(DestinationsList));
////        spinnerListM = new SpinnerListModel(destinationsList);
//        
////      jSpinner.setModel(new javax.swing.SpinnerListModel(stringList))
//        
////        spnrCreatIn = new JSpinner(spinnerListM);
////        panel.add(spnrCreatIn, "8, 6, fill, fill");
//        
//        destinationsComboboxModel = new DefaultComboBoxModel();
//        destinationsComboboxModel.addElement("Local");
////        model.addElement("Strewberry");
////        model.addElement("Vanilla");
//        cmboDestinations = new JComboBox(destinationsComboboxModel);
//        panel.add(cmboDestinations, "8, 6, fill, fill");
////        panel.add(cmboDestinations);
//        
////		ConnPreffsHostTextField = new JTextField();
////		ConnPreffsHostTextField.setText("192.168.215.251");
////		panel.add(ConnPreffsHostTextField, "8, 6, fill, default");
////		ConnPreffsHostTextField.setColumns(10);
//		
//		lblObjectTemplate = new JLabel("Rule Archetype:");
//		lblObjectTemplate.setToolTipText("Select a starting point");
//		panel.add(lblObjectTemplate, "4, 8");
//		
//		// Doing this here temporarily.
////        String[] objectArchetypes = { "Blank", "Select pool based on URI" };
//        String[] objectArchetypes = { };
//        ArchetypesListList = new ArrayList<String>(Arrays.asList(objectArchetypes));
//        archetypesComboboxModel = new DefaultComboBoxModel();
//        
////		lstObjectArchetype = new JList(objectArchetypes);
//		lstObjectArchetype = new JList(archetypesComboboxModel);
//		lstObjectArchetype.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		lstObjectArchetype.setSelectedValue("Blank", true);
////		panel.add(lstObjectArchetype, "8, 8, 1, 7, fill, fill");
//        
//		JScrollPane listScroller = new JScrollPane(lstObjectArchetype);
//        listScroller.setPreferredSize(new Dimension(250, 80));
//        panel.add(listScroller, "8, 8, 1, 7, fill, fill");
        
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_1, BorderLayout.EAST);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		panel_1.add(verticalStrut_1, BorderLayout.SOUTH);
        
        
//        optionPaneTwo = new JOptionPane(
        //Create the JOptionPane.
		// If i need this bit then try adding a message as the first part.
//		optionPane = new JOptionPane(JOptionPane.QUESTION_MESSAGE,
//                JOptionPane.YES_NO_OPTION,
//                null,
//                options,
//                options[0]);
                
//		optionPane = new JOptionPane(array1,
//                                    JOptionPane.QUESTION_MESSAGE,
//                                    JOptionPane.YES_NO_OPTION,
//                                    null,
//                                    options,
//                                    options[0]);

        //Make this dialog display it.
        //setContentPane(optionPane); // The normal generic one
//        setContentPane(optionPane); // Testing the real quick multi option one
        
        //Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(
                                        JOptionPane.CLOSED_OPTION));
            }
        });
        
        //Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent ce) {
                pwdHiddenTextField.requestFocus();
            }
        });
        
    }
    
    public String getInputText() {
        return new String(pwdHiddenTextField.getPassword());
    }
    
    public char[] getHiddenInputText() {
        return pwdHiddenTextField.getPassword();
    }
    
    /** This method reacts to state changes in the option pane. */
    public void propertyChange(PropertyChangeEvent e) {
        String prop = e.getPropertyName();

        if (isVisible()
         && (e.getSource() == optionPane)
         && (JOptionPane.VALUE_PROPERTY.equals(prop) ||
             JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
            Object value = optionPane.getValue();

            if (value == JOptionPane.UNINITIALIZED_VALUE) {
                //ignore reset
                return;
            }
            
            //Reset the JOptionPane's value.
            //If you don't do this, then if the user
            //presses the same button next time, no
            //property change event will be fired.
            optionPane.setValue(
                    JOptionPane.UNINITIALIZED_VALUE);
            
        }
    }
    
    /** This method clears the dialog and hides it. */
    public void clearAndHide() {
        // Clear the text for the next time the dialog is used.
        pwdHiddenTextField.setText(null);
        setVisible(false);
    }
    
    /**
     * Detects actions on objects for which an action listener has been added and takes action based on them.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String showHidePass = e.getActionCommand();
        if (showHidePass.equals("Show")) {
            System.out.println("Show");
            pwdHiddenTextField.setEchoChar((char) 0);
            chkbxShowHideText.setActionCommand("Hide");
        } else {
            System.out.println("Hide");
            pwdHiddenTextField.setEchoChar('*');
            chkbxShowHideText.setActionCommand("Show");
        }
    }
}
package com.theuberlab.njord.ui;

import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.theuberlab.njord.main.NjordConcierge;

/**
 * <code>ImageProvider</code> provides an interface to fetch images used in Njord thus alleviating the need to construct icons explicitely and 
 * providing a central place to manage images if they are to be updated or replaced.
 * 
 * @author Aaron Forster
 *
 */
public class ImageProvider {
    /** Holds the ImageIcons used for Njord buttons.*/
    private Map<String,ImageIcon> icons = new HashMap<String,ImageIcon>();
    /** Holds the concierge */
    private NjordConcierge concierge;
    
//  ImageIcon img = new ImageIcon(getClass().getResource("/iconsInUse/Njord2LogoN.png"));
    // ###############################################################
    // VARIABLES
    // ###############################################################
    
    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################
    
    /**
     * 
     */
    public ImageProvider(NjordConcierge concierge) {
        this.concierge = concierge;
        
        // NjordSquareLogo is the image that should be used in the upper left corner as well as for the application icon.
        icons.put("NjordSquareLogo", new ImageIcon(getClass().getResource("/Res/ButtonIcons/Njord2Logo64x64.png")));
        icons.put("NjordFullLogo", new ImageIcon(getClass().getResource("/Res/Images/Njord2Logo72x200.png")));
        
        
        
        // The default docks
        icons.put("DefDockNavigation", new ImageIcon(getClass().getResource("/Res/ButtonIcons/iRuleIconClean.png")));
        icons.put("DefDockFind", new ImageIcon(getClass().getResource("/Res/ButtonIcons/find.png")));
        icons.put("DefDockNotices", new ImageIcon(getClass().getResource("/Res/ButtonIcons/iRuleIconDirty.png")));
        icons.put("DefDockWelcome", new ImageIcon(getClass().getResource("/Res/ButtonIcons/text12.gif")));
        icons.put("DefDockTasks", new ImageIcon(getClass().getResource("/Res/ButtonIcons/table12.gif")));
        
        

        // This currently is my only custom action icon
        icons.put("GetIrules", new ImageIcon(getClass().getResource("/Res/ButtonIcons/page_white_put.png")));
        // The toolbar buttons        
        icons.put("ButtonDockablahAdd", new ImageIcon(getClass().getResource("/Res/ButtonIcons/script_add.png")));
        icons.put("ButtonDockablahSave", new ImageIcon(getClass().getResource("/Res/ButtonIcons/script_save.png")));
        icons.put("ButtonDockablahDelete", new ImageIcon(getClass().getResource("/Res/ButtonIcons/script_delete.png")));
        icons.put("ButtonDockablahConnect", new ImageIcon(getClass().getResource("/Res/ButtonIcons/connect.png")));
        icons.put("ButtonDockablahFetch", new ImageIcon(getClass().getResource("/Res/ButtonIcons/page_white_put.png")));
        icons.put("ButtonDockablahAddBIG-IP", new ImageIcon(getClass().getResource("/Res/ButtonIcons/server_add.png")));
        icons.put("ButtonDockablahDeleteBIG-IP", new ImageIcon(getClass().getResource("/Res/ButtonIcons/server_delete.png")));
        icons.put("ButtonDockablahEditBIG-IP", new ImageIcon(getClass().getResource("/Res/ButtonIcons/server_edit.png")));
        icons.put("ButtonDockablahFind", new ImageIcon(getClass().getResource("/Res/ButtonIcons/find.png")));
        icons.put("ButtonDockablahUndo", new ImageIcon(getClass().getResource("/Res/ButtonIcons/arrow_undo.png")));
        icons.put("ButtonDockablahRedo", new ImageIcon(getClass().getResource("/Res/ButtonIcons/arrow_redo.png")));
        
        
        
        // Multi-use
        icons.put("WorldGo", new ImageIcon(getClass().getResource("/Res/ButtonIcons/world_go.png")));
        icons.put("RuleClean", new ImageIcon(getClass().getResource("/Res/ButtonIcons/iRuleIconClean.png")));
        
        // Nav Tree Renderer icons
        icons.put("RuleIconClean", new ImageIcon(getClass().getResource("/Res/ButtonIcons/iRuleIconClean.png")));
        icons.put("RuleIconDirty", new ImageIcon(getClass().getResource("/Res/ButtonIcons/iRuleIconDirty.png")));
        icons.put("FolderIcon", new ImageIcon(getClass().getResource("/Res/ButtonIcons/FolderIcon.png")));
        icons.put("BIGIPIcon", new ImageIcon(getClass().getResource("/Res/ButtonIcons/BigIPIcon.png")));
        icons.put("LocalIcon",  new ImageIcon(getClass().getResource("/Res/ButtonIcons/BigIPIcon.png")));
        
        // A template for adding new images.
        //icons.put("", new ImageIcon(getClass().getResource("")));
                
    }

    // ###############################################################
    // GETTERS AND SETTERS
    // ###############################################################
    
    // ###############################################################
    // METHODS
    // ###############################################################
    
    /**
     * Returns the ImageIcon with the specified Name.
     * 
     * @param iconName
     * @return
     */
    public ImageIcon getIcon(String iconName) {
        try {
            ImageIcon retIcon = icons.get(iconName);
            return retIcon;
        } catch (NullPointerException e) {
            // If the image is not foudnd log an error and return the default.
            concierge.getOwner().getLoggerHandle().error("Image not found for requested key [" + iconName + "]");
            return icons.get("NjordSquareLogo");
        }
    }
    
    /**
     * Returns an Image of the ImageIcon with the specificed name.
     * 
     * @param iconName
     * @return
     */
    public Image getIconImage(String iconName) {
        try {
            ImageIcon retIcon = icons.get(iconName);
            return retIcon.getImage();
        } catch (NullPointerException e) {
            // If the image is not foudnd log an error and return the default.
            concierge.getOwner().getLoggerHandle().error("Image not found for requested key [" + iconName + "]");
            return icons.get("NjordSquareLogo").getImage();
        }
    }
}

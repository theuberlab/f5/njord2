package com.theuberlab.njord.ui;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.slf4j.Logger;

import com.theuberlab.njord.constants.NjordNodeType;
import com.theuberlab.njord.main.Njord;
import com.theuberlab.njord.main.NjordConcierge;

/**
 * NjordTreeRenderer is Njord's extension of DefaultTreeCellRenderer. It renders the navigation tree with customizations specific to Njord.
 * 
 * @author Aaron Forster a.forster@f5.com
 *
 */
public class NjordTreeRenderer extends DefaultTreeCellRenderer {
	/**
	 * The Major Version of the class.
	 */
	private static final long serialVersionUID = 2L;
	/**
     * Holds our logger factory for SLF4J.
     */
    public Logger log = null;
    NjordConcierge concierge;
    //TODO: I should get these from the plugin.
	public Icon cleanIcon;                           
	public Icon dirtyIcon;   
	public Icon folderIcon;                           
	public Icon bigipIcon;                     
	public Icon localIcon;
	public NjordNodeType nodeType = null;
	public Njord owner = null;
	
	/**
	 * Instantiates an instance of NjordTreeRenderer
	 */
	public NjordTreeRenderer(NjordConcierge concierge) {
	    this.concierge = concierge;
	    log = concierge.getOwner().getLoggerHandle();
	    concierge.getImageProvider().getIcon("RuleIconClean");
	    concierge.getImageProvider().getIcon("RuleIconDirty");
	    concierge.getImageProvider().getIcon("FolderIcon");
	    concierge.getImageProvider().getIcon("BIGIPIcon");
	    concierge.getImageProvider().getIcon("LocalIcon");
	}
	
	/**
	 * The primary Rendering component. This is the element which actually defines the display of each component.
	 * 
	 * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
	 */
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
//		TextEditorPane njordiRuleNode = null; 
//		Component njordiRuleNode = null;
		NjordRuleEditorPanel njordPanel = null;
		
		NjordTreeNode node = (NjordTreeNode)value;
		nodeType = node.getNodeType();
		
		log.debug("Getting renderer for object of type [{}]", nodeType);
		
		//TODO: This whole thing still needs to be gutted and replaced with things that call the plugin. I can only have one renderer per tree. NjordTreeRenderer should be calling methods like pluginManager.getCleanNavIconForType(String objectType)     pluginManager.getEditorPaneForType(String editorType)
		if ( nodeType == NjordNodeType.NODE_TYPE_EDITABLE ) {
	          njordPanel = (NjordRuleEditorPanel) node.getUserObject();
		 }
		
		super.getTreeCellRendererComponent(
				tree, value, sel,
				expanded, leaf, row,
				hasFocus);
		switch (nodeType) {
		    case NODE_TYPE_ORIGIN: {
                //TODO: Add an isConnected for nodes of type origin. Then do an if getIsConnecte() { connectedicon } else { notConnectedIcon
		        setIcon(bigipIcon);
		        setToolTipText("A remote BIG-IP.");
		        break;
		        // Doesn't need to match this anymore do cleanDirty instead.
//		        if (node.toString().matches("BigIP")) {
//	            }
		    }
		    case NODE_TYPE_CATEGORY: {
                // Don't do anything different here.
		        break;
            }
		    case NODE_TYPE_CONTEXT: {
		       // Don't do anything different here.
		        break;
            }
		    case NODE_TYPE_TOP: {
		      // Don't do anything different here.
		        break;
            }
		    case NODE_TYPE_EDITABLE:
		    default: {
	            // Default is editable
	            if (!njordPanel.getIsDirty()) {
	                setIcon(cleanIcon);
	                setToolTipText("The object is up to date on the server.");
	                setName(njordPanel.getName());
	            } else {
	                setIcon(dirtyIcon);
	                setToolTipText("The object has been modified locally and must be saved");
	                setText(" *** " + njordPanel.getName());
	            }
		    }
		}
		
		//else {
		//	        	setIcon(folderIcon);
		//	            setToolTipText(null);
		//  }
		return this;
	}
	
//	/** 
//	 * Generates an image that can be used in a JTree.
//	 * 
//	 * @param path a java.net.URL location to an image.
//	 * @return An imageIcon suitable for use in a JTree or null if the path was invalid.
//	 */
//	public static ImageIcon createImageIcon(String path) {
//		java.net.URL imgURL = NjordTreeRenderer.class.getResource(path);
//		if (imgURL != null) {
//			return new ImageIcon(imgURL);
//		} else {
//			System.err.println("Couldn't find file: " + path);
//			return null;
//		}
//	}
}
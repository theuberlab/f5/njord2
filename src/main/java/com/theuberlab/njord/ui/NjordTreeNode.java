/**
 * 
 */
package com.theuberlab.njord.ui;

import javax.swing.tree.DefaultMutableTreeNode;

import com.theuberlab.njord.constants.NjordNodeCategory;
import com.theuberlab.njord.constants.NjordNodeContext;
import com.theuberlab.njord.constants.NjordNodeType;
import com.theuberlab.njord.model.BigIP;

/**
 * NjordTreeNode is a class to override the DefaultMutableTreeNode in order to give us capabilities such as not building the iRules tree 
 * until we actually expand the iRulesTree.
 * NjordTreeNode v2 is a major departure from v1. Where v2 was only in place until the tree was populated NjordTreeNode v2 will always be the tree node.
 * Then if a tree node is double clicked it will return the path to the object we want to load and we will load that in a tab.
 * 
 * @author forster
 *
 */
public class NjordTreeNode extends DefaultMutableTreeNode {
    //TODO: NjordTreeNode should be able to tell me things like: What BIGIP is the parent of this node and other path type related stuff.
	/**
	 * The Major Version of the class.
	 */
	private static final long serialVersionUID = 2L;
	/**
	 * The short name of this node. I.E. myiRule
	 */
	public String name;
	/**
	 * The full name including the path prefix for v10 systems. I.E. /Common/myiRule
	 */
	public String fullName;
	/**
	 * Has the object that this node represents been fetched
	 */
    public boolean populated;
    /**
     * The node type. I.E. NODE_TYPE_EDITABLE NODE_TYPE_ORIGIN, etc.
     */
    public NjordNodeType nodeType = null;
    /**
     * The node origin. I.E. The BIG-IP this node resides on. Or local.
     */
    public NjordTreeNode nodeOrigin = null;
    /**
     * The node context. Local or remote.
     */
    public NjordNodeContext nodeContext = null;
    /**
     * The node category. 
     */
    public NjordNodeCategory nodeCategory = null;
    /**
     * Dirty if the object that this node represents has been modified.
     */
    public boolean dirty = false;
    public BigIP bigIP;
    
//    /**
//     * DEPRICATED Creates a new node from an RSTA TextEditorPane.
//     * When created with the TextEditorPane constructor and no nodeType the NjordTreeNode will be initialized with a node Type of NjordConstants.NODE_TYPE_IRULE.
//     * 
//     * @param editorPane
//     */
//    public NjordTreeNode(TextEditorPane editorPane) {
//    	super(editorPane);
//    	nodeType = NjordConstants.NODE_TYPE_IRULE;
//    	name = editorPane.getName();
//    	fullName = editorPane.getFileFullPath();
//    	
//    }

    
    /**
     * Eventually this should become the only constructor When created with the TextEditorPane constructor and no nodeType the NjordTreeNode will be initialized
     * with a node Type of NjordConstants.NODE_TYPE_IRULE.
     * 
     * @param editorPane
     */
    public NjordTreeNode(NjordRuleEditorPanel editorPane) {
        super(editorPane);
        nodeType = NjordNodeType.NODE_TYPE_EDITABLE;
        name = editorPane.getName();
        fullName = editorPane.getFullName();
        
    }
    
    /**
     * Creates a new node from a bigip.
     * 
     * @param name
     */
    public NjordTreeNode(BigIP bigip) {
        super(bigip.getName());
        this.nodeType = NjordNodeType.NODE_TYPE_ORIGIN;
        this.bigIP = bigip;
        this.name = bigip.getName();
        this.fullName = bigip.getName();
    }

//    /**
//     * DEPRICATED Creates a new node from an RSTA TextEditorPane.
//     * When created with the TextEditorPane constructor and no nodeType the NjordTreeNode will be initialized with a node Type of NjordConstants.NODE_TYPE_IRULE.
//     * 
//     * @param editorPane
//     */
//    public NjordTreeNode(String name, NjordNodeType nodeType, Object object) {
//        super(object);
//        this.nodeType = nodeType;
//        this.name = name;
////        fullName = editorPane.getFileFullPath();
//    }
    
    /**
     * Creates a new node specifying the type.
     * 
     * @param name
     * @param nodeType
     */
    public NjordTreeNode(String name, NjordNodeType nodeType) {
    	super(name);
    	this.name = name;
    	this.nodeType = nodeType;
    	this.fullName = name;
    }
    
    
	/**
     * Sets the node type
     * 
     * @param newType
     */
    public void setNodeType(NjordNodeType newType) {
    	nodeType = newType;
    }
    
    /**
     * Gets the node type
     * 
     * @return NjordNodeType
     */
    public NjordNodeType getNodeType() {
    	return nodeType;
    }
    
    /**
     * Sets the node Context
     * 
     * @param newContext
     */
    public void setNodeContext(NjordNodeContext newContext) {
        nodeContext = newContext;    
    }
    
    /**
     * Gets the node context
     * 
     * @return NjordNodeType
     */
    public NjordNodeContext getNodeContext() {
        return nodeContext;
    }
    
    /**
     * Sets the node category.
     * 
     * @param newType
     */
    public void setNodeCategory(NjordNodeCategory newCategory) {
        nodeCategory = newCategory;
    }
    
    /**
     * Gets the node type
     * 
     * @return NjordNodeType
     */
    public NjordNodeCategory getNodeCategory() {
        return nodeCategory;
    }
    
    /**
     * Gets the short name.
     * 
     * @return
     */
    public String getShortName() {
        return name;
    }
    
    /**
     * Gets the full name.
     * @return
     */
    public String getFullName() {
        return fullName;
    }
    
    /**
     * Gets the bigip if that is what's stored.
     * 
     * @return
     */
    public BigIP getBigIP() {
        return bigIP;
    }
    
    /**
     * Returns a NjordTreeNode version of the parent node.
     */
    @Override
    public NjordTreeNode getParent() {
        NjordTreeNode parent = (NjordTreeNode) super.getParent();
        return parent;
    }
    
    /**
     * Returns the short name.
//     * 
     * @return
     */
    @Override
    public String toString() {
        if (name == null) {
            return super.toString();
        } else {
            return name;
        }
        
    }
}
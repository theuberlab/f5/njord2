package com.theuberlab.njord.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.slf4j.Logger;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import com.theuberlab.njord.main.Njord;

/**
 * <code>BIGIPSettingsDialog</code> Produces a dialog used to edit the settings for a BIG-IP. It also allows for the creation of a new BIG-IP object. 
 * 
 * @author Aaron Forster
 *
 */
public class BIGIPSettingsDialog extends JDialog implements PropertyChangeListener, ActionListener {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Njord owner;

    private JOptionPane optionPane;
    
    // Do I still need these?
    private String btnString1 = "Enter";
    private String btnString2 = "Cancel";
    
	public JFrame frame;
	public JTextField ConnPreffsBigIPName;
	public JTextField ConnPreffsHostTextField;
	public JTextField ConnPreffsPortTextField;
	public JTextField ConnPreffsUserTextField;
	public JPasswordField ConnPreffsPasswordTextField;
	public JCheckBox chckbxShow = null;
	public JCheckBox chkbxConnPrefsSavePassword;
	public JCheckBox chkbxConnPrefsConnectAutomatically;
	public JPanel panel_1;
	/**
     * Holds our logger factory for SLF4J.
     */
    public final Logger log;
	
    /** The constructor. */
    public BIGIPSettingsDialog(Frame aFrame, Njord parent) {
        super(aFrame, true);
        owner = parent;
        
        setTitle("Connection Settings");
        
        log = owner.getLoggerHandle();
        
        //Create an array specifying the number of dialog buttons
        //and their text. this makes btnString1 oktext and btnString2 cancel text
        Object[] options = {btnString1, btnString2};
        
        // I wonder what these were for..
//        JTextField xField = new JTextField(5);
//        JTextField yField = new JTextField(5);
        
        ////////////////////////////////////              Panel Stuff
        
        frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		panel_1 = new JPanel();
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new BorderLayout(10, 10));
		
		Component verticalStrut = Box.createVerticalStrut(20);
		panel_1.add(verticalStrut, BorderLayout.NORTH);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut, BorderLayout.WEST);
		
		JPanel panel = new JPanel();
		panel_1.add(panel);
		panel.setLayout(new FormLayout(new ColumnSpec[] {
		        FormFactory.RELATED_GAP_COLSPEC,
		        FormFactory.DEFAULT_COLSPEC,
		        FormFactory.RELATED_GAP_COLSPEC,
		        ColumnSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_COLSPEC,
		        FormFactory.DEFAULT_COLSPEC,
		        FormFactory.RELATED_GAP_COLSPEC,
		        ColumnSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_COLSPEC,
		        FormFactory.DEFAULT_COLSPEC,
		        FormFactory.RELATED_GAP_COLSPEC,
		        ColumnSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_COLSPEC,
		        FormFactory.DEFAULT_COLSPEC,
		        FormFactory.RELATED_GAP_COLSPEC,},
		    new RowSpec[] {
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        RowSpec.decode("default:grow"),
		        FormFactory.RELATED_GAP_ROWSPEC,
		        RowSpec.decode("default:grow"),}));
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(120);
		panel.add(horizontalStrut_2, "8, 2");
		
		JLabel lblBigipName = new JLabel("Bigip Name:");
		lblBigipName.setToolTipText("A unique human readable name that identifies this bigip. No spaces or special characters.");
		panel.add(lblBigipName, "4, 4");
		
		ConnPreffsBigIPName = new JTextField();
		ConnPreffsBigIPName.setText("newBigIP");
        panel.add(ConnPreffsBigIPName, "8, 4, fill, default");
        ConnPreffsBigIPName.setColumns(10);
		
        JLabel lblBigipAddress = new JLabel("Host:");
        lblBigipAddress.setToolTipText("IP Address or DNS name");
        panel.add(lblBigipAddress, "4, 6");
        
		ConnPreffsHostTextField = new JTextField();
		ConnPreffsHostTextField.setText("192.168.215.251");
		panel.add(ConnPreffsHostTextField, "8, 6, fill, default");
		ConnPreffsHostTextField.setColumns(10);
		
		JLabel lblBigipConnectionPort = new JLabel("Port:");
		lblBigipConnectionPort.setToolTipText("Likely 443");
		panel.add(lblBigipConnectionPort, "4, 8");
		
		ConnPreffsPortTextField = new JTextField();
		ConnPreffsPortTextField.setText("443");
		panel.add(ConnPreffsPortTextField, "8, 8, fill, default");
		ConnPreffsPortTextField.setColumns(10);
		
		JLabel lblUnsername = new JLabel("Username:");
		lblUnsername.setToolTipText("A user with Administrative access to the BigIP System");
		panel.add(lblUnsername, "4, 10");
		
		ConnPreffsUserTextField = new JTextField();
		ConnPreffsUserTextField.setText("admin");
		panel.add(ConnPreffsUserTextField, "8, 10, fill, default");
		ConnPreffsUserTextField.setColumns(10);
		
		//TODO: Make this contain asterisks
		//TODO: Potentially add a shoulder surfing replace display text as you type with asterisks.
		//TODO: Make sure not to write asterisks to the password variable stored in preferences
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setToolTipText("The password for the above account");
		panel.add(lblPassword, "4, 12");
		
		ConnPreffsPasswordTextField = new JPasswordField();
//		ConnPreffsPasswordTextField = new JTextField();
		ConnPreffsPasswordTextField.setText("admin");
		panel.add(ConnPreffsPasswordTextField, "8, 12, fill, default");
		ConnPreffsPasswordTextField.setColumns(10);
		
		chckbxShow = new JCheckBox("Show");
		panel.add(chckbxShow, "12, 12");
		chckbxShow.addActionListener(this);
		
		JLabel lblSavePassword = new JLabel("Save Password:");
		lblSavePassword.setToolTipText("Write the password (encrypted) to the preferences file.");
        panel.add(lblSavePassword, "4, 14");
        
        chkbxConnPrefsSavePassword = new JCheckBox();
        panel.add(chkbxConnPrefsSavePassword, "8, 14");
        
		JLabel lblConnectAutomatically = new JLabel("Connect Automatically:");
		lblConnectAutomatically.setToolTipText("Should we connect to this bigip at Njord start-up");
        panel.add(lblConnectAutomatically, "4, 16");
		
        chkbxConnPrefsConnectAutomatically = new JCheckBox();
        panel.add(chkbxConnPrefsConnectAutomatically, "8, 16");
        
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_1, BorderLayout.EAST);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		panel_1.add(verticalStrut_1, BorderLayout.SOUTH);
        
        //Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(
                                        JOptionPane.CLOSED_OPTION));
            }
        });
        
        //Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent ce) {
                ConnPreffsBigIPName.requestFocusInWindow();
            }
        });
        
    }
    
    /**
     * This method reacts to state changes in the option pane.
     */
    public void propertyChange(PropertyChangeEvent e) {
        String prop = e.getPropertyName();

        if (isVisible()
         && (e.getSource() == optionPane)
         && (JOptionPane.VALUE_PROPERTY.equals(prop) ||
             JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
            Object value = optionPane.getValue();

            if (value == JOptionPane.UNINITIALIZED_VALUE) {
                //ignore reset
                return;
            }

            //Reset the JOptionPane's value.
            //If you don't do this, then if the user
            //presses the same button next time, no
            //property change event will be fired.
            optionPane.setValue(
                    JOptionPane.UNINITIALIZED_VALUE);
            
        }
    }
    
    //TODO: Modify this method so that it takes your text and validates it.
    /**
     * NOT YET IMPLEMENTED, currently just returns the text you feed it.
     * Returns null if the typed string was invalid;
     * otherwise, returns the string as the user entered it.
     */
    public String getValidatedText(String text) {
        return text;
    }
    
    /**
     * This method clears the dialog and hides it instead of destroying it.
     */
    public void clearAndHide() {
        // Clear the text for the next time the dialog is used.
        ConnPreffsPasswordTextField.setText(null);
        setVisible(false);
    }
    
    /**
     * Detects actions on objects for which an action listener has been added and takes action based on them.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String showHidePass = e.getActionCommand();
        if (showHidePass.equals("Show")) {
            ConnPreffsPasswordTextField.setEchoChar((char) 0);
            chckbxShow.setActionCommand("Hide");
        } else {
            ConnPreffsPasswordTextField.setEchoChar('*');
            chckbxShow.setActionCommand("Show");
        }
    }
}
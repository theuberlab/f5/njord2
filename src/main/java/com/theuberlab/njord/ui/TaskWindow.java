package com.theuberlab.njord.ui;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.parser.Parser;
import org.fife.ui.rsyntaxtextarea.parser.ParserNotice;
import org.fife.ui.rsyntaxtextarea.parser.TaskTagParser;

import com.theuberlab.njord.interfaces.NjordEditorPanel;
import com.theuberlab.njord.main.Njord;
import com.theuberlab.njord.main.NjordConcierge;



    
   /**
   * A window that displays text flagged in source code comments that have
   * been designated as "tasks."  Tasks are identified by configurable
   * identifiers, such as "<code>FIXME</code>", "<code>TODO</code>" and
   * "<code>HACK</code>".<p>
   *
   * Parsing for tasks is only done if the tasks window is visible.
   *
   * @author Aaron Forster
   * @version 1.0
   */
public class TaskWindow extends AbstractParserNoticeWindow
                  implements PropertyChangeListener {
  
      private JTable table;
      private TaskNoticeTableModel model;
      private TaskTagParser taskParser;
      private boolean installed;
      private ImageIcon icon;
      private NjordConcierge concierge;
  
      /**
       * is this window active.
       */
      private boolean active = false;
      
      public TaskWindow(NjordConcierge concierge, String taskIdentifiers) {
          super(concierge);
          concierge.getOwner().getLoggerHandle().debug("Task Panel startup");
          installed = false;
          this.concierge = concierge;
  
//          model = new TaskNoticeTableModel(concierge.getActiveEditorPane().getString("TaskList.Task"));
          model = new TaskNoticeTableModel("Task");
          table = createTable(model);
          
//          RTextUtilities.removeTabbedPaneFocusTraversalKeyBindings(table);
          JScrollPane scrollPane = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                  ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
          
//          RScrollPane sp = new DockableWindowScrollPane(table);
//          RTextUtilities.removeTabbedPaneFocusTraversalKeyBindings(sp);
  
          setLayout(new BorderLayout());
          add(scrollPane);
  
          // active and position are set by caller, from TasksPrefs
//          setDockableWindowName("Tasks");
          setName("tasks");
          
          URL url = getClass().getResource("page_white_edit.png");
//          setIcon(new ImageIcon(url));
  
          icon = new ImageIcon(getClass().getResource("/images/table12.gif"));
          
          taskParser = new TaskTagParser();
          setTaskIdentifiers(taskIdentifiers);
 
//          if (concierge.getActiveEditorPane().getComponentOrientation() != null) {
//              applyComponentOrientation(concierge.getActiveEditorPane().getComponentOrientation());    
//          }
          
  
      }
    
    
    
	// Static fields.
	public static final int LIST = 0;
	public static final int TABLE = 1;
	
	// Fields.
	private int tableSize = TABLE;
	
	// Constructors.
	
//	public TaskWindow(int tableSize) {
//		super(new BorderLayout());
//		
//		this.tableSize = tableSize; 
//		
//		MyTableModel dataModel = new MyTableModel();
//		JTable table = new JTable(dataModel);
//		JScrollPane scrollpane = new JScrollPane(table);
//		add(scrollpane, BorderLayout.CENTER);
//		
//	}
	
	// Getters / Setters.
	
	public int getTableSize() {
		return tableSize;
	}
	
	public void setTableSize(int size) {
		this.tableSize = size;
	}
	
//	private class MyTableModel extends AbstractTableModel {
//		public int getColumnCount() {
//			if (tableSize == LIST) {
//				return 1;
//			}
//			return 4;
//		}
//		
//		public int getRowCount() {
//			if (tableSize == LIST) {
//				return 20;
//			}
//			return 4;
//		}
//		
//		public Object getValueAt(int row, int col) {
//			return "Hello";
//		}
//	}

	   /**
     * Returns whether a parser is the task parser.
     *
     * @param parser The parser to check.
     * @return Whether the parser is the task parser.
     */
    public boolean isTaskParser(Parser parser) {
        return parser==taskParser;
    }
    
	/**
     * Returns the identifiers scanned for to identify "tasks" (e.g.
     * "<code>TODO</code>", "<code>FIXME</code>", "<code>IDEA</code>", etc.).
     *
     * @return The identifiers.  This will always return a value, even it task
     *         parsing is disabled.  If there are no identifiers, an empty
     *         string is returned.
     * @see #setTaskIdentifiers(String)
     */
    public String getTaskIdentifiers() {
        String pattern = taskParser.getTaskPattern();
        if (pattern!=null) {
            pattern = pattern.replaceAll("\\\\\\?", "?");
        }
        else {
            pattern = "";
        }
        return pattern;
    }
    
    /**
     * Sets the identifiers scanned for when locating tasks.
     *
     * @param identifiers The identifiers, separated by the '<code>|</code>'
     *        character.
     * @return Whether the task pattern was actually modified.  This will be
     *         <code>false</code> if <code>identifiers</code> is the same as
     *         the current value.
     * @see #getTaskIdentifiers()
     */
    public boolean setTaskIdentifiers(String identifiers) {
        if (!identifiers.equals(getTaskIdentifiers())) {
            identifiers = identifiers.replaceAll("\\?", "\\\\\\?");
            taskParser.setTaskPattern(identifiers);
            return true;
        }
        return false;
    }
	
    /* (non-Javadoc)
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        // TODO Auto-generated method stub
        
    }
    
    public void setIcon(ImageIcon icon) {
        this.icon = icon;
        // TODO: Update parent UI
    }
    
    /**
     * Returns whether this dockable window is active (i.e., visible).
     *
     * @return Whether this dockable window is not active.
     * @see #getPosition
     * @see #setActive
     */
    public boolean isActive() {
        return active;
    }
    
    /**
     * Overridden to disable the task parser when the task window isn't active
     * (visible).
     *
     * @param active Whether the task window should be active.
     */
    public void setActive(boolean active) {
        concierge.getOwner().getLoggerHandle().debug("Setting active state to [{}]", !active);
        if (active!=isActive()) {
            this.active = active;
            if (active && !installed) {
                installParser();
            }
            else if (!active && installed) {
                uninstallParser();
            }
        }
    }
    
    /**
     * @see #uninstallParser()
     */
    private void installParser() {
        concierge.getOwner().getLoggerHandle().debug("Installing Parser");
        if (!installed) {
//            RText rtext = getRText();
//            AbstractMainView mainView = rtext.getMainView();
            Njord mainView = concierge.getOwner();
            //Uncomment these when you start working on this again.
//            mainView.addPropertyChangeListener(Njord.TEXT_AREA_ADDED_PROPERTY, this);
//            mainView.addPropertyChangeListener(Njord.TEXT_AREA_REMOVED_PROPERTY, this);
//            for (int i=0; i<mainView.getNumDocuments(); i++) {
            
                NjordEditorPanel textArea = concierge.getActiveEditorPane();
                addTaskParser(textArea);
                concierge.getOwner().getLoggerHandle().debug("for panel [{}]", textArea.getName());
//            }
            installed = true;
        }
    }
    
    /**
     * @see #installParser()
     */
    private void uninstallParser() {
        concierge.getOwner().getLoggerHandle().debug("Uninstalling Parser");
        if (installed) {
//            RText rtext = getRText();
//            AbstractMainView mainView = rtext.getMainView();
            Njord mainView = concierge.getOwner();
            //Uncomment these when you start working on this again.
//            mainView.removePropertyChangeListener(Njord.TEXT_AREA_ADDED_PROPERTY, this);
//            mainView.removePropertyChangeListener(Njord.TEXT_AREA_REMOVED_PROPERTY, this);
//            for (int i=0; i<mainView.getNumDocuments(); i++) {
                NjordEditorPanel textArea = concierge.getActiveEditorPane();
                concierge.getOwner().getLoggerHandle().debug("for panel [{}]", textArea.getName());
                removeTaskParser(textArea);
//            }
            model.setRowCount(0);
            installed = false;
        }
    }


    //TODO Do these things better
    /**
     * Removes the listeners that parse a text area for tasks.
     *
     * @param textArea The text area to stop parsing for tasks.
     * @see #addTaskParser(RTextEditorPane)
     */
    private void removeTaskParser(NjordEditorPanel textArea) {
        textArea.removePropertyChangeListener(
                RSyntaxTextArea.PARSER_NOTICES_PROPERTY, this);
        textArea.removeParser(taskParser);
    }
    
    /**
     * Adds listeners to start parsing a text area for tasks.
     *
     * @param textArea The text area to start parsing for tasks.
     * @see #removeTaskParser(RTextEditorPane)
     */
    private void addTaskParser(NjordEditorPanel textArea) {
        textArea.addPropertyChangeListener(
                RSyntaxTextArea.PARSER_NOTICES_PROPERTY, this);
        textArea.addParser(taskParser);
    }
    
    private class TaskNoticeTableModel extends ParserNoticeTableModel {

        public TaskNoticeTableModel(String lastColHeader) {
            super(lastColHeader);
        }

        protected void addNoticesImpl(NjordRuleEditorPanel textArea, List notices) {
            for (Iterator i=notices.iterator(); i.hasNext(); ) {
                ParserNotice notice = (ParserNotice)i.next();
                if (notice.getParser()==taskParser) {
                    Object[] data = {   new ImageIcon(getClass().getResource("/images/table12.gif")), textArea,
                            // Integer.intValue(notice.getValue()+1) // TODO: 1.5
                            new Integer(notice.getLine()+1),
                            notice.getMessage() };
                    addRow(data);
                }
            }
        }
    }
    
//    
//    
//    /**
//     * Constructor.
//     *
//     * @param tableModel ???
//     * @param tableHeader ???
//     */
//    public FileExplorerTableModel(TableModel tableModel, JTableHeader tableHeader) {
//        this();
//        setTableHeader(tableHeader);
//        setTableModel(tableModel);
//    }
//
//    /**
//     * Sets the table this sorter is the model for.  By setting this to a
//     * non-<code>null</code> value, any columns by which the table is sorted
//     * will be colored with a slightly-darker background (similar to how it's
//     * done in Windows XP).
//     *
//     * @param table The table for which this model is the model.
//     */
//    public void setTable(JTable table) {
//        this.table = table;
//        setTableHeader(table.getTableHeader());
//    }
    
    
    
    
    
//    private class TaskNoticeTableModel extends ParserNoticeTableModel {
//
//        public TaskNoticeTableModel(String lastColHeader) {
//            super(lastColHeader);
//        }
//
//        protected void addNoticesImpl(RTextEditorPane textArea, List notices) {
//            for (Iterator i=notices.iterator(); i.hasNext(); ) {
//                ParserNotice notice = (ParserNotice)i.next();
//                if (notice.getParser()==taskParser) {
//                    Object[] data = {   getIcon(), textArea,
//                            // Integer.intValue(notice.getValue()+1) // TODO: 1.5
//                            new Integer(notice.getLine()+1),
//                            notice.getMessage() };
//                    addRow(data);
//                }
//            }
//        }
//        
//    }
    
    
}









/*
 * ########################################################################
 * FROM RSYNTAXTEXTEDITOR (which uses RSYNTAXTEXTAREA exentisvely. ^_^
 * ########################################################################
 */





///*
// * 10/17/2009
// *
// * TaskWindow.java - A dockable window that lists tasks (todo's, fixme's, etc.)
// * in open files.
// * Copyright (C) 2009 Robert Futrell
// * http://fifesoft.com/rtext
// * Licensed under a modified BSD license.
// * See the included license file for details.
// */
//package org.fife.rtext.plugins.tasks;
//
//import java.awt.BorderLayout;
//import java.beans.PropertyChangeEvent;
//import java.beans.PropertyChangeListener;
//import java.net.URL;
//import java.util.Iterator;
//import java.util.List;
//
//import javax.swing.ImageIcon;
//import javax.swing.JTable;
//
//import org.fife.rtext.AbstractMainView;
//import org.fife.rtext.AbstractParserNoticeWindow;
//import org.fife.rtext.RText;
//import org.fife.rtext.RTextEditorPane;
//import org.fife.rtext.RTextUtilities;
//import org.fife.ui.RScrollPane;
//import org.fife.ui.dockablewindows.DockableWindowScrollPane;
//import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
//import org.fife.ui.rsyntaxtextarea.parser.Parser;
//import org.fife.ui.rsyntaxtextarea.parser.ParserNotice;
//import org.fife.ui.rsyntaxtextarea.parser.TaskTagParser;
//
//
///**
// * A window that displays text flagged in source code comments that have
// * been designated as "tasks."  Tasks are identified by configurable
// * identifiers, such as "<code>FIXME</code>", "<code>TODO</code>" and
// * "<code>HACK</code>".<p>
// *
// * Parsing for tasks is only done if the tasks window is visible.
// *
// * @author Robert Futrell
// * @version 1.0
// */
//class TaskWindow extends AbstractParserNoticeWindow
//                implements PropertyChangeListener {
//
//    private JTable table;
//    private TaskNoticeTableModel model;
//    private TaskTagParser taskParser;
//    private boolean installed;
//
//
//    public TaskWindow(RText rtext, String taskIdentifiers) {
//
//        super(rtext);
//        installed = false;
//
//        model = new TaskNoticeTableModel(rtext.getString("TaskList.Task"));
//        table = createTable(model);
//        RTextUtilities.removeTabbedPaneFocusTraversalKeyBindings(table);
//        RScrollPane sp = new DockableWindowScrollPane(table);
//        RTextUtilities.removeTabbedPaneFocusTraversalKeyBindings(sp);
//
//        setLayout(new BorderLayout());
//        add(sp);
//
//        // active and position are set by caller, from TasksPrefs
//        setDockableWindowName(rtext.getString("TaskList.Tasks"));
//
//        URL url = getClass().getResource("page_white_edit.png");
//        setIcon(new ImageIcon(url));
//
//        taskParser = new TaskTagParser();
//        setTaskIdentifiers(taskIdentifiers);
//
//        applyComponentOrientation(rtext.getComponentOrientation());
//
//    }
//
//
//    /**
//     * Adds listeners to start parsing a text area for tasks.
//     *
//     * @param textArea The text area to start parsing for tasks.
//     * @see #removeTaskParser(RTextEditorPane)
//     */
//    private void addTaskParser(RTextEditorPane textArea) {
//        textArea.addPropertyChangeListener(
//                RSyntaxTextArea.PARSER_NOTICES_PROPERTY, this);
//        textArea.addParser(taskParser);
//    }
//
//
//    /**
//     * Returns the identifiers scanned for to identify "tasks" (e.g.
//     * "<code>TODO</code>", "<code>FIXME</code>", "<code>IDEA</code>", etc.).
//     *
//     * @return The identifiers.  This will always return a value, even it task
//     *         parsing is disabled.  If there are no identifiers, an empty
//     *         string is returned.
//     * @see #setTaskIdentifiers(String)
//     */
//    public String getTaskIdentifiers() {
//        String pattern = taskParser.getTaskPattern();
//        if (pattern!=null) {
//            pattern = pattern.replaceAll("\\\\\\?", "?");
//        }
//        else {
//            pattern = "";
//        }
//        return pattern;
//    }
//
//
//    /**
//     * @see #uninstallParser()
//     */
//    private void installParser() {
//        if (!installed) {
//            RText rtext = getRText();
//            AbstractMainView mainView = rtext.getMainView();
//            mainView.addPropertyChangeListener(AbstractMainView.TEXT_AREA_ADDED_PROPERTY, this);
//            mainView.addPropertyChangeListener(AbstractMainView.TEXT_AREA_REMOVED_PROPERTY, this);
//            for (int i=0; i<mainView.getNumDocuments(); i++) {
//                RTextEditorPane textArea = mainView.getRTextEditorPaneAt(i);
//                addTaskParser(textArea);
//            }
//            installed = true;
//        }
//    }
//
//
//    /**
//     * Returns whether a parser is the task parser.
//     *
//     * @param parser The parser to check.
//     * @return Whether the parser is the task parser.
//     */
//    public boolean isTaskParser(Parser parser) {
//        return parser==taskParser;
//    }
//
//
//    /**
//     * Notified when a text area is parsed, or when a text area is added or
//     * removed (so listeners can be added/removed as appropriate).
//     *
//     * @param e The event.
//     */
//    public void propertyChange(PropertyChangeEvent e) {
//
//        String prop = e.getPropertyName();
//
//        // A text area has been re-parsed for tasks.
//        if (RSyntaxTextArea.PARSER_NOTICES_PROPERTY.equals(prop)) {
//            RTextEditorPane source = (RTextEditorPane)e.getSource();
//            List notices = source.getParserNotices();//(List)e.getNewValue();
//            model.update(source, notices);
//        }
//
//        if (AbstractMainView.TEXT_AREA_ADDED_PROPERTY.equals(prop)) {
//            RTextEditorPane textArea = (RTextEditorPane)e.getNewValue();
//            addTaskParser(textArea);
//        }
//
//        else if (AbstractMainView.TEXT_AREA_REMOVED_PROPERTY.equals(prop)) {
//            RTextEditorPane textArea = (RTextEditorPane)e.getNewValue();
//            textArea.removeParser(taskParser);
//            textArea.removePropertyChangeListener(
//                            RSyntaxTextArea.PARSER_NOTICES_PROPERTY, this);
//        }
//
//    }
//
//
//    /**
//     * Removes the listeners that parse a text area for tasks.
//     *
//     * @param textArea The text area to stop parsing for tasks.
//     * @see #addTaskParser(RTextEditorPane)
//     */
//    private void removeTaskParser(RTextEditorPane textArea) {
//        textArea.removePropertyChangeListener(
//                RSyntaxTextArea.PARSER_NOTICES_PROPERTY, this);
//        textArea.removeParser(taskParser);
//    }
//
//
//    /**
//     * Overridden to disable the task parser when the task window isn't active
//     * (visible).
//     *
//     * @param active Whether the task window should be active.
//     */
//    public void setActive(boolean active) {
//        if (active!=isActive()) {
//            super.setActive(active);
//            if (active && !installed) {
//                installParser();
//            }
//            else if (!active && installed) {
//                uninstallParser();
//            }
//        }
//    }
//
//
//    /**
//     * Sets the identifiers scanned for when locating tasks.
//     *
//     * @param identifiers The identifiers, separated by the '<code>|</code>'
//     *        character.
//     * @return Whether the task pattern was actually modified.  This will be
//     *         <code>false</code> if <code>identifiers</code> is the same as
//     *         the current value.
//     * @see #getTaskIdentifiers()
//     */
//    public boolean setTaskIdentifiers(String identifiers) {
//        if (!identifiers.equals(getTaskIdentifiers())) {
//            identifiers = identifiers.replaceAll("\\?", "\\\\\\?");
//            taskParser.setTaskPattern(identifiers);
//            return true;
//        }
//        return false;
//    }
//
//
//    /**
//     * @see #installParser()
//     */
//    private void uninstallParser() {
//        if (installed) {
//            RText rtext = getRText();
//            AbstractMainView mainView = rtext.getMainView();
//            mainView.removePropertyChangeListener(AbstractMainView.TEXT_AREA_ADDED_PROPERTY, this);
//            mainView.removePropertyChangeListener(AbstractMainView.TEXT_AREA_REMOVED_PROPERTY, this);
//            for (int i=0; i<mainView.getNumDocuments(); i++) {
//                RTextEditorPane textArea = mainView.getRTextEditorPaneAt(i);
//                removeTaskParser(textArea);
//            }
//            model.setRowCount(0);
//            installed = false;
//        }
//    }
//
//
//    private class TaskNoticeTableModel extends ParserNoticeTableModel {
//
//        public TaskNoticeTableModel(String lastColHeader) {
//            super(lastColHeader);
//        }
//
//        protected void addNoticesImpl(RTextEditorPane textArea, List notices) {
//            for (Iterator i=notices.iterator(); i.hasNext(); ) {
//                ParserNotice notice = (ParserNotice)i.next();
//                if (notice.getParser()==taskParser) {
//                    Object[] data = {   getIcon(), textArea,
//                            // Integer.intValue(notice.getValue()+1) // TODO: 1.5
//                            new Integer(notice.getLine()+1),
//                            notice.getMessage() };
//                    addRow(data);
//                }
//            }
//        }
//        
//    }
//
//
//}
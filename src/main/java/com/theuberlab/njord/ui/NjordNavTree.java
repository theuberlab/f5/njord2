package com.theuberlab.njord.ui;

import java.awt.BorderLayout;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.slf4j.Logger;

import com.theuberlab.njord.constants.NjordNodeCategory;
import com.theuberlab.njord.constants.NjordNodeContext;
import com.theuberlab.njord.constants.NjordNodeType;
import com.theuberlab.njord.main.NjordConcierge;
import com.theuberlab.njord.model.BigIP;

/**
 * The navigation window. A dockable pane which contains a scrollable JTree.
 * @author Aaron Forster
 * 
 */
public class NjordNavTree extends JPanel implements TreeSelectionListener {
	// ###############################################################
	// VARIABLES
	// ###############################################################
	/**
	 * The Major Version of the class.
	 */
	private static final long serialVersionUID = 2L;
	/**
     * Holds our logger factory for SLF4J.
     */
    public Logger log = null;
    /**
     * Holds the TreeModel which is used to controll where things are inserted into the tree.
     */
	public TreeModel treeModel = null;
	/**
	 * Holds the invisible top of the tree.
	 */
	public NjordTreeNode top = null;
	public NjordTreeNode remoteTree = null;
	public NjordTreeNode localTree = null;
	public NjordTreeNode localiRulesCategory = null;
	public NjordTreeNode localiAppsTemplatesCategory = null;
	public JTree navigationTree = null;
	public NjordConcierge concierge = null;
	
	public NjordTreeNode lastSelectedOrigin = null;
	public NjordTreeNode lastSelectedContext = null;
	public NjordTreeNode lastSelectedCategory = null;
	
	// ##########################
	// Path components
	/**
	 * The last editable node selected.
	 */
	NjordTreeNode lastSelectedEditablenode = null;
	/**
	 * Last selected category node. Category nodes are things like 'iRules.' since we can have multiple BIG-IPS as well as local 
	 * this will store the path component so that we can get the last selected category+context. Useful for 'new' type dialogs.
	 */
	NjordTreeNode lastSelectedCategoryNode = null; 
	
	// ###############################################################
	// CONSTRUCTORS
	// ###############################################################
	/**
	 * Constructs a tree with the contact persons in the different countries.
	 *
	 */
	public NjordNavTree(NjordConcierge concierge) {
		this.concierge = concierge;
		top = new NjordTreeNode("top", NjordNodeType.NODE_TYPE_TOP);
		//Update this from LOCAL_REMOTE
		remoteTree = new NjordTreeNode("Remote", NjordNodeType.NODE_TYPE_CONTEXT);
		remoteTree.setNodeContext(NjordNodeContext.NODE_CONTEXT_REMOTE);
		localTree = new NjordTreeNode("Local", NjordNodeType.NODE_TYPE_CONTEXT);
		localTree.setNodeContext(NjordNodeContext.NODE_CONTEXT_LOCAL);
		top.add(remoteTree);
		top.add(localTree);
		log = concierge.getOwner().getLoggerHandle();
	    
	    localiRulesCategory = new NjordTreeNode("iRules", NjordNodeType.NODE_TYPE_CATEGORY);
        localTree.add(localiRulesCategory);
        
        // Set some defaults
        lastSelectedOrigin = localTree;
        lastSelectedContext = localTree;
        lastSelectedCategory = localiRulesCategory;
        
        for (BigIP bigip: concierge.getPreferences().getBIGIPS()) {
            //TODO: the NjordTreeNode should be able to contain the BIGIP object so I can just fetch connection information from that.
//        	NjordTreeNode bigipNode = new NjordTreeNode(bigip.getName(), NjordConstants.NODE_TYPE_REMOTE);
        	NjordTreeNode bigipNode = new NjordTreeNode(bigip);
        	bigipNode.setNodeContext(NjordNodeContext.NODE_CONTEXT_REMOTE);
        	
        	//TODO: The section 'iRules' for each bigip isn't showing up as a collapsable folder.
        	remoteTree.add(bigipNode);
        	//TODO: Build the nav tree for this bigip once we click on it. This here should make it so we don't automatically fetch the list if it's configured not to but currently we have no way to subsequently connect.
        	
        	//Moving this bit back into Njord so I can get the gui buit before populating the list.
//    	    if (bigip.getConnectAutomatically()) {
//                owner.buildNodes(bigipNode, NjordNodeType.NODE_TYPE_ORIGIN, NjordNodeContext.NODE_CONTEXT_REMOTE, "IRULE");
//    	    }
        	
        }
        
        //TODO: This whole bit should probably be moved to Njord.CreateNavTree()?
        if (concierge.getPreferences().getDisplayExperimental() == true) {
            // Build the iApps Templates tree items (even if there isn't any content yet.)
    		localiAppsTemplatesCategory = new NjordTreeNode("iApps Templates", NjordNodeType.NODE_TYPE_CATEGORY);
            localTree.add(localiAppsTemplatesCategory);
            //TODO: This needs to loop  through the remote tree instead of the list of bigips or it will never work.
//            for (BigIP bigip: preferences.getBIGIPS()) {
            for (BigIP bigip: concierge.getPreferences().getBIGIPS()) {
            	NjordTreeNode bigipNode = new NjordTreeNode(bigip.getName(), NjordNodeType.NODE_TYPE_ORIGIN);
            	NjordTreeNode remoteiAppsTemplatesCategory = new NjordTreeNode("iApps Templates", NjordNodeType.NODE_TYPE_CATEGORY);
            	bigipNode.add(remoteiAppsTemplatesCategory);
            }
        }
        
		treeModel = new DefaultTreeModel(top);
		
		navigationTree = new JTree(treeModel);
		
		// Pre-expand the tree.
		// This actually works great because at this point we have only structure, no content. Content is added later so we will be expanded down to the final categories (iRules, etc) but those categories won't be expanded. Which is handy since for remote objects we won't fetch the list of templates/rules until we expand those categories.
		for (int row = 1; row < navigationTree.getRowCount(); row++)
		{
			navigationTree.expandRow(row);
		}
		
		// Add the tree in a scroll pane.
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(navigationTree), BorderLayout.CENTER);
		
        navigationTree.setRootVisible(false); //Hides the top node
		
		ToolTipManager.sharedInstance().registerComponent(navigationTree);
		navigationTree.setCellRenderer(new NjordTreeRenderer(concierge));
		
		navigationTree.getSelectionModel().setSelectionMode
	            (TreeSelectionModel.SINGLE_TREE_SELECTION);
		
		//Listen for when the selection changes.
//		navigationTree.addTreeSelectionListener(parent);
		//Perhaps I should move MouseLIstener here as well?
		navigationTree.addTreeSelectionListener(this);
		navigationTree.addMouseListener(concierge.getOwner());
		
		// Set the last selected to default to local until they actually change.
		lastSelectedCategory = localiRulesCategory;
		lastSelectedContext = localTree;
	}
    
	// ###############################################################
    // GETTERS AND SETTERS
    // ###############################################################
    
    // ###############################################################
    // METHODS
    // ###############################################################
	
//	getRowForLocation
	
    /**
     * 
     */
    public Object getLastSelectedPathComponent() {
        // TODO Auto-generated method stub
        return navigationTree.getLastSelectedPathComponent();
    }
    
    /**
     * Eventually this will take a look at the tree you're in and crawl upwards until it finds 
     * an origin (Likely a bigip possibly LOCAL) and return that. For now it's just going to grab the bigip from the treenode.
     * Which means it will only work if a bigip is selected. Otherwise you get grabage.
     * 
     * @param treenode
     */
    public BigIP getPathOrigin(NjordTreeNode treeNode) {
        //TODO: Throw an exception here if it's not a bigip. Which I would swallow in the create Rule section because I just need to know. I don't actually CARE.
        BigIP bigip = null;
        if (treeNode.getNodeType().equals(NjordNodeType.NODE_TYPE_ORIGIN)) {
            bigip = treeNode.getBigIP();
        } else {
            bigip = null;
        }
        
        return bigip;
        // Some additional stuff to do later.
//          // It's a remote node go ahead and connect
//          BigIP fetchBigIP = treeNode.getBigIP();
//          
//          if (fetchBigIP.getIsConnected()) {
//              tuNotify("Connection to BIG-IP [" + fetchBigIP + "] is initialized. Fetching iRules");
//              //getiRulesFrom(Bigip bigip) or something like that.
//              buildNodes(treeNode, NjordNodeType.NODE_TYPE_EDITABLE, NjordConstants.NODE_CATEGORY_REMOTE);
//          } else {
    }
    
    //TODO: replace the above with this.
    /**
     * Returns the selected origin or the origin for which a sub node is selected.
     * 
     * @param treeNode
     * @return
     */
    public NjordTreeNode getLastSelectedOrigin() {
        return lastSelectedOrigin;
    }
    
    /**
     * NOT PROPERLY IMPLEMEMENTED.
     * Eventually this will take a look at the last selected item in the tree and crawl upwards until it figures out the context, 
     * local or remote, that you have selected.
     * 
     * Returns the actual node for the context.
     * @param treeNode
     * @return
     */
    public NjordTreeNode getPathContextNode() {
        return lastSelectedContext;
    }
    
    /**
     * NOT PROPERLY IMPLEMEMENTED.
     * Eventually this will take a look at the last selected item in the tree and crawl upwards until it figures out the context, 
     * local or remote, that you have selected.
     * 
     * Returns the NjordNodeContext for the selected context.
     * @param treeNode
     * @return
     */
    public NjordNodeContext getPathContextType() {
        if (lastSelectedContext == null) {
            // No context selected at all yet. Let's pretend it's Local.
            return NjordNodeContext.NODE_CONTEXT_LOCAL;
        }
        return lastSelectedContext.getNodeContext();
    }
    
       /**
     * NOT PROPERLY IMPLEMEMENTED.
     * Eventually this will take a look at the last selected item in the tree and crawl upwards until it figures out the parent 
     * category (Local irule, global irule, virtual server) of the object that is selected.
     * 
     * @param treeNode
     * @return
     */
    public NjordTreeNode getPathCategory() {
        return lastSelectedCategory;
    }
    
    /**
     * valueChanged(TreeSelectionEvent) is triggered whenever someone clicks on an element in the navigation tree.
     */
    public void valueChanged(TreeSelectionEvent e) {
        log.debug("TreeSelectionEvent Triggered");
        System.out.println("TreeSelectionEvent Triggered");
        NjordTreeNode node = (NjordTreeNode) this.getLastSelectedPathComponent();
        log.debug("Node has name {}", node.name);
        NjordNodeType nodeType = node.getNodeType();
        NjordNodeContext nodeContext = node.getNodeContext();
        NjordNodeCategory nodeCategory = node.getNodeCategory();
        
        
        switch (nodeType) {
            case NODE_TYPE_CATEGORY: {
                lastSelectedCategory = node;
                
                NjordTreeNode nodeParent = node.getParent();
                switch (nodeParent.getNodeType()) {
                    case NODE_TYPE_ORIGIN: {
                        lastSelectedOrigin = nodeParent;
                        lastSelectedContext = remoteTree;
                        break;
                    }
                    case NODE_TYPE_CONTEXT: {
                        // This had better be 'local'
                        if (nodeParent.getShortName().equals("Local")) {
                            //I'm not sure how well this will work out but 'LOCAL' is both the origin and the context
                            lastSelectedOrigin = nodeParent;
                            lastSelectedContext = nodeParent;
                        } else {
                            log.error("Category Node without either an origin or \"Local\" as a parent");
                        }
                        lastSelectedOrigin = nodeParent;
                        break;
                    }
                    default: {
                        log.error("Category node with something odd as a parent");
                        break;
                    }
                }
                break;
            }
            case NODE_TYPE_CONTEXT: {
                lastSelectedContext = node;
                break;
            }
            case NODE_TYPE_ORIGIN: {
                lastSelectedOrigin = node;
                lastSelectedContext = remoteTree;
                break;
            }
            case NODE_TYPE_TOP: {
                log.error("Node TOP selected");
                // Top is invisible so this should be completely unreachable.
                break;
            }
            case NODE_TYPE_EDITABLE: {
                // an editable's parent should be a category.
                // Though I should probably add a new node type of 'directory' or 'folder'
                NjordTreeNode nodeParent = node.getParent();
                switch (nodeParent.getNodeType()) {
                    case NODE_TYPE_CATEGORY: {
                        lastSelectedCategory = nodeParent;
                        NjordTreeNode nodeParentParent = nodeParent.getParent();
                        switch (nodeParentParent.getNodeType()) {
                            case NODE_TYPE_ORIGIN: {
                                lastSelectedOrigin = nodeParentParent;
                                lastSelectedContext = remoteTree;
//                                NjordTreeNode nodeParentParent = nodeParent.getParent();
                                break;
                            }
                            case NODE_TYPE_CONTEXT: {
                                // This had better be 'local'
                                if (nodeParentParent.getShortName().equals("Local")) {
                                    //I'm not sure how well this will work out but 'LOCAL' is both the origin and the context
                                    lastSelectedOrigin = nodeParentParent;
                                    lastSelectedContext = nodeParentParent;
                                } else {
                                    log.error("Category Node without either an origin or \"Local\" as a parent");
                                }
                                lastSelectedOrigin = nodeParentParent;
//                                NjordTreeNode nodeParentParent = nodeParent.getParent();
                                break;
                            }
                            default: {
                                log.error("Category node with something odd as a parent");
                                break;
                            }
                        }
                        break;
                    }
                    default: {
                        log.error("Editable node without a category as a parent");
                        break;
                    }
                }
                break;
            }   
            default: {
                // I'll prolly ditch this.
                break;
            }
        }
        
//        public NjordTreeNode lastSelectedOrigin = null;
//        public NjordTreeNode lastSelectedContext = null;
//        public NjordTreeNode lastSelectedCategory = null;
        
//        
//        switch (nodeContext) {
//            case NODE_CONTEXT_LOCAL:
//                break;
//            case NODE_CONTEXT_REMOTE:
//                break;
//            default:
//                break;
//            
//        }
        
//        // I don't think I'm going to need to do this part.
//        switch (nodeCategory) {
//            case NODE_CATEGORY_GLOBAL_IRULE:
//                break;
//            case NODE_CATEGORY_IAPPS_TEMPLATE:
//                break;
//            case NODE_CATEGORY_LOCAL_IRULE:
//                break;
//            default:
//                break;
//            
//        }
//          public NjordNodeType nodeType = null;
//          public NjordNodeContext nodeContext = null;
//          public NjordNodeCategory nodeCategory = "";
//          public NjordTreeNode nodeOrigin = null;
//          
    }
    
    public void expandPath(TreePath path) {
        navigationTree.expandPath(path);
    }
    
    // A great description of various enumeration types
//    http://stackoverflow.com/questions/1470857/traversing-tree-made-from-defaultmutabletreenode
    //TODO: This should throw NoSuchBigIPException
    public NjordTreeNode getCategoryNodeForBigIP(BigIP bigip) {
        NjordTreeNode returnNode = null;
        // This doesn't work at all. We need to enumerate through all the categories then getParent.
        
        Enumeration<NjordTreeNode> e = top.breadthFirstEnumeration();
        while (e.hasMoreElements()) {
            NjordTreeNode node = e.nextElement();
            if (node.toString().equalsIgnoreCase("iRules")) {
                NjordTreeNode parentNode = node.getParent();
                if (parentNode.toString().equalsIgnoreCase(bigip.getName())) {
                    return node;
                }
                
                
//                Enumeration<NjordTreeNode> f = node.breadthFirstEnumeration();
//                
//                while (f.hasMoreElements()) {
//                    NjordTreeNode subNode = e.nextElement();
//                    //TODO: Do something like getCategory.
//                    if (subNode.toString().equalsIgnoreCase("iRules")) {
//                        return subNode;
////                        model.insertNodeInto(bigipNode, subNode, subNode.getChildCount());
//                    }
//                }
                    
                
//                DefaultTreeModel model = (DefaultTreeModel)navTree.getModel();
//                model.insertNodeInto(bigipNode, node, node.getChildCount());
//              tuNotify("Hit Save to complete creation on BIGIP.");
//              navTree.expandPath(remoteiRulesPath);
//              node.add(bigipNode);
              // Might not need this.
//              remoteiRulesPath = new TreePath(node.getPath());
            }
        }
        
        return returnNode;
    }
    
    public NjordTreeNode getNodeForBigIP(BigIP bigip) {
        NjordTreeNode returnNode = null;
        // This doesn't work at all. We need to enumerate through all the categories then getParent.
        
        Enumeration<NjordTreeNode> e = top.breadthFirstEnumeration();
        while (e.hasMoreElements()) {
            NjordTreeNode node = e.nextElement();
            if (node.toString().equalsIgnoreCase(bigip.getName())) {
                    return node;
            }
        }
        
        return returnNode;
    }
    
    //Get a tree model for this node.
    public TreeModel getTreeModel() {
        return treeModel;
    }
    
    
    
    //TODO: Make sure that Njord's updateLastSelected or whatever that method is called updates this information as well. Probably via adding a method here something like setSelected or something.
    
	
}

/**
 * Objects stores types which represent descrete objects such as BIG-IPS, iRules and others.
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.model;
package com.theuberlab.njord.model;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.rpc.ServiceException;

import com.theuberlab.common.constants.TUNotificationType;
import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.main.NjordConcierge;

// TODOS
/*
 * TODO: Properly store the passwords. I should prompt for a pass phrase to unlock passwords if stored on disk. I should also have an easy way to disable this completely. Like a distribution that just flat won't support saving pass phrases.
 */

/**
 * <code>BigIP</code> is a container for information pertaining to a BIG-IP.
 * It will hold connection information as well as preferences such as show iApps Templates, iRules, bigip.conf, etc. Eventually it will also hold
 * additional preferences which are deemed useful such as "Read Only" "Never store credentials" and anything else that seems useful.
 * 
 * @author Aaron Forster
 *
 */
public class BigIP {
	// ###############################################################
	// VARIABLES
	// ###############################################################
	/**
	 * Holds the short name of the BIG-IP.
	 * Used as a display in the nav tree and in preferences. Must be unique.
	 */
	public String name;
	/**
	 * Holds the host name or IP address we are going to connect to.
	 */
	public String hostNameOrAddress;
	/**
	 * The port to use to connect to this device.
	 */
	public int iControlPort;
	/**
	 * The user name to use in the credentials.
	 */
	public String userName;
	/**
	 * The password to connect with. I need to come up with a way to store this properly.
	 */
	public String password;
	/**
	 * The version of TMOS running on the BIG-IP. Will be used to determine some capabilities.
	 */
	public String version;
	/**
	 * Should we connect when the app is started?
	 */
	public boolean connectAutomatically;
	/**
	 * Stores the names of the iRules on the remote system.
	 */
	public List<String> iRulesnames = new ArrayList<String>();
	/**
	 * Tells us if we are connected or not.
	 */
	public boolean connectionInitialized = false;
	/**
	 * Holds the icontrol interface once it has been initialized.
	 */
	private iControl.Interfaces icInterface;
	/**
	 * Tells us if the nav tree has been built for this BIGIP. Perhaps his should be removed? Perhaps I should store a referene to 
	 * the tree I contain and then getNavTreeBuilt() can check if that's there to return true/false. 
	 */
	public boolean navTreeBuilt = false;
	private NjordConcierge concierge;
	
	
	// ###############################################################
	// CONSTRUCTORS
	// ###############################################################
	/**
	 * Version isn't set when we create this. it will be set once we connect.
	 */
	public BigIP(NjordConcierge concierge, String name, String hostNameOrAddress, int iControlPort, String userName, String password, boolean connecAutomatically) {
		this.name = name;
		this.hostNameOrAddress = hostNameOrAddress;
		this.iControlPort = iControlPort;
		this.userName = userName;
		this.password = password;
		this.connectAutomatically = connecAutomatically;
		this.concierge = concierge;
	}
	
	// ###############################################################
	// GETTERS AND SETTERS
	// ###############################################################
	/**
	 * Get's the short name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Get the connection address.
	 * @return
	 */
	public String gethostNameOrAddress() {
		return hostNameOrAddress;
	}
	
	/**
     * Set the version of the BIG-IP.
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }
    
	/**
	 * Returns the port to connect to.
	 * @return
	 */
	public int getiControlPort() {
		return iControlPort;
	}
	
	/**
	 * Returns the user name to use in the connection credentials.
	 * @return
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * Returns the password to use in the connection credentials.
	 * @return
	 */
	public String getPassword() {
		return password;
	}
	
	/**
     * Get the version of the BIG-IP.
     * 
     * @return
     */
    public String getVersion() {
        return version;
    }
	
	/**
	 * Update/change the connection address.
	 * @param hostNameOrAddress
	 */
	public void sethostNameOrAddress(String hostNameOrAddress) {
		
	}
	
	/**
     * Get the auto connect preference.
	 * @return
	 */
    public boolean getConnectAutomatically() {
        return connectAutomatically;
    }
    
	/**
     * Update/change the auto connect preference.
     * @param hostNameOrAddress
     */
    public void setConnectAutomatically(boolean connectAutomatically) {
        this.connectAutomatically = connectAutomatically;
    }
    
    public List<String> getiRulesNames() {
        return iRulesnames;
    }
    
    public void setiRulesNames(List<String> iRules) {
        iRulesnames.addAll(iRules);
    }
    
    // TODO: this should become private.
    public void setInterface(iControl.Interfaces ic) {
        icInterface = ic;
        connectionInitialized = true;
    }
    public boolean getIsConnected() {
        return connectionInitialized;
    }
    
    public iControl.Interfaces getInterface() {
        return icInterface;
    }
    
    public boolean getNavTreeBuilt() {
        return navTreeBuilt;
    }
    
    public void setNavTreeBuilt(Boolean navTreeBuilt) {
        this.navTreeBuilt = navTreeBuilt;
    }
    
    /**
     * Overriding the default equals from object. Return true if our major settings are all the same.
     * Be warned, this means if you create two instances of a big-ip and initialize an iControl connection for one but not the 
     * other they will count as the same. This is for functions like replaceBigIP(BigIP oldBigip).
     * 
     */
    @Override
    public boolean equals(Object object) {
        BigIP bigipObject = (BigIP) object;
        if (name.equals(bigipObject.getName()) && hostNameOrAddress.equals(bigipObject.gethostNameOrAddress()) &&
                (iControlPort == bigipObject.getiControlPort()) && userName.equals(bigipObject.getUserName()) &&
                password.equals(bigipObject.getPassword()) && (connectAutomatically == bigipObject.getConnectAutomatically())               
                ) {
            return true;
        } else {
            return false;
        }
    }
	// ###############################################################
	// METHODS
	// ###############################################################
	
    public iControl.Interfaces getConnection() {
        return icInterface;
    }
    
//    public String hostNameOrAddress;
//    public int iControlPort;
//    public String userName;
//    public String password;
    
    /**
     * Initializes a connection to the BigIP.
     * Runs the initialize method on the library which sets up the connection object. Then do a get version. If it works we've got good connection settings.
     * @return
     */
    public void connect() {
//        lblStatusLabel.setText("Connecting");
//        resultsPanelNoticesBox.setText("");
//        String ipAddress = hostNameOrAddress;
//        long port = iControlPort;
//        String userName = userName;
//        String passWord = password();
        String version = null;
        String bigIPVersion = null;
        icInterface = new iControl.Interfaces(); //From Assembly
        
        icInterface.initialize(hostNameOrAddress, iControlPort, userName, concierge.getSecurityProvider().decryptPassword(password)); // Initialize the interface to the BigIP
        
        try {
            //TODO: There has to be a better way to do this
            version = icInterface.getSystemSystemInfo().get_version();
            Pattern versionPrefix = Pattern.compile("BIG-IP_v");
            Matcher versionMatcher = versionPrefix.matcher(version);
            bigIPVersion = versionMatcher.replaceAll(""); //Kinda redundant, I know.
        } catch (RemoteException e1) {  
            //TODO: Add a return type so I can do a return here.
            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e1);
            exceptionHandler.processException();
        } catch (ServiceException e1) {
            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e1);
            exceptionHandler.processException();
        } catch (Exception e1) {
            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e1);
            exceptionHandler.processException();
        }
        
        boolean success = false;
        //TODO: Move this part, the setting of the progress bar and displaying of connection validity out of this section and maybe back into the listener. We should only be returning true/false here.
        if (version != null) {
            concierge.getOwner().getLoggerHandle().debug("My Big-IP is version:" + bigIPVersion);
            if ( bigIPVersion.startsWith("10") ) {
                concierge.getNotifier().sendNotification("v" + bigIPVersion + ", success", TUNotificationType.NOTIFICATION_TYPE_APPEND);
                connectionInitialized = true;
                setVersion(version);
            } else if (bigIPVersion.startsWith("11")) {
                concierge.getNotifier().sendNotification("v" + bigIPVersion + ", success", TUNotificationType.NOTIFICATION_TYPE_APPEND);
                setVersion(version);
//                bigip.setInterface(ic);
//                resultsPanelNoticesBox.setText("Connected to BIG-IP version: " + bigIPVersion);
                connectionInitialized = true;
//                success = true; //It worked;
//                lblStatusLabel.setText("Connected");
            } else if (bigIPVersion.startsWith("9")) {
                concierge.getNotifier().sendNotification("v" + bigIPVersion + ", unclear", TUNotificationType.NOTIFICATION_TYPE_APPEND);
                setVersion(version);
//                bigip.setInterface(ic);
//                resultsPanelNoticesBox.setText("njord is currently untested on BIG-IP version 9 systems. Proceed at your own risk.");
                connectionInitialized = true;
//                success = true; 
//                lblStatusLabel.setText("Connected");
            } else {
                concierge.getNotifier().sendNotification("v" + bigIPVersion + ", fail", TUNotificationType.NOTIFICATION_TYPE_APPEND);
//                resultsPanelNoticesBox.setText("BIG-IP version: " + bigIPVersion + "Is unsupported. You must connect to a BIG-IP version 9.4 or higher.");
                connectionInitialized = false;
//                success = false;
//                lblStatusLabel.setText("Disconnected");
            }
            //Now populate the provisioned modules list
            
        } else {
            // scream, run and cry
            //TODO: Check the return code of the exception to see what the cause of failure is.
            concierge.getOwner().getLoggerHandle().error("Connection failed. Most likely cause: settings invalid");
            concierge.getNotifier().sendNotification("Connection failed. Most likely cause: settings invalid");
            connectionInitialized = false;
//            return false; //We are failz
        }
    }
    
    
    
}
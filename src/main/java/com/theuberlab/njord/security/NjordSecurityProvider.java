package com.theuberlab.njord.security;

/**
 * <code>NjordSecurityProvider</code>
 * 
 * @author Aaron Forster
 *
 */
public interface NjordSecurityProvider {
    /**
     * This method should take an encrypted password and return a decrypted one.
     * @param encrypedPassword
     * @return
     */
    public String decryptPassword(String encrypedPassword);
    /**
     * This method should take a clear text password and return an encrypted one.
     * @param clearPassword
     * @return
     */
    public String encryptPassword(String clearPassword);
    //TODO: I should have methods for doing this via char[];
    //TODO: I should probably also have some methods around capabilities /etc.
}

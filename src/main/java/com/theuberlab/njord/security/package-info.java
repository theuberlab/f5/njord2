/**
 * The security subsystem.
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.security;
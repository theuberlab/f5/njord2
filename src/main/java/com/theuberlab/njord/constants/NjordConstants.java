package com.theuberlab.njord.constants;

/**
 * <code>NjordConstants</code> General constants used throughout Njord. Of type enum so that we can use them in switches.
 * 
 * @author Aaron Forster
 * 
 */
public enum NjordConstants {
	// Node types are for determining what type of object you're screwing with.
    // DEPRECATED
    // Node types have been moved to NjordNodeTypes
//	NODE_TYPE_IRULE,
//	NODE_TYPE_IAPPS_TEMPLATE,
//	NODE_TYPE_LOCAL_REMOTE, // LOCAL_REMOTE is sort of an odd object which holds root nodes. I should change it to two types BIG_IP and LOCAL once I have the ability to connect to multiple BiGIPs.
//	NODE_TYPE_CATEGORY, // Category will hold things like 'iRules,' 'iApps Templates' and others which will hold groups of objects. 
//	NODE_TYPE_TOP, // The top of the tree which will be invisible
//	NODE_TYPE_REMOTE, // Remote Nodes like Bigips. In fact I can't think of anything else I will support for now.
	
	
	// DEPRECATED.
	// The concepts of local and remote have been moved to NjordNodeContext.
	// Node Categories are Local (Stored on the local machine's hard disk) and remote (Stored on a bigip.)
//	NODE_CATEGORY_LOCAL, // This is stored locally.
//	NODE_CATEGORY_REMOTE, // This is stored remotely.
	
	//This needs to be moved into a constants defined somewhere in the iRule plugin.
	//IRule types
	IRULE_TYPE_LTM, // "Local" type iRules
	IRULE_TYPE_GTM, // "Global" type iRules
	
	// This is definately going to go away. It _should_ show up in a constants portion of an iApps plugin.
	//iApps Template Definition portions
	IAPPS_TEMPLATE_DEFINITION_PRESENTATION_PORTION,
	IAPPS_TEMPLATE_DEFINITION_IMPLEMENTATION_PORTION,
	IAPPS_TEMPLATE_DEFINITION_HTML_HELP_PORTION,
	
	//Not sure what I'm going to do with this.
	// Parser types
	PARSER_TYPE_TASK,
	PARSER_TYPE_COMMAND
	
	// A great description of ENUM with good examples. http://javahowto.blogspot.com/2008/04/java-enum-examples.html
	
	// The very simplest example
//	CONSTONE, CONSTTWO, CONSTTHREE, CONSTFOUR
	
	
	
	
	// Specifying values
//	 WHITE(21), BLACK(22), RED(23), YELLOW(24), BLUE(25);
//	 
//	 private int code;
//	 
//	 private NjordConstants(int c) {
//	   code = c;
//	 }
//	 
//	 public int getCode() {
//	   return code;
//	 }
	

	
		
	// And if I want a toString which only capitalizes the first letter.
//	 @Override public String toString() {
//		   //only capitalize the first letter
//		   String s = super.toString();
//		   return s.substring(0, 1) + s.substring(1).toLowerCase();
//		 }

	
	
	
	
	
	
	// A super fancy example
//    PASSED(1, "Passed", "The test has passed."),
//    FAILED(-1, "Failed", "The test was executed but failed."),
//    DID_NOT_RUN(0, "Did not run", "The test did not start.");
// 
//    private int code;
//    private String label;
//    private String description;
// 
//    /**
//     * A mapping between the integer code and its corresponding Status to facilitate lookup by code.
//     */
//    private static Map<Integer, Status> codeToStatusMapping;
// 
//    private NjordConstants(int code, String label, String description) {
//        this.code = code;
//        this.label = label;
//        this.description = description;
//    }
// 
//    public static NjordConstants getStatus(int i) {
//        if (codeToStatusMapping == null) {
//            initMapping();
//        }
//        return codeToStatusMapping.get(i);
//    }
// 
//    private static void initMapping() {
//        codeToStatusMapping = new HashMap<Integer, NjordConstants>();
//        for (NjordConstants s : values()) {
//            codeToStatusMapping.put(s.code, s);
//        }
//    }
// 
//    public int getCode() {
//        return code;
//    }
// 
//    public String getLabel() {
//        return label;
//    }
// 
//    public String getDescription() {
//        return description;
//    }
// 
//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder();
//        sb.append("Status");
//        sb.append("{code=").append(code);
//        sb.append(", label='").append(label).append('\'');
//        sb.append(", description='").append(description).append('\'');
//        sb.append('}');
//        return sb.toString();
//    }
// 
//    public static void main(String[] args) {
//        System.out.println(NjordConstants.PASSED);
//        System.out.println(NjordConstants.getStatus(-1));
//    }
	
	
	
}

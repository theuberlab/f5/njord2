package com.theuberlab.njord.constants;

/**
 * <code>NjordNodeContext</code>
 * 
 * @author Aaron Forster
 *
 */
public enum NjordNodeContext {
    NODE_CONTEXT_LOCAL, // Stored on your local machine or a filesystem accessable to it via normal filesystem tools so the OS doesn't view it as non-local.
    NODE_CONTEXT_REMOTE; // Stored on a remote machine and needs to be accessed/saved/deleted/etc via some custom mechanism
}

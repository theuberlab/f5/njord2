package com.theuberlab.njord.constants;

/**
 * <code>NjordDefaultPreferences</code> holds the default values used for preferences.
 * 
 * @author Aaron Forster
 *
 */
public enum NjordDefaultPreferences {
	// Directories
	SETTINGS_DIRECTORY("/.theuberlab/njord/"),
	// Files
	GEOMETRY_FILE("/geometry.properties"),
	PREFERENCES_FILE("/preferences.properties");
	
//	WHITE(21), BLACK(22), RED(23), YELLOW(24), BLUE(25);
	
	private String preference;
	
	private NjordDefaultPreferences(String thisPreference) {
		preference = thisPreference;
	}
	
	public String getDefault() {
		return preference;
	}
	
}

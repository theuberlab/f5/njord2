/**
 * Contains Enums and other types of constants. 
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.constants;
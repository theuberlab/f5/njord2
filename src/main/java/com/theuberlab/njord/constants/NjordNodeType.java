package com.theuberlab.njord.constants;

/**
 * <code>NjordNodeTypes</code>
 * 
 * @author Aaron Forster
 *
 */
public enum NjordNodeType {
    // Node types are for determining what type of object you're screwing with.
    NODE_TYPE_EDITABLE, // This node holds (or will hold) an editor. EDITABLE nodes will be things like iRules, Virtual Servers, etc. Things for which an editor can be used for. EDITABLE nodes will all have a method which returns an editor as well as some methods for dirty/etc.
//    NODE_TYPE_LOCAL_REMOTE, // DEPRECATED, replaced by NODE_TYPE_ORIGIN. LOCAL_REMOTE is sort of an odd object which holds root nodes. I should change it to two types BIG_IP and LOCAL once I have the ability to connect to multiple BiGIPs.
    NODE_TYPE_ORIGIN, // Origin/destination/server. Where objects reside. IE A server, or Local. Replaces LOCAL_REMOTE
    NODE_TYPE_CATEGORY, // Category will hold things like 'iRules,' 'iApps Templates' and others which will hold groups of objects. 
    NODE_TYPE_TOP, // The top of the tree which will be invisible. There can only be one NODE_TYPE_TOP in any tree.
    NODE_TYPE_CONTEXT;
//    NODE_TYPE_REMOTE; // DEPRECATED. Local vs remote is now a NODE_CONTEXT and most of what this is used for is now NODE_TYPE_ORIGIN. Remote Nodes like Bigips. In fact I can't think of anything else I will support for now.
}

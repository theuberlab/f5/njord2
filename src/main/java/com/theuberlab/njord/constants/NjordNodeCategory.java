package com.theuberlab.njord.constants;

/**
 * <code>NjordNodeCategories</code> Not sure what I'm going to use this for. Categories are going to be things that plugins support. iRule, iApp, VirtualSerer, etc.
 * Here as a temporary hack until I get plugins working.
 * 
 * @author Aaron Forster
 *
 */
public enum NjordNodeCategory {
    NODE_CATEGORY_LOCAL_IRULE, // LTM iRUles. My first "plugin" functionality could be to produce a local and global iRules editor.
    NODE_CATEGORY_GLOBAL_IRULE,
    NODE_CATEGORY_IAPPS_TEMPLATE;
    
}

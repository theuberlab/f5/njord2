/**
 * The actions package holds models for actions. I.E. GUI objects which define the action name, description and include all it's 
 * ActionPeformed() onMouseEvent() etc, type methods
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.actions;
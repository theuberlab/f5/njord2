package com.theuberlab.njord.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.slf4j.Logger;

import com.theuberlab.common.constants.TUNotificationType;
import com.theuberlab.njord.constants.NjordNodeContext;
import com.theuberlab.njord.main.Njord;
import com.theuberlab.njord.main.NjordConcierge;
import com.theuberlab.njord.model.BigIP;
import com.theuberlab.njord.operations.ConnectToBigIPOpAndFetch;
import com.theuberlab.njord.operations.FetchiRulesOp;

/**
 * Refresh the list of iRules.
 * 
 * @author Aaron Forster
 *
 */
public class FetchiRules extends AbstractAction {
    private static final long serialVersionUID = 1L;
    public static final String name = "Fetch";
    
    //TODO: add an icon here
//    private ImageIcon myIcon;
    private Logger log;
    
    private NjordConcierge concierge;
    
    public FetchiRules(NjordConcierge concierge, ImageIcon icon) {
        super(name, icon);
        log = concierge.getOwner().getLoggerHandle();
        putValue(Action.SHORT_DESCRIPTION, name);
        putValue(Action.NAME, name);
//        putValue(MNEMONIC_KEY, Integer.valueOf('R'));
//        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
        this.concierge = concierge;
    }
    
    // ----------------------------------------------------------------------------
    // ActionListener
    // ----------------------------------------------------------------------------
    
    public void actionPerformed(ActionEvent evt) {
        log.debug("FetchiRules invoked");
        // This is GUI stuff so figuring out what is selected/etc should all happen here in the action. It shouldn't go to the op
        // until it's time to move to a network operation.
        concierge.getNotifier().sendNotification("Attempting to fetch");
        
        if (concierge.getSelectedPathContext().equals(NjordNodeContext.NODE_CONTEXT_REMOTE)) {
            BigIP bigIP = ((Njord) concierge.getOwner()).getSelectedOrigin();
            concierge.getNotifier().sendNotification("Fetching iRules list from BIG-IP [" + bigIP.getName() + "]", TUNotificationType.NOTIFICATION_TYPE_APPEND);
            
            if (!bigIP.getIsConnected()) {
                String confirmConnectMessage = "Connection to BIG-IP [" + bigIP.getName() + "] is not initialized. Initialize connection then fetch iRules?";
                sendNotification(confirmConnectMessage);
                int result = JOptionPane.showConfirmDialog(null, confirmConnectMessage, "Connect then fetch?", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    confirmConnectMessage = "Attempting to connect";
                    sendNotification(confirmConnectMessage);
                    // Now that we're done w/ the GUI stuff it's time to move on to the network stuff.
                    new ConnectToBigIPOpAndFetch(concierge).start();
                } else {
                    sendNotification("Fetch canceled");
                }
            } else {
                new FetchiRulesOp(concierge).start();
            }
        } else {
            JOptionPane.showMessageDialog(null, "No BIG-IP selected. Please select a BIG-IP and try again.", 
                    "No BIG-IP selected", JOptionPane.WARNING_MESSAGE, null);
//            JOptionPane.showConfirmDialog(null, "No BIG-IP selected. Please select a BIG-IP and try again.", 
//                  "No BIG-IP selected", JOptionPane.OK_OPTION);
        }
        
    }

    private void sendNotification(String message) {
        concierge.getNotifier().sendNotification(message, TUNotificationType.NOTIFICATION_TYPE_APPEND);
    }
}
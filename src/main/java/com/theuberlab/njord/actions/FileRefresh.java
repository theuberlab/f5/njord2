// Copyright (c) 2010 Keith D Gregory, all rights reserved
package com.theuberlab.njord.actions;


// FileRefresh from Keith Gregory's s3util application. Here as an example of what an action should likely look like.



//package com.kdgregory.app.s3util.actions;
//
//import java.awt.event.ActionEvent;
//import java.awt.event.KeyEvent;
//
//import javax.swing.AbstractAction;
//import javax.swing.KeyStroke;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//
//import com.kdgregory.app.s3util.main.Concierge;
//import com.kdgregory.app.s3util.s3ops.S3RefreshOp;
//
//
///**
// *  Refreshes the main window.
// */
//public class FileRefresh
//extends AbstractAction
//{
//    private static final long serialVersionUID = 1L;
//
//    private Log _logger = LogFactory.getLog(this.getClass());
//
//    private Concierge _concierge;
//
//
//    public FileRefresh(Concierge concierge)
//    {
//        super("Refresh");
//        putValue(MNEMONIC_KEY, Integer.valueOf('R'));
//        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
//
//        _concierge = concierge;
//    }
//
//
////----------------------------------------------------------------------------
////  ActionListener
////----------------------------------------------------------------------------
//
//    public void actionPerformed(ActionEvent evt)
//    {
//        _logger.info("invoked");
//        new S3RefreshOp(_concierge).start();
//    }
//}

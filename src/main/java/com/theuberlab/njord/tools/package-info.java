/**
 * A package which holds tools. Tools are stand alone applications or scripts which are used to build data files used by Njord.
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.tools;
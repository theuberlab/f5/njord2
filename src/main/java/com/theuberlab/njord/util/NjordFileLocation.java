package com.theuberlab.njord.util;

import iControl.GlobalLBRuleRuleDefinition;
import iControl.LocalLBRuleRuleDefinition;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.RemoteException;

import org.fife.ui.rsyntaxtextarea.FileLocation;
import org.fife.ui.rsyntaxtextarea.TextEditorPane;

import com.theuberlab.common.interfaces.TUNotifiable;
import com.theuberlab.common.notificationsandlogging.TUNotifier;
import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.constants.NjordNodeCategory;
import com.theuberlab.njord.main.NjordConcierge;

/**
 * @author forster
 *
 */
public class NjordFileLocation extends FileLocation {
	//TODO: Fetch this from prefs. Implies I'm passing prefs object or some way to fetch said.
	public String nl = System.getProperty("line.separator");
	public String objectName = null;
	public String objectDefinition = null;
	public iControl.Interfaces ic = null;
	public Object anObject = null; //This is totally hackish
//	public LocalLBRuleRuleDefinition rule = null;
	public boolean exists = false; // Used to determine if the file needs to be created. IE is this a new file we've created or one we have loaded.
	public boolean local = false;
	public NjordConcierge concierge = null;
	public File fileHandle = null;
	public NjordNodeCategory objectCategory = null;
	private TUNotifier notifier;
	//slf4j logger factory
	/**
	 * Holds our logger factory for SLF4J.
	 */
//	final Logger log = LoggerFactory.getLogger(owner.class);
	
	/**
	 * FileLocation for an object on a BIGIP. Currently only supports iRules and iApps templates
	 * 
	 * @param mainWindow A handle for the main window in case we need to send notices/etc.
	 * @param objectName The name of the Object to fetch.
	 * @param ic An initialized iControl object to fetch rules/etc.
	 */
	public NjordFileLocation(NjordConcierge concierge, String objectName, NjordNodeCategory objectCategory, iControl.Interfaces ic) {
	    this.concierge = concierge;
		this.objectName = objectName;
		this.ic = ic;
		this.local = false;
		this.exists = true;
		this.objectCategory = objectCategory;
		this.notifier = concierge.getNotifier();
		getObjectFromBIGIP(objectName, objectCategory);
	}
	
	/**
	 * Giving us a Rule Definition tells us to set the 
	 * local variable which will cause us to create the iRule on the server when we save it instead of saving it.
	 * 
	 * @param mainWindow A handle for the main window in case we need to send notices/etc.
	 * @param objectName The name of the iRule.
	 * @param ic An initialized iControl object to fetch rules/etc.
	 * @param objectDefinition The text of the iRule.
	 */
	public NjordFileLocation(NjordConcierge concierge, String objectName, NjordNodeCategory objectCategory, iControl.Interfaces ic, String objectDefinition) {
	    this.concierge = concierge;
		this.notifier = concierge.getNotifier();
		this.objectName = objectName;
		this.objectCategory = objectCategory;
		this.ic = ic;
		local = false;
		exists = false;
		this.objectDefinition = objectDefinition;
	}

	/**
	 * A constructor for creating local iRules which will be saved on the local file system instead of the BIGIP.
	 * 
	 * @param mainWindow A handle for the main window in case we need to send notices/etc.
	 * @param objectName The name of the iRule.
	 * @param localPath A java IO file.
	 */
	public NjordFileLocation(NjordConcierge concierge, String objectName, File localPath) {
	    this.concierge = concierge;
		this.notifier = concierge.getNotifier();
		this.objectName = objectName;
		local = true;
		fileHandle = localPath;
		if (fileHandle.exists()) {
			this.exists = true;
		}
	}
	
//	/**
//	 * A wrapper for getRuleFromBIGIP(String ruleName, int ruleType) which assumes iRule is of type LTM
//	 * @param ruleName
//	 */
//	public void getRuleFromBIGIP(String ruleName) {
//		getObjectFromBIGIP(ruleName, NjordConstants.IRULE_TYPE_LTM);
//	}
	
	//TODO: Should this take an object category as an argument? Probably, it should take it and pass it to getEditorFor or that should be somewhere else?
	/**
	 * Fetches the actual content from the BIGIP.
	 * 
	 * @param thisObjectName
	 */
	public void getObjectFromBIGIP(String thisObjectName, NjordNodeCategory thisObjectCategory) {
		notifier.sendNotification("Fetching object [" + thisObjectName + "] from the BIG-IP");
		String[] objectNames = { thisObjectName };
		LocalLBRuleRuleDefinition LTMRule = null;
		GlobalLBRuleRuleDefinition GTMRule = null;
		// the iApps Templates methods return strings instead of their own type
		String[][] actions = { { "definition" } };
		// Currently this is called bogus because I won't be fully building this
		String bogusiAppsDefinition = null;
		
		//TODO: All this should be moved into the plugin.
		try {
			switch (thisObjectCategory) {
			    case NODE_CATEGORY_IAPPS_TEMPLATE: {
					// This is a gammy way to get an entire iApps Template. Currently I'm only barely implementing it. The real way we're going to be doing this going forward will be to grab each section separately
					// Looks like I _have_ to get all the components of the template separately. Even the min/max versions
					// In fact even the devcentral powershell code which imports and exports these templates to/from .tmpl files is building them by hand
					String implementationPortion = null;
					String presentationPortion = null;
					String htmlHelpPortion = null;
					
					// The methods used to fetch the data
//					get_action_implementation
//					get_action_presentation
//					get_action_presentation_help
//					
					// Currently the only action is a �definition�
//					This returns a String[][]
					implementationPortion = ic.getManagementApplicationTemplate().get_action_implementation(objectNames, actions)[0][0];   //getLocalLBRule().query_rule(ruleNames)[0];
					presentationPortion = ic.getManagementApplicationTemplate().get_action_presentation(objectNames, actions)[0][0];
					htmlHelpPortion = ic.getManagementApplicationTemplate().get_action_presentation_help(objectNames, actions)[0][0];
					
					bogusiAppsDefinition = "definition {" + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + "    html-help {" + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + htmlHelpPortion + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + "    }" + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + "    implementation {" + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + implementationPortion + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + "    }" + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + "    presentation {" + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + presentationPortion + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + "    }" + nl;
					bogusiAppsDefinition = bogusiAppsDefinition + "}" + nl;
					
//							definition {
//			            html-help {
//			                <!-- insert html help text -->
//			            }
//			            implementation {
//			                # insert tmsh script
//			            }
//			            presentation {
//			                # insert apl script
//			            }
//			        }

							
							
							
//					LTMRule = (LocalLBRuleRuleDefinition)anObject;
//					objectDefinition = LTMRule.getRule_definition();
					
					// These are all of the methods in Management::ApplicationTemplate
//					 create	 Creates a set of application templates. Each template automatically is created with a default empty "definition" action.	 BIG-IP_v11.0.0
//					 delete_all_templates	 Deletes all application templates.	 BIG-IP_v11.0.0
//					 delete_application_template	 Deletes a set of application templates.	 BIG-IP_v11.0.0
//					 get_action_implementation	 Gets the implementation script text for the specified application template actions.	 BIG-IP_v11.0.0
//					 get_action_list	 Gets the names of the actions in the application templates.	 BIG-IP_v11.0.0
//					 get_action_presentation	 Gets the presentation text for the specified application template actions.	 BIG-IP_v11.0.0
//					 get_action_presentation_help	 Gets the HTML help text for the specified application template actions.	 BIG-IP_v11.0.0
//					 get_description	 Gets the descriptions for the specified application templates.	 BIG-IP_v11.0.0
//					 get_list	 Gets the names of all application templates.	 BIG-IP_v11.0.0
//					 get_prerequisite_bigip_version_maximum	 Gets the maximum version of BIG-IP for which this template is valid.	 BIG-IP_v11.2.0
//					 get_prerequisite_bigip_version_minimum	 Gets the minimum version of BIG-IP for which this template is valid.	 BIG-IP_v11.2.0
//					 get_prerequisite_errors	 Gets the error string indicating the prerequisite errors for a template. A blank string indicates no errors. A non-blank error string means that there are prerequisites for the template that are not met on that machine, and creating a system application object from that template on that machine will result in an exception. Note that the contents of the error string are meant to be human readable. While the presence of a non-blank error string can be used programmatically, the contents should only be used for non-programmatic purposes.	 BIG-IP_v11.2.0
//					 get_prerequisite_modules	 Gets the list of modules required by the specified templates.	 BIG-IP_v11.2.0
//					 get_version	 Gets the version information for this interface.	 BIG-IP_v11.0.0
//					 set_action_implementation	 Sets the implementation script text for a set of actions in an application templates. The implementation is a TMSH script that can use the answers to the questions in the presentation, provided as variables, to create the configuration objects that make up the application service. This script is executed whenever the application is created or modified.	 BIG-IP_v11.0.0
//					 set_action_presentation	 Sets the presentation text for a set of actions in application templates. The presentation contains Application Presentation Language (APL) text that describes what input is needed and how it should be displayed in the UI.	 BIG-IP_v11.0.0
//					 set_action_presentation_help	 Sets the HTML help text for a set of actions in application templates.	 BIG-IP_v11.0.0
//					 set_description	 Sets the descriptions for the specified appliation templates.	 BIG-IP_v11.0.0
//					 set_prerequisite_bigip_version_maximum	 Sets the maximum version of BIG-IP for which this template is valid. Attempting to create an application from this template on an incompatible version of BIG-IP will produce an error. The version is of the form #.#.#, #.#, or #. For example, "11", "11.1", and "11.1.2" would all be valid version strings.	 BIG-IP_v11.2.0
//					 set_prerequisite_bigip_version_minimum	 Sets the minimum version of BIG-IP for which this template is valid. Attempting to create an application from this template on an incompatible version of BIG-IP will produce an error. The version is of the form #.#.#, #.#, or #. For example, "11", "11.1", and "11.1.2" would all be valid version strings.	 BIG-IP_v11.2.0
//					 set_prerequisite_modules	 Sets the list of modules required by each template. Attempting to create an application from this template when one or more of these modules are not provisioned will produce an error.	 BIG-IP_v11.2.0
//					
					objectDefinition = bogusiAppsDefinition;
					break;
				}
//				case IAPPS_TEMPLATE_DEFINITION_IMPLEMENTATION_PORTION: {
//					objectDefinition = ic.getManagementApplicationTemplate().get_action_implementation(objectNames, actions)[0][0];
//					break;
//				}
//				case IAPPS_TEMPLATE_DEFINITION_PRESENTATION_PORTION: {
//					objectDefinition = ic.getManagementApplicationTemplate().get_action_presentation(objectNames, actions)[0][0];
//					break;
//				}
//				case IAPPS_TEMPLATE_DEFINITION_HTML_HELP_PORTION: {
//					objectDefinition = ic.getManagementApplicationTemplate().get_action_presentation_help(objectNames, actions)[0][0];
//					break;
//				}
				//TODO: I have to figure out a way to handle this part.
				case NODE_CATEGORY_GLOBAL_IRULE: {
					anObject = ic.getGlobalLBRule().query_rule(objectNames)[0];
					GTMRule = (GlobalLBRuleRuleDefinition)anObject;
					objectDefinition = GTMRule.getRule_definition();  //getRule_definition();
					break;
				}
				case NODE_CATEGORY_LOCAL_IRULE:
				default: {
					anObject = ic.getLocalLBRule().query_rule(objectNames)[0];
					LTMRule = (LocalLBRuleRuleDefinition)anObject;
					objectDefinition = LTMRule.getRule_definition();
				}
				break;
			}
		} catch (RemoteException e) {
			TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
			exceptionHandler.processException();
		} catch (Exception e) {
			TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
			exceptionHandler.processException();
		}
			
	}
	
	/* (non-Javadoc)
	 * @see org.fife.ui.rsyntaxtextarea.FileLocation#getActualLastModified()
	 */
	@Override
	protected long getActualLastModified() {
		return TextEditorPane.LAST_MODIFIED_UNKNOWN;
	}
	
	/* (non-Javadoc)
	 * @see org.fife.ui.rsyntaxtextarea.FileLocation#getFileFullPath()
	 */
	@Override
	public String getFileFullPath() {
		// TODO Auto-generated method stub
		if (local) {
			return fileHandle.getAbsolutePath();
		} else {
			//Do something better here for remote.
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fife.ui.rsyntaxtextarea.FileLocation#getFileName()
	 */
	@Override
	public String getFileName() {
		return objectName;
	}
	
	/* (non-Javadoc)
	 * @see org.fife.ui.rsyntaxtextarea.FileLocation#getInputStream()
	 */
	@Override
	public InputStream getInputStream() throws IOException {
		if (!local) {
			//Here is the only place I am specifying the encoding.
			InputStream is = new ByteArrayInputStream( objectDefinition.getBytes() );
//			InputStream is = new ByteArrayInputStream( ruleDefinition.getBytes("ISO-8859-1") );
			return is;
		} else {
			InputStream is = new BufferedInputStream(new FileInputStream(fileHandle));
			return is;
		}
		
	}
	
	/* (non-Javadoc)
	 * @see org.fife.ui.rsyntaxtextarea.FileLocation#getOutputStream()
	 */
	@Override
	public OutputStream getOutputStream() throws IOException {
		if (!local) {
			OutputStream os = new NjordOutputStream(concierge, objectName, ic, local, exists, objectCategory);
			return os;			
		} else {
			if (!fileHandle.exists()) {
				fileHandle.createNewFile();
			}
			OutputStream os = new FileOutputStream(fileHandle);
			return os;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fife.ui.rsyntaxtextarea.FileLocation#isLocal()
	 */
	@Override
	public boolean isLocal() {
		return local;
	}
	
//	//TODO: I think I can get rid of this.
//	public boolean isLocal(boolean local) {
//		this.local = local;
//		return this.local;
//	}
//	
	/* (non-Javadoc)
	 * @see org.fife.ui.rsyntaxtextarea.FileLocation#isLocalAndExists()
	 */
	@Override
	public boolean isLocalAndExists() {
		if (local && exists) {
			return true;
		} else {
			return false;
		}
	}
		
	@Override
	public boolean isRemote() {
		return !local;
	}
	
//	/**
//	 * A bogus hack until I get slf4j working for this class
//	 * 
//	 * @param message
//	 */
//	private void log_info(String message) {
//		System.out.println(message);
//	}
//
//	public boolean save() {
//		
//		return false;
//	}
}

package com.theuberlab.njord.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.fife.ui.rtextarea.SearchContext;
import org.fife.ui.rtextarea.SearchEngine;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import com.theuberlab.njord.main.Njord;

/**
 * The UI for a find dialog.
 * 
 * @author Aaron Forster.
 */
public class NjordFindPanel extends JPanel implements ActionListener {
    /** Holds the version ID. */
    private static final long serialVersionUID = 2L;
    /** The text field which hold the text to be searched for. */
	private JTextField findField;
	/** The text, if any, to replace the found text with. */
	private JTextField replaceField;
	/** Search using regular expression. I assume this is a Java regex but honestly I'm guessing there. */
	private JCheckBox chckbxRegex;
	/** Search backwards. */
	private JCheckBox chckbxReverse;
	/** Search for exact case match only. */
	private JCheckBox chckbxMatchCase;
	/** Match whole words only. */
	private JCheckBox chckbxMatchWholeWords;
	/** Search within the highlighted text only. */
	private JCheckBox chckbxSearchWithinSelection;
	/** Replace the found text with the texxt from replaceField. */
	protected JButton btnReplace;
	/** Replace the found text with the texxt from replaceField and then find the next match. */
	protected JButton btnReplaceFind;
	/** The main window. */
	private Njord owner;
	/** the find button */
	private JButton btnFind = null;

	/**
	 * Constructs the UI for a find dialog.
	 */
	public NjordFindPanel(Njord owner) {
	    this.owner = owner;
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		setLayout(new FormLayout(new ColumnSpec[] {
		        ColumnSpec.decode("30px"),
		        ColumnSpec.decode("50px:grow"),
		        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
		        ColumnSpec.decode("default:grow"),
		        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
		        ColumnSpec.decode("80px:grow"),
		        ColumnSpec.decode("80px:grow"),
		        FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
		        ColumnSpec.decode("53px"),},
		    new RowSpec[] {
		        RowSpec.decode("25px"),
		        RowSpec.decode("23px"),
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.RELATED_GAP_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,
		        FormFactory.DEFAULT_ROWSPEC,}));
		
		// Create the combobox.
		
		// Create the label.
		JLabel findLabel = new JLabel("Find:");
		add(findLabel, "2, 2, right, center");
		
		findField = new JTextField();
		add(findField, "4, 2, 3, 1, fill, default");
		findField.setColumns(10);
		
		JLabel lblReplaceWith = new JLabel("Replace With:");
		add(lblReplaceWith, "2, 4, right, default");
		
		replaceField = new JTextField();
		add(replaceField, "4, 4, 3, 1, fill, default");
		replaceField.setColumns(10);
		
		JLabel lblOptions = new JLabel("Options:");
		add(lblOptions, "2, 6, right, default");
		
		chckbxReverse = new JCheckBox("Reverse");
		add(chckbxReverse, "4, 8");
		
		chckbxRegex = new JCheckBox("Regex");
		add(chckbxRegex, "6, 8");
		
		chckbxMatchCase = new JCheckBox("Match case");
		add(chckbxMatchCase, "4, 10");
		
		chckbxMatchWholeWords = new JCheckBox("Whole words");
		add(chckbxMatchWholeWords, "6, 10");
		
		chckbxSearchWithinSelection = new JCheckBox("Within Selection");
		add(chckbxSearchWithinSelection, "4, 12");
		
		// The find button.
		btnFind = new JButton("Find");
		add(btnFind, "4, 16");
		btnFind.setActionCommand("Find");
		btnFind.addActionListener(this);
		
		// Replace selected text and then do nothing else.
        // Create the button.
	    btnReplace = new JButton("Replace");
	    add(btnReplace, "4, 18");
	    btnReplace.setActionCommand("Replace");
	    btnReplace.addActionListener(this);
	    btnReplace.setEnabled(false);
      
		// Replace selected text and find next match.
		btnReplaceFind = new JButton("Replace/Find");
		add(btnReplaceFind, "6, 16");
		btnReplaceFind.setActionCommand("ReplaceFind");
		btnReplaceFind.addActionListener(this);
		btnReplaceFind.setEnabled(false);
		
		// Replace selected text and find next match.
		JButton btnReplaceAll = new JButton("Replace All");
        add(btnReplaceAll, "6 18");
        btnReplaceAll.setActionCommand("ReplaceAll");
        btnReplaceAll.addActionListener(this);
        
	}

	   public void actionPerformed(ActionEvent e) {
	       String command = e.getActionCommand();
	       boolean forward = !chckbxReverse.isSelected();
	       owner.getActiveEditorPane();
	       int numChanges = 0;
	       
	       // Create an object defining our search parameters.
	       SearchContext context = new SearchContext();
	       String text = findField.getText();
	       if (text.length() == 0) {
	          return;
	       }
	       
	       context.setSearchFor(text);
	       //TODO: Implement these options.
	       context.setMatchCase(chckbxMatchCase.isSelected());
	       context.setRegularExpression(chckbxRegex.isSelected());
	       context.setSearchForward(forward);
	       context.setWholeWord(chckbxMatchWholeWords.isSelected());
	       context.setSearchSelectionOnly(chckbxSearchWithinSelection.isSelected());
	       
	       // TODO: Figure out how to wrap search. Maybe if I have a lastSearchSuccessfull boolean as well Then if lastSearchSuccessful is true and result is false we set the cursor to the beginning and try again.
	       // TODO: Set put the cursor back in the editor after clicking one of these buttons so we can undo or something. It takes focus away when we click find/etc.
           // TODO: Figure out what SearchEngine needs to be able to do in order to search so I can override those methods in my own editorpanel class.	       
	       boolean result = false;
	       if (command.equals("Find")) {
	           result = SearchEngine.find(owner.getActiveEditorPane().getTextArea(), context);
	           btnReplace.setEnabled(result);
	           btnReplaceFind.setEnabled(result);
	       } else if (command.equals("Replace")) {
	           // Actually it's o.k. if replace text is empty.
	           context.setReplaceWith(replaceField.getText());
	           result = SearchEngine.replace(owner.getActiveEditorPane().getTextArea(), context);
	           btnReplace.setEnabled(false);
	           btnReplaceFind.setEnabled(false);
	       } else if (command.equals("ReplaceFind")) {
	           result = SearchEngine.replace(owner.getActiveEditorPane().getTextArea(), context);
	           result = SearchEngine.find(owner.getActiveEditorPane().getTextArea(), context);
	           btnReplace.setEnabled(result);
	           btnReplaceFind.setEnabled(result);
	       } else if (command.equals("ReplaceAll")) {
	           //TODO: Expand on this so that if we do replaceall the dialog below says "N occurances replaced."
	           numChanges = SearchEngine.replaceAll(owner.getActiveEditorPane().getTextArea(), context);
	           if (numChanges > 0) {
	               result = true;
	           }
	           btnReplace.setEnabled(false);
               btnReplaceFind.setEnabled(false);
           }
	       
	       if (!result) {
	          JOptionPane.showMessageDialog(this, "Text not found");
	          
	       } else if (numChanges > 0) {
	           JOptionPane.showMessageDialog(this, "Success: [" + numChanges + "] matches replaced");
	       }
	    }
	   
	   /**
	    * Does a default search as if you had typed text into the search box and clicked find.
	    * @param text
	    * @return
	    */
	   public void doSearch(String text) {
	       findField.setText(text);
	       ActionEvent ae = new ActionEvent(btnFind, ActionEvent.ACTION_PERFORMED, "Find");
	       actionPerformed(ae);
	       
	   }
}

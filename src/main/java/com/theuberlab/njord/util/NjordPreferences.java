package com.theuberlab.njord.util;

import java.awt.Dimension;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.exceptions.NoSuchBigIPException;
import com.theuberlab.njord.main.Njord;
import com.theuberlab.njord.main.NjordConcierge;
import com.theuberlab.njord.model.BigIP;


/**
 * An instance of this class stores all of Njord's preferences for run-time. 
 * This should help prevent creating a thousand global variables.
 * 
 * @author Aaron Forster
 *
 */
public class NjordPreferences {
	// ###############################################################
	// VARIABLES
	// ###############################################################
    /**
     * Holds our logger factory for SLF4J.
     */
    public final Logger log = LoggerFactory.getLogger(Njord.class);
    /**
     * Stores the path to the users home directory
     */
    public String userHome = System.getProperty("user.home");
    /**
     * Stores the path to the Njord preferences directory. Currently storing in Njord2
     */
    public String njordPrefsDir = userHome + "/.f5/njord/";
    /**
     * The complete path to the file that will hold all our window geometry/tab information
     */
    private final String layoutPrefsFilePath = njordPrefsDir + "/workspace_1_5.dck";
    /**
     * The file name of the preferences file.
     */
    private final String prefsFileName = "njord2.properties";
	//TOOD: change this to njord.properties after njord2 is no longer alpha. IE once I can edit iRules on the bigip, IE once Njord2 has feature parity with Njord1
	
    // ##################################
    // Preference variables
	/**
	 * Save the size and location of windows when you close Njord.
	 */
	private boolean prefSaveWindowGeometry = true;
    /**
     * Show system iRules.
     */
    private boolean prefShwSysiRules = false;
    /**
     * Show experimental items.
     */
    private boolean prefShwExpmntl = false;
    /**
     * Require that the user enter a password to encrypt and decrypt passwords stored on disk. If set to no Njord will use 
     * a built in value.
     */
    private boolean prefReqMasterPassword = false;
    /**
     * The actual pass phrase used to encrypt and decrypt passwords stored on disk.
     */
    private char[] prefMasterPasword = "PROMPT".toCharArray();
//    String prefMasterPasword = "PROMPT";
    
    // View Preferences
    /** Show visible line numbers yes/no. */
    private boolean prefShwLineNumbers = true;
    /** Code folding yes/no. */
    private boolean prefCodeFoldingEnabled = true;
    /** Line wrap yes/no. */
    private boolean prefLineWrapEnabled = false;
    /**  Auto scroll yes/no. */
    private boolean prefAutoScroll = true;
    /** Should autocomplete activate automatically yes/no. If not a keystroke will need to be supplied. */
    private boolean prefAutoActivationEnabled = true;
    /** If autocomplete activates automatically. How long should we wait before completing. 0 means we will try and complete as you are typing. */
    private int prefAutoActivationDelay = 1;
    /**
     * Shows arrows for tabs "--->--->--->".
     */
    private boolean prefPaintTabLines = false;
    /**
     * Shows dots for spaces and such.
     */
    private boolean prefVisibleWhiteSpace = false;
    /**
     * Paints a special, if somewhat odd character at EOL.
     */
    private boolean prefEOLMarkersVisible = false;
    /**
     * Stores all the BIG-IP entries stored in the preferences file.
     */
    List<BigIP> bigips = null;
    /**
     * The size for the toolbar buttons
     * 
     */
    private Dimension prefButtonDimensions = new Dimension(32,32);
    
    /**
     * 
     * Stores a reference to the concierge
     */
    private NjordConcierge concierge;
    
    //TODO: this should change to like an array so I don't have to keep updating loadPreferences and writePreferences;
    // It should loop through the categories.
    // Preferences should be named 
    // prefs.<prefCategory>.<prefName>.<prefDataType>
    // I.E.
    // prefs.view.prefAutoActivationDelay.bool
    // I'm less sure on how to build that type of data into the name of them.
	// ###############################################################
	// CONSTRUCTORS
	// ###############################################################
	
    /**
	 * 
	 */
	public NjordPreferences(NjordConcierge concierge) {
	    this.concierge = concierge;
		// When instantiated, load preferences from prefs file
	    loadPreferences();
	}
	
	// ###############################################################
	// GETTERS AND SETTERS
	// ###############################################################
	/**
	 * Returns the bigip objects for the bigips we are storing. Currently only returns a dummy list.
	 * 
	 * @return
	 */
	public List<BigIP> getBIGIPS() {
		if (bigips.size() == 0) {
		    bigips.add(new BigIP(concierge, "defaultBigIPSettings", "192.168.1.245", 443, "admin", "admin", false));
		}
		
		return bigips;
	}
	
	/**
	 * Should we display experimental items?
	 * 
	 * @return
	 */
	public boolean getDisplayExperimental() {
//		return prefShwExpmntl;
		return false;
	}
	
	/**
	 * NOT IMPLEMENTED Set/Update the list of BIGIPS. 
	 * Normally this list will be populated from the preferences files however this method will be used to modify it's contents.
	 * 
	 * @param bigips
	 */
	public void setBIGIPS(List<BigIP> bigips) {
		
	}
	
	/**
	 * Adds a bigip to the list of bigips.
	 * Automatically triggers writePreferences()
	 * 
	 * @param newBigIP
	 */
	public void addBigIP(BigIP newBigIP) {
	    this.bigips.add(newBigIP);
	    writePreferences();
	}
	
	/**
     * removes a BigIP from the list of bigips.
     * Automatically triggers writePreferences()
     * 
     * @param newBigIP
     */
    public void removeBigIP(BigIP oldBigIP) {
        // This works because I've overridden equals(Object o) on BigIP
        this.bigips.remove(oldBigIP);
        writePreferences();
    }
    
	/**
	 * Replaces a bigip's settings with new ones.
	 * Really just a wrapper for removeBigIP(BigIP), addBigIp(BigIP)
	 * 
	 * @param oldBigIP
	 * @param newBigIP
	 */
	public void replaceBigIP(BigIP oldBigIP, BigIP newBigIP) {
	    removeBigIP(oldBigIP);
	    addBigIP(newBigIP);
	}
	
	// ###########################################
	// PATHS/ETC
	public String getLayoutPrefsFilePath() {
	    return layoutPrefsFilePath;
	}
	
	// ###########################################
	// VIEW OPTIONS
    public boolean getSaveWindowGeometry() {
        return prefSaveWindowGeometry;
    }
    
    public boolean getShwSysiRules() {
        return prefShwSysiRules;
    }
    
    public boolean getShwExpmntl() {
        return prefShwExpmntl;
    }
    
    public boolean getShwLineNumbers() {
        return prefShwLineNumbers;
    }
    
    public boolean getCodeFoldingEnabled() {
        return prefCodeFoldingEnabled;
    }
    
    public boolean getLineWrapEnabled() {
        return prefLineWrapEnabled;
    }
    public boolean getAutoScroll() {
        return prefAutoScroll;
    }
    public boolean getAutoActivationEnabled() {
        return prefAutoActivationEnabled;
    }
    public int getAutoActivationDelay() {
        return prefAutoActivationDelay;
    }
//    public String getTemplatesDirectoryPath() {
//  return prefTemplatesDirectoryPath;
//    }
    public boolean getPaintTabLines() {
        return prefPaintTabLines;
    }
    public boolean getVisibleWhiteSpace() {
        return prefVisibleWhiteSpace;
    }
    public boolean getEOLMarkersVisible() {
        return prefEOLMarkersVisible;
    }
	
    //Toggle methods
    public boolean toggleShwLineNumbers() {
        prefShwLineNumbers = !prefShwLineNumbers;
        writePreferences();
        return prefShwLineNumbers;
    }
    public boolean toggleCodeFoldingEnabled() {
        prefCodeFoldingEnabled = !prefCodeFoldingEnabled;
        writePreferences();
        return prefCodeFoldingEnabled;
    }
    public boolean toggleLineWrapEnabled() {
        prefLineWrapEnabled = !prefLineWrapEnabled;
        writePreferences();
        return prefLineWrapEnabled;
    }
    public boolean toggleAutoScroll() {
        prefAutoScroll = !prefAutoScroll;
        writePreferences();
        return prefAutoScroll;
    }
    public boolean toggleAutoActivationEnabled() {
        prefAutoActivationEnabled = !prefAutoActivationEnabled;
        writePreferences();
        return prefAutoActivationEnabled;
    }
    public boolean togglePaintTabLines() {
        prefPaintTabLines = !prefPaintTabLines;
        writePreferences();
        return prefPaintTabLines;
    }
    public boolean toggleVisibleWhiteSpace() {
        prefVisibleWhiteSpace = !prefVisibleWhiteSpace;
        writePreferences();
        return prefVisibleWhiteSpace;
    }
    public boolean toggleEOLMarkersVisible() {
        prefEOLMarkersVisible = !prefEOLMarkersVisible;
        writePreferences();
        return prefEOLMarkersVisible;
    }
    public boolean toggleSaveWindowGeometry() {
        prefSaveWindowGeometry = !prefSaveWindowGeometry;
        writePreferences();
        return prefSaveWindowGeometry;
    }    
    public BigIP getBigIP(String bigIPName) throws NoSuchBigIPException {
        for (BigIP bigip : bigips) {
            if (bigip.getName().equals(bigIPName)) {
                return bigip;
            }
        }
        // If we've made it to here we have a failure.
        throw new NoSuchBigIPException(bigIPName);
    }
    
    /** Returns the master password used to encrypt and decrypt passwords stored on disk. This value is stored only in memory. */
    public char[] getMasterPassword() {
        return prefMasterPasword;
    }
    
    /** Sets the master password used to encrypt and decrypt passwords stored on disk. This value is stored only in memory. */
    public void setMasterPassword(char[] password) {
        this.prefMasterPasword = password;
    }
    
    public boolean getReqMasterPassword() {
        return prefReqMasterPassword;
    }
    
    public boolean setReqMasterPassword(boolean reqMasterPass) {
        prefReqMasterPassword = reqMasterPass;
        return prefReqMasterPassword;
    }
    
    public boolean toggleReqMasterPassword() {
        prefReqMasterPassword = !prefReqMasterPassword;
        writePreferences();
        return prefReqMasterPassword;
    }
    
    public Dimension getfButtonDimensions() {
        return prefButtonDimensions;
    }
    
	// ###############################################################
	// METHODS
	// ###############################################################
	   /**
     * loadPreferences will handle reading in connections preferences and creating the prefs file if need be. 
     * That is if I use the whole file thing at all instead of changing to java.util.prefs. 
     * There are actually two separate types of preferences Application preferences and connection preferences. This was written
     * when there was only the latter and this method needs to be replaced.
     * 
     * @return Success/Failure.
     */
    private boolean loadPreferences() {
//        userHome = System.getProperty("user.home");
        
        //http://www.particle.kth.se/~lindsey/JavaCourse/Book/Part1/Java/Chapter10/Preferences.html
        //TODO: Replace this whole file thing with java.util.prefs maybe
        //Check and see if the prefs and bigips directory exists and create it if it doesn't
//        String settingsDirPath = userHome + "/.f5/njord/";
        File settingsDirectory = new File(njordPrefsDir);
        
        if ( !settingsDirectory.exists() ) {
            log.info("Settings directory doesn't exist. Creating.");
            // Also creates parent directories if neccesary
            writePreferences();
        }
        
        //TODO: Even though this looks like I was going to load from multiple prefs files I'm not actually doing so here. I'm storing all the 'preferences' in this single file. Then the javadocking framework is storing all the window locations/etc in a separate one.
        //Same for each individual file.
        File connectionSettingsFile = new File(settingsDirectory + "/" + prefsFileName);
        
        if ( !connectionSettingsFile.exists() ) {
            log.info("Settings file doesn't exist");
            writePreferences();
        } else {
            log.debug("Settings file exists");
        }
        
        // Get the preferences from a file
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(connectionSettingsFile));
        } catch (IOException e) {
            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
            exceptionHandler.processException();
        }
        
        // Big IPs
        Set<String> propertyNames = properties.stringPropertyNames();
        
        List<String> BigipNames = new ArrayList<String>();
        
        for (String prop : propertyNames) {
            if (prop.startsWith("bigip.")) {
                String[] propPathElements = prop.split("\\.");
                if (!BigipNames.contains(propPathElements[1])) {
                    BigipNames.add(propPathElements[1]);    
                }
            }
        }
        
        bigips = new ArrayList<BigIP>();
        
        for (String bigip : BigipNames) {
//            String bigipName = properties.getProperty("bigip." + bigip + ".connection.name");
            String bigipHostOrAddress = properties.getProperty("bigip." + bigip + ".connection.host");
            int bigipPort = Integer.parseInt(properties.getProperty("bigip." + bigip + ".connection.port"));
            String bigipUser = properties.getProperty("bigip." + bigip + ".connection.username");
            String bigipPass = properties.getProperty("bigip." + bigip + ".connection.password");
            boolean bigipConnectAuto = Boolean.valueOf(properties.getProperty("bigip." + bigip + ".connection.prefConnectAutomatically"));
            BigIP addBigIP = new BigIP(concierge, bigip, bigipHostOrAddress, bigipPort, bigipUser, bigipPass, bigipConnectAuto);
            bigips.add(addBigIP);
        }
        
        // Connection settings
//      properties.getProperty(key, defaultValue)
//        iIPAddress = properties.getProperty("connection.host", iIPAddress);
//        iPort = Long.valueOf(properties.getProperty("connection.port", String.valueOf(iPort)));
//        iUserName = properties.getProperty("connection.usernameame", iUserName);
//        iPassword = properties.getProperty("connection.password", iPassword);
//        
        // This looks ugly but it's like this for a reason. properties.getProperty(String propertyname, String defaultValue) will return a property name and if it doesn't exist in the properties file it will return defaultValue.
        // Njord Preferences
        // First we'll get all the boolean preferences.
        // Behavior Preferences
//        prefConnectAtStartup = Boolean.valueOf(properties.getProperty("prefs.general.prefConnectAtStartup", String.valueOf(prefConnectAtStartup)));
        prefSaveWindowGeometry = Boolean.valueOf(properties.getProperty("prefs.general.prefSaveWindowGeometry", String.valueOf(prefSaveWindowGeometry)));
        prefShwSysiRules = Boolean.valueOf(properties.getProperty("prefs.general.prefShwSysiRules", String.valueOf(prefShwSysiRules)));
        prefShwExpmntl = Boolean.valueOf(properties.getProperty("prefs.general.prefShwExpmntl", String.valueOf(prefShwExpmntl)));
        
        // View Preferences
        prefShwLineNumbers = Boolean.valueOf(properties.getProperty("prefs.view.prefsShwLineNumbers", String.valueOf(prefShwLineNumbers)));
        prefShwLineNumbers = Boolean.valueOf(properties.getProperty("prefs.view.prefShwLineNumbers", String.valueOf(prefShwLineNumbers)));
        prefCodeFoldingEnabled = Boolean.valueOf(properties.getProperty("prefs.view.prefCodeFoldingEnabled", String.valueOf(prefCodeFoldingEnabled)));
        prefLineWrapEnabled = Boolean.valueOf(properties.getProperty("prefs.view.prefLineWrapEnabled", String.valueOf(prefLineWrapEnabled)));
        prefAutoScroll = Boolean.valueOf(properties.getProperty("prefs.view.prefAutoScroll", String.valueOf(prefAutoScroll)));
        prefAutoActivationEnabled = Boolean.valueOf(properties.getProperty("prefs.view.prefAutoActivationEnabled", String.valueOf(prefAutoActivationEnabled)));
        prefPaintTabLines = Boolean.valueOf(properties.getProperty("prefs.view.prefPaintTabLines", String.valueOf(prefPaintTabLines)));
        prefVisibleWhiteSpace = Boolean.valueOf(properties.getProperty("prefs.view.prefVisibleWhiteSpace", String.valueOf(prefVisibleWhiteSpace)));
        prefEOLMarkersVisible = Boolean.valueOf(properties.getProperty("prefs.view.prefEOLMarkersVisible", String.valueOf(prefEOLMarkersVisible)));
        
        prefReqMasterPassword = Boolean.valueOf(properties.getProperty("prefs.view.prefReqMasterPassword", String.valueOf(prefReqMasterPassword)));
        
        
        
        // Now let's get any preferences that are not boolean.
        prefAutoActivationDelay = Integer.parseInt(properties.getProperty("prefs.view.prefAutoActivationDelay", String.valueOf(prefAutoActivationDelay)));
//      prefTemplatesDirectoryPath = properties.getProperty("prefs.view.prefTemplatesDirectoryPath", String.valueOf(prefTemplatesDirectoryPath)));
        prefButtonDimensions.width = Integer.parseInt(properties.getProperty("prefs.view.prefButtonDimensions.width", String.valueOf(prefButtonDimensions.width)));
        prefButtonDimensions.height = Integer.parseInt(properties.getProperty("prefs.view.prefButtonDimensions.height", String.valueOf(prefButtonDimensions.height)));
        
        
//        prefReqMasterPassword
//        prefButtonDimensions
        
        return true;
    }
    
    /**
     * Writes preferences to disk after you have edited them.
     * 
     * @return Success/Failure status.
     */
    public boolean writePreferences() {
        String settingsDirPath = userHome + "/.f5/njord/";
        File settingsDirectory = new File(settingsDirPath);
        boolean success = false;
        if ( !settingsDirectory.exists() ) {
        success = (new File(settingsDirPath)).mkdirs();
          if (success) {
          log.info("Directories: " 
           + settingsDirPath + " created");
          }
        }
        
        File propsFile = new File(settingsDirectory + "/" + prefsFileName);
        success = false;
        try {
            // Create the prefs file if it doesn't already exist.
            success = propsFile.createNewFile();
            if (success) {
                log.info( propsFile + " created");
            }
            FileWriter fstream = new FileWriter(propsFile);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write("# Njord the java iRule editor settings");
            out.newLine();
            out.write("# Preferences");
            out.newLine();
            out.write("# Behavior Preferences");
            out.newLine();
            out.write("prefs.general.prefSaveWindowGeometry = " + prefSaveWindowGeometry);
            out.newLine();
            out.write("prefs.general.prefSwSysiRules = " + prefShwSysiRules);
            out.newLine();
            out.write("prefs.general.prefShwExpmntl = " + prefShwExpmntl);
            out.newLine();
            out.write("prefs.view.prefReqMasterPassword = " + prefReqMasterPassword);
            out.newLine();
            out.write("# View Preferences");
            out.newLine();
            out.write("prefs.view.prefShwLineNumbers = " + prefShwLineNumbers);
            out.newLine();
            out.write("prefs.view.prefCodeFoldingEnabled = " + prefCodeFoldingEnabled);
            out.newLine();
            out.write("prefs.view.prefLineWrapEnabled = " + prefLineWrapEnabled);
            out.newLine();
            out.write("prefs.view.prefAutoScroll = " + prefAutoScroll);
            out.newLine();
            out.write("prefs.view.prefAutoActivationEnabled = " + prefAutoActivationEnabled);
            out.newLine();
            out.write("prefs.view.prefAutoActivationDelay = " + prefAutoActivationDelay);
            out.newLine();
//            out.write("prefs.view.prefTemplatesDirectoryPath = " + prefTemplatesDirectoryPath);
//            out.newLine();
            out.write("prefs.view.prefPaintTabLines = " + prefPaintTabLines);
            out.newLine();
            out.write("prefs.view.prefVisibleWhiteSpace = " + prefVisibleWhiteSpace);
            out.newLine();
            out.write("prefs.view.prefEOLMarkersVisible = " + prefEOLMarkersVisible);
            out.write("prefs.view.prefButtonDimensions.width = " + prefButtonDimensions.width);
            out.newLine();
            out.write("prefs.view.prefButtonDimensions.height = " + prefButtonDimensions.height);
            out.newLine();
            out.newLine();                        
                        
            // Loop through each bigip and write them all
            out.write("# Connection settings");
            out.newLine();
            for (BigIP bigip : bigips) {
                out.write("# BIG-IP " + bigip.getName());
                out.newLine();
                out.write("bigip." + bigip.getName() + ".connection.host = " + bigip.gethostNameOrAddress()); //TODO: replace this with our default settings
                out.newLine();
                out.write("bigip." + bigip.getName() + ".connection.port = " + bigip.getiControlPort());
                out.newLine();
                out.write("bigip." + bigip.getName() + ".connection.username = " + bigip.getUserName());
                out.newLine();
                out.write("bigip." + bigip.getName() + ".connection.password = " + bigip.getPassword());
                out.newLine();
                out.write("bigip." + bigip.getName() + ".connection.prefConnectAutomatically = " + bigip.getConnectAutomatically());
                out.newLine();
                out.write("bigip." + bigip.getName() + ".prefVersion = " + bigip.getVersion());
                out.newLine();
                out.newLine();
            }
            
            //Close the output stream
            out.close();
        } catch (IOException e) {
            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
            exceptionHandler.processException();
        }
        return true;
    }
    
    
    
}

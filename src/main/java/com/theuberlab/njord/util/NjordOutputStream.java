/**
 * 
 */
package com.theuberlab.njord.util;

import iControl.GlobalLBRuleRuleDefinition;
import iControl.LocalLBRuleRuleDefinition;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.theuberlab.common.interfaces.TUNotifiable;
import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.constants.NjordNodeCategory;
import com.theuberlab.njord.main.NjordConcierge;

//TODO: Don't try and make this global. This should only be used by the plugins.
/**
 * This is the outputstream for iRules right now. It should become two output streams one for LOCAL IRULE and one for GLOBAL IRULE.
 * 
 * @author forster
 *
 */
public class NjordOutputStream extends OutputStream  {
	public iControl.Interfaces ic = null;
	public String iRuleName = "";
	public boolean local = false; // True if the iRule does not exist on the server
	public boolean exists = false;
	public StringBuilder buffer;
	public ByteArrayOutputStream baos = new ByteArrayOutputStream();
	public NjordConcierge concierge = null;
	public boolean isInError = false; // This is a stupid hack to try and figure out why TextEditorPane isn't noticing that we're throwing an IOException.
	final Logger log = LoggerFactory.getLogger(NjordOutputStream.class);
	public NjordNodeCategory objectCategory;
	
	public NjordOutputStream(String iRuleName, iControl.Interfaces ic) {
		this.iRuleName = iRuleName;
		this.ic = ic;
	}
	
	public NjordOutputStream(NjordConcierge concierge, String iRuleName, iControl.Interfaces ic, boolean local, boolean exists, NjordNodeCategory category) {
		this.concierge = concierge;
		this.iRuleName = iRuleName;
		this.ic = ic;
		this.local = local;
		this.exists = exists;
		this.objectCategory = category;
	}

	/** 
	 * Takes an input stream of bytes which we will write to the BIGIP
	 */
	@Override
	public void write(int output) {
		baos.write(output);
	}
	
	public void flush() throws IOException {
		write(baos.toString());
		//This did not solve it.
//		write(baos.toString("ISO-8859-1")); //Western (ISO-8859-1)
	}

	public void write(String ruleDefinition) throws IOException{
		switch (objectCategory) {
		case NODE_CATEGORY_GLOBAL_IRULE: {
			GlobalLBRuleRuleDefinition[] saveRules = new GlobalLBRuleRuleDefinition[1]; // Create a list of iRules in order to write them back. We only have one so we only need a tiny list
			saveRules[0] = new GlobalLBRuleRuleDefinition();
			saveRules[0].setRule_definition(ruleDefinition);
			saveRules[0].setRule_name(iRuleName);

			try {
				//          if (nodeInfo.isNew()) {
				if (!exists) {
					//Rule doesn't exist on the server so create it instead of modifying it.
					ic.getGlobalLBRule().create(saveRules);
					this.exists = true;
				} else {
					ic.getGlobalLBRule().modify_rule(saveRules);
				}
			} catch (RemoteException e1) {
				F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e1, concierge);
				exceptionHandler.processException();
				throw new java.io.IOException(e1.getMessage());
			} catch (Exception e1) {
				F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e1, concierge);
				exceptionHandler.processException();
				throw new java.io.IOException(e1.getMessage());
			}
			break;
		}
		case NODE_CATEGORY_LOCAL_IRULE:
		default: {
			LocalLBRuleRuleDefinition[] saveRules = new LocalLBRuleRuleDefinition[1]; // Create a list of iRules in order to write them back. We only have one so we only need a tiny list
			saveRules[0] = new LocalLBRuleRuleDefinition();
			saveRules[0].setRule_definition(ruleDefinition);
			saveRules[0].setRule_name(iRuleName);

			try {
				//          if (nodeInfo.isNew()) {
				if (!exists) {
					//Rule doesn't exist on the server so create it instead of modifying it.
					ic.getLocalLBRule().create(saveRules);
					exists = true;
				} else {
					ic.getLocalLBRule().modify_rule(saveRules);
				}
			} catch (RemoteException e1) {
				F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e1, concierge);
				exceptionHandler.processException();
				throw new java.io.IOException(e1.getMessage());
			} catch (Exception e1) {
				F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e1, concierge);
				exceptionHandler.processException();
				throw new java.io.IOException(e1.getMessage());
			}
			break;
		}
		}

	}

}

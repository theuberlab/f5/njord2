/**
 * 
 */
package com.theuberlab.njord.util;

import org.fife.ui.autocomplete.DefaultCompletionProvider;

/**
 * @author forster
 *
 */
public class NjordCompletionProvider extends DefaultCompletionProvider {
	
//	/**
//	 * 
//	 */
//	public NjordCompletionProvider() {
//		super();
//		setAutoActivationRules(true, ":_");
//	}

//	/**
//	 * @param arg0
//	 */
//	public NjordCompletionProvider(String[] arg0) {
//		super(arg0);
//		setAutoActivationRules(true, ":_");
//	}
	
	/**
	 * Extended to add :: and _ as valid characters for the completion provider so they complete our words.
	 * @see org.fife.ui.autocomplete.DefaultCompletionProvider#isValidChar(char)
	 */
	protected boolean isValidChar(char ch) {
		   return ch==':' || ch=='_' || super.isValidChar(ch);
		}
}

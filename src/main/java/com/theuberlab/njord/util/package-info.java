/**
 * A package which holds utility classes. Utility classes include tools which may be used by multiple other classes.
 * 
 * @author Aaron Forster
 *  
 *
 */
package com.theuberlab.njord.util;
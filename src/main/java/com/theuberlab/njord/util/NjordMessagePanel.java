package com.theuberlab.njord.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 * A text panel with an optional image.
 * 
 * @author Aaron Forster (originally from class Book by Heidi Rakels.)
 */
public class NjordMessagePanel extends JPanel {
	// Static fields.
	private static final String nl = System.getProperty("line.separator");
	
	// Constructors.

	/**
	 * Constructs a message panel with an image.
	 * 
	 * @param 	title		The title of the message.
	 * @param 	text		The text of the message.
	 * @param 	picture		A picture for the message.
	 */
	public NjordMessagePanel(String title, String text, Icon picture) {
		super(new BorderLayout());
		setBackground(Color.white);
        JTextPane textPane = createTextPane(title, text, picture);
        JScrollPane paneScrollPane = new JScrollPane(textPane);
        paneScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        paneScrollPane.setPreferredSize(new Dimension(500, 350));
//        paneScrollPane.setPreferredSize(new Dimension(1200, 350));
        paneScrollPane.setMinimumSize(new Dimension(10, 10));
        textPane.setCaretPosition(0);
        this.add(paneScrollPane, BorderLayout.CENTER);
        
        
        
//        // The panel.
//        setMinimumSize(new Dimension(80,80));
//        setPreferredSize(new Dimension(1200,150));
//        setBackground(Color.white);
//        setBorder(BorderFactory.createLineBorder(Color.lightGray));
	}

	/**
	 * Creates the text pane for the message.
	 * 
	 * @param 	title		The title of the message.
	 * @param 	text		The text of the message.
	 * @param 	picture		A picture for the message.
	 * @return				The text pane for the message.
	 */
	private JTextPane createTextPane(String title, String text, Icon picture) {
        String[] initString ={ title + nl + nl, " ",  nl + nl + text};
        String[] initStyles = {"large", "icon", "regular"};
        JTextPane textPane = new JTextPane();
        StyledDocument styledDocument = textPane.getStyledDocument();
        addStylesToDocument(styledDocument, picture);
        
        try {
            for (int i=0; i < initString.length; i++) {
                styledDocument.insertString(styledDocument.getLength(), initString[i],
                                 styledDocument.getStyle(initStyles[i]));
            }
        } catch (BadLocationException ble) {
            System.err.println("Couldn't insert initial text into text pane.");
        }
        return textPane;
    }
	
	/**
	 * Adds the styles to a styled document.
	 * 
	 * @param doc		The styled document.
	 * @param picture	A picture used in the document.
	 */
	protected void addStylesToDocument(StyledDocument doc, Icon picture) {
        Style def = StyleContext.getDefaultStyleContext().
                        getStyle(StyleContext.DEFAULT_STYLE);
        
        Style regular = doc.addStyle("regular", def);
        StyleConstants.setFontFamily(def, "SansSerif");
        
        Style style = doc.addStyle("italic", regular);
        StyleConstants.setItalic(style, true);
        
        style = doc.addStyle("bold", regular);
        StyleConstants.setBold(style, true);
        
        style = doc.addStyle("small", regular);
        StyleConstants.setFontSize(style, 10);
        
        style = doc.addStyle("large", regular);
        StyleConstants.setFontSize(style, 16);
        StyleConstants.setBold(style, true);
        
        style = doc.addStyle("icon", regular);
        StyleConstants.setAlignment(style, StyleConstants.ALIGN_CENTER);
        
        if (picture != null) {
            StyleConstants.setIcon(style, picture);
        }
    }
}

package com.theuberlab.njord.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenMap;
import org.fife.ui.rsyntaxtextarea.modes.TclTokenMaker;

import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.main.Njord;
import com.theuberlab.njord.main.NjordConcierge;
//* This parser knows nothing about language semantics; it uses
//* <code>RSyntaxTextArea</code>'s syntax highlighting tokens to identify
//* curly braces.  By default, it looks for single-char tokens of type
//* {@link Token#SEPARATOR}, with lexemes '<code>{</code>' or '<code>}</code>'.
//* If your {@link TokenMaker} uses a different token type for curly braces, you
//* should override the {@link #isLeftCurly(Token)} and
//* {@link #isRightCurly(Token)} methods with your own definitions.  In theory,
//* you could extend this fold parser to parse languages that use completely
//* different tokens than curly braces to denote foldable regions by overriding
//* those two methods.<p>
//Here's the thread that I used to figure this stuff out http://fifesoft.com/forum/viewtopic.php?f=10&t=268&p=1749#p1749
public class iRulesTokenMaker extends TclTokenMaker {
    NjordConcierge concierge;
//	org.fife.ui.rsyntaxtextarea.TokenMap
	TokenMap myWordsToHighlight = new TokenMap();
	
	String operatorsPath;
	String statementsPath; // drop, pool and more
	String functionsPath; // findstr, class and others
	String commandsPath; // HTTP::return etc etc
	String tclCommandsPath; // Built in tcl commands
	
	int operatorsClassification = Token.OPERATOR;
	int statementsClassification = Token.RESERVED_WORD_2;
	int functionsClassification = Token.FUNCTION;
	int commandsClassification = Token.RESERVED_WORD_2;
	
	public iRulesTokenMaker(NjordConcierge concierge) {
	    this.concierge = concierge;
	   
	    operatorsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_OPERATORS");
	    statementsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_STATEMENTS"); // drop, pool and more
	    functionsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_FUNCTIONS"); // findstr, class and others
	    commandsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_COMMANDS"); // HTTP::return etc etc
	    tclCommandsPath = concierge.getContentProvider().getCMContent("LANG_IRULES_FILEPATH_TCL"); // Built in tcl commands
	    
		initializeTokens(); // Now for said files let's build our various lists of tokens.

		// Then let's include a few hand coded others which are used primarily for testing and developing the syntax highlighting scheme.
		//TODO: I need to decide what to set my words to. I like the idea of setting the irules commands to FUNCTION and then just making function not be FECKING UGLY like it is in the default. Maybe I'll set it to purple.
		myWordsToHighlight.put("ANNOTATION", Token.ANNOTATION);
		myWordsToHighlight.put("VARIABLE", Token.VARIABLE);
		myWordsToHighlight.put("OPERATOR", Token.OPERATOR);
		myWordsToHighlight.put("IDENTIFIER", Token.IDENTIFIER);
		myWordsToHighlight.put("FUNCTION", Token.FUNCTION);
		myWordsToHighlight.put("REGEX", Token.REGEX);
		myWordsToHighlight.put("COMMENT_KEYWORD", Token.COMMENT_KEYWORD);
		myWordsToHighlight.put("COMMENT_MARKUP", Token.COMMENT_MARKUP);
		myWordsToHighlight.put("COMMENT_MULTILINE", Token.COMMENT_MULTILINE);
		myWordsToHighlight.put("COMMENT_EOL", Token.COMMENT_EOL);
		myWordsToHighlight.put("RESERVED_WORD", Token.RESERVED_WORD);
		myWordsToHighlight.put("RESERVED_WORD_2", Token.RESERVED_WORD_2);
		myWordsToHighlight.put("VARIABLE", Token.VARIABLE);
		
//		myWordsToHighlight.put("thisword", Token.IDENTIFIER);
//		myWordsToHighlight.put("thatword", Token.FUNCTION);
//		myWordsToHighlight.put("oneword", Token.RESERVED_WORD);
//		myWordsToHighlight.put("twoword", Token.RESERVED_WORD_2);
//		myWordsToHighlight.put("won_derline", Token.RESERVED_WORD);
//		myWordsToHighlight.put("two_derline", Token.RESERVED_WORD_2);
	}
	
	public void addToken(char[] array, int start, int end, int tokenType, int startOffset) {
		   if (tokenType==Token.IDENTIFIER) { // I might also want to use FUNCTION. I'll have to decide
		      int value = myWordsToHighlight.get(array, start, end); 
		      if (value != -1) {
		         tokenType = value;
		      }
		   } else if (tokenType==Token.FUNCTION) { 
			   int value = myWordsToHighlight.get(array, start, end);
			   if (value != -1) {
				   tokenType = value;
			   }
		   }
		   super.addToken(array, start,end, tokenType, startOffset);
		}
	
	private void initializeTokens() {
		initializeOperators(); // contains, matches_glob, etc
		initializeStatements(); // drop, pool and more
		initializeFunctions(); // findstr, class and others
		initializeCommands(); // HTTP::return etc etc
		// We're not doing Events
	}
	
	private void initializeOperators() {
//		File functionsFile = new File(operatorsURL.getPath());
//		System.out.println("Looking for " + operatorsURL.getPath());
//		if ( !functionsFile.exists() ) {
//			System.out.println("Operators file doesn't exist!");
//			return;
//		} else {
////			System.out.println("Functions file exists");
//		}
		BufferedReader reader = null;
		try {
//			InputStream stream = MainGuiWindow.class.getResourceAsStream(operatorsPath);
//			InputStreamReader fileReader = new InputStreamReader(stream);
//			BufferedReader reader = new BufferedReader(fileReader);  
       
            reader = new BufferedReader(new InputStreamReader(Njord.class.getResourceAsStream(operatorsPath)));
		    String str;

		    while ((str = reader.readLine()) != null) {
		        String[] words = str.split(","); // split on commas
		        for (String word : words) {
		        	myWordsToHighlight.put(word, operatorsClassification);
		        }
		        
		    }
		} catch (IOException e) {
		} finally {
			try {
				if (reader !=null) { 
					reader.close();  
				}
			} catch (IOException e) {
			    TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
	            exceptionHandler.processException();
			}
		}
		
	}
	
	private void initializeStatements() {
//		if ( !statementsFile.exists() ) {
//			System.out.println("Statements file doesn't exist!");
//			return;
//		} else {
//			System.out.println("Settings file exists");
//		}
		BufferedReader reader = null;
		try {
//			InputStream stream = MainGuiWindow.class.getResourceAsStream(statementsPath);
//			InputStreamReader fileReader = new InputStreamReader(stream);
//			reader = new BufferedReader(fileReader);  
            
            reader = new BufferedReader(new InputStreamReader(Njord.class.getResourceAsStream(statementsPath)));
		    String str;
		    while ((str = reader.readLine()) != null) {
		        String[] words = str.split(","); // split on commas
		        for (String word : words) {
		        	myWordsToHighlight.put(word, statementsClassification);
		        }
		        
		    }
		} catch (IOException e) {
		} finally {
			try {
				if (reader !=null) { 
					reader.close();  
				}
			} catch (IOException e) {
			    TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
	            exceptionHandler.processException();
			}
		}
	}
	
	private void initializeFunctions() {
//		File functionsFile = new File(functionsFilePath);

//		if ( !functionsFile.exists() ) {
//			System.out.println("Functions file doesn't exist!");
//			return;
//		} else {
////			System.out.println("Functions file exists");
//		}
		BufferedReader reader = null;
		try {
//			InputStream stream = MainGuiWindow.class.getResourceAsStream(functionsPath);
//			InputStreamReader fileReader = new InputStreamReader(stream);
//			reader = new BufferedReader(fileReader);
			
			reader = new BufferedReader(new InputStreamReader(Njord.class.getResourceAsStream(functionsPath)));
		    String str;
		    while ((str = reader.readLine()) != null) {
		        String[] words = str.split(","); // split on commas
		        for (String word : words) {
		        	myWordsToHighlight.put(word, functionsClassification);
		        }
		        
		    }
		} catch (IOException e) {
		} finally {
			try {
				if (reader !=null) { 
					reader.close();  
				}
			} catch (IOException e) {
			    TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
	            exceptionHandler.processException();
			}
		}
	}
	
	private void initializeCommands() {
		BufferedReader reader = null;
		try {
//			InputStream stream = MainGuiWindow.class.getResourceAsStream(commandsPath);
//			InputStreamReader fileReader = new InputStreamReader(stream);
//			reader = new BufferedReader(fileReader); 
			
			reader = new BufferedReader(new InputStreamReader(Njord.class.getResourceAsStream(commandsPath)));
		    String str;
		    while ((str = reader.readLine()) != null) {
		        String[] words = str.split(","); // split on commas
		        for (String word : words) {
		        	myWordsToHighlight.put(word, commandsClassification);
		        }
		    }
		} catch (IOException e) {
		} finally {
			try {
				if (reader !=null) { 
					reader.close();  
				}
			} catch (IOException e) {
			    TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
	            exceptionHandler.processException();
			}
		}
	}
	
	//Add these

	public void addHighlightedIdentifier(String word, int tokenType) {
	    myWordsToHighlight.put(word, tokenType);
	}
	
//	public boolean removeHighlightedIdentifier(String word) {
//	    myWordsToHighlight.
//	}
//  public void clearAddedHighlightedIdentifiers();
	
	
	//  public void addHighlightedIdentifier(String word, int tokenType);
//  public boolean removeHighlightedIdentifier(String word);
//  public void clearAddedHighlightedIdentifiers();
  
	
	
}

package com.theuberlab.njord.util;

import java.awt.AWTEvent;
import java.awt.Dimension;
import java.awt.event.AWTEventListener;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.swing.JFrame;

import org.slf4j.Logger;

import com.theuberlab.common.interfaces.TUNotifiable;
import com.theuberlab.common.notificationsandlogging.TUNotifier;
import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.constants.NjordDefaultPreferences;
import com.theuberlab.njord.main.NjordConcierge;

public class NjordWindowSaver implements AWTEventListener {
	/**
	 * Holds the WindowSaver object
	 */
	public static NjordWindowSaver saver;
	/**
	 * used to store a map of any/all of the windows that have been opened and/or closed.
	 */
	public Map framemap;
	//slf4j logger factory
	/**
	 * Holds our logger factory for SLF4J. This doesn't work here either.
	 */
	public TUNotifier notifier;
	/** What to prefix log messages with */
//	public String logPrefix = "NjordWindowSaver: ";
	public String logPrefix = this.getClass().getName() + ": ";
	/**
	 * The default constructor.
	 */
	private NjordWindowSaver(NjordConcierge concierge) {
		framemap = new HashMap( );
		notifier = concierge.getNotifier();
	}

	/**
	 * An interface that lets us listen for window change events. This one catches a window open event which is the
	 * perfect time to tell the GUI what it's geometry should be.
	 * 
	 * @see java.awt.event.AWTEventListener#eventDispatched(java.awt.AWTEvent)
	 */
	@Override
	public void eventDispatched(AWTEvent evt) {
		try {
			if(evt.getID( ) == WindowEvent.WINDOW_OPENED) {
				ComponentEvent cev = (ComponentEvent)evt;
				if(cev.getComponent( ) instanceof JFrame) {
					JFrame frame = (JFrame)cev.getComponent( );
					loadSettings(frame);
				}
			}
		} catch(Exception ex) {
			notifier.sendNotification("Error: " + ex.toString( ));
		}
	}

	/**
	 * Loads geometry settings from the properties file for the specified frame.
	 * 
	 * @param frame
	 * @throws IOException
	 */
	public void loadSettings(JFrame frame) throws IOException {
	    notifier.sendNotification("Running loadSettings");
		String userHome = System.getProperty("user.home");
		
		//http://www.particle.kth.se/~lindsey/JavaCourse/Book/Part1/Java/Chapter10/Preferences.html
		//TODO: Replace this whole file thing with java.util.prefs maybe?
		//Check and see if the prefs and bigips directory exists and create it if it doesn't
		String settingsDirPath = userHome + NjordDefaultPreferences.SETTINGS_DIRECTORY.getDefault();
		File settingsDirectory = new File(settingsDirPath);
		if ( !settingsDirectory.exists() ) {
		    notifier.sendNotification("Preferences directory doesn't exist!");
			// Create multiple directories
		}
		
		File geometryFile = new File(settingsDirectory + NjordDefaultPreferences.GEOMETRY_FILE.getDefault());

		if ( !geometryFile.exists() ) {
			notifier.sendNotification("Geometry file doesn't exist");
		} else {
			notifier.sendNotification("Geometry file exists");
			// Get the properties from a file
			Properties settings = new Properties( );
			settings.load(new FileInputStream(geometryFile));
			String name = frame.getName( );
			int x = getInt(settings,name+".x",100);
			int y = getInt(settings,name+".y",100);
			int w = getInt(settings,name+".w",500);
			int h = getInt(settings,name+".h",500);
//			int extendedState = getInt(settings,name+".extendedState",JFrame.MAXIMIZED_BOTH);
			int extendedState = getInt(settings,name+".extendedState",0);
			frame.setLocation(x,y);
			frame.setSize(new Dimension(w,h));
			frame.setExtendedState(extendedState);
			saver.framemap.put(name,frame);
			frame.validate( );
		}
	}

	/**
	 * Writes window geometry to the properties file.
	 * @throws IOException
	 */
	public void saveSettings( ) throws IOException {
		String userHome = System.getProperty("user.home");
		
		String settingsDirPath = userHome + NjordDefaultPreferences.SETTINGS_DIRECTORY.getDefault();
		File settingsDirectory = new File(settingsDirPath);
		boolean success = false;
		if ( !settingsDirectory.exists() ) {
		success = (new File(settingsDirPath)).mkdirs();
		  if (success) {
			  notifier.sendNotification("Directories: " + settingsDirPath + " created");
		  }
		}
		
		File geometryFile = new File(settingsDirectory + NjordDefaultPreferences.GEOMETRY_FILE.getDefault());
		
		if ( !geometryFile.exists() ) {
			notifier.sendNotification("Geometry file doesn't exist");
			try {
				success = geometryFile.createNewFile();
			    if (success) {
			    	notifier.sendNotification( geometryFile + " created");
				}
			} catch (IOException e) {
				TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
				exceptionHandler.processException();
			}
		} else {
			notifier.sendNotification("Gometry file exists");
		}
		Properties settings = new Properties( );
		settings.load(new FileInputStream(geometryFile));
		
		Iterator it = saver.framemap.keySet( ).iterator( );
		while(it.hasNext( )) {    
			String name = (String)it.next( ); 
			JFrame frame = (JFrame)saver.framemap.get(name);
			// If the frame is maximized let's save only that. If not let's save the actual dimensions.
			if (frame.getExtendedState( ) == JFrame.MAXIMIZED_BOTH ) {
				settings.setProperty(name+".extendedState",""+frame.getExtendedState( ));
			} else {
				settings.setProperty(name+".x",""+frame.getX( ));    
				settings.setProperty(name+".y",""+frame.getY( ));    
				settings.setProperty(name+".w",""+frame.getWidth( ));    
				settings.setProperty(name+".h",""+frame.getHeight( ));	
				settings.setProperty(name+".extendedState",""+frame.getExtendedState( ));
			}
		} 
		settings.store(new FileOutputStream(geometryFile),null); 
	}
	
//	public static void putGeometry(String name, JFrame frame, )
	/**
	 * Settings are all saved as string. unfortunately we're dealing with settings that all need to be represented as ints.
	 * This method retrieves the string of the int from our preferences map and returns it as an it. If not found it returns the 
	 * optionally supplied int provided.
	 * 
	 * @param props An instance of preferences.
	 * @param name The name of the property to retrieve from the preferences object.
	 * @param value The default value to return if the property isn't found.
	 * @return
	 */
	public int getInt(Properties props, String name, int value) { 
		String v = props.getProperty(name); 
		if(v == null) {
			return value;
		}
		return Integer.parseInt(v);

	}
	
	/**
	 * not sure what this is for. Perhaps I call this class statically?
	 * 
	 * @param owner
	 * @return
	 */
	public static NjordWindowSaver getInstance(NjordConcierge concierge) {
		if(saver == null) {
			saver = new NjordWindowSaver(concierge);
		}
		return saver;
	}
	 
}
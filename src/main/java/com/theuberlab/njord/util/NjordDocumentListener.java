package com.theuberlab.njord.util;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.theuberlab.njord.main.Njord;
import com.theuberlab.njord.main.NjordConcierge;


/**
 * @author forster
 *
 */
public class NjordDocumentListener implements DocumentListener {
	/**
	 * Holds the logger factory for SLF4J so we can log.
	 */
	final Logger log = LoggerFactory.getLogger(NjordDocumentListener.class);
	/**
	 * Holder for the primary class so that we interact with it and do things like update the contents of the notices box.
	 */
	public NjordConcierge concierge = null;
	
	/**
	 * An overridden constructor which sets the owner.
	 * @param owner
	 */
	public NjordDocumentListener(NjordConcierge concierge) {
		this.concierge = concierge;
	}
	
	/**
	 * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
	 */
	@Override
	public void changedUpdate(DocumentEvent e) {
        //Plain text components apparently don't fire these events.
	}
	
	//TODO: Find a better way to handle the navTree.repaint() and btActionSave stuff
	/**
	 * Enables the save button an triggers a repaint of the navigation tree which will display the documents now dirty status.
	 * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
	 */
	@Override
	public void insertUpdate(DocumentEvent e) {
	    
		log.debug("Insert on " + e);
		concierge.getOwner().navTree.repaint();
//		owner.btnActionSave.setEnabled(true);
	}
	
	/**
	 * Enables the save button an triggers a repaint of the navigation tree which will display the documents now dirty status.
	 * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
	 */
	@Override
	public void removeUpdate(DocumentEvent e) {
		log.info("Remove on " + e);
		concierge.getOwner().navTree.repaint();
//		owner.btnActionSave.setEnabled(true);
	}
}

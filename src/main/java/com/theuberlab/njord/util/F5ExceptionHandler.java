package com.theuberlab.njord.util;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;
import org.slf4j.Logger;

import com.theuberlab.njord.main.NjordConcierge;
import com.theuberlab.njord.main.NjordNotifier;

/**
 * <code>NjordExceptionHandler</code>
 * 
 * @author Aaron Forster
 *
 */
public class F5ExceptionHandler {
    // ###############################################################
    // VARIABLES
    // ###############################################################
    private Logger log;
    private String message;
    private Exception e;
    private NjordNotifier notifier;
    private NjordConcierge concierge;
    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################
    
//    /**
//     * @param e
//     */
//    public NjordExceptionHandler(Exception e) {
//        super(e);
//        // TODO Auto-generated constructor stub
//    }
//    
//    /**
//     * @param e
//     * @param Message
//     */
//    public NjordExceptionHandler(Exception e, String Message) {
//        super(e, Message);
//        // TODO Auto-generated constructor stub
//    }
    
    //These two are the only ones that should actually be used.
    /**
     * @param e
     * @param owner
     */
    public F5ExceptionHandler(Exception e,  NjordConcierge concierge) {
        this(e, concierge, "");
//        this(e, owner, "");
//        this.e = e;
    }
    
    /**
     * @param e
     * @param owner
     * @param message
     */
    public F5ExceptionHandler(Exception e, NjordConcierge concierge, String message) {
//        super(e, owner, message);
        this.e = e;
        this.message = message;
        this.concierge = concierge;
        log = concierge.getOwner().getLoggerHandle();
        notifier = (NjordNotifier)concierge.getNotifier();
    }
    
//    /**
//     * @param e
//     * @param owner
//     * @param message
//     * @param processImediately
//     */
//    public NjordExceptionHandler(Exception e, TUNotifiable owner, String message, boolean processImediately) {
//        super(e, owner, message, processImediately);
//        // TODO Auto-generated constructor stub
//    }

    // ###############################################################
    // GETTERS AND SETTERS
    // ###############################################################
    
    // ###############################################################
    // METHODS
    // ###############################################################
    
    
    // Mostly from Njord1
    public void processException() {
        if (log != null) {
            log.debug("Error is an instance of " + e.getClass().toString());
        } else {
            System.out.println("Error is an instance of " + e.getClass().toString());
        }
        if (e instanceof ServiceException) {
            // Log ServiceException
            // What is the difference between ServiceException and RemoteExeption?
            // Log a generic vulture exception message
            if ( message != "" ) {
                message = "ServiceException: " + e.getMessage();
            } else {
                message = message + ": " + e.getMessage();
            }
            log.error(message);
            //log(message) //somehow
        } else if (e instanceof RemoteException) {
            log.debug("Processing Remote Exception");
            // Log RemoteException
            // Try and reconenct?

            String errorContents = e.getMessage();
            Pattern pattern = Pattern.compile(".*error_string         :", Pattern.DOTALL);
            //This only works if we get a syntax error modifying an iRule.
//          Pattern pattern = Pattern.compile(".*error_string         :.*error:", Pattern.DOTALL);
            Matcher matcher = pattern.matcher(errorContents);
            //TODO: Modify the pattern and matcher so we get rid of this crap at the beginning as well. 
            //Error:  01070151:3: Rule [/Common/myIrulesOutputTest] error:
            // But to do that I'm going to have to 
            //TODO: Figure out what sort of error I'm getting. So far I have
            //Modify iRule error iRule already exists. Show everything from error_string
            //Modify iRule syntax error in iRule, cut out all the error number and rule [rulename] error: crap
            

            //Uncomment if working on the regex. The commented code shows what we are matching.
//          while (matcher.find()) {
//              log.info("Start index: " + matcher.start());
//              log.info(" End index: " + matcher.end() + " ");
//              log.info(matcher.group());
//              log.info("End matcher section ##############");
//          }
            
            //TODO: Replace this println with something that either pops up an error or sets the contents of a status box in the main gui. I prefer the latter.
            String errorMessage = matcher.replaceAll("");
            log.info("Error: " + errorMessage);
            
//          editorNoticesBox.setText(errorMessage);
            
            //If the error is that we are trying to create a rule on top of an existing rule then it doesn't have the <stuff> Rule [<rule_name>] error: part
            //This is what getMessage returns in the case of extra text. I need to pull out the last part error_string:
//          Exception caught in LocalLB::urn:iControl:LocalLB/Rule::modify_rule()
//          Exception: Common::OperationFailed
//              primary_error_code   : 17236305 (0x01070151)
//              secondary_error_code : 0
//              error_string         : 01070151:3: Rule [/Common/http_responder] error: 
//          line 15: [parse error: extra characters after close-brace] [ffff]
            
            
            log.error(errorMessage);
            
            notifier.sendNotification(errorMessage);
//            if (owner != null) {
//                owner.setNoticesText(errorMessage);
//            } 
            
            if ( message != "" ) {
            message = "RemoteException: " + message;
            } else {
            message = message + ": " + message;
            }
            log.error(message);
            
            //log(message) //somehow
        } else if (e instanceof MalformedURLException) {
            // Log RemoteException
            // Try and reconenct?
            // Log a generic vulture exception message
            if ( !message.equals("") ) {
                message = "MalformedURLException: " + e.getMessage();
            } else {
                message = message + ": " + e.getMessage();
            }
            log.error(message);
            //log(message) //somehow    
        } else if (e instanceof AxisFault) {
            //This might be where we end up if we get an error in the irule saving.
            if ( !message.equals("") ) {
                message = "AxisFault: " + e.getMessage();
            } else {
                message = message + ": " + e.getMessage();
            }
            log.error(message);
        } else {
            // Log some new exception we were unnaware of happened here.
            // Perhaps now we stack trace but likely only if in debug
            // Log a generic vulture exception message
            if ( !message.equals("") ) {
                message = "Un-known Exception of type: " + e.getClass() + " encountered sent message: " + e.getMessage();
            } else {
                message = "Un-known Exception of type: " + e.getClass() + " encountered sent message: " + e.getMessage();
//              message = message + ": " + e.getMessage();
            }
            System.out.println("Unknown exception caught of type " + e.getClass());
            System.out.println(e.getStackTrace());
//          log.error(message);
            //log(message) //somehow
        }
    }
}
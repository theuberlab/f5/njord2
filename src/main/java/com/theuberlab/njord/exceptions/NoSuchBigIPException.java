package com.theuberlab.njord.exceptions;


/**
 * <code>NoSuchBigIPException</code>
 * 
 * @author Aaron Forster
 *
 */
public class NoSuchBigIPException extends Exception {
  String bigIPName;
  
  public NoSuchBigIPException() {
    super();             // call superclass constructor
    bigIPName = "unknown";
  }
  
  public NoSuchBigIPException(String bigip) {
    super("Bigip [" + bigip + "] not found");     // call super class constructor
    bigIPName = bigip;  // save bigipname
  }
  
  public String getError() {
      String errorMessage = "Bigip [" + bigIPName + "] not found";
      return errorMessage;
  }
}
  
//public BigIP getBigIP(String bigIPName) throws NoSuchBigIPException {
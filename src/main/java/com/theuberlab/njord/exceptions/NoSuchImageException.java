package com.theuberlab.njord.exceptions;

/**
 * <code>NoSuchImageException</code>
 * 
 * @author Aaron Forster
 *
 */
public class NoSuchImageException extends Exception {
    // ###############################################################
    // VARIABLES
    // ###############################################################
    
    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################
        
    /**
     * 
     */
    public NoSuchImageException() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * @param message
     */
    public NoSuchImageException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }
    
    /**
     * @param cause
     */
    public NoSuchImageException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }
    
    /**
     * @param message
     * @param cause
     */
    public NoSuchImageException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    // ###############################################################
    // GETTERS AND SETTERS
    // ###############################################################
    
    // ###############################################################
    // METHODS
    // ###############################################################
    
}

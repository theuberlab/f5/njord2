package com.theuberlab.njord.exceptions;


/**
 * <code>NoSuchPluginRegisteredException</code>
 * 
 * @author Aaron Forster
 *
 */
public class NoSuchPluginRegisteredException extends Exception {
  String pluginName;
  
  public NoSuchPluginRegisteredException() {
    super();             // call superclass constructor
    pluginName = "unknown";
  }
  
  public NoSuchPluginRegisteredException(String plugin) {
    super("Plugin [" + plugin + "] not registered");     // call super class constructor
    pluginName = plugin;  // save plugin name
  }
  
  public String getError() {
      String errorMessage = "Plugin [" + pluginName + "] not registered";
      return errorMessage;
  }
}
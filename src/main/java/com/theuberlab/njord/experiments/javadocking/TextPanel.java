package com.theuberlab.njord.experiments.javadocking;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TextPanel extends JPanel
{
	private JLabel label; 
	
	public TextPanel(String text)
	{
		super(new FlowLayout());
		
		// The panel.
		setMinimumSize(new Dimension(80,80));
		setPreferredSize(new Dimension(150,150));
		setBackground(Color.white);
		setBorder(BorderFactory.createLineBorder(Color.lightGray));
		
		// The label.
		label = new JLabel(text);
		label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		add(label);
	}
}
/**
 * Tests which involve kdgregory.com's swinglib
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.experiments.swinglib;
package com.theuberlab.njord.experiments.swinglib;
//package com.kdgregory.example.swingAsync;

import java.awt.Cursor;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

// Copyright (c) 2010 Keith D Gregory, all rights reserved


/**
 *  Creates a Swing application with two text boxes, one that displays its
 *  normal cursor and one that displays the busy cursor.
 */
public class CursorDemo
{
    public static void main(String[] argv)
    throws Exception
    {
        SwingUtilities.invokeAndWait(new Runnable()
        {
            public void run()
            {
                JFrame frame = new JFrame("Cursor Demo");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setContentPane(buildContent());
                frame.pack();
                frame.setVisible(true);
            }
        });
    }


    private static JPanel buildContent()
    {
        JTextArea text1 = new JTextArea(10, 20);
        JTextArea text2 = new JTextArea(10, 20);

        text2.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        JPanel panel = new JPanel(null);
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(text1);
        panel.add(new JSeparator(JSeparator.VERTICAL));
        panel.add(text2);
        return panel;
    }
}

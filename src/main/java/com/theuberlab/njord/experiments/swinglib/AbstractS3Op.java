// Copyright (c) 2010 Keith D Gregory, all rights reserved
package com.theuberlab.njord.experiments.swinglib;

import javax.swing.JOptionPane;

import net.sf.swinglib.AsynchronousOperation;
import net.sf.swinglib.components.ProgressMonitor;

import org.apache.commons.logging.LogFactory;


/**
 *  Holds common code for all S3 operations.
 */
public abstract class AbstractS3Op<T>
extends AsynchronousOperation<T>
{
//    private Concierge _concierge;
//    private String _description;
//    private S3Factory _factory;     // lazily initialized
//    private S3Bucket _bucket;       // lazily initialized

    private ProgressMonitor _progressMonitor;


//    protected AbstractS3Op(Concierge concierge, String description)
    protected AbstractS3Op()
    {
//        _concierge = concierge;
//        _description = description;
    }


//----------------------------------------------------------------------------
//  Workflow Methods
//----------------------------------------------------------------------------

    /**
     *  Initiates a wait cursor and then puts this operation on the background
     *  thread. Subclasses may override to provide addition initialization,
     *  but should always delegate to this implementation.
     */
    public void start()
    {
        setBusyState(true);
//        _concierge.execute(this);
    }


    /**
     *  Clears the wait cursor / progress monitor.
     */
    @Override
    protected void onComplete()
    {
        setBusyState(false);
    }


    /**
     *  Common exception handler. Turns off wait cursor, logs exception, and
     *  notifies user.
     *  <p>
     *  Note that this is marked as <code>final</code>, to prevent subclasses
     *  from overriding.
     */
    @Override
    protected void onFailure(Throwable ex)
    {
        LogFactory.getLog(this.getClass()).error("request failed", ex);
//        JOptionPane.showMessageDialog(
//                _concierge.getDialogOwner(),
//                "Unable to process this request: " + ex.getMessage()
//                    + "\nSee log for more information",
//                "Unable to Execute Operation",
//                JOptionPane.ERROR_MESSAGE);
    }


//----------------------------------------------------------------------------
//  Support methods for subclasses
//----------------------------------------------------------------------------

//    protected Concierge getConcierge()
//    {
//        return _concierge;
//    }


    /**
     *  Returns an <code>S3Factory</code> object reflecting current user
     *  configuration.
     */
//    protected S3Factory getFactory()
//    {
//        // factory may be reused during request; Concierge will always
//        // rebuild, so we don't want to keep going back there
//        if (_factory == null)
//        {
//            _factory = _concierge.getS3Factory();
//        }
//        return _factory;
//    }


    /**
     *  Returns an <code>S3Bucket</code> object reflecting current user
     *  configuration.
     */
//    protected S3Bucket getBucket()
//    {
//        if (_bucket == null)
//        {
//            _bucket = getFactory().newBucket(
//                        _concierge.getConfig().getAmazonBucketName());
//        }
//        return _bucket;
//    }


    /**
     *  Returns an <code>S3Object</code> for the given key, reflecting
//     *  current user configuration for bucket.
//     */
//    protected S3Object createS3Object(String key)
//    {
//        return getFactory().newObject(getBucket(), key);
//    }
//
//
//    /**
//     *  Called during operation to update the progress monitor status
//     *  message.
//     */
//    protected void updateProgressMonitor(String message)
//    {
//        _progressMonitor.setStatus(message);
//    }


//----------------------------------------------------------------------------
//  Internals
//----------------------------------------------------------------------------

    /**
     *  Controls the various "wait indicators" -- pass <code>true</code> at
     *  the start of an operation, <code>false</code> at the end.
     */
    private void setBusyState(boolean isBusy)
    {
//        _concierge.getMainFrame().setBusyState(isBusy);
        if (isBusy)
        {
//            _progressMonitor = new ProgressMonitor(
//                                    _concierge.getDialogOwner(),
//                                    "S3 Operation in Progress",
//                                    _description,
//                                    ProgressMonitor.Options.MODAL,
//                                    ProgressMonitor.Options.CENTER,
//                                    ProgressMonitor.Options.SHOW_STATUS);
//            _progressMonitor.show();
        }
        else if (_progressMonitor != null)
        {
            _progressMonitor.dispose();
            _progressMonitor = null;
        }
    }
}

//package com.theuberlab.njord.experiments.swinglib;
//
//import java.rmi.RemoteException;
//import java.util.Arrays;
//import java.util.List;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import javax.xml.rpc.ServiceException;
//
//import net.sf.swinglib.components.ProgressMonitor;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.theuberlab.common.util.TheUberlabExceptionHandler;
//import com.theuberlab.njord.model.BigIP;
//
///**
// * <code>fetchiRules</code>
// * 
// * @author Aaron Forster
// *
// */
//public class fetchiRules extends AbstractS3Op<List<String>> {
//    /**
//     * Holds our logger factory for SLF4J.
//     */
//    public final Logger log = LoggerFactory.getLogger(fetchiRules.class);
//    private AsyncListiRules owner;
//    
//    private ProgressMonitor _progressMonitor;
//    
//    /**
//     * 
//     */
//    public fetchiRules(AsyncListiRules owner) {
//        // TODO Auto-generated constructor stub
//        this.owner = owner;
//    }
//    // ###############################################################
//    // VARIABLES
//    // ###############################################################
//    public void start() {
//        setBusyState(true);
//        owner.execute(this);
//    }
//    
//    
//    
//protected void onComplete()
//{
//    log.debug("onComplete()");
//    setBusyState(false);
//}
//
//private void setBusyState(boolean isBusy)
//{
////    _concierge.getMainFrame().setBusyState(isBusy);
//    if (isBusy)
//    {
//        _progressMonitor = new ProgressMonitor(
//                owner.getDialogOwner(),
//                                "S3 Operation in Progress",
//                                "this is a description",
//                                ProgressMonitor.Options.MODAL,
//                                ProgressMonitor.Options.CENTER,
//                                ProgressMonitor.Options.SHOW_STATUS);
//        _progressMonitor.show();
//    }
//    else if (_progressMonitor != null)
//    {
//        _progressMonitor.dispose();
//        _progressMonitor = null;
//    }
//}
//
//protected void updateProgressMonitor(String message)
//{
//    _progressMonitor.setStatus(message);
//}
//    
//    
//    
//    /**
//     * This should become plugin.getObjectsList()
//     * 
//     * getiRuleList returns only a string list of the names of the iRules instead of actual iRule objects 
//     * 
//     * @return The list of iRules from the BIGIP.
//     * @throws Exception
//     */
//    public List<String> getiRuleList(iControl.Interfaces ic) {
//        // LTM and GTM are the only two sections that have iRules.
//        String[] LTMiRules = null;
//        String[] GTMiRules = null;
//        List<String> iRules = null;
//        
//        try {
//            LTMiRules = ic.getLocalLBRule().get_list();
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        try {
//            GTMiRules = ic.getGlobalLBRule().get_list();
////          iRules = ic.getGlobalLBRule().get_list();
//        } catch (RemoteException e) {
//            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
//            exceptionHandler.processException();
//        } catch (Exception e) {
//            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
//            exceptionHandler.processException();
//        }
//        //This is crap but until I come up with a better way we're still only going to do LTM iRules.       
//        iRules = Arrays.asList(LTMiRules);
//        //TODO: Make this part work. I'm pretty sure if we try and edit a GTM iRule we won't be able to save it. This is because I'm just adding everything so I don't know which is a local and which is a remote.
//        //Not sure if this will work. I'll have to be able to determine the rule type when i try and save it.
//        //TODO: O.K. I'm NOT going to make fetching of GTM iRules work together with the local iRules. GTM (or "global iRules") will be handled by a separate plugin.
////        iRules.addAll(Arrays.asList(GTMiRules));
//        
//        log.info("Available Rules");
//        for (String string : iRules) {
//            log.debug("   " + string);
//        }
//        return iRules;
//    }
//    
//    
////    @Override
////    protected List<String> performOperation() throws Exception {
//    protected List<String> performOperation() {
//        log.debug("Fetching content");
//        
//        List<String> result = null;
//     
//        
//        BigIP thisBigIp = new BigIP("thisBigIP", "v11ve3.localdomain", 443, "admin", "G!mmef5", true);
//        
//        
//        iControl.Interfaces ic = getConnectionTo(thisBigIp);
//        
//        
////        NjordTreeNode remoteiRulesCategory = new NjordTreeNode("iRules", NjordNodeType.NODE_TYPE_CATEGORY);
////        tree.add(remoteiRulesCategory);
////        
////        NjordTreeNode addRule = null;
////        // iRuleList = new ArrayList<String>(getiRuleList(thisBigIp));
//        
//        List<String> theRules = getiRuleList(thisBigIp.getInterface());
////        thisBigIp.setiRulesNames(getiRuleList(thisBigIp.getInterface()));
//        
//        if (thisBigIp.getiRulesNames().isEmpty()) {
//            log.debug("No valid iRules found on BIGIP!");
//            // resultsPanelNoticesBox.setText("No valid iRules found on BIGIP.");
//            // TextEditorPane ruleEditor = getEditorForRule(null, "No iRules Found On BIGIP");
//        
//        } else {
//            for (String rule : theRules) {
//                log.debug("Rule: [{}] found", rule);    
//            }
//            
//        }
////        List<Map<String,String>> data = getBucket().listObjectsWithMetadata();
////        SortedSet<S3File> result = new TreeSet<S3File>();
////        for (Map<String,String> entry : data)
////        {
////            String key = entry.get(S3Constants.BUCKETLIST_KEY);
////            String size = entry.get(S3Constants.BUCKETLIST_SIZE);
////            String lastMod = entry.get(S3Constants.BUCKETLIST_LASTMOD);
////            result.add(new S3File(key, size, lastMod));
////        }
//        
//        log.debug("request complete: " + result.size() + " files");
//
//        return result;
//    }
//
//
////    @Override
//    protected void onSuccess(List<String> results) {
//        log.debug("onSuccess");
//        for (String result : results) {
//            log.debug("Found rule [{}]", result);
//        }
////        concierge.getNotifier().sendNotification(result,TUNotificationType.NOTIFICATION_TYPE_APPEND);
//    }
//    
////    public final void run() {
////        log.debug("in run()");
////        try
////        {
////            final List<String> results = performOperation();
////            SwingUtilities.invokeLater(new Runnable()
////            {
////                public void run()
////                {
////                    onComplete();
////                    onSuccess(results);
////                }
////            });
////        }
////        catch (final Throwable e)
////        {
////            SwingUtilities.invokeLater(new Runnable()
////            {
////                public void run()
////                {
////                    onComplete();
////                    onFailure(e);
////                }
////            });
////        }
////    }
//    
//    
////    public void onComplete() {
////        log.debug("onComplete()");
////    }
//    
//    public void onFailure(Throwable error) {
//        log.debug("onFailure");
//        log.debug("Failed with error [{}]", error.getMessage());
//    }
//
//    /**
//     * Initializes a connection to the specified BigIP.
//     * Runs the initialize method on the library which sets up the connection object. Then do a get version. If it works we've got good connection settings.
//     * @return
//     */
//    private iControl.Interfaces getConnectionTo(BigIP bigip) {
////        lblStatusLabel.setText("Connecting");
////        resultsPanelNoticesBox.setText("");
//        String ipAddress = bigip.gethostNameOrAddress();
//        long port = bigip.getiControlPort();
//        String userName = bigip.getUserName();
//        String passWord = bigip.getPassword();
//        String version = null;
//        String bigIPVersion = null;
//        iControl.Interfaces ic = new iControl.Interfaces(); //From Assembly
//        ic.initialize(ipAddress, port, userName, passWord); // Initialize the interface to the BigIP
//        
//        
//        try {
//            //TODO: There has to be a better way to do this
//            version = ic.getSystemSystemInfo().get_version();
//            Pattern versionPrefix = Pattern.compile("BIG-IP_v");
//            Matcher versionMatcher = versionPrefix.matcher(version);
//            bigIPVersion = versionMatcher.replaceAll(""); //Kinda redundant, I know.
//        } catch (RemoteException e1) {  
//            //TODO: Add a return type so I can do a return here.
//            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e1);
//            exceptionHandler.processException();
//        } catch (ServiceException e1) {
//            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e1);
//            exceptionHandler.processException();
//        } catch (Exception e1) {
//            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e1);
//            exceptionHandler.processException();
//        }
//        
//        boolean success = false;
//        //TODO: Move this part, the setting of the progress bar and displaying of connection validity out of this section and maybe back into the listener. We should only be returning true/false here.
//        if (version != null) {
//            log.debug("My Big-IP is version:" + bigIPVersion);
//            if ( bigIPVersion.startsWith("10") ) {
//                log.debug("v" + bigIPVersion + ", success");
//                bigip.setVersion(version);
//                bigip.setInterface(ic);
////                resultsPanelNoticesBox.setText("Connected to BIG-IP version: " + bigIPVersion);
////                connectionInitialized = true;
////                success = true; //It worked
////                lblStatusLabel.setText("Connected");
//            } else if (bigIPVersion.startsWith("11")) {
//                log.debug("v" + bigIPVersion + ", success");
//                bigip.setVersion(version);
//                bigip.setInterface(ic);
////                resultsPanelNoticesBox.setText("Connected to BIG-IP version: " + bigIPVersion);
////                connectionInitialized = true;
////                success = true; //It worked;
////                lblStatusLabel.setText("Connected");
//            } else if (bigIPVersion.startsWith("9")) {
//                log.debug("v" + bigIPVersion + ", unclear");
//                bigip.setVersion(version);
//                bigip.setInterface(ic);
////                resultsPanelNoticesBox.setText("njord is currently untested on BIG-IP version 9 systems. Proceed at your own risk.");
////                connectionInitialized = true;
////                success = true; 
////                lblStatusLabel.setText("Connected");
//            } else {
//                log.debug("v" + bigIPVersion + ", fail");
////                resultsPanelNoticesBox.setText("BIG-IP version: " + bigIPVersion + "Is unsupported. You must connect to a BIG-IP version 9.4 or higher.");
////                connectionInitialized = false;
////                success = false;
////                lblStatusLabel.setText("Disconnected");
//            }
//            
//            return ic;
//        } else {
//            // scream, run and cry
//            //TODO: Check the return code of the exception to see what the cause of failure is.
//            log.error("Connection settings invalid");
////            connectionInitialized = false;
////            return false; //We are failz
//            return ic;
//        }
//    }
//}

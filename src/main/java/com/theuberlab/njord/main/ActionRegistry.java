// Copyright (c) 2010 Keith D Gregory, all rights reserved
package com.theuberlab.njord.main;

import javax.swing.ImageIcon;

import com.theuberlab.njord.actions.FetchiRules;






/**
*  A central location for access to shared action instances. All
*  instances are public members, set when the assocated GUI object
*  is constructed.
*/
public class ActionRegistry {
    // This is static and needs to be updated to use the plugin registry.
    public FetchiRules  fetchiRules;
    
    
    protected ActionRegistry(NjordConcierge concierge) {
        fetchiRules = new FetchiRules(concierge,concierge.getImageProvider().getIcon("GetIrules"));
    }
    
    
    //Stuff like this would be good to implement to handle synchronization between what's selected in the nav pane and what tab is active.
//  /**
//  *  Called when the selection state changes (either some row is selected
//  *  or no rows are selected).
//  */
// public void updatePerSelection(int count)
// {
//     fileDownload.setEnabled(count > 0);
//     fileDelete.setEnabled(count > 0);
//     editSelectNone.setEnabled(count > 0);
//     editCopyUrl.setEnabled(count == 1);
// }
    
}















// The original s3util ActionResistry




//
//import com.kdgregory.app.s3util.actions.EditCopyUrl;
//import com.kdgregory.app.s3util.actions.EditSelectAll;
//import com.kdgregory.app.s3util.actions.EditSelectNone;
//import com.kdgregory.app.s3util.actions.FileDelete;
//import com.kdgregory.app.s3util.actions.BucketDelete;
//import com.kdgregory.app.s3util.actions.FileDownload;
//import com.kdgregory.app.s3util.actions.FileQuit;
//import com.kdgregory.app.s3util.actions.FileRefresh;
//import com.kdgregory.app.s3util.actions.BucketSelect;
//import com.kdgregory.app.s3util.actions.FileSetPrefs;
//import com.kdgregory.app.s3util.actions.FileUpload;
//
//
///**
// *  A central location for access to shared action instances. All
// *  instances are public members, set when the assocated GUI object
// *  is constructed.
// */
//public class ActionRegistry
//{
//    public FileRefresh          fileRefresh;
//    public FileUpload           fileUpload;
//    public FileDownload         fileDownload;
//    public FileDelete           fileDelete;
//    public FileSetPrefs         fileSetPrefs;
//    public FileQuit             fileQuit;
//
//    public EditSelectAll        editSelectAll;
//    public EditSelectNone       editSelectNone;
//    public EditCopyUrl          editCopyUrl;
//
//    public BucketSelect         bucketSelect;
//    public BucketDelete         bucketDelete;
//
//
////----------------------------------------------------------------------------
////  Only one constructor, meant to be called within the package
////----------------------------------------------------------------------------
//
//    protected ActionRegistry(Concierge concierge)
//    {
//        fileRefresh = new FileRefresh(concierge);
//        fileUpload = new FileUpload(concierge);
//        fileDownload = new FileDownload(concierge);
//        fileDelete = new FileDelete(concierge);
//        fileSetPrefs = new FileSetPrefs(concierge);
//        fileQuit = new FileQuit();
//
//        editSelectAll = new EditSelectAll(concierge);
//        editSelectNone = new EditSelectNone(concierge);
//        editCopyUrl = new EditCopyUrl(concierge);
//
//        bucketSelect = new BucketSelect(concierge);
//        bucketDelete = new BucketDelete(concierge);
//    }
//
//
////----------------------------------------------------------------------------
////  Public methods - used to enable/disable groups of actions
////----------------------------------------------------------------------------
//
//    /**
//     *  Called when the selection state changes (either some row is selected
//     *  or no rows are selected).
//     */
//    public void updatePerSelection(int count)
//    {
//        fileDownload.setEnabled(count > 0);
//        fileDelete.setEnabled(count > 0);
//        editSelectNone.setEnabled(count > 0);
//        editCopyUrl.setEnabled(count == 1);
//    }
//
//    /**
//     *  Called to indicate whether there's currently an active bucket list
//     *  being displayed.
//     */
//    public void updatePerActiveBucket(boolean isActive)
//    {
//        bucketDelete.setEnabled(isActive);
//    }
//}

package com.theuberlab.njord.main;

import java.util.HashMap;
import java.util.Map;

import com.theuberlab.common.util.TUContentProvider;

/**
 * <code>NjordCM</code>
 * 
 * @author Aaron Forster
 *
 */
public class NjordContentProvider extends TUContentProvider {
    // ###############################################################
    // VARIABLES
    // ###############################################################
    /**
     * This will store all the content we have fetched from wherever said content is stored.
     */
    private Map<String, String> contentMap = new HashMap<String, String>();
    
    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################
    
    /**
     * 
     */
    public NjordContentProvider(NjordConcierge concierge) {
        //TODO: I should really load this from some resource but for now I'm just setting it all here.
//        contentMap.put(key, value);
        // Eventually I will probably wrap this in something like java's resource bundle so I can A) get it out of the source B) store it in something
        // way less memory heavy than a map but I want the interface to be reasonably consistent so when I'm ready to move to a resource bundle I'll 
        // just implement it in the content provider.
        concierge.registerContentProvider(this);
        //Encryption information
        contentMap.put("ENCRYPTION_MP_TITLE", "Enter Master Password"); 
        contentMap.put("ENCRYPTION_MP_TEXT", "Please provide the password used to encrypt stored passwords"); 
        contentMap.put("ENCRYPTION_MP_FIELD_TEXT", "password");
        contentMap.put("ENCRYPTION_MP_TOOLTIP_TEXT", "The password used to encrypt passwords stored on disk.");
        contentMap.put("ENCRYPTION_MP_DEFAULT_TEXT", "");
        
        // Message content is for messages to send to the user, "Welcome to Njord" "There is no BIG-IP Selected" etc.
        // They should be in the form of MESSAGE_<message type>_<message_name>
        contentMap.put("MESSAGE_INFO_NJORD_STARTUP", "Welcome to Njord 2!");
        contentMap.put("MESSAGE_QUIT_NJORD_DITRY_EDiTORS", "There are still open editors with pending changes. Do you want to continue?");
        contentMap.put("MESSAGE_DITRY_EDITOR_CLOSE", "There are still open editors with pending changes. Do you want to continue?");
        
        
        
        //Language Definitions for iRules. Tokens, tooltip texts etc.
        contentMap.put("LANG_IRULES_FILEPATH_EVENTS", "/Res/Wordlists/iRulesEventsUncategorized.txt");
        contentMap.put("LANG_IRULES_FILEPATH_OPERATORS", "/Res/Wordlists/iRulesOperatorsUncategorized.txt");
        contentMap.put("LANG_IRULES_FILEPATH_STATEMENTS", "/Res/Wordlists/iRulesStatementsUncategorized.txt");
        contentMap.put("LANG_IRULES_FILEPATH_FUNCTIONS", "/Res/Wordlists/iRulesFunctionsUncategorized.txt");
        contentMap.put("LANG_IRULES_FILEPATH_COMMANDS", "/Res/Wordlists/iRulesCommandsUncategorized.txt");
        contentMap.put("LANG_IRULES_FILEPATH_TCL", "/Res/Wordlists/tclCommandsUncategorized.txt");
        contentMap.put("LANG_IRULES_FILEPATH_EDITORTOOLTIPS", "/Res/Wordlists/toolTipTexts.txt");
        
        
//        contentMap.put("LANG_IRULES_", "");
//        contentMap.put("LANG_IRULES_", "");
//        contentMap.put("LANG_IRULES_", "");
//        contentMap.put("LANG_IRULES_", "");
        
        
//        contentMap.put("", "");
        
    }
    
    // ###############################################################
    // GETTERS AND SETTERS
    // ###############################################################
    
    // ###############################################################
    // METHODS
    // ###############################################################
    
    /**
     * Returns the appropriate content for the specified key.
     * 
     * @param contentKey
     * @return
     */
    public String getCMContent(String contentKey) {
        return contentMap.get(contentKey);
    }
    
}

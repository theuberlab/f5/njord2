package com.theuberlab.njord.main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;

import net.sf.swinglib.AsynchronousOperation;

import org.slf4j.Logger;

import com.theuberlab.common.exceptions.TURegistrationFailedException;
import com.theuberlab.common.notificationsandlogging.TUNotifier;
import com.theuberlab.njord.constants.NjordNodeContext;
import com.theuberlab.njord.constants.NjordNodeType;
import com.theuberlab.njord.interfaces.NjordEditorPanel;
import com.theuberlab.njord.security.NjordSecurityProvider;
import com.theuberlab.njord.ui.ImageProvider;
import com.theuberlab.njord.ui.NjordRuleEditorPanel;
import com.theuberlab.njord.ui.NjordTreeNode;
import com.theuberlab.njord.util.NjordPreferences;

/**
 * <code>NjordConcierge</code> will provide a single interface for fetching most of the shared content in the application. 
 * Instead of passing an owner and having to devine a bunch of getLoggerHandle(), getNotifier(), etc methods in the main class,
 * the main class will be instantiating a Concierge and stuffing it full of all the relavent objects. Some of these may be read-only after initialization.
 * Some may be modified or configured during operation.
 * <p>
 * I like the description from Hulu's series 'The Awesomes.' "Concierge means a person who gets things done."
 * 
 * http://en.wikipedia.org/wiki/Concierge
 * 
 * From com.kdgregory.app.s3util (where I got the idea):
 *  Provides access to common services. Some of these are read-only after
 *  startup, some may be configured during operation.
 *  <p>
 *  A single instance of the Concierge is created at application startup,
 *  and initialized with dependent objects. This single instance will be
 *  passed as a constructor parameter to everything that needs it.
 * @author Aaron Forster
 *
 */
//public class NjordConcierge extends TUConcierge {
public class NjordConcierge {
    // ###############################################################
    // VARIABLES
    // ###############################################################
    /**
     * The notifier in case we are called upon to send messages to it.
     */
    private TUNotifier notifier;
    /**
     * The Concierge is the only thing that should hold the owner. It should be customized per app thus
     * it is of type Njord not TUNotifiable.
     */
    private Njord owner;
    /**
     * Content providers maybe should be a list based on type?
     */
    private NjordContentProvider contentProvider;
    /**
     * The thread pool executor
     */
    private ExecutorService _threadPool;
    /**
     * Stores a list of rules open.
     */
    private List<NjordRuleEditorPanel> openEditors = new ArrayList<NjordRuleEditorPanel>();
    /**
     * The image provider provieds ImageIcons and other images used by Njord.
     */
    private ImageProvider imageProvider = new ImageProvider(this);
    
    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################

    /**
     * @throws TURegistrationFailedException 
     * 
     */
    public NjordConcierge(Njord owner) {
        this.owner = owner;
//        try {
//            this.notifier.registerForNotifications(owner);
//        } catch (TURegistrationFailedException e) {
//            e.printStackTrace();
//        }
    }
    
    // ###############################################################
    // METHODS
    // ###############################################################
    
    /**
     * Returns a handler to the notification system.
     * 
     * @return
     */
    public TUNotifier getNotifier() {
        return notifier;
    }
    
    /**
     * Sets's the notifier.
     * 
     * @param notifier
     * @return
     */
    public TUNotifier setNotifier(TUNotifier notifier) {
        this.notifier = notifier;
        return notifier;
    }
    
    /**
     * Returns a handler to the content provider. Eventually this may become deprecated in favor of something like getContentProviderByType(T type)
     * 
     * @return
     */
    public NjordContentProvider getContentProvider() {
        // TODO Auto-generated method stub
        return contentProvider;
    }
    
//    @Override
    public NjordContentProvider registerContentProvider(NjordContentProvider contentProvider) {
        this.contentProvider = contentProvider;
        return this.contentProvider;
    }
    
    public Njord getOwner() {
        return owner;
    }
    
    public Logger getLoggerHandle() {
        return owner.getLoggerHandle();
    }
    
    public void setBusyState(boolean isBusy) {
        ((Njord) owner).setBusyState(isBusy);
    }
    
    public boolean getBusyState() {
        return ((Njord) owner).getBusyState();
    }
    
    /**
     * A crappy legacy method implemented to try and get a task list working in a hurry.
     * 
     * @return
     */
    public JFrame getDialogOwner() {
        return ((Njord) owner).mainFrame;
    }
    
    public NjordNodeType getLastSelectedPathComponentType() {
        return owner.getLastSelectedPathComponentType();
    }
    
    public NjordNodeContext getSelectedPathContext() {
        return owner.navTree.getPathContextType();
        
    }
    
    public NjordSecurityProvider getSecurityProvider() {
        return (NjordSecurityProvider)owner;
    }
    
    public NjordTreeNode getLastSelectedOrigin() {
        return owner.navTree.getLastSelectedOrigin();
    }
    
    public NjordPreferences getPreferences() {
        return owner.preferences;
        
    }
    
    /**
     * Returns the last active editor panel.
     * 
     * @return
     */
    public NjordEditorPanel getActiveEditorPane() {
        return owner.getActiveEditorPane();
    }
    
    /**
     * Registers a newly opened editor pane.
     * 
     * @param editor
     */
    public void registerEditorPanel(NjordRuleEditorPanel editor) {
        openEditors.add(editor);
    }
    
    /**
     * Removes the editor panel from the internal list of open editors.
     * 
     * @param editor
     */
    public void deregisterEditorPanel(NjordRuleEditorPanel editor) {
       openEditors.remove(editor);
       //TODO: This probably isn't going to work this way.
    }
    
    /**
     * Checks the registered editors to ensure that none of them have un-saved changes.
     * @return
     */
    public boolean hasDirtyEditors() {
        for (NjordRuleEditorPanel editor : openEditors) {
            if (editor.getIsDirty()) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Returns Njord's ImageProvider.
     * 
     * @return
     */
    public ImageProvider getImageProvider() {
        return imageProvider;
    }
//    /**
//     * Returns a list of editors with un-saved changes.
//     * 
//     * @return
//     */
//    public List<String> getDirtyEditors() {
//       //TODO: This should prolly return refrences to said editors? 
//    }
    
    /**
     *  Adds an operation to the shared background thread pool. This pool is
     *  created at instantiation, and cannot be changed.
     *  Originally from kdgregory.com's s3util AsynchronousOperation requires swinglib.
     */
    public void execute(AsynchronousOperation<?> op)
    {
        if (_threadPool == null)
            _threadPool = Executors.newFixedThreadPool(1);
        _threadPool.execute(op);
    }
    
    //TODO: Update TUNotifier to get rid of setNotifier(notifier) and use this instead.
    /**
     * Registers a notifier system. Notifiers handle the inter-object communication.
     * 
     * @param notifier
     */
    public void registerNotifier(NjordNotifier notifier) {
        this.notifier = notifier;
    }
    
}

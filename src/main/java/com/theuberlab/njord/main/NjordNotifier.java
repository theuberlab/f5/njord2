package com.theuberlab.njord.main;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;

import com.theuberlab.common.constants.TUNotificationType;
import com.theuberlab.common.exceptions.TURegistrationFailedException;
import com.theuberlab.common.interfaces.TUNotifiable;
import com.theuberlab.common.notificationsandlogging.TUNotifier;
import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.util.F5ExceptionHandler;

/**
 * <code>NjordNotifier</code>
 * 
 * @author Aaron Forster
 *
 */
public class NjordNotifier extends TUNotifier implements TUNotifiable {
    // ###############################################################
    // VARIABLES
    // ###############################################################
//    /**
//     * Holds the owner
//     */
//    private TUNotifiable owner;
    /**
     * Holds the notification destinations. 
     */
    private List<TUNotifiable> notificationDestinations = null;
    /**
     * Holds our logger factory for SLF4J.
     */
//    private final Logger log = LoggerFactory.getLogger(Njord.class);
    private Logger log;
    private NjordConcierge concierge;
    /**
     * @param owner
     */
    public NjordNotifier(NjordConcierge concierge) {
        //TODO: Fix this stuff and stop passing owner
        super(concierge.getOwner());
        // I shouldn't have to register the owner here since that _should_ be handled by the super.
        log = concierge.getOwner().getLoggerHandle();
        
        concierge.registerNotifier(this);
        
        try {
            registerForNotifications(this);
        } catch (TURegistrationFailedException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Sets a handle to the concierge
     * @param concierge
     */
    public void setConcierge(NjordConcierge concierge) {
        this.concierge = concierge;
    }
    
    public void initNotificationDestinations() {
        notificationDestinations = new ArrayList<TUNotifiable>();
    }
    
    /**
     * @see com.theuberlab.common.notificationsandlogging.TUNotifier#registerForNotifications(com.theuberlab.common.interfaces.TUNotifiable)
     */
    @Override
    public void registerForNotifications(TUNotifiable notifiable) throws TURegistrationFailedException {
        if (notificationDestinations == null) {
            this.initNotificationDestinations();
        }
        notificationDestinations.add(notifiable);
    }

    /**
     * @see com.theuberlab.common.notificationsandlogging.TUNotifier#getLoggerHandle()
     */
    @Override
    public Logger getLoggerHandle() {
        return log;
    }

    /**
     * @see com.theuberlab.common.notificationsandlogging.TUNotifier#setLoggerHandle(org.slf4j.Logger)
     */
    @Override
    public void setLoggerHandle(Logger log) {
        this.log = log;
    }
    
    /**
     * Convenience method when all you want to do is set the notices box.
     * 
     * @param message
     */
    public void sendNotification(String message) {
        sendNotification(message, TUNotificationType.NOTIFICATION_TYPE_NOTIFICATION);
    }
    
    /**
     * @see com.theuberlab.common.notificationsandlogging.TUNotifier#sendNotification(java.lang.String)
     */
    @Override
    public void sendNotification(String message, TUNotificationType notificationType) {
        switch (notificationType) {
            case NOTIFICATION_TYPE_LOGONLY:
                log.error(message);
                break;
            case NOTIFICATION_TYPE_APPEND:
                doNotifications(message, TUNotificationType.NOTIFICATION_TYPE_APPEND);
                break;
            case NOTIFICATION_TYPE_OVERWRITE:
            case NOTIFICATION_TYPE_POPUP:
            case NOTIFICATION_TYPE_NOTIFICATION:
            default:
                doNotifications(message);
                break;
        }
    }
    
    /**
     * Appends to the box instead of overwriting.
     */
    private void doNotifications(String message, TUNotificationType notificationType) {
        switch (notificationType) {
            case NOTIFICATION_TYPE_LOGONLY:
                log.error("Error in notifier: This branch of the switch should never be reached");
                break;
            case NOTIFICATION_TYPE_APPEND:
                for (TUNotifiable dest: notificationDestinations) {
                    dest.tuNotify(message, true);
                }
                break;
            case NOTIFICATION_TYPE_OVERWRITE:
            case NOTIFICATION_TYPE_POPUP:
            case NOTIFICATION_TYPE_NOTIFICATION:
            default:
                for (TUNotifiable dest: notificationDestinations) {
                    dest.tuNotify(message, false);
                }
                break;
        }
        
    }
    
    /**
     * @see com.theuberlab.common.notificationsandlogging.TUNotifier#doNotifications()
     */
    private void doNotifications(String message) {
        for (TUNotifiable dest: notificationDestinations) {
            dest.tuNotify(message);
        }
    }

    /* (non-Javadoc)
     * @see com.theuberlab.common.interfaces.TUNotifiable#tuNotify(java.lang.String)
     */
    @Override
    public void tuNotify(String message) {
        // TODO Auto-generated method stub
//        log.debug("Got message [{}] with append from the notifier", message);
    }

    /* (non-Javadoc)
     * @see com.theuberlab.common.interfaces.TUNotifiable#tuNotify(java.lang.String, boolean)
     */
    @Override
    public void tuNotify(String message, boolean append) {
        // TODO Auto-generated method stub
//        log.debug("Got message [{}] with append [{}] from the notifier", message, append);
    }

    @Override
    public void handleException(Exception e) {
      F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
      exceptionHandler.processException();
    }
    
//    /* (non-Javadoc)
//     * @see com.theuberlab.common.notificationsandlogging.TUNotifier#sendNotification(java.lang.String, java.lang.Object[])
//     */
//    @Override
//    public void sendNotification(String message, Object... arguments) {
//        log.debug(message, arguments);
//    }

    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################
    
    // ###############################################################
    // GETTERS AND SETTERS
    // ###############################################################
    
    // ###############################################################
    // METHODS
    // ###############################################################
    
}

package com.theuberlab.njord.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.LookAndFeel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.xml.rpc.ServiceException;

import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimplePBEConfig;
import org.jasypt.properties.PropertyValueEncryptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javadocking.DockingManager;
import com.javadocking.component.DockHeader;
import com.javadocking.component.PointDockHeader;
import com.javadocking.dock.BorderDock;
import com.javadocking.dock.CompositeLineDock;
import com.javadocking.dock.FloatDock;
import com.javadocking.dock.LeafDock;
import com.javadocking.dock.LineDock;
import com.javadocking.dock.Position;
import com.javadocking.dock.SplitDock;
import com.javadocking.dock.TabDock;
import com.javadocking.dock.docker.BorderDocker;
import com.javadocking.dock.factory.CompositeToolBarDockFactory;
import com.javadocking.dock.factory.ToolBarDockFactory;
import com.javadocking.dockable.ActionDockable;
import com.javadocking.dockable.ButtonDockable;
import com.javadocking.dockable.DefaultDockable;
import com.javadocking.dockable.Dockable;
import com.javadocking.dockable.DockableState;
import com.javadocking.dockable.DockingMode;
import com.javadocking.dockable.DraggableContent;
import com.javadocking.dockable.StateActionDockable;
import com.javadocking.dockable.action.DefaultDockableStateAction;
import com.javadocking.dockable.action.DefaultDockableStateActionFactory;
import com.javadocking.drag.DragListener;
import com.javadocking.drag.DraggerFactory;
import com.javadocking.drag.StaticDraggerFactory;
import com.javadocking.drag.painter.CompositeDockableDragPainter;
import com.javadocking.drag.painter.DefaultRectanglePainter;
import com.javadocking.drag.painter.DockableDragPainter;
import com.javadocking.drag.painter.ImageDockableDragPainter;
import com.javadocking.drag.painter.LabelDockableDragPainter;
import com.javadocking.drag.painter.RectangleDragComponentFactory;
import com.javadocking.drag.painter.SwDockableDragPainter;
import com.javadocking.drag.painter.TransparentWindowDockableDragPainter;
import com.javadocking.drag.painter.WindowDockableDragPainter;
import com.javadocking.event.DockingEvent;
import com.javadocking.event.DockingListener;
import com.javadocking.model.DefaultDockingPath;
import com.javadocking.model.DockModel;
import com.javadocking.model.DockingPath;
import com.javadocking.model.DockingPathModel;
import com.javadocking.model.FloatDockModel;
import com.javadocking.model.codec.DockModelPropertiesDecoder;
import com.javadocking.model.codec.DockModelPropertiesEncoder;
import com.javadocking.util.LookAndFeelUtil;
import com.javadocking.visualizer.DockingMinimizer;
import com.javadocking.visualizer.Externalizer;
import com.javadocking.visualizer.FloatExternalizer;
import com.javadocking.visualizer.SingleMaximizer;
import com.theuberlab.common.constants.TUNotificationType;
import com.theuberlab.common.exceptions.TURegistrationFailedException;
import com.theuberlab.common.interfaces.TUNotifiable;
import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.actions.FetchiRules;
import com.theuberlab.njord.constants.NjordNodeCategory;
import com.theuberlab.njord.constants.NjordNodeContext;
import com.theuberlab.njord.constants.NjordNodeType;
import com.theuberlab.njord.exceptions.NoSuchBigIPException;
import com.theuberlab.njord.exceptions.NoSuchPluginRegisteredException;
import com.theuberlab.njord.interfaces.NjordEditorPanel;
import com.theuberlab.njord.model.BigIP;
import com.theuberlab.njord.plugin.interfaces.NjordPluggable;
import com.theuberlab.njord.plugin.plugins.NjordiRulePlugin;
import com.theuberlab.njord.security.NjordSecurityProvider;
import com.theuberlab.njord.ui.BIGIPSettingsDialog;
import com.theuberlab.njord.ui.FlexibleTextDialog;
import com.theuberlab.njord.ui.ImageProvider;
import com.theuberlab.njord.ui.NewiRuleDialog;
import com.theuberlab.njord.ui.NjordNavTree;
import com.theuberlab.njord.ui.NjordRuleEditorPanel;
import com.theuberlab.njord.ui.NjordTreeNode;
import com.theuberlab.njord.util.LAF;
import com.theuberlab.njord.util.NewsFetcher;
import com.theuberlab.njord.util.F5ExceptionHandler;
import com.theuberlab.njord.util.NjordFileLocation;
import com.theuberlab.njord.util.NjordFindPanel;
import com.theuberlab.njord.util.NjordMessagePanel;
import com.theuberlab.njord.util.NjordPreferences;
import com.theuberlab.njord.util.SampleComponentFactory;
import com.theuberlab.njord.util.ToolBarButton;


/*
 * Every dockable can be closed. When the dockable is added again later,
 * the dockable is docked as good as possible like it was docked before.
 * 
 * Most of the dockables can be maximized, except the 'Find' panel.
 * 
 * All the dockables can be minimized. The minimized components are also
 * put in dockables. They can be dragged and docked in other docks.
 * They can also be moved in their bar.
 * 
 * There are some tool bars with buttons. The buttons can be moved in their
 * tool bar or dragged to other tool bars. The tool bars can also be dragged
 * to other borders or they can be made floating.
 * 
 * The structure of the application window is like this:
 * 		First there is a border dock for tool bars with buttons. 
 * 		Inside that border dock is a minimizer that minimizes the dockables at the borders. 
 * 		Inside the minimizer is a maximizer. 
 * 		Inside the maximizer is the root dock for all the normal docks.
 * 
 * When the application is stopped, the workspace is saved.
 * When the application is restarted again, the workspace is recovered.
 * The dockables, docks, minimized dockables, and button dockables
 * are showed in the same state as when they were closed.
 * 
 * This example uses a float dock model.
 * The model can be saved with a dock model encoder.
 * The model can be reloaded with a dock model decoder.
 * 
 * @author Heidi Rakels
 */

// GLOBAL TODO Section
//TODO: Have an option to 'lock' the toolbar so you can disable dragging of buttons.
//TODO: show a splash screen at startup.
//TODO: Move loading the objects lists so that the gui renders first and then attempts to load stuff. 
//TODO: Move relavent Njord1 todos over here.
//TODO: Get saving/deleting to bigip working.
//TODO: Later, let's not load an editor until something is doubleclicked on.
//TODO: Make ctr+f and the binoculars bring up the find window. If it's already open (either docked or floating) Then make it the active window/tab

/**
 * This will become Njord the Java iRules Editor.
 * 
 * I'm starting by modifying a sample from http://www.javadocking.com/
 * 
 * Njord, the first Java iRules Editor.
 * The Norse god of winds, sea and fire. He brings good fortune at sea and in the hunt. He is married to the giantess Skadi. His children are Freya and Freyr, whom he fathered on his own sister.
 * 
 * Originally, Njord was one of the Vanir but when they made peace with the Aesir, he and his children were given to them as hostages. The Aesir appointed both Njord and Freyr as high priests to preside over sacrifices. Freya was consecrated as sacrificial priestess. She taught the Aesir witchcraft, an art that was common knowledge among the Vanir.
 * http://en.wikipedia.org/wiki/Nj%C3%B6r%C3%B0r
 *  
 * Njord will begin it's life solely as an iRule editor but I'd like it to become a more complete bigip manager thus the name.
 * At minimum there will be functionality to enable/disable pool members and virtual servers and manage what virtual servers iRules are attached to.
 * 
 * THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
 * EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
 * MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * @author Aaron Forster
 * @version 2.0.12-SNAPSHOT
 * 
 */
public class Njord extends JPanel implements TUNotifiable, ActionListener, MouseListener, NjordSecurityProvider  {
//public class Njord extends JPanel implements TheUberlabNotifiable, ActionListener, TreeSelectionListener, TreeExpansionListener, WindowListener, HyperlinkListener  {
	// ###############################################################
	// VARIABLES
	// ###############################################################
	/** Holds the version ID */
	private static final long serialVersionUID = 2L;
	/** Holds the main window */
	protected JFrame mainFrame = null;
	/** The version of Njord */
	public static String NjordVersion = "2.0.12-SNAPSHOT";
	/** Holds the main window title. I should probably get this from somewhere else */
	public String mainWindowTitle = "Njord " + NjordVersion;
	/**
     * Holds the line separator string for the os we're running on.
     */
    public String nl = System.getProperty("line.separator");
	/** What to prefix log messages with */
	public String logPrefix = this.getClass().getName() + ": ";
	/**
	 * Holds our logger factory for SLF4J.
	 */
	public final Logger log = LoggerFactory.getLogger(Njord.class);

	/**
	 * The default width if we aren't pulling it from the saved prefs file.
	 */
	public static final int 			defaultFrameWidth 			= 900;
	/**
	 * The default Height if we aren't pulling it from the saved prefs file.
	 */
	public static final int 			defaultFrameHeight 			= 650;
	/**
	 * I'm not really sure yet.
	 */
	public static final String 			CENTER_DOCKING_PATH_ID  = "centerDockingPathId";
	/**
	 * An array which will store our look and feel.
	 */
	public static LAF[] 				LAFS;
	/**
	 * Not sure what this is for yet.
	 */
	static int newDockableCount = 0;
	/**
	 * Stores all preferences. 
	 * This includes BIGIP connectivity information, user preferences and eventually window geometry.
	 * NOTE: Currently window geometry is stored and retrieved completely independently of this object.
	 */
	public NjordPreferences preferences = null;
	/**
	 * The content provider.
	 */
	private NjordContentProvider njordContentProvidor = null;
	/**
     * navigationTree is the JTree that holds the list of iRules and all that which we are going to work on.
     */
    public NjordNavTree navTree = null;
    /**
     * Will hold the active editor panel.
     */
	public NjordEditorPanel lastActiveEditorPanel = null;
	/**
	 * Holds messages we wish to convey to the user.
	 */
	protected JTextArea noticesTextArea = new JTextArea("New");
	
	private ImageProvider imageProvider;
    /* 
     * #######################################################################
     * Variables directly related to the new Threaded MVC architecture
     * #######################################################################
     */
	
	/**
	 * The plugin manager will manage all our plugins.
	 */
	protected NjordPluginManager pluginManager;
	private NjordConcierge concierge;
	private NjordNotifier notifier;
	private ActionRegistry actionRegistry;
	private boolean busyState = false;
	
    /* 
     * #######################################################################
     * Editor colors
     * #######################################################################
     */
    
    //These are good enough for now.
    /**
     * The color to use for highlighting annotations
     */
    public Color annotationColor = Color.decode("#646464");
    /**
     * The color to use for highlighting variables
     */
    public Color variableColor = Color.decode("#0000C0");
//  public Color identifyerColor = Color.decode("#880080");
    /**
     * The color to use for highlighting comments
     */
    public Color commentColor = Color.decode("#3F7F5F"); // Leave this the same green for now but may override with this green from eclipse
    /**
     * The color to use for highlighting functions
     */
    public Color functionColor = Color.decode("#880080");
    /**
     * The color to use for highlighting regular expressions
     */
    public Color regexColor = Color.decode("#2A00FF");
    /**
     * The color to use for highlighting tcl built in commands
     */
    public Color reservedWordColor = Color.decode("#0033FF"); // TCL built in functions (not entirely accurate)
    /**
     * The color to use for highlighting iRules added commands
     */
    public Color reservedWord2Color = Color.decode("#000099"); // Irules stuff
    /**
     * The color to use for highlighting operators
     */
    public Color operatorColor = new Color(880080);
    /**
     * The color to use for highlighting quoted text
     */
    public Color doublequoteColor = Color.decode("#1a5a9a");
    /**
     * The color to use for highlighting backquoted text
     */
    public Color backquoteColor = Color.decode("#4a2a3a");
    /**
     * The color to use for highlighting brackets
     */
    public Color bracketColor = Color.decode("#990099");
    
    // Make Linkable items
    /**
     * The user's desktop environment
     */
    public Desktop desktop = java.awt.Desktop.getDesktop();
    /**
     * The beginning of an HTML link. <a href="
     */
    private static final String A_HREF = "<a href=\"";
    /**
     * The first closing bracked of an HTML link. \>.
     */
    private static final String HREF_CLOSED = "\">";
    /**
     * The closing item for an HTML link. </a>.
     */
    private static final String HREF_END = "</a>";
    /**
     * An open HTML tag.
     */
    private static final String HTML = "<html>";
    /**
     * The closing HTML tag.
     */
    private static final String HTML_END = "</html>";
    
    /* 
     * #######################################################################
     * Tab Docks
     * #######################################################################
     */
    
    /**
     * A placeholder for the center (main) tab dock.
     */
    TabDock centerTabbedDock = null;
    /**
     * A placeholder for the bottom (notifications) tab dock.
     */
    TabDock bottomTabbedDock = null;
    /**
     * A placeholder for the left (navigation) tab dock.
     */
    TabDock leftTabbedDock = null;
    //I May get rid of this one but maybe not. If I add something other than the find window it would be a good place for it. Maybe Function lookup, acronomicon functionality/etc.
    /**
     * A placeholder for the right (Reserved for future? Possibly find, etc?) tab dock.
     */
    TabDock rightTabbedDock = null;
    /**
     * A placeholder for the docking path of the center dock.
     */
    DockingPath centerDockingPath = null;
    /**
     * The find window.
     */
    NjordFindPanel find = null;
    
    
	// JavaDocking Fields.
	
	/** The ID for the owner window. */
	private String 						frameId 		= "frame";
	/** The model with the docks, dockables, and visualizers (a minimizer and a maximizer). */
	private FloatDockModel 				dockModel;
	/** All the dockables of the application. */
	private Dockable[] dockables;
	/** All the dockables for buttons of the application. */
//	private Dockable[] buttonDockables;
	/** The new button dockables for the actual buttons which will end up in the application. Needs to be renamed after the existing buttonDockables is deleted.*/
	private Dockable[] buttonDockablahs;
	
	
	
	
	//Some crazy crap to make the task list work until I get it figured out and working properl
//	public static final String AUTO_INSERT_CLOSING_CURLYS       = "MainView.autoInsertClosingCurlys";
//	public static final String CURRENT_DOCUMENT_PROPERTY        = "MainView.currentDocument";
//	public static final String DEFAULT_ENCODING_PROPERTY        = "MainView.defaultEncoding";
//	public static final String FILE_SIZE_CHECK_PROPERTY     = "MainView.fileSizeCheck";
//	public static final String FRACTIONAL_METRICS_PROPERTY      = "MainView.fractionalMetrics";
//	public static final String MARK_ALL_COLOR_PROPERTY      = "MainView.markAllColor";
//	public static final String MARK_OCCURRENCES_COLOR_PROPERTY  = "MainView.markOccurrencesColor";
//	public static final String MARK_OCCURRENCES_PROPERTY        = "MainView.markOccurrences";
//	public static final String MAX_FILE_SIZE_PROPERTY           = "MainView.maxFileSize";
//	public static final String REMEMBER_WS_LINES_PROPERTY       = "MainView.rememberWhitespaceLines";
//	public static final String ROUNDED_SELECTION_PROPERTY       = "MainView.roundedSelection";
//	public static final String SMOOTH_TEXT_PROPERTY         = "MainView.smoothText";
//	public static final String TEXT_AREA_ADDED_PROPERTY     = "MainView.textAreaAdded";
//	public static final String TEXT_AREA_REMOVED_PROPERTY   = "MainView.textAreaRemoved";
	
//	private TaskWindow taskList = null;
	
	// This is a stupid hack until I figure out why javadocking isn't saving the fetch iRules button when I use my custom action.
	private FetchiRules fetchiRulesAction;
	
	
	// ###############################################################
	// CONSTRUCTORS
	// ###############################################################
	
	public Njord(JFrame frame) {
		super(new BorderLayout());
		
        concierge = new NjordConcierge(this);
		//TODO: Should probably get the content provider and plugin manager here.
		actionRegistry = new ActionRegistry(concierge);
		fetchiRulesAction = actionRegistry.fetchiRules;
		// Create a content provider
        njordContentProvidor = new NjordContentProvider(concierge);
        imageProvider = concierge.getImageProvider();
        
        notifier = new NjordNotifier(concierge);
		try {
            notifier.registerForNotifications(this);
        } catch (TURegistrationFailedException e) {
            e.printStackTrace();
        }
		
		// Stuff the frame handle into here so we can access it when we need to.
		this.mainFrame = frame;
		
		// This seems out of place here but it doesn't work without it. I need to find a better place for it though since editors will use different tokenmakers
		// Create my tokenMakerFactory. This is the bit that does the syntax highlighting from RSyntaxTextArea. Technically it's more the bit 
		// that determines what is a token (a word) and what class that token is. Then the token class "command, comment, etc" determines how 
		// it should be highlighted.
		AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
        atmf.putMapping("SYNTAX_STYLE_IRULES", "com.theuberlab.njord.util.iRulesTokenMaker");
        TokenMakerFactory.setDefaultInstance(atmf); //Don't know if I need this line or not
        
        // Initialize our plugins
        initPlugins();
        
		frame.setIconImage(imageProvider.getIcon("NjordSquareLogo").getImage());
		
		// Retrieve preferences and create a container for them.
		preferences = new NjordPreferences(concierge);
		
		
		// Set our custom component factory.
		DockingManager.setComponentFactory(new WorkspaceComponentFactory());
		
		// Create a maximizer which will be used for any window which can be maximized.
		SingleMaximizer maximizer = new SingleMaximizer();
		
		// Create a minimizer which will be used for any window which can be minimized.
		BorderDocker borderDocker = new BorderDocker();
		DockingMinimizer minimizer = new DockingMinimizer(borderDocker);
		
		// Add an externalizer to the dock model which will be used for any window which can be externalized.
		Externalizer externalizer = new FloatExternalizer(frame);
		
	    // Create the default components. These are the commponents that will always open at startup like the navigation tree.
		
		// The message panel is a special tab that won't ever go away (though it can be minimized and externalized.
		// It will eventually fetch updates from some centralized place either on devcentral or on theuberlab.com's website.
		NjordMessagePanel news = new NjordMessagePanel(	"News: " + NewsFetcher.WelcomeNjord2.getTitle(), 
				NewsFetcher.getText(NewsFetcher.WelcomeNjord2), imageProvider.getIcon("NjordFullLogo"));
		
		
		//TODO: Wrapp all the task stuff in a check for showExperimental.
		//TODO: Found a purpose for this. A task list. Unclear how to turn this into a plugin. I think that's a v2.5 or later idea. table has become taskList. The object Table should become NjordTaskList
//		taskList = new TaskWindow(concierge, "FIXME|TODO|HACK");
		
		// The navigation tree.
		navTree = createNavTree();
		
		// Create some tab docks for the various sections of the editor
		// The center/editor dock
        centerTabbedDock = new TabDock();
        // The bottom/messages dock
        bottomTabbedDock = new TabDock();
        // The left/navigation dock
        leftTabbedDock = new TabDock();
        // the right/addtional dock. I May get rid of this one but maybe not. If I add something other than the find window it would 
        // be a good place for it. Maybe Function lookup, acronomicon functionality/etc.
        rightTabbedDock = new TabDock();
        
        // Create a find window. At some point I need to only create this if it's needed.
        find = new NjordFindPanel(this);
		find.setFocusable(true);
		
		
		
		// Right here I could create a Njord 1 style frame layout (but with the new RHS pane. 
		// Then each of the tab docks goes in one of these non-javadocking windows. Thus I still get the resizeweight control.
		
		
		
		
		
		// The arrays for the dockables and button dockables.
		// dockables holds the default windows like the nav tree, the find window the Notices and Welcome windows.
		dockables = new Dockable[4];
		//buttonDockablahs holds the buttons which go on the button bar.
		buttonDockablahs = new Dockable[11];
		
		//TODO: Update this to not iRule/iApp specific
		String navBarToolTipText;
		if (preferences.getDisplayExperimental()) {
			navBarToolTipText = "Local and remote iRules and iApps Templates";
		} else {
			navBarToolTipText = "Local and remote iRules";
		}
		
		

        noticesTextArea.setText(concierge.getContentProvider().getCMContent("MESSAGE_INFO_NJORD_STARTUP"));
		JScrollPane noticesScrollPane = new JScrollPane(noticesTextArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
		        ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//		noticesScrollPane.setH
		
		//TODO: I used to be screwing up where the welcome window goes in here. Confirm that this is resolved.
		// Create the dockables around the default components.
//		dockables[0] = createDockable("Navigation", navTree,	"Navigation",	new ImageIcon(getClass().getResource("/iconsInUse/iRuleIconClean.png")),   navBarToolTipText);
//		dockables[1] = createDockable("Find",	find,		"Find",		new ImageIcon(getClass().getResource("/iconsInUse/find.png")),"Find text");
////		dockables[2] = createDockable("Notices", noticesTextArea,        "Notices",  new ImageIcon(getClass().getResource("/iconsInUse/iRuleIconDirty.png")),    "Alerts and messages");
//		dockables[2] = createDockable("Notices", noticesScrollPane,        "Notices",  new ImageIcon(getClass().getResource("/iconsInUse/iRuleIconDirty.png")),    "Alerts and messages");
//		dockables[3] = createDockable("Welcome",	news,		"Welcome To Njord",		new ImageIcon(getClass().getResource("/images/text12.gif")),     "<html>Welcome to Njord " + NjordVersion + "<html>");
////		dockables[4] = createDockable("Tasks",	taskList,		"Tasks",	new ImageIcon(getClass().getResource("/images/table12.gif")),    "A filterable task list (NOT YET IMPLEMENTED)");
		
		dockables[0] = createDockable("Navigation", navTree,  "Navigation",   imageProvider.getIcon("DefDockNavigation"),   navBarToolTipText);
        dockables[1] = createDockable("Find", find, "Find", imageProvider.getIcon("DefDockFind"),"Find text");
//      dockables[2] = createDockable("Notices", noticesTextArea,        "Notices",  new ImageIcon(getClass().getResource("/iconsInUse/iRuleIconDirty.png")),    "Alerts and messages");
        dockables[2] = createDockable("Notices", noticesScrollPane,        "Notices",  imageProvider.getIcon("DefDockNotices"),    "Alerts and messages");
        dockables[3] = createDockable("Welcome",    news,       "Welcome To Njord",     imageProvider.getIcon("DefDockrWelcome"),     "<html>Welcome to Njord " + NjordVersion + "<html>");
//      dockables[4] = createDockable("Tasks",  taskList,       "Tasks",    imageProvider.getIcon("ToolbarTasks"),    "A filterable task list (NOT YET IMPLEMENTED)");
		
        
        
        
        
        
		
		
		// The dockable with the find panel may not be maximized.
		((DefaultDockable)dockables[1]).setPossibleStates(DockableState.CLOSED | DockableState.NORMAL | DockableState.MINIMIZED | DockableState.EXTERNALIZED);
		
		// Add all default actions to the rest of the dockables.
		for (int index = 0; index < dockables.length; index++) {
			if (index == 1) {
				// All actions, except the maximize.
				dockables[index] = addLimitActions(dockables[index]);
			} else {
				// All actions.
				dockables[index] = addAllActions(dockables[index]);
			}
		}
		//TODO: Change the icon for the messages window from the iRule one to error.png
		//TODO: Add a "Deploy to BIGIP" button
		//TODO: This whole thing needs to go and the plugin needs to provide buttons.
		// The Button Dockables
		// Most of these don't actually do anything yet.
		// Going to only have new, save and delete instead of object specific ones. Then things like 'new' which bring up a dialog can just ask you which one.
		
		
		
//		buttonDockablahs[0]  = createButtonDockable("ButtonDockablahAdd",			"New",		new ImageIcon(getClass().getResource("/iconsInUse/script_add.png")));
//		// I don't think I need an edit button but i'm leaving it here in case I remember why I thought I did.
//		//		buttonDockablahs[1]  = createButtonDockable("ButtonDockablahEdit",			"Edit",		new ImageIcon(getClass().getResource("/iconsInUse/script_edit.png")),			"Edit!");
//		buttonDockablahs[1]  = createButtonDockable("ButtonDockablahSave",			"Save",		new ImageIcon(getClass().getResource("/iconsInUse/script_save.png")));
//		buttonDockablahs[2]  = createButtonDockable("ButtonDockablahDelete",		"Delete",		new ImageIcon(getClass().getResource("/iconsInUse/script_delete.png")));
//		//iApps, I need to replace layout_link w/ one that has <> instead of a link. Layout sidebar will be help, layout_content will be presentation and layout link will be implemenatation until I replace it. Assuming I use them at all. For now I just need add/delete/etc.
////		buttonDockablahs[4]  = createButtonDockable("ButtonDockablahAddTemplate",	"Add Template",		new ImageIcon(getClass().getResource("/iconsInUse/layout_add.png")),				"Add iApps Template!");
////		buttonDockablahs[5]  = createButtonDockable("ButtonDockablahEditTemplate",	"Edit Template",	new ImageIcon(getClass().getResource("/iconsInUse/layout_edit.png")),			"Edit iApps Template!");
////		// Until I can grab layout_save from the sprited image.
////		buttonDockablahs[6]  = createButtonDockable("ButtonDockablahSaveTemplate",	"Save Template",	new ImageIcon(getClass().getResource("/iconsInUse/layout_add.png")),			"Save iApps Template!");
////		buttonDockablahs[7]  = createButtonDockable("ButtonDockablahDeleteTemplate",	"Delete Template",	new ImageIcon(getClass().getResource("/iconsInUse/layout_delete.png")),			"Delete iApps Template!");
////		//Connect
//		//Do I want this here or just a connect and fetch under each bigip?
//		buttonDockablahs[3]  = createButtonDockable("ButtonDockablahConnect",		"Connect",			new ImageIcon(getClass().getResource("/iconsInUse/connect.png")));
//		
//		
//		
////		buttonDockablahs[4]  = createButtonDockable("ButtonDockablahFetch",     "Fetch",          new ImageIcon(getClass().getResource("/iconsInUse/page_white_put.png")));
//		
////		buttonDockablahs[4]  = createButtonDockable(actionRegistry.fetchiRules);
////		buttonDockablahs[4]  = createButtonDockable(actionRegistry.fetchiRules, new ImageIcon(getClass().getResource("/iconsInUse/page_white_put.png")));
//		
//		
//		// This is a really gammy hack. I'm going to create the real action globally then have this.acttionperformed call it. eff.
//		buttonDockablahs[4]  = createButtonDockable("ButtonDockablahFetch",     "Fetch",          new ImageIcon(getClass().getResource("/iconsInUse/page_white_put.png")));
		
		
		
		
		buttonDockablahs[0]  = createButtonDockable("ButtonDockablahAdd",           "New", imageProvider.getIcon("ButtonDockablahAdd"));
        // I don't think I need an edit button but i'm leaving it here in case I remember why I thought I did.
        //      buttonDockablahs[1]  = createButtonDockable("ButtonDockablahEdit",          "Edit",     new ImageIcon(getClass().getResource("/iconsInUse/script_edit.png")),           "Edit!");
        buttonDockablahs[1]  = createButtonDockable("ButtonDockablahSave",          "Save",     imageProvider.getIcon("ButtonDockablahSave"));
        buttonDockablahs[2]  = createButtonDockable("ButtonDockablahDelete",        "Delete",       imageProvider.getIcon("ButtonDockablahDelete"));
        //iApps, I need to replace layout_link w/ one that has <> instead of a link. Layout sidebar will be help, layout_content will be presentation and layout link will be implemenatation until I replace it. Assuming I use them at all. For now I just need add/delete/etc.
//      buttonDockablahs[4]  = createButtonDockable("ButtonDockablahAddTemplate",   "Add Template",     new ImageIcon(getClass().getResource("/iconsInUse/layout_add.png")),                "Add iApps Template!");
//      buttonDockablahs[5]  = createButtonDockable("ButtonDockablahEditTemplate",  "Edit Template",    new ImageIcon(getClass().getResource("/iconsInUse/layout_edit.png")),           "Edit iApps Template!");
//      // Until I can grab layout_save from the sprited image.
//      buttonDockablahs[6]  = createButtonDockable("ButtonDockablahSaveTemplate",  "Save Template",    new ImageIcon(getClass().getResource("/iconsInUse/layout_add.png")),            "Save iApps Template!");
//      buttonDockablahs[7]  = createButtonDockable("ButtonDockablahDeleteTemplate",    "Delete Template",  new ImageIcon(getClass().getResource("/iconsInUse/layout_delete.png")),         "Delete iApps Template!");
//      //Connect
        //Do I want this here or just a connect and fetch under each bigip?
        buttonDockablahs[3]  = createButtonDockable("ButtonDockablahConnect",       "Connect",imageProvider.getIcon("ButtonDockablahConnect"));
        // This is a really gammy hack. I'm going to create the real action globally then have this.acttionperformed call it. eff.
        buttonDockablahs[4]  = createButtonDockable("ButtonDockablahFetch",     "Fetch", imageProvider.getIcon("ButtonDockablahFetch"));
        
		//Server icons add, delete
		buttonDockablahs[5]  = createButtonDockable("ButtonDockablahAddBIG-IP",		"Add BIG-IP", imageProvider.getIcon("ButtonDockablahAddBIG-IP"));
		buttonDockablahs[6]  = createButtonDockable("ButtonDockablahDeleteBIG-IP",	"Delete BIG-IP", imageProvider.getIcon("ButtonDockablahDeleteBIG-IP"));
		buttonDockablahs[7]  = createButtonDockable("ButtonDockablahEditBIG-IP",	"Edit BIG-IP", imageProvider.getIcon("ButtonDockablahEditBIG-IP"));
		//find
		buttonDockablahs[8]  = createButtonDockable("ButtonDockablahFind",			"Search", imageProvider.getIcon("ButtonDockablahFind"));
		//undo/redo
		buttonDockablahs[9]  = createButtonDockable("ButtonDockablahUndo",			"Undo",	imageProvider.getIcon("ButtonDockablahUndo"));
		buttonDockablahs[10]  = createButtonDockable("ButtonDockablahRedo",			"Redo", imageProvider.getIcon("ButtonDockablahRedo"));
		
        
        
        
        
		
		
		NjordDockModelPropertiesDecoder dockModelDecoder = null;
		if (preferences.getSaveWindowGeometry()) {
		    log.debug("Loading workspace geometry");
	        // The DockModelPropertiesDecoder is the bit from javadocking that handles saving geometry settings
	        dockModelDecoder = new NjordDockModelPropertiesDecoder();
	        if (dockModelDecoder.canDecodeSource(preferences.getLayoutPrefsFilePath())) {
	            try  {
	                // Create the map with the dockables, that the decoder needs.
	                Map<String,Dockable> dockablesMap = new HashMap<String,Dockable>();
	                for (int index = 0; index < dockables.length; index++) {
	                    dockablesMap.put( dockables[index].getID(), dockables[index]); 
	                } for (int index = 0; index < buttonDockablahs.length; index++) {
	                    dockablesMap.put( buttonDockablahs[index].getID(), buttonDockablahs[index]);
	                }           
	                
	                // Create the map with the owner windows, that the decoder needs.
	                Map<String, JFrame> ownersMap = new HashMap<String, JFrame>();
	                ownersMap.put(frameId, frame);
	                
	                // Create the map with the visualizers, that the decoder needs.
	                Map visualizersMap = new HashMap();
	                visualizersMap.put("maximizer", maximizer);
	                visualizersMap.put("minimizer", minimizer);
	                visualizersMap.put("externalizer", externalizer);
	                
	                // layoutPrefsFile is the dock settings. it's currently called workspace_1_5.dck
	                // Decode the file.
	                dockModel = (FloatDockModel)dockModelDecoder.decode(preferences.getLayoutPrefsFilePath(), dockablesMap, ownersMap, visualizersMap);
	            } catch (FileNotFoundException fileNotFoundException) {
	                //TODO: Move this log bit to the exception handler?
	                log.warn("Could not find the file [{}] with the saved dock model.", preferences.getLayoutPrefsFilePath());
	                log.warn("Continuing with the default dock model.");
	                
	                concierge.getNotifier().handleException(fileNotFoundException);
//	                NjordExceptionHandler exceptionHandler = new NjordExceptionHandler(fileNotFoundException, concierge);
//	                exceptionHandler.processException();
	            } catch (IOException ioException){
	                //TODO: Move this log bit to the exception handler?
	                log.error("Could not decode a dock model: [{}].", ioException);
	                ioException.printStackTrace();
	                log.error("Continuing with the default dock model.");
	                
	                F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(ioException, concierge);
	                exceptionHandler.processException();
	                
//	                NjordExceptionHandler exceptionHandler = new NjordExceptionHandler(e, concierge);
//	                exceptionHandler.processException();
	            }
	        }
	        
		}
		if (!preferences.getSaveWindowGeometry()) {
		    log.debug("Geometry saving disabled, loading with default settings.");
		}
		
		// These are the root docks.
		SplitDock totalSplitDock = null;
		BorderDock toolBarBorderDock = null;
		BorderDock minimizerBorderDock = null;
		
		//TODO: here in the default would be the best place to put my first run. If we have prefs it doesn't show up. THAT bit should be floating.
		if (dockModel == null) {
			// Create the dock model for the docks because they could not be retrieved from a file.
            dockModel = new FloatDockModel(preferences.getLayoutPrefsFilePath());
			dockModel.addOwner(frameId, frame);
			
			// Give the dock model to the docking manager.
			DockingManager.setDockModel(dockModel);
			
			// Add the dockables to these tab docks.
			centerTabbedDock.addDockable(dockables[3], new Position(0)); // Set the welcome tab to the center dock.
			
			centerTabbedDock.setSelectedDockable(dockables[3]); // Now select that dock. 
			bottomTabbedDock.addDockable(dockables[2], new Position(0)); // Set the notices tab to the bottom dock.
			leftTabbedDock.addDockable(dockables[0], new Position(0)); // Set the Navigation tab to the left dock.
//			rightTabbedDock.addDockable(dockables[4], new Position(0)); // Set the table tab to the right dock. This should probably be only find unless table becomes useful.
			rightTabbedDock.addDockable(dockables[1], new Position(0)); // Set the find tab to the right dock.
			// Somewhere in here I think I want to use PointDockHeader
			// I need some bar so that the whole box of dockables can be dragged.
			// Nav = 0
			// Find = 1
			// Notices = 2
			// Welcome = 3
			// Table = 4			
			
			Dimension centerDimension = centerTabbedDock.getSize();
			log.debug("centerTabbedDock is H[{}] by W[{}] before screwing with it.", centerDimension.height, centerDimension.width);
//			centerTabbedDock.setMinimumSize(new Dimension(700, 700));
//			centerTabbedDock.setPreferredSize(new Dimension(2500, 2500));
//			centerTabbedDock.setPreferredSize(new Dimension(2500, 2500));
			
			// The 4 windows have to be splittable.
			SplitDock centerSplitDock = new SplitDock();
			centerSplitDock.addChildDock(centerTabbedDock, new Position(Position.CENTER));
			centerSplitDock.addChildDock(rightTabbedDock, new Position(Position.RIGHT));

			//TODO: there's some messing about here I should be able to do.
			centerSplitDock.setDividerLocation(500);
			
			SplitDock bottomSplitDock = new SplitDock();
			bottomSplitDock.addChildDock(bottomTabbedDock, new Position(Position.CENTER));
			SplitDock rightSplitDock = new SplitDock();
			rightSplitDock.addChildDock(centerSplitDock, new Position(Position.CENTER));
			rightSplitDock.addChildDock(bottomSplitDock, new Position(Position.BOTTOM));
			rightSplitDock.setDividerLocation(380);
			SplitDock leftSplitDock = new SplitDock();
			leftSplitDock.addChildDock(leftTabbedDock, new Position(Position.CENTER));
			totalSplitDock = new SplitDock();
			totalSplitDock.addChildDock(leftSplitDock, new Position(Position.LEFT));
			totalSplitDock.addChildDock(rightSplitDock, new Position(Position.RIGHT));
			totalSplitDock.setDividerLocation(180);
			
			//TODO: Figure out how to make the centerTabbedDock always display even when nothing is in it so that when you minimise/maximise or just open new rules they will always go there.
			// I'm going to try and handle this for now by having a notices window which is like the pane that loads when Njord1 starts. This will probably need to be excluded from geometry saving so that it always loads at startup even if you've closed it previously. Once I get Njord2 to the point where it will restore previously opened iRules then I can stop forcing it. 
			// I think this bit here actually causes it to not be removed but doesn't actually do anything about making a big white block there.
			
			// When this SplitDock has only one child that is empty, it will not be removed.
			// This SplitDock will never be empty and will never be removed.
			// This is the main panel of the application where the windows of the application will be added.
			centerSplitDock.setRemoveLastEmptyChild(false);
			
			// Add the root dock to the dock model.
			dockModel.addRootDock("totalDock", totalSplitDock, frame);
			
			// The find dockable should float. Add dockable 1 to the float dock of the dock model (this is a default root dock).
//			FloatDock floatDock = dockModel.getFloatDock(frame);
//			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//			floatDock.addDockable(dockables[1], new Point(screenSize.width / 2 + 100, screenSize.height / 2 + 60),new Point());
			
			// Add the maximizer to the dock model.
			dockModel.addVisualizer("maximizer", maximizer, frame);
			
			// Create the border dock of the minimizer.
			minimizerBorderDock = new BorderDock(new ToolBarDockFactory());
			minimizerBorderDock.setMode(BorderDock.MODE_MINIMIZE_BAR);
			minimizerBorderDock.setCenterComponent(maximizer);
			borderDocker.setBorderDock(minimizerBorderDock);
			
			// Add the minimizer to the dock model.
			dockModel.addVisualizer("minimizer", minimizer, frame);
			
			// Create the tool bar border dock for the buttons.
			toolBarBorderDock = new BorderDock(new CompositeToolBarDockFactory(), minimizerBorderDock);
			toolBarBorderDock.setMode(BorderDock.MODE_TOOL_BAR);
			CompositeLineDock mainButtonsDock = new CompositeLineDock(CompositeLineDock.ORIENTATION_HORIZONTAL, false,
					new ToolBarDockFactory(), DockingMode.HORIZONTAL_TOOLBAR, DockingMode.VERTICAL_TOOLBAR);
			CompositeLineDock compositeToolBarDock2 = new CompositeLineDock(CompositeLineDock.ORIENTATION_VERTICAL, false,
					new ToolBarDockFactory(), DockingMode.HORIZONTAL_TOOLBAR, DockingMode.VERTICAL_TOOLBAR);
			toolBarBorderDock.setDock(mainButtonsDock, Position.TOP);
			toolBarBorderDock.setDock(compositeToolBarDock2, Position.LEFT);
			
			// Add this dock also as root dock to the dock model.
			dockModel.addRootDock("toolBarBorderDock", toolBarBorderDock, frame);
			
			// The line docks for the buttons.
			LineDock newSaveDeleteDock = new LineDock(LineDock.ORIENTATION_HORIZONTAL, false, DockingMode.HORIZONTAL_TOOLBAR, DockingMode.VERTICAL_TOOLBAR);
			LineDock manageBigipsDock = new LineDock(LineDock.ORIENTATION_HORIZONTAL, false, DockingMode.HORIZONTAL_TOOLBAR, DockingMode.VERTICAL_TOOLBAR);
			LineDock editUndoDock = new LineDock(LineDock.ORIENTATION_HORIZONTAL, false, DockingMode.HORIZONTAL_TOOLBAR, DockingMode.VERTICAL_TOOLBAR);
//			LineDock toolBarDock4 = new LineDock(LineDock.ORIENTATION_HORIZONTAL, false, DockingMode.HORIZONTAL_TOOLBAR, DockingMode.VERTICAL_TOOLBAR);
			
			// Add the button dockables to the line docks.
			// iRule buttons
			newSaveDeleteDock.addDockable(buttonDockablahs[0],  new Position(0));
			newSaveDeleteDock.addDockable(buttonDockablahs[1],  new Position(1));
			newSaveDeleteDock.addDockable(buttonDockablahs[2],  new Position(2));
//			buttonDockablahs[0].
			
			// 3-6 connect, fetch, add, edit, delete
			manageBigipsDock.addDockable(buttonDockablahs[3],  new Position(0));
			
			manageBigipsDock.addDockable(buttonDockablahs[4], new Position(1));
			manageBigipsDock.addDockable(buttonDockablahs[5], new Position(2));
			manageBigipsDock.addDockable(buttonDockablahs[6], new Position(3));
			manageBigipsDock.addDockable(buttonDockablahs[7], new Position(4));
			
			// This does nothing.
//			manageBigipsDock.setDebugGraphicsOptions(DebugGraphics.BUFFERED_OPTION);

			// Search Undo/redo
//			editUndoDock.addDockable(buttonDockablahs[8], new Position(0));
			editUndoDock.addDockable(buttonDockablahs[9], new Position(1));
			editUndoDock.addDockable(buttonDockablahs[10], new Position(2));

			// Add the button line docks to their composite parents.
			mainButtonsDock.addChildDock(newSaveDeleteDock, new Position(0));
			mainButtonsDock.addChildDock(manageBigipsDock, new Position(1));
			mainButtonsDock.addChildDock(editUndoDock, new Position(2));
//			compositeToolBarDock1.addChildDock(toolBarDock4, new Position(3));
			
			// Add an externalizer to the dock model. An externalizer I believe is the bit that allows us to pop it out.
			dockModel.addVisualizer("externalizer", externalizer, frame);
			
			// Add the paths of the docked dockables to the model with the docking paths.
			addDockingPath(dockables[0]);
			addDockingPath(dockables[1]);
			addDockingPath(dockables[2]);
			addDockingPath(dockables[3]);
//			addDockingPath(dockables[4]);
			
			// This whole section is un-neccesary since I'm not docking anything by default.
			// Leaving it here as an example of how to set docks docked, minimized, etc.
			
			// Add the path of the dockables that are not docked already.
			// We want dockable 8 to be docked, when it is made visible, where dockable 2 is docked.
//			DockingPath dockingPathToCopy11 = DockingManager.getDockingPathModel().getDockingPath(dockables[2].getID());
//			DockingPath dockingPath8 = DefaultDockingPath.copyDockingPath(dockables[8], dockingPathToCopy11);
//			DockingManager.getDockingPathModel().add(dockingPath8);
//			// We want dockable 3, 4, 5, 6, 7 to be docked, when they are made visible, where dockable 3 is docked.
//			DockingPath dockingPathToCopy1 = DockingManager.getDockingPathModel().getDockingPath(dockables[3].getID());
////			DockingPath dockingPath3 = DefaultDockingPath.copyDockingPath(dockables[3], dockingPathToCopy1);
//			DockingPath dockingPath4 = DefaultDockingPath.copyDockingPath(dockables[4], dockingPathToCopy1);
//			DockingPath dockingPath5 = DefaultDockingPath.copyDockingPath(dockables[5], dockingPathToCopy1);
//			DockingPath dockingPath6 = DefaultDockingPath.copyDockingPath(dockables[6], dockingPathToCopy1);
//			DockingPath dockingPath7 = DefaultDockingPath.copyDockingPath(dockables[7], dockingPathToCopy1);
////			DockingManager.getDockingPathModel().add(dockingPath3);
//			DockingManager.getDockingPathModel().add(dockingPath4);
//			DockingManager.getDockingPathModel().add(dockingPath5);
//			DockingManager.getDockingPathModel().add(dockingPath6);
//			DockingManager.getDockingPathModel().add(dockingPath7);
			
			// The docking path where very new windows will be placed.
			centerDockingPath = DefaultDockingPath.copyDockingPath(CENTER_DOCKING_PATH_ID, DockingManager.getDockingPathModel().getDockingPath(dockables[0].getID()));
		} else {
		    // Since the docking model is NOT null that means we found the preference. Let's use those instead of the defaults.
			// Get the root dock from the dock model.
			totalSplitDock = (SplitDock)dockModel.getRootDock("totalDock");
			toolBarBorderDock = (BorderDock)dockModel.getRootDock("toolBarBorderDock");
			
			// Get the border dock of the minimizer. Set it as border dock to the border docker.
			minimizerBorderDock = (BorderDock)toolBarBorderDock.getChildDockOfPosition(Position.CENTER);
			minimizerBorderDock.setCenterComponent(maximizer);
			borderDocker.setBorderDock(minimizerBorderDock);
			
			// Get the docking path where very new windows have to be docked.
			centerDockingPath = dockModelDecoder.getCenterDockingPath();
			
			// As a freak workaround for the dissapearing buttons thing I'm not going to allow you to drag the buttons within the button docks. 
			//TODO: SO add buttons into button docks here.
			
			
			// Here's a dirty hack, let's add the fetch iRules anyway.
//			buttonDockablahs[4]  = createButtonDockable(actionRegistry.fetchiRules);
			// Not fisible here, I'd have to get the manageBigipsDock but on a new load that will be something anonymous
//			manageBigipsDock.addDockable(buttonDockablahs[4], new Position(1));
			
			// This might be directions on how to do what I want.
//			http://www.javadocking.com/developerguide/actions.html
		}
		
		// Listen when the frame is closed. The workspace should be saved.
		frame.addWindowListener(new WorkspaceSaver(concierge, centerDockingPath));
		
		// Add the content to the maximize panel.
		maximizer.setContent(totalSplitDock);
		
		// Add the border dock of the minimizer to the panel.
		add(toolBarBorderDock, BorderLayout.CENTER);
		
		// Create the menubar.
		JMenuBar menuBar = createMenu(dockables, centerDockingPath);
		frame.setJMenuBar(menuBar);
		
        for (BigIP bigip: preferences.getBIGIPS()) {
            //TODO: the NjordTreeNode should be able to contain the BIGIP object so I can just fetch connection information from that.
//          NjordTreeNode bigipNode = new NjordTreeNode(bigip.getName(), NjordConstants.NODE_TYPE_REMOTE);
//            NjordTreeNode bigipNode = new NjordTreeNode(bigip);
            
            //TODO: The section 'iRules' for each bigip isn't showing up as a collapsable folder.
//            remoteTree.add(bigipNode);
            //TODO: Build the nav tree for this bigip once we click on it. This here should make it so we don't automatically fetch the list if it's configured not to but currently we have no way to subsequently connect.
            
            NjordTreeNode bigipNode = navTree.getNodeForBigIP(bigip);
            //Moving this bit back into Njord so I can get the gui buit before populating the list.
            if (bigip.getConnectAutomatically()) {
                    buildNodes(bigipNode, NjordNodeType.NODE_TYPE_ORIGIN, NjordNodeContext.NODE_CONTEXT_REMOTE, "IRULE");
                    // Expand the tree by default here.
            }
            
            log.debug("centerTabbedDock is H[{}] by W[{}] Here.", centerTabbedDock.getHeight(), centerTabbedDock.getWidth());
        }
		
//      if (bigip.getConnectAutomatically()) {
//      owner.buildNodes(bigipNode, NjordNodeType.NODE_TYPE_ORIGIN, NjordNodeContext.NODE_CONTEXT_REMOTE, "IRULE");
//}
		
	}
	
	// ###############################################################
	// METHODS
	// ###############################################################
	
	// ###############################################################
	// NEW METHODS CREATED SPECIFICALLY FOR NJORD2
	
	/**
	 * Create the navigation tree. This might actually be moved inside of NjordNavTree. I have to check.
	 * 
	 * @return A NjordNavTree with content.
	 */
	public NjordNavTree createNavTree() {
		log.debug("Running createNavTree()");
		NjordNavTree thisNavTree = new NjordNavTree(concierge);
		
		buildNodes(thisNavTree.localiRulesCategory, NjordNodeType.NODE_TYPE_ORIGIN, NjordNodeContext.NODE_CONTEXT_LOCAL, "IRULE");
		
	    if (preferences.getDisplayExperimental()) {
	        buildNodes(thisNavTree.localiAppsTemplatesCategory, NjordNodeType.NODE_TYPE_ORIGIN, NjordNodeContext.NODE_CONTEXT_LOCAL, "IAPPS_TEMPLATES");
	    }
		return thisNavTree;
	}
	
    /**
     * Builds a portion of the navigation tree.
     * 
     * @param tree
     *            A JTree element to build the local tree under.
     * @param tree
     * @param nodeType
     * @param nodeContext
     * @param nodeCategory
     */
    public void buildNodes(NjordTreeNode tree, NjordNodeType nodeType, NjordNodeContext nodeContext, String nodeCategory) {
        log.debug("Running buildNodes(NjordTreeNode tree, NjordNodeType [{}], NjordNodeContext [{}], NjordNodeCategory [{}])", nodeType, nodeContext,
                nodeCategory);
        if (nodeContext.equals(NjordNodeContext.NODE_CONTEXT_LOCAL)) {
            // check to see that the local folder exists.
            // If not return nuthin'
            // if so get the list of the files and create editors for them the way that we do for remote but with a local path.
            JFileChooser fc = new JFileChooser();
            FileSystemView fv = fc.getFileSystemView();
            File documentsDir = fv.getDefaultDirectory();// I should move this to something global so I'm not getting it all the time.
            
            String localObjectsDirPath = null;
            
            // This should be localObjectsDirPath = pluginManager.getLocalPathFor(node.getCategory); but I'm not ready for that yet.
            localObjectsDirPath = documentsDir.getAbsolutePath() + pluginManager.getLocalPathFor(nodeCategory);
            
            File localObjectsDir = new File(localObjectsDirPath);
            
            File[] localObjects = null;
            if (localObjectsDir.exists()) {
                localObjects = localObjectsDir.listFiles(); // or perhaps I should use listFiles(fileFilter[])
                if (localObjects != null) {
                    // There are rules in this list let's add them to the tree
                    for (File thisFile : localObjects) {
                        // Interestingly editing and saving are working fine but loading from disk is not
                        String objectName = thisFile.getName();
                        log.debug("Adding entry for object [{}]", objectName);
                        // Not sure why I had this here.
                        // NjordFileLocation objectLocation = newNjordFileLocation(concierge, objectName, thisFile);
                        
                        // TODO: Replace this bit with something generic so I can do the plugin thing.
                        NjordRuleEditorPanel editorPane = null;
                        
                        NjordTreeNode addObject = null;
                        // switch (nodeType) {
                        // case NODE_TYPE_IAPPS_TEMPLATE: {
                        NjordFileLocation loc = new NjordFileLocation(concierge, objectName, thisFile.getAbsoluteFile());
                        editorPane = new NjordRuleEditorPanel(concierge, loc, objectName);
                        addObject = new NjordTreeNode(editorPane);
                        // break;
                        // }
                        // case NODE_TYPE_IRULE: {
                        // NjordFileLocation loc = new NjordFileLocation(concierge, objectName, thisFile.getAbsoluteFile());
                        // editorPane = new NjordRuleEditorPanel(this, loc, objectName);
                        // addObject = new NjordTreeNode(editorPane);
                        // break;
                        // }
                        // }
                        
                        tree.add(addObject);
                    }
                }
            }
        } else if (nodeContext.equals(NjordNodeContext.NODE_CONTEXT_REMOTE)) {
            // TODO: I need to properly handle connection timeout in here.
            log.debug("Building nodes for BIG-IP [{}]", tree);
            // TODO: Don't do this here, make NjordTreeNode store the whole thing.
            BigIP thisBigIp = tree.getBigIP();
            // thisBigIp.setInterface(getConnectionTo(thisBigIp));
            iControl.Interfaces ic = getConnectionTo(thisBigIp);
            
            // TODO: I need to detect a double click on the bigip node and trigger this method if that happens. Connect and fetch rules should also be options
            // under right click of said node.
            
            NjordTreeNode remoteiRulesCategory = new NjordTreeNode("iRules", NjordNodeType.NODE_TYPE_CATEGORY);
            tree.add(remoteiRulesCategory);
            
            NjordTreeNode addRule = null;
            thisBigIp.setiRulesNames(getiRuleList(thisBigIp.getInterface()));
            
            if (thisBigIp.getiRulesNames().isEmpty()) {
                tuNotify("No valid iRules found on BIGIP!");
                // resultsPanelNoticesBox.setText("No valid iRules found on BIGIP.");
                // TextEditorPane ruleEditor = getEditorForRule(null, "No iRules Found On BIGIP");
                NjordRuleEditorPanel ruleEditor = new NjordRuleEditorPanel(concierge, null, "No iRules Found On BIGIP");
                NjordTreeNode bogusEmptyRuleNode = new NjordTreeNode(ruleEditor);
                // addRule = new NjordTreeNode(ruleEditor);
                tree.add(bogusEmptyRuleNode);
            } else {
                // This is for debugging purposes
                // int childcount = tree.getChildCount();
                // TODO: DO this better. Something like tree.getPath then check if we're looking for an iRule or a template (or however I do this for plugin
                // method) Then treepath + category.
                NjordTreeNode categoryNode = (NjordTreeNode) tree.getFirstChild();
                // List<String> theRules = thisBigIp.getiRulesNames();
                ArrayList<String> theRules = (ArrayList<String>) thisBigIp.getiRulesNames();
                // for (String rule : theRules) {
                for (Iterator<String> it = theRules.iterator(); it.hasNext();) {
                    String rule = it.next();
                    
                    if (!preferences.getShwSysiRules()) {
                        log.debug("Checking sys iRules for rule [{}]", rule);
                        if (rule.matches("/Common/_sys_.*")) { // V11 syntax
                            log.info("Sys iRule, Skipping");
                            // This is ALSO generating an unsupportedOperationException? WTF?
                            it.remove();
                            // This is generating an unsupportedOperationException?
                            // theRules.remove(rule);
                            continue;
                        } else if (rule.matches("_sys_.*")) { // V10 and maybe V9
                            log.info("Sys iRule, Skipping");
                            it.remove();
                            // theRules.remove(rule);
                            continue;
                        } else {
                            log.debug("Non-sys iRule, keeping");
                        }
                    }
                    // This is crap but until I get the time to work out a better way to do the GTM iRules as well we're still assuming LTM only.
                    NjordFileLocation ruleLocation = new NjordFileLocation(concierge, rule, NjordNodeCategory.NODE_CATEGORY_LOCAL_IRULE, ic);
                    NjordRuleEditorPanel ruleEditor = new NjordRuleEditorPanel(concierge, ruleLocation, rule);
                    NjordTreeNode addRuleNode = new NjordTreeNode(ruleEditor);
                    
                    categoryNode.add(addRuleNode);
                }
                // Don't forget to write the truncated list back to thisBigIp
                thisBigIp.setiRulesNames(theRules);
            }
            
        } else {
            // TODO: Invalid type. Make an error happen here.
        }
    }
    
    //hrm.. maybe not
    protected void buildNodesFromList(NjordTreeNode tree, List<String> list, NjordNodeContext nodeContext, NjordNodeCategory category) {
        if (nodeContext.equals(NjordNodeContext.NODE_CONTEXT_REMOTE)) {
            
        } else {
            //local
        }
    }
    
    /**
     * This should become plugin.getObjectsList()
     * 
     * getiRuleList returns only a string list of the names of the iRules instead of actual iRule objects 
     * 
     * @return The list of iRules from the BIGIP.
     * @throws Exception
     */
    public List<String> getiRuleList(iControl.Interfaces ic) {
        // LTM and GTM are the only two sections that have iRules.
        String[] LTMiRules = null;
        String[] GTMiRules = null;
        List<String> iRules = null;
        
        try {
            LTMiRules = ic.getLocalLBRule().get_list();
        } catch (RemoteException e) {
            F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
            exceptionHandler.processException();
        } catch (Exception e) {
            F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
            exceptionHandler.processException();
        }
        try {
            GTMiRules = ic.getGlobalLBRule().get_list();
//          iRules = ic.getGlobalLBRule().get_list();
        } catch (RemoteException e) {
            F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
            exceptionHandler.processException();
        } catch (Exception e) {
            F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
            exceptionHandler.processException();
        }
        //This is crap but until I come up with a better way we're still only going to do LTM iRules.       
        iRules = Arrays.asList(LTMiRules);
        //TODO: Make this part work. I'm pretty sure if we try and edit a GTM iRule we won't be able to save it. This is because I'm just adding everything so I don't know which is a local and which is a remote.
        //Not sure if this will work. I'll have to be able to determine the rule type when i try and save it.
        //TODO: O.K. I'm NOT going to make fetching of GTM iRules work together with the local iRules. GTM (or "global iRules") will be handled by a separate plugin.
//        iRules.addAll(Arrays.asList(GTMiRules));
        
        log.info("Available Rules");
        for (String string : iRules) {
            log.debug("   " + string);
        }
        return iRules;
    }
    
    // public void valueChanged(TreeSelectionEvent e) was moved to NjordNavTree 
    
    /**
     * actionPerformed is the method that listens for events like clicking a button or a menu item.
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     * @param event The ActionEvent triggered.
     */
    public void actionPerformed(ActionEvent event) {
        log.debug("Running actionPerformed(ActionEvent e)");
        String actionCommand = event.getActionCommand(); //This will get me the text of the source action without having to instantiate the source
        Class sourceClass = event.getSource().getClass(); //This would allow me to identify the class first before getting it with e.getSource() if I needed to instantiate the whole class for something.
        
        if (actionCommand == "Save") {
            //TODO: Make sure it's not null before doing this.
            log.debug("Save event detected. EditorPanel [{}] is the last active", lastActiveEditorPanel.getName());
            try {
                // For some reason this works but it isn't updating the display in the nav tree until you navigate off of the rule you are editing.
                lastActiveEditorPanel.save();
                tuNotify("Save successful");
            } catch (IOException ex) {
                F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(ex, concierge);
                exceptionHandler.processException();
            }
        } else if (actionCommand == "New") {
            log.debug("New event detected");
            
            newObjectFromDialog();
            
            tuNotify("New rule created in memory. Save to push to remote system");
            
            //TODO: Add something like this for taking a local iRule and automatically creating it on a bigip.
//            else if (actionCommand == "Deploy to BIGIP"){
//                //Local rule that needs to be created on the BIGIP.
//                NjordTreeNode node = (NjordTreeNode)
//                        navigationTree.getLastSelectedPathComponent();
//                if (node == null) return;
//                deployLocaliRuleToBigIP(node);  
//            } 
            
           } else if (actionCommand == "Delete") {
            log.debug("Delete event detected");
            //Delete is implemented by hand in Njord 1'll need to do something interesting here as well.
            NjordTreeNode objectNode = (NjordTreeNode) navTree.getLastSelectedPathComponent();
            if (!objectNode.getNodeType().equals(NjordNodeType.NODE_TYPE_EDITABLE)) {
                
                tuNotify("No iRule Selected. Please select one and try again");
                return;
            }
            // it's a rule, delete
            String objectName = objectNode.getShortName();
 //           String objectFullName = objectNode.getFullName();
            String objectType = "iRule";
            
            String deleteMessage = "Are you sure you want to delete the selected " + objectType + " [" + objectName + "] ";
            if (((NjordRuleEditorPanel) objectNode.getUserObject()).getIsLocal()) {
                deleteMessage = deleteMessage + "From the local File System?";
            } else {
                deleteMessage = deleteMessage + "from the BIGIP?";
            }
            
            int result = JOptionPane.showConfirmDialog(null, deleteMessage, 
                    "Delete iRule", JOptionPane.OK_CANCEL_OPTION);
            NjordRuleEditorPanel ruleEditor = (NjordRuleEditorPanel) objectNode.getUserObject();
            
            if (result == JOptionPane.OK_OPTION) {
                if (ruleEditor.getIsLocal()) {
                    //Local file remove from the file system
                    String path = ruleEditor.getFullName();
                    log.info(logPrefix + "Deleting " + path);
                    File deleteFile = new File(path);
                    deleteFile.delete();
                    tuNotify("Rule [" + objectName + "] successfully deleted from the local file system.");
                    DefaultTreeModel model = (DefaultTreeModel)navTree.getTreeModel();
                    model.removeNodeFromParent(objectNode);
                    //This works now update notices pane and the nav tree
                } else {
                    //Remote so remove it from the BIGIP
                    //First figure out what bigip the rule is located on.
//                    objectNode.getUserObject()
                    
                    NjordTreeNode bigipNode = objectNode.getParent().getParent();
                    String bigipName = bigipNode.getShortName();
                    
                    // Then build the list for iControl.
                    String[] objectNames = { objectName };
                    // Then run the delete.
                    
                    
                    BigIP destBigIP = null;
                    try {
                        destBigIP = preferences.getBigIP(bigipName);
                    } catch (NoSuchBigIPException e) {
                        F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
                        exceptionHandler.processException();
//                        resultsPanelNoticesBox.setText("Unable to create rule a rule with this name already exists on the BIGIP. Please choose another");
                        tuNotify("The specified BIG-IP, somehow, does not exist. Cancelling creation.");
                        return;
                    }
                    
                    // Check and see if we have a populated objects list. If not ask if they want to connect and fetch
                    if (!destBigIP.getNavTreeBuilt()) {
                        // Throw a dialog asking if they want to connect and build the tree.
//                        tuNotify("The specified BIG-IP is not connected. Please connect and try again.");
//                        return;
                        // Then fail if they said no or we were unable to build it.
//                        tuNotify("Unable to fetch remote objects. Creation Failed!");
//                        return;
                    }
                    
                    
                    //TODO: Move this into NjordiRulePlugin
                    try {
                        destBigIP.getInterface().getLocalLBRule().delete_rule(objectNames);
                    } catch (RemoteException e) {
                        F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
                        exceptionHandler.processException();
                    } catch (Exception e) {
                        F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
                        exceptionHandler.processException();
                    }
                    
                    tuNotify("Delete successful.");
                    
                    DefaultTreeModel model = (DefaultTreeModel)navTree.getTreeModel();
                    model.removeNodeFromParent(objectNode);
                    
                    // This is really wierd. nothing after model.removeNodeFromParent(objectNode) actually occurs.
                }
            }
            
            
            
        } else if (actionCommand == "Connect") {
            //TODO: Have a visual representation of if a bigip is connected or not. BIG-IPs should be represented by a bigip icon or an F5 ball. I can add a green and red ball to the bottom right of that.
            //TODO: Update the tree to show that the BIG-IP is connected.
            log.debug("Connect event triggered");
            NjordTreeNode treeNode = (NjordTreeNode) navTree.getLastSelectedPathComponent();
//            BigIP connectBigIP = getTreeSelectedBigip(treeNode); // Once I add the exception.
            if (treeNode.getNodeType().equals(NjordNodeType.NODE_TYPE_ORIGIN)) {
                // It's a remote node go ahead and connect
                BigIP connectBigIP = treeNode.getBigIP();
                tuNotify("Initializing connection to BIG-IP [" + connectBigIP + "]");
                getConnectionTo(connectBigIP);
                if (connectBigIP.getIsConnected()) {
                    tuNotify("Connected to BIG-IP [" + connectBigIP + "]");
                } else {
                    tuNotify("Failed to connect to BIG-IP [" + connectBigIP + "]");
                }
            } else {
                // No remote node selected, Throw an error.
                tuNotify("No BIG-IP selected. Please select a BIG-IP from the remote tree and try again");
            }
        } else if (actionCommand == "Fetch"){
            log.info("Fetch detected");
            fetchiRulesAction.actionPerformed(event);
//            NjordTreeNode treeNode = (NjordTreeNode) navTree.getLastSelectedPathComponent();
//            
//            if (treeNode.getNodeType().equals(NjordNodeType.NODE_TYPE_ORIGIN)) {
//                // It's a remote node go ahead and connect
//                BigIP fetchBigIP = treeNode.getBigIP();
//                
//                if (fetchBigIP.getIsConnected()) {
//                    tuNotify("Connection to BIG-IP [" + fetchBigIP + "] is initialized. Fetching iRules");
//                    //getiRulesFrom(Bigip bigip) or something like that.
//                    buildNodes(treeNode, NjordNodeType.NODE_TYPE_ORIGIN, NjordNodeContext.NODE_CONTEXT_REMOTE, "IRULE");
//                } else {
//                    tuNotify("Connection to BIG-IP [" + fetchBigIP + "] is not initialized. Connect then fetch?");
//                    //TODO: Create a special dialog that in addition to the messasge and title has a checkbox message. The checkbox message would be something like "Show this message next time" or "Don't show again."
//                    //TODO: Then create preferences for certain dialogs that allows you to not show them anymore.
//                    //TODO: Then create a mechanism for resetting said preferences.
//                    String confirmConnectMessage = "Connection to BIG-IP [" + fetchBigIP + "] is not initialized. Initialize connection then fetch iRules?";
//                    int result = JOptionPane.showConfirmDialog(null, confirmConnectMessage, 
//                            "Connect then fetch?", JOptionPane.OK_CANCEL_OPTION);
//                      if (result == JOptionPane.OK_OPTION) {
//                          getConnectionTo(fetchBigIP);
//                          if (fetchBigIP.getIsConnected()) {
//                              tuNotify("Connected to BIG-IP [" + fetchBigIP + "]");
//                              //getiRulesFrom(Bigip bigip) or something like that.
//                              buildNodes(treeNode, NjordNodeType.NODE_TYPE_ORIGIN, NjordNodeContext.NODE_CONTEXT_REMOTE, "IRULE");
//                          } else {
//                              tuNotify("Failed to connect to BIG-IP [" + fetchBigIP + "] fetch canceled");
//                          }
//                      } else {
//                          tuNotify("Canceled fetch");
//                      }
//                }
//            } else {
//                // No remote node selected, Throw an error.
//                tuNotify("No BIG-IP selected. Please select a BIG-IP from the remote tree and try again");
//            }
        } else if (actionCommand == "Add BIG-IP") {
            log.debug("Add BIG-IP event triggered");
            BigIP addBigIP = null;
            editBigIPSettings(addBigIP);
        } else if (actionCommand == "Delete BIG-IP") {
            //TODO: Check and see if there are unsaved rules on said BIGIP before deleting it. It's about time to create a deleteBigIP method here in Njord that will do that sort of stuff before calling the prefs method.
            log.debug("Delete BIG-IP event triggered");
            NjordTreeNode treeNode = (NjordTreeNode) navTree.getLastSelectedPathComponent();
            if (treeNode.getNodeType().equals(NjordNodeType.NODE_TYPE_ORIGIN)) {
                // It's a remote node, make sure they're REALLY sure then go ahead
                BigIP removeBigIP = treeNode.getBigIP();
                String deleteMessage = "Delete connection settings for BIG-IP [" + removeBigIP.getName() + "] are you sure?";
                int result = JOptionPane.showConfirmDialog(null, deleteMessage, 
                      "Delete BIG-IP Settings", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    preferences.removeBigIP(removeBigIP);
                    
                    
                 // This is how to remove an item from a tree. Might need to call tree.updateUI() afterwards.
                    DefaultTreeModel model = (DefaultTreeModel)navTree.getTreeModel();
                    model.removeNodeFromParent(treeNode);
                    
                    tuNotify("Deled BIG-IP [" + removeBigIP.getName() + "]");
                } else {
                    tuNotify("Canceled delete");
                }
            } else {
                // No remote node selected, Throw an error.
                tuNotify("No BIG-IP selected. Please select a BIG-IP from the remote tree and try again");
            }
        } else if (actionCommand == "Edit BIG-IP") {
            log.debug("Edit BIG-IP event triggered");
            tuNotify("Editing settings for BIG-IP");
            NjordTreeNode treeNode = (NjordTreeNode) navTree.getLastSelectedPathComponent();
            if (treeNode.getNodeType().equals(NjordNodeType.NODE_TYPE_ORIGIN)) {
                // It's a remote node, go ahead
                BigIP editBigIP = treeNode.getBigIP();
              editBigIPSettings(editBigIP);                
            } else {
                //TODO: Make checks like this scan up the tree so if you have a rule on a bigip selected and hit edit it will edit the settings for that bigip.
                // No remote node selected, Throw an error.
                tuNotify("No BIG-IP selected. Please select a BIG-IP from the remote tree and try again");
            }
        } else if (actionCommand == "Get Tasks"){
//            log.info("Tasks detected");
//            taskList.setActive(!taskList.isActive());
        } else if (actionCommand == "Find") {
            log.debug("Find event triggered");
            tuNotify("This button is NOT YET IMPLEMENTED! However find still works. Please use the find dialog which should be on right hand side of the window if you have not yet moved or hidden it.");
        } else if (actionCommand == "Undo") {
            log.debug("Undo event triggered");
            lastActiveEditorPanel.undoLastAction();
        } else if (actionCommand == "Redo") {
            log.debug("Redo event triggered");
            lastActiveEditorPanel.redoLastAction();
        } else if (actionCommand == "Cut") {
            log.debug("Cut event triggered");
            lastActiveEditorPanel.cut();
        } else if (actionCommand == "Copy") {
            log.debug("Copy event triggered");
            lastActiveEditorPanel.copy();
        } else if (actionCommand == "Copy as Rich Text") {
            log.debug("Copy as Rich Text event triggered");
            // I should make this option hidden in the menu unless it supports it.
            //TODO: Do this better
            ((NjordRuleEditorPanel) lastActiveEditorPanel).copyAsRichText();
        } else if (actionCommand == "Paste") {
            log.debug("Paste event triggered");
            lastActiveEditorPanel.paste();
        } else if (actionCommand == "Delete Text") {
            log.debug("Delete Text event triggered");
            // Delete is currently removed from the system.
        } else if (actionCommand == "Select All") {
            log.debug("Select All event triggered");
            tuNotify("NOT YET IMPLEMENTED FROM MENU! However ctrl+a should still work.");
        } else if (actionCommand == "Code Template") {
            log.debug("Code Template event triggered");
        } else if (actionCommand == "Show Line Numbers") {
            log.debug("Show Line Numbers event triggered");
            preferences.toggleShwLineNumbers();
        } else if (actionCommand == "Code Folding") {
            log.debug("Code Folding event triggered");
          preferences.toggleCodeFoldingEnabled();
//          updateViewSettingsFromPrefs();
        } else if (actionCommand == "Auto Scroll") {
            log.debug("Auto Scroll event triggered");
          preferences.toggleAutoScroll();
        } else if (actionCommand == "Line Wrap") {
            log.debug("Line Wrap event triggered");
          preferences.toggleLineWrapEnabled();
        } else if (actionCommand == "Autocomplete Automatically") {
            log.debug("Autocomplete Automatically event triggered");
          preferences.toggleAutoActivationEnabled();
//          preferences.setAutoActivationDelay(Int delay);
        } else if (actionCommand == "Visible Tab LInes") {
            log.debug("Visible Tab LInes event triggered");
          preferences.togglePaintTabLines();
        } else if (actionCommand == "Visible WhiteSpace") {
            log.debug("Visible WhiteSpace event triggered");
          preferences.toggleVisibleWhiteSpace();
        } else if (actionCommand == "Visible EOL Characters") {
            log.debug("Visible EOL Characters event triggered");
            preferences.toggleEOLMarkersVisible();
        } else if (actionCommand == "Save Window Geometry") {
            log.debug("Save Window Geometry event triggered");
            preferences.toggleSaveWindowGeometry();
        }else if (actionCommand == "DevCentral Home Page" || actionCommand == "Njord2 Manual" || actionCommand == "Devcentral Forums" || actionCommand == "iRules Reference" || actionCommand == "TCL Reference" || actionCommand == "iApps Wiki") {
            log.debug("Help menu URL event triggered");
            // Some actionEvent methods
            String URL = ((JMenuItem) event.getSource()).getToolTipText();
            try {
                java.awt.Desktop.getDesktop().browse(java.net.URI.create(URL));
            } catch (IOException e) {
                F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
                exceptionHandler.processException();
            }
        } else if (actionCommand == "THISWILLNEVERHIT") {
            log.debug("THISWILLNEVERHIT event triggered");
        } else {
            log.error("Un-Known Action Event Detected:" 
                  + nl
                  + "    Event source: " + actionCommand
                  + " (an instance of " + getClassName(event.getSource().getClass()) + ")");
        }
        
//      mntmDevCentralHome.setName("DevCentral Home Page");
      //        if (isBrowsingSupported()) {
//          makeLinkable(defaultResultsDevCentralURLLabel, new LinkMouseListener());
//      } else {
//          defaultResultsDevCentralURLLabel.setText(defaultResultsDevCentralURLLabel.getText() + ": " + defaultResultsDevCentralURLLabel.getToolTipText());
//      }
        
        
    }
    
    
    
    // ##################################
    // Some methods introduced by implementing the MouseListener event.
    
    // This one is wierd. I've got other code handling single and double clicks so I don't know what this method is going to be for.
    /**
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }
    
    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }
    
    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }
    
    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }
    
//    public NjordTreeNode newiRuleFromDialog() {
    public void newObjectFromDialog() {
     // Create the settings dialog
        final NewiRuleDialog newObjectDialog = new NewiRuleDialog(mainFrame, this);
        String objectType = "iRule";
        Map archetypes = null;
        
//        List<String> creatDestinations = new ArrayList<String>();
//        creatDestinations.add("Local");
        String createDestination = "NONE"; 
        String objectArchetype = "NONE";
        
        String newObjectName = "";
        
        this.tuNotify("Creating new " + objectType);
        
        for (BigIP bigip : preferences.getBIGIPS()) {
            log.debug("Adding bigip [{}] to destinations list", bigip.getName());
//            creatDestinations.add(bigip.getName());
            newObjectDialog.addToDestinations(bigip.getName());
//            newObjectDialog.addToSpinnerList(bigip.getName());
//            newObjectDialog.spnrCreatIn.getValue();
        }
        
        // Wow it looks like this works.
        if (navTree.getPathContextNode().equals(NjordNodeContext.NODE_CONTEXT_LOCAL)) {
            // Select "local" for 'create in'
            newObjectDialog.setSelectedDestination("Local");
        } else {
            // Else select the BIGIP to create it on.
            newObjectDialog.setSelectedDestination(navTree.getLastSelectedOrigin().getShortName());
        }
        
        // This is a dirty hack until I get smarter.
        NjordPluggable iRulePlugin = new NjordiRulePlugin();
        
        archetypes = iRulePlugin.getObjectArchitypes();
                for (Object archetype: archetypes.keySet()) {
                    newObjectDialog.addToListboxList(archetype.toString());
                }
        
        int result = JOptionPane.showConfirmDialog(null, newObjectDialog.panel_1, "New " + objectType, JOptionPane.OK_CANCEL_OPTION);
        
        if (result == JOptionPane.OK_OPTION) {
//            createDestination = newObjectDialog.getSpinnerValue();
            createDestination = (String) newObjectDialog.cmboDestinations.getSelectedItem();
            //TODO: Replace this with a plugin.getArchetypeText(newObjectDialog.getSelectedArchetypeText())
            
            
            try {
                NjordPluggable plugin = pluginManager.getPluginForCategory("IRULE");
                objectArchetype = ((NjordiRulePlugin) plugin).getArchitypeText(newObjectDialog.getSelectedArchetypeText());
            } catch (NoSuchPluginRegisteredException e1) {
                F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e1, concierge);
                exceptionHandler.processException();
                // Error and bail.
                objectArchetype = "An error occurred fetching archetype.";
            }
            
//            objectArchetype = (String) newObjectDialog.lstObjectArchetype.getSelectedValuesList().get(0); // There should only be one result.
//            objectArchetype = newObjectDialog.getListboxValue();
            newObjectName = newObjectDialog.txtObjectName.getText();
            
            log.debug("creating with destination [{}] and Archetype [{}]", createDestination, objectArchetype);    
            
            //TODO: Come up with a better (read plugin friendly) way to do this.
            newObjectName = getValidatedObjectName(newObjectDialog.txtObjectName.getText(), "iRule");
            String newOjbectFullName = "";//Start w/ blank.
            NjordFileLocation objectLocation = null;
            
            // this only works for iRules.
//            sdfsd
//            objectArchetype
            
            if (createDestination.equals("Local")) {
              //Create a local only rule
              JFileChooser fc = new JFileChooser();  
              FileSystemView fv = fc.getFileSystemView();  
              File documentsDir = fv.getDefaultDirectory();
  
              String localiRulesDirPath = documentsDir.getAbsolutePath() + "/Njord/Local/iRules/";
              File localiRulesDir = new File(localiRulesDirPath);
              if (!localiRulesDir.exists()) {
                  log.info(logPrefix + "Local Files directory doesn't exist. Creating.");
                  localiRulesDir.mkdirs();
              } 
              String localFilePath = localiRulesDirPath + newObjectName;
              File localFile = new File(localFilePath);
  
              objectLocation = new NjordFileLocation(concierge, newObjectName, localFile);
              NjordRuleEditorPanel addRule = new NjordRuleEditorPanel(concierge, objectLocation, newObjectName);
              
              addRule.setText(objectArchetype);
              addRule.discardAllEdits();
//              DefaultTreeModel model = (DefaultTreeModel)navTree.getModel();
              
              
              String treeBase = "iRules";
              NjordTreeNode insertNode = new NjordTreeNode(addRule);
//              navTree.top;
              
              Enumeration<NjordTreeNode> e = navTree.top.depthFirstEnumeration();
              while (e.hasMoreElements()) {
                  NjordTreeNode node = e.nextElement();
                  if (node.toString().equalsIgnoreCase(treeBase)) {
//                      remoteTree = node;
//                      remoteTree.add(bigipNode);
                      // We need the iRules under local
                      if (node.getParent().toString().equalsIgnoreCase("Local")) {
                          DefaultTreeModel model = (DefaultTreeModel)navTree.getTreeModel();
                          model.insertNodeInto(insertNode, node, node.getChildCount());
                      }
                      
//                      DefaultTreeModel model2 = (DefaultTreeModel)node.getM
                      
//                      tuNotify("Hit Save to complete creation on BIGIP.");
//                      navTree.expandPath(remoteiRulesPath);
//                      node.add(bigipNode);
                      // Might not need this.
//                      remoteiRulesPath = new TreePath(node.getPath());
                  }
              }
              
              
//              model.insertNodeInto(addRule, localiRulesCategory, localiRulesCategory.getChildCount());
              tuNotify("New local rule created. Hit save to complete");
//              navTree.expandPath(localiRulesPath);
              
            } else {
                // Create on specified BIGIP.
                //TODO: Deal with partitions here.
                // In Njord 1 I check the bigip version and set the name to /Common. I should deal with that in Njord 2.
//                if (bigIPVersion.startsWith("11")) {
//                  newiRulePartition = "/Common"; //v11 objects will always have a partition so populate this by default.
//                }
                
                
                
                // First get the BIG-IP that they selected
                String destBigIPName = newObjectDialog.getSpinnerValue();
                // Then figure out how to get a bigip out of that info....
                BigIP destBigIP = null;
                try {
                    destBigIP = preferences.getBigIP(destBigIPName);
                } catch (NoSuchBigIPException e) {
                    F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
                    exceptionHandler.processException();
//                    resultsPanelNoticesBox.setText("Unable to create rule a rule with this name already exists on the BIGIP. Please choose another");
                    tuNotify("The specified BIG-IP, somehow, does not exist. Cancelling creation.");
                    return;
                }
                
                // Check and see if we have a populated objects list. If not ask if they want to connect and fetch
                if (!destBigIP.getNavTreeBuilt()) {
                    // Throw a dialog asking if they want to connect and build the tree.
                    
                    // Then fail if they said no or we were unable to build it.
//                    tuNotify("Unable to fetch remote objects. Creation Failed!");
//                    return;
                }
                // Nav tree is built so we're GTG.
                
                //TODO: Do this.
                // Once the list is popluated make sure there isn't a rule with the same name.
                
                // Create the location for the new object
//                objectLocation = new NjordFileLocation(concierge,  newObjectName, destBigIP.getInterface(), objectArchetype);
                objectLocation = new NjordFileLocation(concierge, newObjectName, NjordNodeCategory.NODE_CATEGORY_LOCAL_IRULE, destBigIP.getInterface(), objectArchetype);
                
                // Create an editor panel from said node. Hand it an archetype which tells the editorPane to create it as a new Rule..
                NjordRuleEditorPanel ruleEditor = new NjordRuleEditorPanel(concierge, objectLocation, newObjectName);
//                ruleEditor.setText(objectArchetype);
                ruleEditor.discardAllEdits(); //This prevents undo from 'undoing' the loading of the editor with an iRule
                
                // Create a NjordTreeNode
                NjordTreeNode addRuleNode = new NjordTreeNode(ruleEditor);
                
//                destBigIP.get
                
                // Get the default tree model and stuff the new rule into it.
                DefaultTreeModel model = (DefaultTreeModel)navTree.getTreeModel();
                
                // Find the category from within that bigIP.
                NjordTreeNode remoteCategoryNode = navTree.getCategoryNodeForBigIP(destBigIP);
                model.insertNodeInto(addRuleNode, remoteCategoryNode, remoteCategoryNode.getChildCount());
                
//                = new TreePath(remoteCategoryNode.getPath());
                navTree.expandPath(new TreePath(remoteCategoryNode.getPath()));
                
                //TODO: There has to be a better way to do this.
                List<String> ruleNames = destBigIP.getiRulesNames();
                ruleNames.add(newObjectName);
                destBigIP.setiRulesNames(ruleNames);
                
            }
            
        }
        
        tuNotify(objectType + " created");
        
    }
    
    //TODO: Come up with a better way other than String objectType. There has to be some way to add object types to a list when we init all the plugins. Then object Type would tell me what class to call the name validator on.
    /**
     * NOT YET IMPLEMENTED.
     * Eventually this will validate an object's new name. Right now it just returns what you sent it.
     * Once I get plugins working this should probably call a validator on the object's type.
     * 
     * @param unvalidatedName
     * @return
     */
    public String getValidatedObjectName(String unvalidatedName, String objectType) {
        return unvalidatedName;
    }
    
    /**
     * Provides the dialog for editing a BIG-IP. Also saves the settings once they have been provided. 
     * This method is used for editing an existing object as well as adding a new one.
     * 
     * @param bigip The bigip object to edit. If null will provide the user with a New bigip dialog.
     */
    public void editBigIPSettings(BigIP bigip) {
        // Create the settings dialog
        final BIGIPSettingsDialog settingsPane = new BIGIPSettingsDialog(mainFrame, this);
        boolean bigipIsNew = false;
        String ibigipName;
        String iHostOrIPAddress;
        int iPort;
        String iUserName;
        String iPassword;
        boolean iconnectAuomatically;
        
        if (bigip == null) {
            this.tuNotify("Adding new BIG-IP");
            //bigip == null which means we are adding a new one.
            bigipIsNew = true;
        } else {
            this.tuNotify("Editing settings for BIG-IP [" + bigip.getName() + "]");
            //Not null so fetch settings from the bigip object.
            // Set the dialog settings based on what we have retrieved from the prefernces file (or Njord default)
            settingsPane.ConnPreffsBigIPName.setText(bigip.getName());
            settingsPane.ConnPreffsHostTextField.setText(bigip.gethostNameOrAddress());
            settingsPane.ConnPreffsPortTextField.setText(String.valueOf(bigip.getiControlPort()));
            settingsPane.ConnPreffsUserTextField.setText(bigip.getUserName());
            if (bigip.getPassword().startsWith("ENC")) {
                //If the password is encrypted attempt to decrypt it first.
                settingsPane.ConnPreffsPasswordTextField.setText(decryptPassword(bigip.getPassword()));
            } else {
                settingsPane.ConnPreffsPasswordTextField.setText(bigip.getPassword());
            }
            
            settingsPane.chkbxConnPrefsConnectAutomatically.setSelected(bigip.getConnectAutomatically());
        }
        
        int result = JOptionPane.showConfirmDialog(null, settingsPane.panel_1, "Connection Settings", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            ibigipName = settingsPane.ConnPreffsBigIPName.getText();
            iHostOrIPAddress = settingsPane.ConnPreffsHostTextField.getText();
            iPort = Integer.parseInt(settingsPane.ConnPreffsPortTextField.getText());
            iUserName = settingsPane.ConnPreffsUserTextField.getText();
//            iPassword = settingsPane.ConnPreffsPasswordTextField.getText();
            iPassword = encryptPassword(new String(settingsPane.ConnPreffsPasswordTextField.getPassword()));
            
            iconnectAuomatically = settingsPane.chkbxConnPrefsConnectAutomatically.isSelected();
            BigIP newBigIP = new BigIP(concierge, ibigipName, iHostOrIPAddress, iPort, iUserName, iPassword, iconnectAuomatically);
            log.debug("Adding connection information for:");
            log.debug("Name: [{}]", ibigipName);
            log.debug("Host: [{}]", iHostOrIPAddress);
            log.debug("Port: [{}]", iPort);
            log.debug("User: [{}]", iUserName);
            log.debug("Pass: [{}]", iPassword);
            log.debug("AutoConnect: [{}]" + iconnectAuomatically);
            if (bigipIsNew) {
                preferences.addBigIP(newBigIP);
            } else {
                preferences.replaceBigIP(bigip, newBigIP);
            }
            
            NjordTreeNode bigipNode = new NjordTreeNode(newBigIP);
//            TreePath remoteiRulesPath = null;
            
            String treeBase = "Remote";
            NjordTreeNode remoteTree = null;
//            navTree.top;
            
            if (bigipIsNew) {
                // Add new entries to the nav tree.
                Enumeration<NjordTreeNode> e = navTree.top.depthFirstEnumeration();
                while (e.hasMoreElements()) {
                    NjordTreeNode node = e.nextElement();
                    if (node.toString().equalsIgnoreCase(treeBase)) {
                        DefaultTreeModel model = (DefaultTreeModel)navTree.getTreeModel();
                        model.insertNodeInto(bigipNode, node, node.getChildCount());
                        navTree.updateUI();
                    }
                }    
            }
            
            // writePreferences();
            // TODO: Set connected state to false if connection settings have been edited.
            tuNotify("Connection Settings Saved");
        } else {
            tuNotify("Canceled");
        }
    }
    
    
	// #############################################################################################################################
	// ########### ORIGinal METHODS FROM JAVADOCKING SAMPLE
    // #############################################################################################################################
    
	/**
	 * Creates a dockable for a given content component.
	 * 
	 * @param 	id 		The ID of the dockable. The IDs of all dockables should be different.
	 * @param 	content The content of the dockable. 
	 * @param 	title 	The title of the dockable.
	 * @param 	icon 	The icon of the dockable.
	 * @return			The created dockable.
	 * @throws 	IllegalArgumentException	If the given ID is null.
	 */
	private Dockable createDockable(String id, Component content, String title, Icon icon, String description) {
	    log.debug("Running createDockable(String id, Component content, String title, Icon icon, String description)");
		// Create the dockable.
		DefaultDockable dockable = new DefaultDockable(id, content, title, icon);
		
		// Add a description to the dockable. It will be displayed in the tool tip.
		dockable.setDescription(description);
        
		
//		...
//		component.setFocusable(true);
//		FocusRequester focusRequester = new FocusRequester(component);
//		component.addMouseListener(focusRequester);
//		component.addFocusListener(focusRequester);
//		...
		
		return dockable;
	}

	/**
	 * Decorates the given dockable with all state actions.
	 * 
	 * @param dockable	The dockable to decorate.
	 * @return			The wrapper around the given dockable, with actions.
	 */
	private Dockable addAllActions(Dockable dockable) {
	    // This part might need to become addDefaultActions because it will just be for the built in windows like Welcome, nav, etc. Then I need an addEditorActions or addEditableActions.
	    log.debug("Running addAllActions(Dockable dockable)");
		Dockable wrapper = new StateActionDockable(dockable, new DefaultDockableStateActionFactory(), DockableState.statesClosed());
		wrapper = new StateActionDockable(wrapper, new DefaultDockableStateActionFactory(), DockableState.statesAllExceptClosed());
		// TODO: Figure out how to add something that notifies us of what was last selected. Add it to limitedActions As well.
		
		return wrapper;
	}
	
	/**
	 * Decorates the given dockable with some state actions (not maximized).
	 * 
	 * @param dockable	The dockable to decorate.
	 * @return			The wrapper around the given dockable, with actions.
	 */
	private Dockable addLimitActions(Dockable dockable) {
	    log.debug("Running addLimitActions(Dockable dockable)");
		Dockable wrapper = new StateActionDockable(dockable, new DefaultDockableStateActionFactory(), DockableState.statesClosed());
		int[] limitStates = {DockableState.NORMAL, DockableState.MINIMIZED, DockableState.EXTERNALIZED};
		wrapper = new StateActionDockable(wrapper, new DefaultDockableStateActionFactory(), limitStates);
		return wrapper;
	}
	
    /**
    * Creates a dockable with a button as content.
    * 
    * @param id            The ID of the dockable that has to be created.
    * @param title         The title of the dialog that will be displayed.
    * @param icon          The icon that will be put on the button.
    * @return              The dockable with a button as content.
    */
   private Dockable createButtonDockable(String id, String title, Icon icon) {
       log.debug("Running createButtonDockable(String id, String title, Icon icon, String message)");
       //TODO: Change this from just openning a dialog to using an actionlistener.
       // Create the action.
       
       ButtonAction action = new ButtonAction(this, title, icon);
       // Nice I'm already instantiating a new action for each button. I just need to do it for my own created gui items. I need to modify
       // this method to call the new actions.
       
       // Create the button.
       ToolBarButton button = new ToolBarButton(concierge, action);
       
       // Create the dockable with the button as component.
       ButtonDockable buttonDockable = new ButtonDockable(id, button);
//     here
       // Add a dragger to the individual dockable.
       createDockableDragger(buttonDockable);
       
       //Well poop, this doesn't work. I'm going to have to do something odd with something like messageAction or something to make it work.
//     buttonDockable.addActionListener(this);
       
       return buttonDockable;
   }
   
   
   
   // Something like this?
  private Dockable createButtonDockable(AbstractAction action) {
      log.debug("Running createButtonDockable(Action)");
      //TODO: Change this from just openning a dialog to using an actionlistener.
      // Create the action.
      
//      ButtonAction buttonAction = new ButtonAction(action, action.toString(), icon);
      // Nice I'm already instantiating a new action for each button. I just need to do it for my own created gui items. I need to modify
      // this method to call the new actions.
      
      // Create the button.
      ToolBarButton button = new ToolBarButton(concierge, action);
      
//      String actionName = action.toString();
      // Create the dockable with the button as component.
      ButtonDockable buttonDockable = new ButtonDockable(action.toString(), button);
      
      
      Action[][] actions = new Action[1][];
      actions[0] = new Action[1];
      actions[0][0] = action;
      
//      actions[0][1] = cautionAction;
      ActionDockable dockable = new ActionDockable(buttonDockable, actions);
      
//    here
      // Add a dragger to the individual dockable.
      createDockableDragger(buttonDockable);
      
      //Well poop, this doesn't work. I'm going to have to do something odd with something like messageAction or something to make it work.
//    buttonDockable.addActionListener(this);
      return dockable;
//      return buttonDockable;
  }
   
  
  
  //maybe like this
 private Dockable createButtonDockable(AbstractAction action, ImageIcon icon) {
     log.debug("Running createButtonDockable(Action)");
     //TODO: Change this from just openning a dialog to using an actionlistener.
     // Create the action.
     
     ButtonAction buttonAction = new ButtonAction(action, action.toString(), icon);
     // Nice I'm already instantiating a new action for each button. I just need to do it for my own created gui items. I need to modify
     // this method to call the new actions.
     
     // Create the button.
//     ToolBarButton button = new ToolBarButton(concierge, action);
     ToolBarButton button = new ToolBarButton(concierge, buttonAction);
     
//     String actionName = action.toString();
     // Create the dockable with the button as component.
     ButtonDockable buttonDockable = new ButtonDockable(action.toString(), button);
     
     
     Action[][] actions = new Action[1][];
     actions[0] = new Action[1];
     actions[0][0] = action;
     
//     actions[0][1] = cautionAction;
     ActionDockable dockable = new ActionDockable(buttonDockable, actions);
     
//   here
     // Add a dragger to the individual dockable.
//     createDockableDragger(buttonDockable);
     createDockableDragger(dockable);
     
     //Well poop, this doesn't work. I'm going to have to do something odd with something like messageAction or something to make it work.
//   buttonDockable.addActionListener(this);
     return dockable;
//     return buttonDockable;
 }
  
  
   
   
//   	
//	/**
//	 * Creates a dockable with a button who's action is only to pop up a message.
//	 * 
//	 * @param id			The ID of the dockable that has to be created.
//	 * @param title			The title of the dialog that will be displayed.
//	 * @param icon			The icon that will be put on the button.
//	 * @param message		The message that will be displayed when the action is performed.
//	 * @return				The dockable with a button as content.
//	 */
//	private Dockable createButtonDockable(String id, String title, Icon icon, String message) {
//	    log.debug("Running createButtonDockable(String id, String title, Icon icon, String message)");
//	    //TODO: Change this from just openning a dialog to using an actionlistener.
//	    // Create the action.
//		MessageAction action = new MessageAction(this, title, icon, message);
//		
//		// Create the button.
//		ToolBarButton button = new ToolBarButton(action);
//		
//		// Create the dockable with the button as component.
//		ButtonDockable buttonDockable = new ButtonDockable(id, button);
////		here
//		// Add a dragger to the individual dockable.
//		createDockableDragger(buttonDockable);
//		
//		//Well poop, this doesn't work. I'm going to have to do something odd with something like messageAction or something to make it work.
////		buttonDockable.addActionListener(this);
//		
//		return buttonDockable;
//	}
	

	/**
	 * Adds a drag listener on the content component of a dockable.
	 */
	private void createDockableDragger(Dockable dockable) {
		// Create the dragger for the dockable.
		DragListener dragListener = DockingManager.getDockableDragListenerFactory().createDragListener(dockable);
		dockable.getContent().addMouseListener(dragListener);
		dockable.getContent().addMouseMotionListener(dragListener);
	}
	
	/**
	 * Creates the menubar with menus: File, Window, Look and Feel, and Drag Painting.
	 * 
	 * @param dockables		The dockables for which a menu item has to be created.
	 * @return				The created menu bar.
	 */
	private JMenuBar createMenu(Dockable[] dockables, DockingPath centerDockingPath) {
		// Create the menu bar.
		JMenuBar menuBar = new JMenuBar();
		//TODO: Figure out if I'm still using this and make it go if not. Oh, I think the buttonGroup is what makes them take effect immediately.
		ButtonGroup buttonGroup = new ButtonGroup();
		
		// ###
		// Build File menu.
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.getAccessibleContext().setAccessibleDescription("The File Menu");
		menuBar.add(fileMenu);
		
		String toolTipObjectName;
		
		if (preferences.getDisplayExperimental()) {
		    toolTipObjectName = "iRule";
        } else {
            toolTipObjectName = "Object";
        }
		
		//TODO: Add icons next to these in the menu.
		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.setName("New");
		mntmNew.setToolTipText("Create a new " + toolTipObjectName);
		mntmNew.addActionListener(this);
		mntmNew.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        fileMenu.add(mntmNew);
        
		JMenuItem mntmSave = new JMenuItem("Save");
        mntmSave.setName("Save");
        mntmSave.setToolTipText("Save the current  " + toolTipObjectName);
        mntmSave.addActionListener(this);
        mntmSave.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        fileMenu.add(mntmSave);
		
		JMenuItem mntmSaveAll = new JMenuItem("Save All");
		mntmSaveAll.setName("Save All");
		mntmSaveAll.setToolTipText("Save all " + toolTipObjectName + "s which have pending changes");
		mntmSaveAll.addActionListener(this);
        fileMenu.add(mntmSaveAll);
        
		JMenuItem mntmDelete = new JMenuItem("Delete");
		mntmDelete.setName("Delete");
		mntmDelete.setToolTipText("Delete the selected " + toolTipObjectName);
		mntmDelete.addActionListener(this);
        fileMenu.add(mntmDelete);
        
//        JCheckBoxMenuItem chkbxmntmGetTasks = new JCheckBoxMenuItem("Get Tasks");
//        chkbxmntmGetTasks.setName("Get Tasks");
//        chkbxmntmGetTasks.setToolTipText("Should populate the tasks list we hope");
//        chkbxmntmGetTasks.addActionListener(this);
//        fileMenu.add(chkbxmntmGetTasks);
//        
        // A test of the new threaded MVC architecture. Calling the actions works now on to the operations
        fileMenu.add(actionRegistry.fetchiRules);
        
        fileMenu.addSeparator();
        
		JMenuItem mntmExit = new JMenuItem("Exit", KeyEvent.VK_Q);
		mntmExit.setToolTipText("Quit Njord");
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.ALT_MASK));
		mntmExit.getAccessibleContext().setAccessibleDescription("Exit the application");
		mntmExit.addActionListener(new ActionListener() {
		    //TODO: Move this action to the main actionPerformed method?
		    //TODO: Make sure this does the same thing as the x.
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		    });
		fileMenu.add(mntmExit);
		
		// ###
		// End build file menu
		
		
		// ###
		// Build edit menu
        JMenu editMenu = new JMenu("Edit");
        editMenu.setMnemonic(KeyEvent.VK_E);
        editMenu.getAccessibleContext().setAccessibleDescription("The Edit Menu");
        menuBar.add(editMenu);
        
        JMenuItem mntmUndo = new JMenuItem("Undo");
        mntmUndo.setName("Undo");
        mntmUndo.setToolTipText("Undo the last edit");
        mntmUndo.addActionListener(this);
        mntmUndo.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
        editMenu.add(mntmUndo);
        
        JMenuItem mntmRedo = new JMenuItem("Redo");
        mntmRedo.setName("Redo");
        mntmRedo.setToolTipText("Redo the last undone edit");
        mntmRedo.addActionListener(this);
        mntmRedo.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Y, ActionEvent.CTRL_MASK));
        editMenu.add(mntmRedo);
        
        editMenu.addSeparator();
        
        JMenuItem mntmCut = new JMenuItem("Cut");
        mntmCut.setName("Cut");
        mntmCut.setToolTipText("Cut the selected text to the clipboard");
        mntmCut.addActionListener(this);
        mntmCut.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_X, ActionEvent.CTRL_MASK));
        editMenu.add(mntmCut);
        
        JMenuItem mntmCopy = new JMenuItem("Copy");
        mntmCopy.setName("Copy");
        mntmCopy.setToolTipText("Copy the selected text to the clipboard");
        mntmCopy.addActionListener(this);
        mntmCopy.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_C, ActionEvent.CTRL_MASK));
        editMenu.add(mntmCopy);
        
        JMenuItem mntmCopyAsRTF = new JMenuItem("Copy as Rich Text");
        mntmCopyAsRTF.setName("Copy as Rich Text");
        mntmCopyAsRTF.setToolTipText("Cut the selected text to the clipboard in RTF (Rich Text Format)");
        mntmCopyAsRTF.addActionListener(this);
        mntmCopyAsRTF.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_C, ActionEvent.CTRL_MASK+ActionEvent.SHIFT_MASK));
        editMenu.add(mntmCopyAsRTF);
        
        JMenuItem mntmPaste = new JMenuItem("Paste");
        mntmPaste.setName("Paste");
        mntmPaste.setToolTipText("Paste text from the clipboard");
        mntmPaste.addActionListener(this);
        mntmPaste.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_V, ActionEvent.CTRL_MASK));
        editMenu.add(mntmPaste);
        
        editMenu.addSeparator();
        
        // Removing from the menu until I figure out how to send the delete event.
//        JMenuItem mntmDeleteText = new JMenuItem("Delete Text");
//        mntmDeleteText.setName("Delete Text");
//        mntmDeleteText.setToolTipText("");
//        mntmDeleteText.addActionListener(this);
//        mntmDeleteText.setAccelerator(KeyStroke.getKeyStroke(
//                KeyEvent.VK_DELETE, 0));
//        editMenu.add(mntmDeleteText);
        
        if (preferences.getDisplayExperimental()) {
            JMenuItem mntmSelectAll = new JMenuItem("Select All");
            mntmSelectAll.setName("Select All");
            mntmSelectAll.setToolTipText("Select all text within the editor");
            mntmSelectAll.addActionListener(this);
            mntmSelectAll.setAccelerator(KeyStroke.getKeyStroke(
                    KeyEvent.VK_A, ActionEvent.CTRL_MASK));
            editMenu.add(mntmSelectAll);
            
            editMenu.addSeparator();
            
            //TODO: Come up with a better name for this.
            JMenuItem mntmCodeTemplate = new JMenuItem("Code Template");// (I.E. expand code template from selection)
            mntmCodeTemplate.setName("Code Template");
            mntmCodeTemplate.setToolTipText("Expand the selected text if there is a matching code template");
            mntmCodeTemplate.addActionListener(this);
            //TODO: I need to moddify this so that you can set it. 
            mntmCodeTemplate.setAccelerator(KeyStroke.getKeyStroke(
                    KeyEvent.VK_SPACE, ActionEvent.CTRL_MASK));
            editMenu.add(mntmCodeTemplate);
        }
        
        
        // ###
        // End build edit menu
		
		// ###
        // Build view settings menu
        
        JMenu viewMenu = new JMenu("View");
//        viewMenu.setMnemonic(KeyEvent.VK_E);
        viewMenu.setToolTipText("Options which effect the way Njord is displayed");
        viewMenu.getAccessibleContext().setAccessibleDescription("Options which effect the way Njord is displayed");
        menuBar.add(viewMenu);
        
        
        JCheckBoxMenuItem chkbxmntmShwLineNumbers = new JCheckBoxMenuItem("Show Line Numbers");
        chkbxmntmShwLineNumbers.setName("Show Line Numbers");
        chkbxmntmShwLineNumbers.setState(preferences.getShwLineNumbers());
        chkbxmntmShwLineNumbers.setToolTipText("Show line numbers in the editor");
        chkbxmntmShwLineNumbers.addActionListener(this);
        viewMenu.add(chkbxmntmShwLineNumbers);
        
        JCheckBoxMenuItem chkbxmntmCodeFoldingEnabled = new JCheckBoxMenuItem("Code Folding");
        if (preferences.getDisplayExperimental()) {
            
            chkbxmntmCodeFoldingEnabled.setName("Code Folding");
            chkbxmntmCodeFoldingEnabled.setState(preferences.getCodeFoldingEnabled());
            chkbxmntmCodeFoldingEnabled.setToolTipText("Adds a widget which will let you hide and expand sections of code");
            chkbxmntmCodeFoldingEnabled.addActionListener(this);
            viewMenu.add(chkbxmntmCodeFoldingEnabled);    
        }
        
        JCheckBoxMenuItem chkbxmntmAutoScroll = new JCheckBoxMenuItem("Auto Scroll");
        chkbxmntmAutoScroll.setName("Auto Scroll");
        chkbxmntmAutoScroll.setState(preferences.getAutoScroll());
        chkbxmntmAutoScroll.setToolTipText("Automatically scroll the window when drag highlighting extends beyond the visible area");
        chkbxmntmAutoScroll.addActionListener(this);
        viewMenu.add(chkbxmntmAutoScroll);
        
        JCheckBoxMenuItem chkbxmntmLineWrapEnabled = new JCheckBoxMenuItem("Line Wrap");
        chkbxmntmLineWrapEnabled.setName("Line Wrap");
        chkbxmntmLineWrapEnabled.setState(preferences.getLineWrapEnabled());
        chkbxmntmLineWrapEnabled.setToolTipText("Wrap lines of code");
        chkbxmntmLineWrapEnabled.addActionListener(this);
        viewMenu.add(chkbxmntmLineWrapEnabled);
        
        JCheckBoxMenuItem chkbxmntmAutoActivationEnabled = new JCheckBoxMenuItem("Autocomplete Automatically");
//      AutoActivationDelay
        chkbxmntmAutoActivationEnabled.setName("Autocomplete Automatically");
        chkbxmntmAutoActivationEnabled.setState(preferences.getAutoActivationEnabled());
//        chkbxmntmShwLineNumbers.setState(preferences.getAutoActivationDelay();// int
        chkbxmntmAutoActivationEnabled.setToolTipText("Automatically provide code suggestions or wait for a keystroke");
        chkbxmntmAutoActivationEnabled.addActionListener(this);
        viewMenu.add(chkbxmntmAutoActivationEnabled);
        
        //Not yet implemented
//        chkbxmntmShwLineNumbers.setState(preferences.getTemplatesDirectoryPath());
        
        JCheckBoxMenuItem chkbxmntmPaintTabLines = new JCheckBoxMenuItem("Visible Tab LInes");
        chkbxmntmPaintTabLines.setName("Visible Tab LInes");
        chkbxmntmPaintTabLines.setState(preferences.getPaintTabLines());
        chkbxmntmPaintTabLines.setToolTipText("Shows tab lines. I think this means where things line up.");
        chkbxmntmPaintTabLines.addActionListener(this);
        viewMenu.add(chkbxmntmPaintTabLines);
        
        JCheckBoxMenuItem chkbxmntmVisibleWhiteSpace = new JCheckBoxMenuItem("Visible WhiteSpace");
        chkbxmntmVisibleWhiteSpace.setName("Visible WhiteSpace");
        chkbxmntmVisibleWhiteSpace.setState(preferences.getVisibleWhiteSpace());
        chkbxmntmVisibleWhiteSpace.setToolTipText("Represent whitespace with a visible character");
        chkbxmntmVisibleWhiteSpace.addActionListener(this);
        viewMenu.add(chkbxmntmVisibleWhiteSpace);
        
        JCheckBoxMenuItem chkbxmntmEOLMarkersVisible = new JCheckBoxMenuItem("Visible EOL Characters");
        chkbxmntmEOLMarkersVisible.setName("Visible EOL Characters");
        chkbxmntmEOLMarkersVisible.setState(preferences.getEOLMarkersVisible());
        chkbxmntmEOLMarkersVisible.setToolTipText("Show a visible character for the end of line character");
        chkbxmntmEOLMarkersVisible.addActionListener(this);
        viewMenu.add(chkbxmntmEOLMarkersVisible);

        JCheckBoxMenuItem chkbxmntmSaveWindowGeometry = new JCheckBoxMenuItem("Save Window Geometry");
        chkbxmntmSaveWindowGeometry.setName("Save Window Geometry");
        chkbxmntmSaveWindowGeometry.setState(preferences.getSaveWindowGeometry());
        chkbxmntmSaveWindowGeometry.setToolTipText("Save the size and shape of windows as well as the size, shape and positio of draggable content.");
        chkbxmntmSaveWindowGeometry.addActionListener(this);
        viewMenu.add(chkbxmntmSaveWindowGeometry);

//        ("Show Line Numbers");
//        ("Code Folding");
//        ("Auto Scroll");
//        ("Line Wrap");
//        ("Autocomplete Automatically");
//        ("Visible Tab LInes");
//        ("Visible WhiteSpace");
//        ("Visible EOL Characters");
//        preferences.getShwLineNumbers();
//        preferences.getCodeFoldingEnabled();
//        preferences.getLineWrapEnabled();
//        preferences.getAutoScroll();
//        preferences.getAutoActivationEnabled();
//        preferences.getAutoActivationDelay();// int
//        //Not yet implemented
////        preferences.getTemplatesDirectoryPath();
//        preferences.getPaintTabLines();
//        preferences.getVisibleWhiteSpace();
//        preferences.getEOLMarkersVisible();

        
//        prefShwLineNumbers
//        prefCodeFoldingEnabled
//        prefLineWrapEnabled
//        prefAutoScroll
//        prefAutoActivationEnabled
//        prefAutoActivationDelay int
//        //Not yet implemented
////        prefTemplatesDirectoryPath
//        prefPaintTabLines
//        prefVisibleWhiteSpace
//        prefEOLMarkersVisible
        
        viewMenu.addSeparator();
        
        // ######################################
        // Menus to move or replace.
        // TODO: Move most of these menus into a preferences dialog.
        // Build the default Windows menu.
        JMenu defaultWindowsMenu = new JMenu("Default Windows");
//        defaultWindowsMenu.setMnemonic(KeyEvent.VK_W);
        defaultWindowsMenu.setToolTipText("Allows you to re-create any of the default windows if you have closed them.");
        defaultWindowsMenu.getAccessibleContext().setAccessibleDescription("The Default Windows Menu");
        viewMenu.add(defaultWindowsMenu);
        
        // The JMenuItems for the default dockables.
        for (int index = 0; index < dockables.length; index++) {
            // Create the check box menu for the dockable.
            JCheckBoxMenuItem cbMenuItem = new DockableMenuItem(dockables[index]);
            defaultWindowsMenu.add(cbMenuItem);         
        }
        
        // End build the default Windows menu.
        
        // Build the Look and Feel menu.
        JMenu lookAndFeelMenu = new JMenu("Look and Feel");
//        lookAndFeelMenu.setMnemonic(KeyEvent.VK_L);
        lookAndFeelMenu.setToolTipText("Change the look and feel of the Njord GUI. If a look and feel is greyed out then it is not supported on your platform.");
        lookAndFeelMenu.getAccessibleContext().setAccessibleDescription("The Lool and Feel Menu");
        viewMenu.add(lookAndFeelMenu);
        
        for (int index = 0; index < LAFS.length; index++) {
            LafMenuItem lafMenuItem = new LafMenuItem(LAFS[index]);
            lookAndFeelMenu.add(lafMenuItem);
            buttonGroup.add(lafMenuItem);
        }
        
       // End build the Look and Feel menu.
        
        // Build the Dragging menu.
        JMenu draggingMenu = new JMenu("Drag Painting");
//        draggingMenu.setMnemonic(KeyEvent.VK_D);
        draggingMenu.setToolTipText("Sets the way that destinations are displayed when dragging dockable objects into dock locations.");
        draggingMenu.getAccessibleContext().setAccessibleDescription("The Dragg Painting Menu");
        viewMenu.add(draggingMenu);

        // The JMenuItems for the draggers.
        DockableDragPainter swDockableDragPainterWithoutLabel = new SwDockableDragPainter(new DefaultRectanglePainter(), false);
        DockableDragPainter swDockableDragPainterWithLabel = new SwDockableDragPainter(new RectangleDragComponentFactory(new DefaultRectanglePainter(), true), false);
        DockableDragPainter swDockableDragPainterWithoutLabelNoFloat = new SwDockableDragPainter(new DefaultRectanglePainter());
        DockableDragPainter swDockableDragPainterWithLabelNoFloat = new SwDockableDragPainter(new RectangleDragComponentFactory(new DefaultRectanglePainter(), true));
        DockableDragPainter labelDockableDragPainter = new LabelDockableDragPainter();
        DockableDragPainter imageDockableDragPainter = new ImageDockableDragPainter();
        DockableDragPainter windowDockableDragPainterWithoutLabel = new WindowDockableDragPainter(new DefaultRectanglePainter());
        DockableDragPainter windowDockableDragPainterWithLabel = new WindowDockableDragPainter(new DefaultRectanglePainter(), true);
        DockableDragPainter transparentWindowDockableDragPainterWithoutLabel = new TransparentWindowDockableDragPainter(new DefaultRectanglePainter());
        DockableDragPainter transparentWindowDockableDragPainterWithLabel = new TransparentWindowDockableDragPainter(new DefaultRectanglePainter(), true);
        buttonGroup = new ButtonGroup();
        DraggingMenuItem[] draggingMenuItems = new DraggingMenuItem[8];
        draggingMenuItems[0] = new DraggingMenuItem("Rectangle", swDockableDragPainterWithoutLabel, null, false);
        draggingMenuItems[1] = new DraggingMenuItem("Rectangle with image", swDockableDragPainterWithoutLabel, imageDockableDragPainter, true);
        draggingMenuItems[2] = new DraggingMenuItem("Labeled rectangle", swDockableDragPainterWithLabel, null, false);
        draggingMenuItems[3] = new DraggingMenuItem("Rectangle with dragged label", swDockableDragPainterWithoutLabel, labelDockableDragPainter, false);
        draggingMenuItems[4] = new DraggingMenuItem("Rectangle with window", swDockableDragPainterWithoutLabelNoFloat, windowDockableDragPainterWithoutLabel, false);
        draggingMenuItems[5] = new DraggingMenuItem("Labeled rectangle with labeled window", swDockableDragPainterWithLabelNoFloat, windowDockableDragPainterWithLabel, false);
        draggingMenuItems[6] = new DraggingMenuItem("Rectangle with transparent window (only fast computers)", swDockableDragPainterWithoutLabelNoFloat, transparentWindowDockableDragPainterWithoutLabel, false);
        draggingMenuItems[7] = new DraggingMenuItem("Labeled rectangle with labeled transparent window (only fast computers)", swDockableDragPainterWithLabelNoFloat, transparentWindowDockableDragPainterWithLabel, false);
        for (int index = 0; index < draggingMenuItems.length; index++) {
            draggingMenu.add(draggingMenuItems[index]);
            buttonGroup.add(draggingMenuItems[index]);
        }
        
        // End build the Dragging menu.
        // ######################################
        // End menus to move or replace.        
        
        // ###
        // End build view settings menu
        
        // ###        
        // Build help menu
        JMenu helpMenu = new JMenu("Help");
        viewMenu.setMnemonic(KeyEvent.VK_H);
        helpMenu.setToolTipText("Additional information about how to use Njord, iRules and more");
        helpMenu.getAccessibleContext().setAccessibleDescription("Additional information about how to use Njord, iRules and more");
        menuBar.add(helpMenu);

        //TODO: make this part work.
//        JMenuItem mntmNjordManual = new JMenuItem("Njord2 Manual"); // This goes to a website. I think perhaps it's time to create theuberlab.com and have the Njord help up there.
//        mntmNjordManual.setName("Njord2 Manual");
//        mntmNjordManual.setToolTipText("");
//        mntmNjordManual.addActionListener(this);
//        helpMenu.add(mntmNjordManual);
        
        JMenuItem mntmDevCentralHome = new JMenuItem("DevCentral Home Page", imageProvider.getIcon("WorldGo"));
        mntmDevCentralHome.setName("DevCentral Home Page");
        mntmDevCentralHome.setToolTipText("https://devcentral.f5.com/");
        mntmDevCentralHome.addActionListener(this);
        helpMenu.add(mntmDevCentralHome);
        
        JMenuItem mntmDevCentralForums = new JMenuItem("Devcentral Forums", imageProvider.getIcon("WorldGo"));
        mntmDevCentralForums.setName("Devcentral Forums");
        mntmDevCentralForums.setToolTipText("https://devcentral.f5.com/forums");
        mntmDevCentralForums.addActionListener(this);
        helpMenu.add(mntmDevCentralForums);
        
        JMenuItem mntmiRulesRef = new JMenuItem("iRules Reference", imageProvider.getIcon("WorldGo"));
        mntmiRulesRef.setName("iRules Reference");
        mntmiRulesRef.setToolTipText("https://devcentral.f5.com/wiki/irules.homepage.ashx");
        mntmiRulesRef.addActionListener(this);
        helpMenu.add(mntmiRulesRef);
        
        JMenuItem mntmTCLRef = new JMenuItem("TCL Reference",  imageProvider.getIcon("WorldGo"));
        mntmTCLRef.setName("TCL Reference");
        mntmTCLRef.setToolTipText("http://www.tcl.tk/man/tcl8.4/TclCmd/contents.htm");
        mntmTCLRef.addActionListener(this);
        helpMenu.add(mntmTCLRef);
        
        if (preferences.getDisplayExperimental()) {
            JMenuItem mntmiAppsRef = new JMenuItem("iApps Wiki",  imageProvider.getIcon("WorldGo"));
            mntmiAppsRef.setName("iApps Wiki");
            mntmiAppsRef.setToolTipText("https://devcentral.f5.com/wiki/iApp.HomePage.ashx");
            mntmiAppsRef.addActionListener(this);
            helpMenu.add(mntmiAppsRef);
        }
        
        //TODO: Make this work.
//        JMenuItem mntmCheckForUpdate = new JMenuItem("Check for Updates");
//        mntmCheckForUpdate.setName("Check for Updates");
//        mntmCheckForUpdate.setToolTipText("");
//        mntmCheckForUpdate.addActionListener(this);
//        helpMenu.add(mntmCheckForUpdate);
        
        helpMenu.addSeparator();
        
        //TODO: Implement this. I'll probably have to write something like what the newsfetcher will be for it.
//        JMenuItem mntmTotD = new JMenuItem("Tip of the Day");
//        mntmTotD.setName("Tip of the Day");
//        mntmTotD.setToolTipText("");
//        mntmTotD.addActionListener(this);
//        helpMenu.add(mntmTotD);
        
        //TODO: make this work. This one just has to open a basic dialog
        JMenuItem mntmAboutNjord = new JMenuItem("About Njord");
        mntmAboutNjord.setName("About Njord");
        mntmAboutNjord.setToolTipText("");
        mntmAboutNjord.addActionListener(this);
        helpMenu.add(mntmAboutNjord);
        
        
        
        //TODO: Move this bit into the actionPerformed and create a makeLinkable(JMenuItem that takes the link from the tooltipText just like the one for jlabel.
//        mntmDevCentralHome.setName("DevCentral Home Page");
        //        if (isBrowsingSupported()) {
//            makeLinkable(defaultResultsDevCentralURLLabel, new LinkMouseListener());
//        } else {
//            defaultResultsDevCentralURLLabel.setText(defaultResultsDevCentralURLLabel.getText() + ": " + defaultResultsDevCentralURLLabel.getToolTipText());
//        }
        
//        JMenuItem mntmUndo = new JMenuItem("Undo");
//        mntmUndo.setName("Undo");
//        mntmUndo.setToolTipText("Undo the last edit");
//        mntmUndo.addActionListener(this);
//        mntmUndo.setAccelerator(KeyStroke.getKeyStroke(
//                KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
//        editMenu.add(mntmUndo);
        
        // ###        
        // End build help menu
				
		return menuBar;
	}
	    
    public void mousePressed(MouseEvent e) {
        // getLastSelectedPathComponent might not work for me when I want to act on selecting a tab.
        // int selRow = navTree.getRowForLocation(e.getX(), e.getY());
        NjordTreeNode node = (NjordTreeNode) navTree.getLastSelectedPathComponent();
        
//        log.debug("MouseEvent with id[{}] triggered", e.getID());
//        AWTEventListener listener =
//                new AWTEventListener() {
//                  public void eventDispatched(AWTEvent event) {
//                      MouseEvent me = (MouseEvent)event;
//                    if (me.getID() == MouseEvent.MOUSE_PRESSED) {
//                        // Activate the dockable around me.getComponent().
//                  }
//                }
//              };
//          Toolkit toolkit = Toolkit.getDefaultToolkit();
//          toolkit.addAWTEventListener(listener, AWTEvent.MOUSE_EVENT_MASK);
        
        //TODO: Be smart enough here to handle null when you click.
        log.debug("Click detected in the nav tree. Object [{}] click count [{}]", node.name, e.getClickCount());
//        if(selRow != -1) {
            if (e.getClickCount() == 1) {
                log.debug("Single click in the nav tree detected");
                //TODO: In here I should do a lot of figuring out what they just clicked and enable/disable various buttons and menu items based on what is selected. Just make sure not to trigger anything excessive if they're about to double click. Enabling/diabling buttons should be fine since I'll want to do that anyway if they are double clicking something that warrants enabling/disabling.
                // The single click will always be detected even if it's about to be a double click. I need to handle that. Maybe I only care about double clicks anyway?
//                mySingleClick(node);
            } else if (e.getClickCount() == 2) {
                //TODO: Should this be getClickCount() >= 2?
                //TODO: Make it so that I don't open a new editor every time I double click the same rule.
                //TODO: Perhaps I should have an OpenEditors map which has the editor/object name and then a path to said object. Then I can make it the selected one if someone double clicks on an item which is already open.
                log.debug("Double click in the nav tree detected");
//                myDoubleClick(node);
                Dockable thisDockable = createDockable(node.name, (Component)node.getUserObject(), node.name, imageProvider.getIcon("RuleClean"), "An iRule");
                thisDockable = addAllActions(thisDockable);
                
                DockingPath dockingPath = DockingManager.getDockingPathModel().getDockingPath(dockables[3].getID());
                // Where do we want the dockable to be placed?
                DockingPath newDockingPath = DefaultDockingPath.copyDockingPath(thisDockable, dockingPath);
                DockingManager.getDockingPathModel().add(newDockingPath);
                // I might be able to replace the above with
                // addDockingPath(Dockable dockable)
                
                // If we've double clicked a rule then call it the last selected dockable
                log.debug("Double click on object type [{}]", node.getUserObject().getClass().toString());
                
                //TODO: I should move this check up higher and only do the dockingpath creation etc if it's an editor panel.
                if (node.getUserObject() instanceof NjordRuleEditorPanel) {
                  setActiveEditorPane((NjordRuleEditorPanel) node.getUserObject());
                }
                
                // Add the dockable to the main window.
                DockingManager.getDockingExecutor().changeDocking(thisDockable, dockingPath);
            }
    }
    
	/**
	 * Sets the look and feel on the application.
	 * 
	 * @param 	lafClassName	The class name of the new look and feel.
	 */
	private void setLookAndFeel(LAF laf) {

		try {
			UIManager.setLookAndFeel(laf.getClassName());
			LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
			LAF.setTheme(lookAndFeel, laf.getTheme());
			UIManager.setLookAndFeel(lookAndFeel);
			
			// Iterate over the owner windows of the dock model.
			for (int index = 0; index < dockModel.getOwnerCount(); index++) {
				// Set the LaF on the owner.
				Window owner = dockModel.getOwner(index);
				SwingUtilities.updateComponentTreeUI(owner);
				
				// Set the Laf on the floating windows.
				FloatDock floatDock = dockModel.getFloatDock(owner);
				for (int childIndex = 0; childIndex < floatDock.getChildDockCount(); childIndex++) {
					Component floatingComponent = (Component)floatDock.getChildDock(childIndex);
					SwingUtilities.updateComponentTreeUI(SwingUtilities.getWindowAncestor(floatingComponent));
				}
				
				// Set the LaF on all the dockable components.
				for (int dockableIndex = 0; dockableIndex < dockables.length; dockableIndex++) {
					SwingUtilities.updateComponentTreeUI(dockables[dockableIndex].getContent());
				}
				for (int dockableIndex = 0; dockableIndex < buttonDockablahs.length; dockableIndex++) {
					SwingUtilities.updateComponentTreeUI(buttonDockablahs[dockableIndex].getContent());
				}
			}
		} catch (Exception e) {
		    F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
		    exceptionHandler.processException();
		}
	}
	
	/**
	 * Creates a docking path for the dockable. This path is added to the docking pah model of the docking
	 * manager.
	 * To create a docking path, the dock model should already be given to the docking manager.
	 * 
	 * @param 	dockable	The dockable for which to create a docking path.
	 * @return				The created docking path.
	 */
	private DockingPath addDockingPath(Dockable dockable) {
		if (dockable.getDock() != null) {
			// Create the docking path of the dockable.
			DockingPath dockingPath = DefaultDockingPath.createDockingPath(dockable);
			DockingManager.getDockingPathModel().add(dockingPath);
			return dockingPath;
		}
		return null;
	}
	
	//TODO: Like WorkspaceSaver Update this to create the directory if needed. And/Or move all this to util/NjordWindowSaver. Perhaps rename NjordWindowSaver to NjordWorkspaceSaver.
	private void saveWorkspace(DockingPath centerDockingPath) {
		// Save the dock model.
		DockModelPropertiesEncoder encoder = new MyDockModelPropertiesEncoder(centerDockingPath);
		if (encoder.canSave(dockModel)) {
			try {
				encoder.save(dockModel);
			} catch (Exception e) {
				log.error("Error while saving the dock model.");
	            F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e, concierge);
	            exceptionHandler.processException();
//				e.printStackTrace();
			}
		} else {
			log.error("Could not save the dock model.");
		}
	}
	
	// Private classes.
	
	// TODO: expand on this.
	/**
	 * An action that shows a message in a dialog.
	 */
	private class MessageAction extends AbstractAction {
		private Component parentComponent;
		private String message;
		private String name;

		public MessageAction(Component parentComponent, String name, Icon icon, String message) {
			super(null, icon);
			putValue(Action.SHORT_DESCRIPTION, name);
			this.message = message;
			this.name = name;
			this.parentComponent = parentComponent;
		}
		
		public void actionPerformed(ActionEvent actionEvent) {
			JOptionPane.showMessageDialog(parentComponent,
					message, name, JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	/**
     * What a button does when clicked.
     */
    private class ButtonAction extends AbstractAction {
        // In this case parentComponent is Njord
        private Njord owner;
        private String message;
        private String name;

        public ButtonAction(Njord owner, String name, Icon icon) {
            super(null, icon);
            putValue(Action.SHORT_DESCRIPTION, name);
            this.name = name;
            this.owner = owner;
        }
        
        public ButtonAction(AbstractAction action, String name, Icon icon) {
            super(null, icon);
            putValue(Action.SHORT_DESCRIPTION, name);
            this.name = name;

        }
        
        public void actionPerformed(ActionEvent e) {
            owner.actionPerformed(e);
        }
        
    }
    
//	THIS SHOULD BE WHERE I FIX THE WHOLE MAXIMIZED THING
	//TODO: Update this to create the directory if needed. And/Or move all this to util/NjordWindowSaver. Perhaps rename NjordWindowSaver to NjordWorkspaceSaver.
	/**
	 * A listener for window closing events. Saves the workspace, when the application window is closed.
	 * 
	 * @author Heidi Rakels.
	 */
	private class WorkspaceSaver implements WindowListener {
		private DockingPath dockingPath;
		private NjordConcierge concierge;
		
		private WorkspaceSaver(NjordConcierge concierge, DockingPath centerDockingPath) {
		    this.concierge = concierge;
			if (centerDockingPath != null) {
				this.dockingPath = DefaultDockingPath.copyDockingPath("centerDockingPath", centerDockingPath);
			}
		}
		
		public void windowClosing(WindowEvent windowEvent)  {
		    if (preferences.getSaveWindowGeometry()) {
		        log.debug("Saving workspace geometry");
//              log.debug("Saving workspace geometry");
                saveWorkspace(dockingPath);
            } else {
                log.debug("Geometry saving disabled. Skipping.");
            }
		    
		    log.debug("Checking for unsaved content");
		    if (concierge.hasDirtyEditors()) {
		        log.debug("Unsaved changes. Prompting user for confirmation.");
                int result = JOptionPane.showConfirmDialog(null, concierge.getContentProvider().getCMContent("MESSAGE_QUIT_NJORD_DITRY_EDiTORS"), "Are you sure?", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    // O.K. close ahead.
                    log.debug("OK Chosen");
                    saveWorkspace(dockingPath);     
                    mainFrame.dispose();
                } else {
                    log.debug("OK Chosen");
                }
                // Else do nothing.
//              MESSAGE_DITRY_EDITOR_CLOSE
            } else {
                // All clear, close away.
                log.debug("No unsaved changes. Closing");
                mainFrame.dispose();
            }
		    
		}
		
		public void windowDeactivated(WindowEvent windowEvent) {}
		public void windowDeiconified(WindowEvent windowEvent) {}
		public void windowIconified(WindowEvent windowEvent) {}
		public void windowOpened(WindowEvent windowEvent) {}
		public void windowActivated(WindowEvent windowEvent) {} // This does _not_ get triggered when a tab is activated.
		public void windowClosed(WindowEvent windowEvent) {}
	}
	
	// I"m not sure this is implemented anywhere.
	/**
	 * A check box menu item to add or remove the dockable.
	 */
	private class DockableMenuItem extends JCheckBoxMenuItem {
		public DockableMenuItem(Dockable dockable) {
			super(dockable.getTitle(), dockable.getIcon());
			
			setSelected(dockable.getDock() != null);
			
			DockableMediator dockableMediator = new DockableMediator(dockable, this);
			dockable.addDockingListener(dockableMediator);
			addItemListener(dockableMediator);
		}
	}
	

	
//	/**
//	 * An action to create a new dockable.
//	 */
//	private class NewDockableAction extends AbstractAction {
//		private DockingPath centerDockingPath;
//		public NewDockableAction(DockingPath centerDockingPath) {
//			super("Create a very new window in a Dockable");
//			this.centerDockingPath = centerDockingPath;
//		}
//		
//		public void actionPerformed(ActionEvent actionEvent) {
//		    log.debug("Running actionPerformed(ActionEvent actionEvent)");
//			Dockable dockable = createDockable("new_window" + newDockableCount++, 	 new TextPanel("I am window " + newDockableCount + "."),      "I am very new dockable " + newDockableCount,  new ImageIcon(getClass().getResource("/images/hello12.gif")),     "This is a dockable that was created when the user clicked an action");		
//			if (centerDockingPath != null) {
//				DockingPath dockingPath = DefaultDockingPath.copyDockingPath(dockable, centerDockingPath);
//				DockingManager.getDockingExecutor().changeDocking(dockable, dockingPath);
//			} else {
//				// Get the root dock.
//				SplitDock totalSplitDock = (SplitDock)dockModel.getRootDock("totalDock");
//				DockingManager.getDockingExecutor().changeDocking(dockable, totalSplitDock);
//			}
//		}
//	}
	
	/**
	 * This is the class for basic content.
	 */
	private class TextPanel extends JPanel implements DraggableContent {
		private JLabel label; 
		
		public TextPanel(String text) {
			super(new FlowLayout());

			// The panel.
			setMinimumSize(new Dimension(80,80));
			setPreferredSize(new Dimension(150,150));
			setBackground(Color.white);
			setBorder(BorderFactory.createLineBorder(Color.lightGray));

			// The label.
			label = new JLabel(text);
			label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			add(label);
		}
		
		// Implementations of DraggableContent.
		
		public void addDragListener(DragListener dragListener) {
			addMouseListener(dragListener);
			addMouseMotionListener(dragListener);
			label.addMouseListener(dragListener);
			label.addMouseMotionListener(dragListener);
		}
	}
	
	/**
	 * A check box menu item to enable a look and feel.
	 */
	private class LafMenuItem extends JRadioButtonMenuItem {
		public LafMenuItem(LAF laf) {
			super(laf.getTitle());
			
			// Is this look and feel supported?
			if (laf.isSupported()) {
				LafListener lafItemListener = new LafListener(laf);
				addActionListener(lafItemListener);
			} else {
				setEnabled(false);
			}
			
			if (laf.isSelected()) {
				setSelected(true);
			}
		}
	}
	
	/**
	 * A listener that installs its look and feel.
	 */
	private class LafListener implements ActionListener {
		// Fields.
		private LAF laf;
		
		// Constructors.
		public LafListener(LAF laf) {
			this.laf = laf;
		}
		
		// Implementations of ItemListener.
		public void actionPerformed(ActionEvent arg0) {
			for (int index = 0; index < LAFS.length; index++) {
				LAFS[index].setSelected(false);
			}
			setLookAndFeel(laf);
			laf.setSelected(true);
		}
	}
	
	/**
	 * A check box menu item to enable a dragger.
	 */
	private class DraggingMenuItem extends JRadioButtonMenuItem {
		// Constructor.
		public DraggingMenuItem(String title, DockableDragPainter basicDockableDragPainter, DockableDragPainter additionalDockableDragPainter, boolean selected) {
			super(title);
			
			// Create the dockable drag painter and dragger factory.
			CompositeDockableDragPainter compositeDockableDragPainter = new CompositeDockableDragPainter();
			compositeDockableDragPainter.addPainter(basicDockableDragPainter);
			if (additionalDockableDragPainter != null) {
				compositeDockableDragPainter.addPainter(additionalDockableDragPainter);
			}
			DraggerFactory draggerFactory 	= new StaticDraggerFactory(compositeDockableDragPainter);
			
			// Give this dragger factory to the docking manager.
			if (selected) {
				DockingManager.setDraggerFactory(draggerFactory);
				setSelected(true);
			}
			
			// Add a dragging listener as action listener.
			addActionListener(new DraggingListener(draggerFactory));
		}
	}
	
	/**
	 * A listener that installs a dragger factory.
	 */
	private class DraggingListener implements ActionListener {
		// Fields.
		private DraggerFactory draggerFactory;
		
		// Constructor.
		public DraggingListener(DraggerFactory draggerFactory) {
			this.draggerFactory = draggerFactory;
			log.debug("Creating DraggingListener.");
		}
		
		// Implementations of ItemListener.
		public void actionPerformed(ActionEvent actionEvent) {
			DockingManager.setDraggerFactory(draggerFactory);
			log.debug("DraggingListener.actionPerformed(ActionEvent) triggered");
		}
	}
	
	/**
	 * A listener that listens when menu items with dockables are selected and deselected.
	 * It also listens when dockables are closed or docked.
	 */
	private class DockableMediator implements ItemListener, DockingListener {
		private Dockable dockable;
		private Action closeAction;
		private Action restoreAction;
		private JMenuItem dockableMenuItem;
		
		public DockableMediator(Dockable dockable, JMenuItem dockableMenuItem) {
			this.dockable = dockable;
			this.dockableMenuItem = dockableMenuItem;
			closeAction = new DefaultDockableStateAction(dockable, DockableState.CLOSED);
			restoreAction = new DefaultDockableStateAction(dockable, DockableState.NORMAL);
		}
		
		public void itemStateChanged(ItemEvent itemEvent) {
			dockable.removeDockingListener(this);
			log.debug("Dockablemediator.itemStateChanged(ItemEvent) Triggered");
			if (itemEvent.getStateChange() == ItemEvent.DESELECTED) {
				// Close the dockable.
				closeAction.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Close"));
			} else {
				// Restore the dockable.
				restoreAction.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Restore"));
			}
			dockable.addDockingListener(this);
		}
		
		public void dockingChanged(DockingEvent dockingEvent) {
			if (dockingEvent.getDestinationDock() != null) {
				dockableMenuItem.removeItemListener(this);
				dockableMenuItem.setSelected(true);
				dockableMenuItem.addItemListener(this);	
			} else {
				dockableMenuItem.removeItemListener(this);
				dockableMenuItem.setSelected(false);
				dockableMenuItem.addItemListener(this);
			}
		}
		
		public void dockingWillChange(DockingEvent dockingEvent) {}
	}
	
	/**
	 * No idea what this bit does.
	 * <code>WorkspaceComponentFactory</code>
	 * 
	 * @author Aaron Forster (Originally Heidi)
	 *
	 */
	private class WorkspaceComponentFactory extends SampleComponentFactory {

		public DockHeader createDockHeader(LeafDock dock, int orientation)
		{
			return new PointDockHeader(dock, orientation);
		}

//		 // Component factory
//	    DockingManager.setComponentFactory(new DefaultSwComponentFactory() {
		
		// This part triggers when an editor tab within the main window is selected.
		// It triggers whenever an editor panel is selected.
		// It triggers when the messeging panel is selecgted if an editor panel has been selected first.
		// It does _not_ trigger when you click within an editor itself. That part is handled by the mouse and focus listeners
		// within the private FocusRequester class inside of NjordRuleEditorPanel.
	        @Override
	        public JTabbedPane createJTabbedPane() {
	            final JTabbedPane tabbedPane = super.createJTabbedPane();
	            // Listens for the selection
	            tabbedPane.addChangeListener(new ChangeListener() {

	                @Override
	                public void stateChanged(ChangeEvent e) {
	                    log.debug("ChangeEvent detected for event [{}]", e.toString());
	                    // Tab dock ancestor
	                    TabDock tabDock = (TabDock) SwingUtilities
	                            .getAncestorOfClass(TabDock.class, tabbedPane);
	                    if (tabDock != null) {
	                        // Gets the selected dockable
	                        Dockable dockable = tabDock.getSelectedDockable();
	                        if (dockable != null) {
	                            Component content = dockable.getContent();
	                            log.debug("Content is of class [{}]", content.getClass().toString());
	                            if (content instanceof NjordRuleEditorPanel) {
	                                log.debug("It's an editorPanel, let's call it the last active one.");
	                                setActiveEditorPane((NjordRuleEditorPanel)content);
	                                //TODO: also highlight said rule in the nav tree. See the below for how.
//	                                http://stackoverflow.com/questions/8210630/how-to-search-a-particular-node-in-jtree-and-make-that-node-expanded
	                            }
	                        }
	                    }
	                }

	            });
	            // OK
	            return tabbedPane;
	        }

//	    });
		
	}
	
	/**
	 * Get's the active editor panel.
	 * 
	 * @param editorPanel
	 */
    public NjordEditorPanel getActiveEditorPane() {
        log.debug("Getting last active panel");
        return lastActiveEditorPanel;
    }
	//TODO: change this to implement NjordEditorPanel and make sure all editor panels extend it.
    //TODO: Make this not public and have it so that there's a better way to do it. Like the various components tell the concierge
	/**
	 * Sets the last active editor panel.
	 * 
	 * @param editorPanel
	 */
	public void setActiveEditorPane(NjordRuleEditorPanel editorPanel) {
	    log.debug("Setting last active panel to [{}]", editorPanel.getName());
	    // Also tell the editor to update it's view settings when we make it active.
	    editorPanel.updateViewSettingsFromPrefs();
	    lastActiveEditorPanel = editorPanel;
	    
	    
	//THis is how rtext does it.
	    //   /**
//	     * Sets the currently active text area.
//	     *
//	     * @param textArea The text area to make active.
//	     * @return Whether the text area was made active.  This will be
//	     *         <code>false</code> if the text area is not contained in this
//	     *         view.
//	     * @see #setSelectedIndex(int)
//	     */
//	    public boolean setSelectedTextArea(RTextEditorPane textArea) {
//	        for (int i=0; i<getNumDocuments(); i++) {
//	            RTextEditorPane ta2 = getRTextEditorPaneAt(i);
//	            if (ta2!=null && ta2==textArea) {
//	                setSelectedIndex(i);
//	                return true;
//	            }
//	        }
//	        return false;
//	    }
	
	}
	
	

	/**
	 * <code>MyDockModelPropertiesEncoder</code> is responsible for saving window geometry information.
	 * 
	 * @author Aaron Forster
	 *
	 */
	private class MyDockModelPropertiesEncoder extends DockModelPropertiesEncoder {
		private DockingPath centerDockingPath;
		
		private MyDockModelPropertiesEncoder(DockingPath centerDockingPath) {
			this.centerDockingPath = centerDockingPath;
		}
		
		protected void saveProperties(DockModel dockModel, DockingPathModel dockingPathModel, Properties properties, Map dockKeys)  {
			super.saveProperties(dockModel, dockingPathModel, properties, dockKeys);
			if (centerDockingPath != null) {
				centerDockingPath.saveProperties(CENTER_DOCKING_PATH_ID, properties, dockKeys);
			}
		}
	}
	
	//TODO: Turn this into a real class instead of a private class?
	private class NjordDockModelPropertiesDecoder extends DockModelPropertiesDecoder {
		private DockingPath centerDockingPath;
		protected DockModel decodeProperties(Properties properties, String sourceName, Map dockablesMap, Map ownersMap, Map visualizersMap, Map docks) throws IOException  {
			DockModel dockModel = super.decodeProperties(properties, sourceName, dockablesMap, ownersMap, visualizersMap, docks);
			if (dockModel != null) {
				centerDockingPath = new DefaultDockingPath();
				centerDockingPath.loadProperties(CENTER_DOCKING_PATH_ID, properties, docks);
				if (centerDockingPath.getID() == null) {
					System.out.println("The file 'workspace_1_5.dck' of an older version is still available, remove this file.");
					centerDockingPath = null;
				}
			}
			
			return dockModel;
		}
		public DockingPath getCenterDockingPath() {
			return centerDockingPath;
		}
	}
	
	// The Main method.
	public static void createAndShowGUI() { 
		// Create the look and feels.
		LAFS  = new LAF[9];	
		LAFS[0] = new LAF("Substance", "org.jvnet.substance.skin.SubstanceModerateLookAndFeel", LAF.THEME_DEAULT);
		LAFS[1] = new LAF("Mac", "javax.swing.plaf.mac.MacLookAndFeel", LAF.THEME_DEAULT);
		LAFS[2] = new LAF("Metal", "javax.swing.plaf.metal.MetalLookAndFeel", LAF.THEME_DEAULT);
		LAFS[3] = new LAF("Liquid", "com.birosoft.liquid.LiquidLookAndFeel", LAF.THEME_DEAULT);
		LAFS[4] = new LAF("Windows", "com.sun.java.swing.plaf.windows.WindowsLookAndFeel", LAF.THEME_DEAULT);
		LAFS[5] = new LAF("Nimrod Ocean", "com.nilo.plaf.nimrod.NimRODLookAndFeel", LAF.THEME_OCEAN);
		LAFS[6] = new LAF("Nimrod Gold", "com.nilo.plaf.nimrod.NimRODLookAndFeel", LAF.THEME_GOLD);
		LAFS[7] = new LAF("Nimbus", "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel", LAF.THEME_DEAULT);
		LAFS[8] = new LAF("TinyLaF", "de.muntjak.tinylookandfeel.TinyLookAndFeel", LAF.THEME_DEAULT);
		
		// Set the first enabled look and feel.
		try  {
			if (LAFS[7].isSupported()) {
				LAFS[7].setSelected(true);
				UIManager.setLookAndFeel(LAFS[7].getClassName());
			}
		} catch (Exception e) {
		    //Here we're going to dump a stack trace because of where we are in the process.
		    e.printStackTrace();
		}
		
		// Remove the borders from the split panes and the split pane dividers.
		LookAndFeelUtil.removeAllSplitPaneBorders();
		
		// Create the frame.
//		String versString = "Njord v" + NjordVersion;
		JFrame frame = new JFrame("Njord v" + NjordVersion);

		// Set the default location and size.
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation((screenSize.width - defaultFrameWidth) / 2, (screenSize.height - defaultFrameHeight) / 2);
		frame.setSize(defaultFrameWidth, defaultFrameHeight);
		
		// Create the panel and add it to the frame.
		Njord panel = new Njord(frame);
		frame.getContentPane().add(panel);
		
		// Removes the default closing action. Either way windowClosing(Event e) is triggered. This way unless we explicitly call
        // dispose() it won't actually close.
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
		// Set the frame properties and show it.
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	/**
	 * The actual main method.
	 * @param args
	 */
	public static void main(String args[]) {
		Runnable doCreateAndShowGUI = new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		};
		SwingUtilities.invokeLater(doCreateAndShowGUI);
	}
	
	//TODO: Since everything is dockable and everything can thus be removed. See if there's a way to check and see if the messages dock has been closed and then if we get a notify then re-open it. (or at least set an icon.)
	/**
	 * Eventually this will be the part that puts a message in the message window.
	 * @see com.theuberlab.common.interfaces.TUNotifiable#tuNotify(java.lang.String)
	 */
	@Override
	public void tuNotify(String message) {
	    tuNotify(message, false);
//	    // For now notify is just going to write to the noticesTextArea and log whatever message. Later it should be modified for more advanced behavior. Potentially forcing dialogs or something.
//	    noticesTextArea.setText(message);
//	    log.info(message);
	}
	
	public void tuNotify(String message, TUNotificationType notificationType) {
	    tuNotify(message, true);
	}
	
	/**
     * Eventually this will be the part that puts a message in the message window.
     * @see com.theuberlab.common.interfaces.TUNotifiable#tuNotify(java.lang.String)
     */
    @Override
    public void tuNotify(String message, boolean append) {
        // For now notify is just going to write to the noticesTextArea and log whatever message. Later it should be modified for more advanced behavior. Potentially forcing dialogs or something.
        if (append) {
            noticesTextArea.append(nl + message);
        } else {
            noticesTextArea.setText(message);
        }
        
        log.info(message);
    }
	
	/**
	 * @see com.theuberlab.common.interfaces.TUNotifiable#getLoggerHandle()
	 */
//	@Override
    public Logger getLoggerHandle() {
		// TODO Auto-generated method stub
		return log;
	}
	
	/**
	 * Returns a NjordPreferences for this instance of Njord so that sub objects can figure out what preference settings are set.
	 * 
	 * @return
	 */
	public NjordPreferences getPreferences() {
	    return preferences;
	}
	
	/**
	 * This needs to be written.
	 */
	public boolean checkForUnsaved() {
	    return false;
	}
	
	/**
     * Triggered when someone clicks the X on the main window. Should give me a chance to make sure that everything is saved.
     * 
     * @param e
     */
    public void windowClosing(WindowEvent e) {
        //I totally need a displayMessage
//        displayMessage("WindowListener method called: windowClosing.");
        //A pause so user can see the message before
        //the window actually closes.
        // This bit here is handled automatically by the workspacesaver
//        try {
//            WindowSaver.saveSettings( );
//        } catch (IOException e1) {
//            f5ExceptionHandler exceptionHandler = new f5ExceptionHandler(e1);
//            exceptionHandler.processException();
//        }
        
//        preferences.writePreferences();
        //TODO: all plugins need to implement a way to check and see if there are unsaved objects of their type.
        //Check and see if we have any unsaved iRules.
        if ( !checkForUnsaved() ) {
            //unsavediRules returned false so we should exit
            log.info("No unsaved rules. Quitting normally");
            mainFrame.dispose();
        } else {
            log.info("Canceling shutdown.");
        }

    }
    
    public void doSearch(String searchText) {
        find.doSearch(searchText);
    }
    
    /**
     * Initialilzes all the plugins in the plugin directory. Currently that's not much.
     */
    private void initPlugins() {
        log.debug("Running initPlugins()");
        pluginManager = new NjordPluginManager(this);
    }
    
    /**
     * Initializes a connection to the specified BigIP.
     * Runs the initialize method on the library which sets up the connection object. Then do a get version. If it works we've got good connection settings.
     * @return
     */
    protected iControl.Interfaces getConnectionTo(BigIP bigip) {
//        lblStatusLabel.setText("Connecting");
//        resultsPanelNoticesBox.setText("");
        String ipAddress = bigip.gethostNameOrAddress();
        long port = bigip.getiControlPort();
        String userName = bigip.getUserName();
        String passWord = bigip.getPassword();
        String version = null;
        String bigIPVersion = null;
        iControl.Interfaces ic = new iControl.Interfaces(); //From Assembly
        ic.initialize(ipAddress, port, userName, decryptPassword(passWord)); // Initialize the interface to the BigIP
        
        try {
            //TODO: There has to be a better way to do this
            version = ic.getSystemSystemInfo().get_version();
            Pattern versionPrefix = Pattern.compile("BIG-IP_v");
            Matcher versionMatcher = versionPrefix.matcher(version);
            bigIPVersion = versionMatcher.replaceAll(""); //Kinda redundant, I know.
        } catch (RemoteException e1) {  
            //TODO: Add a return type so I can do a return here.
            F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e1, concierge);
            exceptionHandler.processException();
        } catch (ServiceException e1) {
            F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e1, concierge);
            exceptionHandler.processException();
        } catch (Exception e1) {
            F5ExceptionHandler exceptionHandler = new F5ExceptionHandler(e1, concierge);
            exceptionHandler.processException();
        }
        
        boolean success = false;
        //TODO: Move this part, the setting of the progress bar and displaying of connection validity out of this section and maybe back into the listener. We should only be returning true/false here.
        if (version != null) {
            log.debug("My Big-IP is version:" + bigIPVersion);
            if ( bigIPVersion.startsWith("10") ) {
                tuNotify("v" + bigIPVersion + ", success");
                bigip.setVersion(version);
                bigip.setInterface(ic);
//                resultsPanelNoticesBox.setText("Connected to BIG-IP version: " + bigIPVersion);
//                connectionInitialized = true;
//                success = true; //It worked
//                lblStatusLabel.setText("Connected");
            } else if (bigIPVersion.startsWith("11")) {
                tuNotify("v" + bigIPVersion + ", success");
                bigip.setVersion(version);
                bigip.setInterface(ic);
//                resultsPanelNoticesBox.setText("Connected to BIG-IP version: " + bigIPVersion);
//                connectionInitialized = true;
//                success = true; //It worked;
//                lblStatusLabel.setText("Connected");
            } else if (bigIPVersion.startsWith("9")) {
                tuNotify("v" + bigIPVersion + ", unclear");
                bigip.setVersion(version);
                bigip.setInterface(ic);
//                resultsPanelNoticesBox.setText("njord is currently untested on BIG-IP version 9 systems. Proceed at your own risk.");
//                connectionInitialized = true;
//                success = true; 
//                lblStatusLabel.setText("Connected");
            } else {
                tuNotify("v" + bigIPVersion + ", fail");
//                resultsPanelNoticesBox.setText("BIG-IP version: " + bigIPVersion + "Is unsupported. You must connect to a BIG-IP version 9.4 or higher.");
//                connectionInitialized = false;
//                success = false;
//                lblStatusLabel.setText("Disconnected");
            }
            //Now populate the provisioned modules list
            
//            try {
//                provisionedModulesList = ic.getManagementProvision().get_provisioned_list();
//            } catch (RemoteException e) {
//                f5ExceptionHandler exceptionHandler = new f5ExceptionHandler(e);
//                exceptionHandler.processException();
//            } catch (Exception e) {
//                f5ExceptionHandler exceptionHandler = new f5ExceptionHandler(e);
//                exceptionHandler.processException();
//            }
            
            return ic;
//            return success;
        } else {
            // scream, run and cry
            //TODO: Check the return code of the exception to see what the cause of failure is.
            log.error("Connection settings invalid");
//            connectionInitialized = false;
//            return false; //We are failz
            return ic;
        }
    }
    
    // #########################################################################################################################
    // #### Methods to make clickable objects
    // #########################################################################################################################
    
    /**
     * Takes a JLabel and converts it's text into a hyperlink using it's tooltipText as the link tarket.
     * 
     * @param label The label to link. 
     * @param ml A mouse listener to notify us when a click event has happened.
     */
    private static void makeLinkable(JLabel label, MouseListener ml) {
        assert ml != null;
        label.setText(htmlIfy(linkIfy(label.getText(), label.getToolTipText())));
        label.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        label.addMouseListener(ml);
    }
    
    /**
     * Checks to see if the platform we are executing on supports Desktop Browsing.
     * @return True/False.
     */
    private static boolean isBrowsingSupported() {
        if (!Desktop.isDesktopSupported()) {
            return false;
        }
        boolean result = false;
        Desktop desktop = java.awt.Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            result = true;
        }
        return result;

    }
    
    /**
     * The mouse event listener for the desktop browsing interface. Detects the mouse click event and opens a browser.
     *
     */
    private static class LinkMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(java.awt.event.MouseEvent evt) {
            JLabel l = (JLabel) evt.getSource();
            try {
                Desktop desktop = java.awt.Desktop.getDesktop();
                URI uri = new java.net.URI(getPlainLink(l.getText()));
                desktop.browse(uri);
            } catch (URISyntaxException use) {
                throw new AssertionError(use);
            } catch (IOException ioe) {
                ioe.printStackTrace();
                JOptionPane.showMessageDialog(null, "Sorry, a problem occurred while trying to open this link in your system's standard browser.", "A problem occured", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * Converts a string into an HTML link. This method requires that s is a plain string that requires no further escaping.
     * 
     * @param s A String to be converted into an HTML link.
     * @return HTML text.
     */
    private static String getPlainLink(String s) {
        return s.substring(s.indexOf(A_HREF) + A_HREF.length(), s.indexOf(HREF_CLOSED));
    }
    
    /**
     * Creates an HTML link from the provided data.
     * 
     * @param textString The text to be presented.
     * @param linkLocation The URL Location to use as the destination of the HREF.
     * @return
     */
    private static String linkIfy(String textString, String linkLocation) {
        return A_HREF.concat(linkLocation).concat(HREF_CLOSED).concat(textString).concat(HREF_END);
    }
    
    //WARNING
    //This method requires that s is a plain string that requires
    //no further escaping
    
    /**
     * Surrounds a string with HTML open and close HTML tags.
     * 
     * @param s Non HTML String text.
     * @return The same text surrounded with HTML open and close text.
     */
    private static String htmlIfy(String s) {
        return HTML.concat(s).concat(HTML_END);
    }
    
    /**
     * Returns a decrypted version of the provided text. If the master password has not yet been provided will prompt the user for it.
     * 
     * @param encrypedPassword
     * @return
     */
    public String decryptPassword(String encrypedPassword) {
        String decryptPassword = null;
        if (encrypedPassword.startsWith("ENC")) {
            log.debug("Decrypting password");
            
            SimplePBEConfig config = new SimplePBEConfig(); 
            config.setAlgorithm("PBEWithMD5AndTripleDES");
            config.setKeyObtentionIterations(1000);
            
            char[] masterPassword = preferences.getMasterPassword();
            
            log.debug("MasterPassword starts as [{}]", masterPassword);
            log.debug("MasterPassword as a string starts as [{}]", new String(masterPassword));
            log.debug("comparing it to [{}]", "PROMPT".toCharArray());
            
            //We haven't fetched the password from the user yet.
            if (Arrays.equals(masterPassword,"PROMPT".toCharArray())) {
                FlexibleTextDialog passwordPrompt = new FlexibleTextDialog(this, mainFrame, njordContentProvidor.getCMContent("ENCRYPTION_MP_TEXT"), njordContentProvidor.getCMContent("ENCRYPTION_MP_FIELD_TEXT"), njordContentProvidor.getCMContent("ENCRYPTION_MP_TOOLTIP_TEXT"), true, true);
                int result = JOptionPane.showConfirmDialog(null, passwordPrompt.panel_1, njordContentProvidor.getCMContent("ENCRYPTION_MP_TITLE"), JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    preferences.setMasterPassword(masterPassword);
                    masterPassword = passwordPrompt.getHiddenInputText();
                    log.debug("MasterPassword value is [{}]", masterPassword);
                }
            }
            
            config.setPasswordCharArray(masterPassword);
            
            StandardPBEStringEncryptor encryptor = new org.jasypt.encryption.pbe.StandardPBEStringEncryptor();
            encryptor.setConfig(config);
            encryptor.initialize();
            
            try {
                decryptPassword = PropertyValueEncryptionUtils.decrypt(encrypedPassword, encryptor);
            } catch (org.jasypt.exceptions.EncryptionOperationNotPossibleException e) {
                // Throw decrypt failed error and redisplay master password dialog.
                log.error("Decryption Failed");
                tuNotify("Decryption Failed");
               
                FlexibleTextDialog passwordPrompt = new FlexibleTextDialog(this, mainFrame, njordContentProvidor.getCMContent("ENCRYPTION_MP_TEXT"), njordContentProvidor.getCMContent("ENCRYPTION_MP_FIELD_TEXT"), njordContentProvidor.getCMContent("ENCRYPTION_MP_TOOLTIP_TEXT"), true, true);
                int result = JOptionPane.showConfirmDialog(null, passwordPrompt.panel_1, njordContentProvidor.getCMContent("ENCRYPTION_MP_TITLE"), JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    preferences.setMasterPassword(masterPassword);
                    masterPassword = passwordPrompt.getHiddenInputText();
                    decryptPassword = decryptPassword(encrypedPassword);
                }
            }
        } else {
            // password not encrypted return original string
            decryptPassword = encrypedPassword;
        }
            
        
        
        return decryptPassword;
    }
    
    /**
     * Returns an encrypted version of the provided text. If the master password has not yet been provided will prompt the user for it.
     * 
     * @param clearPassword
     * @return
     */
    public String encryptPassword(String clearPassword) {
        log.debug("Encrypting password");
        String encryptPassword = null;
        
        SimplePBEConfig config = new SimplePBEConfig(); 
//        config.setAlgorithm("PBEWithMD5AndTripleDES");
        config.setKeyObtentionIterations(1000);
        
        char[] masterPassword = preferences.getMasterPassword();
        
        log.debug("MasterPassword starts as [{}]", masterPassword);
        log.debug("MasterPassword as a string starts as [{}]", masterPassword.toString());
        log.debug("comparing it to [{}]", "PROMPT".toCharArray());
        
        //We haven't fetched the password from the user yet.
        if (Arrays.equals(masterPassword,"PROMPT".toCharArray())) {
            FlexibleTextDialog passwordPrompt = new FlexibleTextDialog(this, mainFrame, njordContentProvidor.getCMContent("ENCRYPTION_MP_TEXT"), njordContentProvidor.getCMContent("ENCRYPTION_MP_FIELD_TEXT"), njordContentProvidor.getCMContent("ENCRYPTION_MP_TOOLTIP_TEXT"), true, true);
            int result = JOptionPane.showConfirmDialog(null, passwordPrompt.panel_1, njordContentProvidor.getCMContent("ENCRYPTION_MP_TITLE"), JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                preferences.setMasterPassword(masterPassword);
                masterPassword = passwordPrompt.getHiddenInputText();
                log.debug("MasterPassword value is [{}]", masterPassword);
            }
        }
        
        config.setPasswordCharArray(masterPassword);
        
        StandardPBEStringEncryptor encryptor = new org.jasypt.encryption.pbe.StandardPBEStringEncryptor();
        encryptor.setConfig(config);
        encryptor.initialize();
        
        try {
            encryptPassword = PropertyValueEncryptionUtils.encrypt(clearPassword, encryptor);
        } catch (org.jasypt.exceptions.EncryptionOperationNotPossibleException e) {
            // Throw encrypt failed error and redisplay master password dialog.
            log.error("Encryption Failed");
            tuNotify("Encryption Failed.");
            FlexibleTextDialog passwordPrompt = new FlexibleTextDialog(this, mainFrame, njordContentProvidor.getCMContent("ENCRYPTION_MP_TEXT"), njordContentProvidor.getCMContent("ENCRYPTION_MP_FIELD_TEXT"), njordContentProvidor.getCMContent("ENCRYPTION_MP_TOOLTIP_TEXT"), true, true);
            int result = JOptionPane.showConfirmDialog(null, passwordPrompt.panel_1, njordContentProvidor.getCMContent("ENCRYPTION_MP_TITLE"), JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                preferences.setMasterPassword(masterPassword);
                masterPassword = passwordPrompt.getHiddenInputText();
                encryptPassword = encryptPassword(clearPassword);
            }
        }
        return encryptPassword;
    }
    
    /**
     * Prompts for text input.
     * 
     * @param title What title to give the dialog box.
     * @param message The text to place in the message body.
     * @param defaultText A default value to place in the text field.
     * @return
     */
    public String promptForText(String title, String message, String defaultText) {
        String userText = defaultText;
        userText = (String)JOptionPane.showInputDialog(
                            mainFrame,
                            message,
                            title,
                            JOptionPane.PLAIN_MESSAGE,
                            null, // I'm not entirely sure this will work.
                            null,
                            defaultText);
        
        return userText;
    }
    
    
    
    
    
    
    // #########################################################################################################################
    // #### Methods Directly related to the new Threaded MVC architecture
    // #########################################################################################################################
    
    
    
    
    public boolean getBusyState() {
        return busyState;
    }
    
    public void setBusyState(boolean isBusy) {
        busyState = isBusy;
    }
    
    //TODO: this should probably move to the plugin architecture and be more generic.
    public BigIP getSelectedOrigin() {
//        NjordTreeNode treeNode = (NjordTreeNode) navTree.getLastSelectedPathComponent();
//        BigIP fetchBigIP = treeNode.getBigIP();
        
        return navTree.getLastSelectedOrigin().getBigIP();
    }
    
    public NjordNodeType getLastSelectedPathComponentType() {
        return ((NjordTreeNode) navTree.getLastSelectedPathComponent()).getNodeType();
    }
    
    
    
    
    // #########################################################################################################################
	// #### Methods I probably don't want to retain.
    // #########################################################################################################################
	
    /**
     * Returns the class name minus the leading package portion.
     * I'll probably want to get rid of this after testing
     * 
     * @param o A class.
     * @return
     */
    protected String getClassName(Object o) {
        String classString = o.getClass().getName();
        int dotIndex = classString.lastIndexOf(".");
        return classString.substring(dotIndex+1);
    }
}
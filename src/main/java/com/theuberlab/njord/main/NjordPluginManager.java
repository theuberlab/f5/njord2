package com.theuberlab.njord.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.exceptions.NoSuchPluginRegisteredException;
import com.theuberlab.njord.plugin.interfaces.NjordPluggable;
import com.theuberlab.njord.plugin.plugins.NjordiRulePlugin;

/**
 * <code>NjordPluginManager</code> is the class that will manage plugins. I can't just have a global list without already knowing what 
 * plugins I will have. I can, however create a NjordPluginManager object and call a getPluginsList() or something like that.
 * 
 * @author Aaron Forster
 *
 */
public class NjordPluginManager {
//    private List<NjordPluggable> plugins = new ArrayList<NjordPluggable>();
    Map<String, NjordPluggable> plugins = new HashMap<String, NjordPluggable>();
    private Njord owner;
    public Logger log;
    
    /**
     * 
     */
    public NjordPluginManager(Njord owner) {
        this.owner = owner;
        log = owner.getLoggerHandle();
        
//        plugins = registerPlugins();
        plugins = registerPluginsTemp();
    }
    // ###############################################################
    // VARIABLES
    // ###############################################################
    
    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################
    
    // ###############################################################
    // GETTERS AND SETTERS
    // ###############################################################
    
    // ###############################################################
    // METHODS
    // ###############################################################
    /**
     * This will eventually be how I load the plugins from the plugins directory but it's not working right now.
     * 
     * @return
     */
    private List<NjordPluggable> registerPlugins() {
        //This is sort of how I WILL do it. 
        Resource[] pluginsList;
        
        // TODO Auto-generated constructor stub
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        List<NjordPluggable> registeredPlugins = new ArrayList<NjordPluggable>();
        try {
            pluginsList = resolver.getResources("classpath*:com/theuberlab/njord/plugin/plugins/*");
            log.debug("Found [{}] plugins", pluginsList.length);
            for (int i = 0; i < pluginsList.length; i++) {
                // Skip package-info
                if (pluginsList[i].getFilename().equals("package-info.class")) {
                    log.debug("Skipping plugin [{}]");
                    continue;
                }
                log.info("Adding plugin [{}]", pluginsList[i].getFilename());
                pluginsList[i].getClass();
                //o.k. this part isn't working. It's generating an instantiationException with no real information in it. 
                NjordPluggable addPlugin = (NjordPluggable) pluginsList[i].getClass().newInstance();
                addPlugin.getFullName();
                log.info("Instantiated plugin [{}]", addPlugin.getShortName());
                registeredPlugins.add(addPlugin);
            }
        } catch (IOException e) {
            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e, owner);
            exceptionHandler.processException();
        } catch (InstantiationException e ) {
            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e, owner);
            exceptionHandler.processException();
        } catch (IllegalAccessException e) {
            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e, owner);
            exceptionHandler.processException();
        }
        return registeredPlugins;
    }
    
    /**
     * A bogus method until I get registerPlugins() working.
     * 
     * @return
     */
    private Map<String, NjordPluggable> registerPluginsTemp() {
        //This is sort of how I WILL do it. 
        Resource[] pluginsList;
        
        // TODO Auto-generated constructor stub
//        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Map<String, NjordPluggable> registeredPlugins = new HashMap<String, NjordPluggable>(); 
//                ArrayList<>();
        
        log.info("Registering plugin [NjordLocalRulPlugin]");
        NjordPluggable addPlugin = (NjordPluggable) new NjordiRulePlugin();
//        addPlugin.getFullName();
        log.info("Instantiated plugin [{}]", addPlugin.getShortName());
        registeredPlugins.put("IRULE", addPlugin);
//        registeredPlugins.add(addPlugin);
        
        return registeredPlugins;
    }
    
    /**
     * Curently a  hackish thing until I get plugins working properly.
     * This will return an implicit path to these objects. it will always start with /Njord/Local/
     * @param pluginCategory
     * @return
     */
    public String getLocalPathFor(String pluginCategory) {
        String implicitPath = "/Njord/Local/";
        
      if (pluginCategory.equals("IRULE")) {
//          localObjectsDirPath = documentsDir.getAbsolutePath() + "/Njord/Local/iAppsTemplates/";
          implicitPath = "/Njord/Local/iRules/";
      } else if (pluginCategory.equals("IAPPS_TEMPLATE")) {
          implicitPath = "/Njord/Local/iAppsTemplates/";
      }
      
      return implicitPath;
////      
//      switch (nodeType) {
//          case NODE_TYPE_IAPPS_TEMPLATE: {
//              localObjectsDirPath = documentsDir.getAbsolutePath() + "/Njord/Local/iAppsTemplates/";
              // editorType = NjordConstants.NODE_TYPE_IAPPS_TEMPLATE;
//              break;
//          }
//          case NODE_TYPE_IRULE: {
//              localObjectsDirPath = documentsDir.getAbsolutePath() + "/Njord/Local/iRules/";
              // editorType = NjordNodeType.NODE_TYPE_EDITABLE;
//              break;
//          }
    }
    
    public NjordPluggable getPluginForCategory(String category) throws NoSuchPluginRegisteredException {
        NjordPluggable retPlugin = plugins.get(category);
        if (retPlugin == null) {
            throw new NoSuchPluginRegisteredException(category);
        } else {
            return retPlugin;
        }
    }
    
    //TODO: this should probably be a map
    public Map<String, NjordPluggable> getPlugins() {
        return plugins;
    }
}

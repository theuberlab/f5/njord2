/**
 * Moving all used iCons (see what I did there) to iconsinUse. The objective is to delete the contents of icons and see what breaks.
 * All new iconds should go here.
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.resources.iconsInUse;
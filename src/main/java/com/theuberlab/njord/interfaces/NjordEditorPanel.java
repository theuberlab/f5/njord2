package com.theuberlab.njord.interfaces;

import java.awt.ComponentOrientation;
import java.beans.PropertyChangeListener;
import java.io.IOException;

import org.fife.ui.rsyntaxtextarea.parser.TaskTagParser;
import org.fife.ui.rtextarea.RTextArea;

/**
 * <code>NjordEditorPanel</code> Is the interface that all editors which can be used in Njord must implement.
 * 
 * @author Aaron Forster
 *
 */
public interface NjordEditorPanel {
    /**
     * Save the content in the editor panel.
     * 
     * @throws IOException
     */
    public void save() throws IOException;
    
    /**
     * Delete the object contained in this editor.
     * 
     */
    public void deleteObject();
    
    /**
     * Undo.
     */
    public void undoLastAction();
    
    /**
     * Redo.
     */
    public void redoLastAction();
    
    /**
     * Get the panel's name (probably the content being edited.)
     * 
     * @return
     */
    public String getName();
    
    /**
     * A temporary class until I fix it. Returns the contained RTextArea. I need to figure out what the searchengine needs to do so I can implement those and no longer use this.
     * @return
     */
    public RTextArea getTextArea();
    
    /**
     * Cut selected text to clipboard.
     */
    public void cut();
    
    /**
     * Copy selected text to clipboard.
     */
    public void copy();
    
    /**
     * Paste text from clipboard.
     */
    public void paste();
    
    public ComponentOrientation getComponentOrientation();
    
    public void removeParser(TaskTagParser taskParser);
    
    public void addParser(TaskTagParser taskParser);
    
    public void addPropertyChangeListener(PropertyChangeListener listener);
    
    public void removePropertyChangeListener(PropertyChangeListener listener);
    
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener);
    
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener);
//    /**
//     * Delete the highlighted items.
//     */
//    public void deleteItems();
    
    /**
     * Select all
     */
    public void selectAll();
}
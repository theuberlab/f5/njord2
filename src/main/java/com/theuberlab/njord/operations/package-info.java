/**
 * Operations. I'm going to be implementing an async mechanism described here http://www.kdgregory.com/index.php?page=swing.async
 * 
 * @author Aaron Forster
 *
 */
package com.theuberlab.njord.operations;
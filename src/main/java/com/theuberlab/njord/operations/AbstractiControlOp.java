package com.theuberlab.njord.operations;

import javax.swing.JOptionPane;

import net.sf.swinglib.AsynchronousOperation;
import net.sf.swinglib.components.ProgressMonitor;

import com.theuberlab.njord.main.Njord;
import com.theuberlab.njord.main.NjordConcierge;



public abstract class AbstractiControlOp<T> extends AsynchronousOperation<T> {
    protected NjordConcierge concierge;
    protected String description;
  //  private S3Factory _factory;     // lazily initialized
  //  private S3Bucket _bucket;       // lazily initialized

    private ProgressMonitor _progressMonitor;


    protected AbstractiControlOp(NjordConcierge concierge, String description) {
        this.concierge = concierge;
        this.description = description;
    }


  //----------------------------------------------------------------------------
  //Workflow Methods
  //----------------------------------------------------------------------------

    /**
     *  Initiates a wait cursor and then puts this operation on the background
     *  thread. Subclasses may override to provide addition initialization,
     *  but should always delegate to this implementation.
     */
    public void start()
    {
        setBusyState(true);
        concierge.execute(this);
    }


    /**
     *  Clears the wait cursor / progress monitor.
     */
    @Override
    protected void onComplete()
    {
        setBusyState(false);
    }


    /**
     *  Common exception handler. Turns off wait cursor, logs exception, and
     *  notifies user.
     *  <p>
     *  Note that this is marked as <code>final</code>, to prevent subclasses
     *  from overriding.
     */
    @Override
    protected void onFailure(Throwable ex) {
        concierge.getLoggerHandle().debug("request failed [{}]", (Object)ex.getStackTrace());
        JOptionPane.showMessageDialog(
                concierge.getDialogOwner(),
                "Unable to process this request: " + ex.getMessage()
                    + "\nSee log for more information",
                "Unable to Execute Operation",
                JOptionPane.ERROR_MESSAGE);
    }


  //----------------------------------------------------------------------------
  //Support methods for subclasses
  //----------------------------------------------------------------------------

    protected NjordConcierge getConcierge() {
        return concierge;
    }

    
    
  

//
//  //----------------------------------------------------------------------------
//  //Operation
//  //----------------------------------------------------------------------------
//
//    @Override
//    protected FileListTableModel performOperation()
//    throws Exception
//    {
//        _logger.debug("starting refresh");
//        List<Map<String,String>> data = getBucket().listObjectsWithMetadata();
//        SortedSet<S3File> result = new TreeSet<S3File>();
//        for (Map<String,String> entry : data)
//        {
//            String key = entry.get(S3Constants.BUCKETLIST_KEY);
//            String size = entry.get(S3Constants.BUCKETLIST_SIZE);
//            String lastMod = entry.get(S3Constants.BUCKETLIST_LASTMOD);
//            result.add(new S3File(key, size, lastMod));
//        }
//        _logger.debug("request complete: " + result.size() + " files");
//
//        return new FileListTableModel(result);
//    }


//    @Override
//    protected void onSuccess(FileListTableModel result)
//    {
//        getConcierge().getMainFrame().resetList(result);
//    }

    /**
     *  Called during operation to update the progress monitor status
     *  message.
     */
    protected void updateProgressMonitor(String message)
    {
        _progressMonitor.setStatus(message);
    }


  //----------------------------------------------------------------------------
  //Internals
  //----------------------------------------------------------------------------

    /**
     *  Controls the various "wait indicators" -- pass <code>true</code> at
     *  the start of an operation, <code>false</code> at the end.
     */
    private void setBusyState(boolean isBusy)
    {
        //TODO: Fix this.
        ((Njord) concierge.getOwner()).setBusyState(isBusy);
        if (isBusy)
        {
            _progressMonitor = new ProgressMonitor(
                                    concierge.getDialogOwner(),
                                    "iControl Operation in progress",
                                    description,
                                    ProgressMonitor.Options.MODAL,
                                    ProgressMonitor.Options.CENTER,
                                    ProgressMonitor.Options.SHOW_STATUS);
            _progressMonitor.show();
        }
        else if (_progressMonitor != null)
        {
            _progressMonitor.dispose();
            _progressMonitor = null;
        }
    }
}
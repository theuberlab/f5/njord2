// Copyright (c) 2010 Keith D Gregory, all rights reserved
package com.theuberlab.njord.operations;

//AbstractS3Op from Keith Gregory's s3util application. Here as an example of what an action should likely look like.



//package com.kdgregory.app.s3util.ops;
//import java.util.List;
//import java.util.Map;
//import java.util.SortedSet;
//import java.util.TreeSet;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//
//import net.sf.s34j.defines.S3Constants;
//
//import com.kdgregory.app.s3util.filelist.FileListTableModel;
//import com.kdgregory.app.s3util.filelist.S3File;
//import com.kdgregory.app.s3util.main.Concierge;
//
//
///**
// *  Retrieves the list of files from S3.
// */
//public class S3RefreshOp
//extends AbstractS3Op<FileListTableModel>
//{
//    private Log _logger = LogFactory.getLog(this.getClass());
//
//    public S3RefreshOp(Concierge concierge)
//    {
//        super(concierge, "Refreshing list of files");
//    }
//
//
////----------------------------------------------------------------------------
////  Operation
////----------------------------------------------------------------------------
//
//    @Override
//    protected FileListTableModel performOperation()
//    throws Exception
//    {
//        _logger.debug("starting refresh");
//        List<Map<String,String>> data = getBucket().listObjectsWithMetadata();
//        SortedSet<S3File> result = new TreeSet<S3File>();
//        for (Map<String,String> entry : data)
//        {
//            String key = entry.get(S3Constants.BUCKETLIST_KEY);
//            String size = entry.get(S3Constants.BUCKETLIST_SIZE);
//            String lastMod = entry.get(S3Constants.BUCKETLIST_LASTMOD);
//            result.add(new S3File(key, size, lastMod));
//        }
//        _logger.debug("request complete: " + result.size() + " files");
//
//        return new FileListTableModel(result);
//    }
//
//
//    @Override
//    protected void onSuccess(FileListTableModel result)
//    {
//        getConcierge().getMainFrame().resetList(result);
//    }
//}

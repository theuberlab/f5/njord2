package com.theuberlab.njord.operations;

import java.util.List;

import net.sf.swinglib.components.ProgressMonitor;

import com.theuberlab.njord.constants.NjordNodeType;
import com.theuberlab.njord.main.NjordConcierge;
import com.theuberlab.njord.model.BigIP;
import com.theuberlab.njord.ui.NjordTreeNode;

/**
 * <code>BuildiRulesNavtreePortionOp</code>
 * 
 * @author Aaron Forster
 *
 */
public class FetchiRulesNavtreePortionOp extends AbstractiControlOp<Boolean> {
    // ###############################################################
    // VARIABLES
    // ###############################################################
    
    private ProgressMonitor _progressMonitor;
    private BigIP fetchBigIP = null;
    

    // ###############################################################
    // CONSTRUCTORS
    // ###############################################################
    
    public FetchiRulesNavtreePortionOp(NjordConcierge concierge) {
        super(concierge, "Fetching list of iRules from server");
    }
    
    // ###############################################################
    // METHODS
    // ###############################################################
    @Override
    protected Boolean performOperation() throws Exception {
        NjordTreeNode tree = concierge.getLastSelectedOrigin();
//      concierge.getOwner().buildNodesFromList(tree, results, NjordNodeContext.NODE_CONTEXT_REMOTE, "IRULE")
      
      
//      buildNodes(treeNode, NjordNodeType.NODE_TYPE_ORIGIN, NjordNodeContext.NODE_CONTEXT_REMOTE, "IRULE");
      // getConcierge().getMainFrame().resetList(result);

  
  
//this is the remote node type section of buildNodes() {
//      
      NjordTreeNode remoteiRulesCategory = new NjordTreeNode("iRules", NjordNodeType.NODE_TYPE_CATEGORY);
//      NjordTreeNode tree = concierge.getLastSelectedPathComponentType();
//      getPathOrigin
      tree.add(remoteiRulesCategory);

      //        
      NjordTreeNode addRule = null;
      fetchBigIP = concierge.getOwner().getSelectedOrigin();
//      fetchBigIP.setiRulesNames(getiRuleList(fetchBigIP.getInterface()));
//      
//      if (thisBigIp.getiRulesNames().isEmpty()) {
//          tuNotify("No valid iRules found on BIGIP!");
//          // resultsPanelNoticesBox.setText("No valid iRules found on BIGIP.");
//          // TextEditorPane ruleEditor = getEditorForRule(null, "No iRules Found On BIGIP");
//          NjordRuleEditorPanel ruleEditor = new NjordRuleEditorPanel(this, null, "No iRules Found On BIGIP");
//          NjordTreeNode bogusEmptyRuleNode = new NjordTreeNode(ruleEditor);
//          // addRule = new NjordTreeNode(ruleEditor);
//          tree.add(bogusEmptyRuleNode);
//      } else {
//          // This is for debugging purposes
//          // int childcount = tree.getChildCount();
//          // TODO: DO this better. Something like tree.getPath then check if we're looking for an iRule or a template (or however I do this for plugin
//          // method) Then treepath + category.
//          NjordTreeNode categoryNode = (NjordTreeNode) tree.getFirstChild();
//          // List<String> theRules = thisBigIp.getiRulesNames();
//          ArrayList<String> theRules = (ArrayList<String>) thisBigIp.getiRulesNames();
//          // for (String rule : theRules) {
//          for (Iterator<String> it = theRules.iterator(); it.hasNext();) {
//              String rule = it.next();
//              
//              if (!preferences.getShwSysiRules()) {
//                  log.debug("Checking sys iRules for rule [{}]", rule);
//                  if (rule.matches("/Common/_sys_.*")) { // V11 syntax
//                      log.info("Sys iRule, Skipping");
//                      // This is ALSO generating an unsupportedOperationException? WTF?
//                      it.remove();
//                      // This is generating an unsupportedOperationException?
//                      // theRules.remove(rule);
//                      continue;
//                  } else if (rule.matches("_sys_.*")) { // V10 and maybe V9
//                      log.info("Sys iRule, Skipping");
//                      it.remove();
//                      // theRules.remove(rule);
//                      continue;
//                  } else {
//                      log.debug("Non-sys iRule, keeping");
//                  }
//              }
//              // This is crap but until I get the time to work out a better way to do the GTM iRules as well we're still assuming LTM only.
//              NjordFileLocation ruleLocation = new NjordFileLocation(concierge, rule, NjordNodeCategory.NODE_CATEGORY_LOCAL_IRULE, ic);
//              NjordRuleEditorPanel ruleEditor = new NjordRuleEditorPanel(this, ruleLocation, rule);
//              NjordTreeNode addRuleNode = new NjordTreeNode(ruleEditor);
//              
//              categoryNode.add(addRuleNode);
//          }
//          // Don't forget to write the truncated list back to thisBigIp
//          thisBigIp.setiRulesNames(theRules);
//      }
//      
//  }
//
      return false;
    }
    
}

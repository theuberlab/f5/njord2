package com.theuberlab.njord.operations;

import javax.swing.JOptionPane;

import net.sf.swinglib.components.ProgressMonitor;

import com.theuberlab.common.constants.TUNotificationType;
import com.theuberlab.njord.main.NjordConcierge;
import com.theuberlab.njord.model.BigIP;

/**
 * <code>FetchiRulesOp</code> The operation for fetching iRules from a BIG-IP. I should probably have an AbstractiControlOP which this extends but let's hold
 * off until we see if I need it.
 * 
 * @author Aaron Forster
 * 
 * @param <T>
 */
public class ConnectToBigIPOpAndFetch extends AbstractiControlOp<Boolean> {
    // private NjordConcierge _concierge;
    // private String _description;
    // private S3Factory _factory; // lazily initialized
    // private S3Bucket _bucket; // lazily initialized
    
    private ProgressMonitor _progressMonitor;
    BigIP bigIP = null;
    
    public ConnectToBigIPOpAndFetch(NjordConcierge concierge) {
        super(concierge, "Initializing connection to BIG-IP");
    }
    
    // ----------------------------------------------------------------------------
    // Workflow Methods
    // ----------------------------------------------------------------------------
    
    // in super
    // /**
    // * Initiates a wait cursor and then puts this operation on the background
    // * thread. Subclasses may override to provide addition initialization,
    // * but should always delegate to this implementation.
    // */
    // public void start()
    // {
    // setBusyState(true);
    // _concierge.execute(this);
    // }
    
    // /**
    // * Clears the wait cursor / progress monitor.
    // */
    // @Override
    // protected void onComplete()
    // {
    // setBusyState(false);
    // }
    
    //
    // /**
    // * Common exception handler. Turns off wait cursor, logs exception, and
    // * notifies user.
    // * <p>
    // * Note that this is marked as <code>final</code>, to prevent subclasses
    // * from overriding.
    // */
    // @Override
    // protected void onFailure(Throwable ex) {
    // _concierge.getLoggerHandle().debug("request failed [{}]", (Object)ex.getStackTrace());
    // JOptionPane.showMessageDialog(
    // _concierge.getDialogOwner(),
    // "Unable to process this request: " + ex.getMessage()
    // + "\nSee log for more information",
    // "Unable to Execute Operation",
    // JOptionPane.ERROR_MESSAGE);
    // }
    
    /**
     * Called during operation to update the progress monitor status message.
     */
    protected void updateProgressMonitor(String message) {
        _progressMonitor.setStatus(message);
    }
    
    // ----------------------------------------------------------------------------
    // Internals
    // ----------------------------------------------------------------------------
    //
    // /**
    // * Controls the various "wait indicators" -- pass <code>true</code> at
    // * the start of an operation, <code>false</code> at the end.
    // */
    // private void setBusyState(boolean isBusy)
    // {
    // //TODO: Fix this.
    // ((Njord) _concierge.getOwner()).setBusyState(isBusy);
    // if (isBusy)
    // {
    // _progressMonitor = new ProgressMonitor(
    // _concierge.getDialogOwner(),
    // "S3 Operation in Progress",
    // _description,
    // ProgressMonitor.Options.MODAL,
    // ProgressMonitor.Options.CENTER,
    // ProgressMonitor.Options.SHOW_STATUS);
    // _progressMonitor.show();
    // }
    // else if (_progressMonitor != null)
    // {
    // _progressMonitor.dispose();
    // _progressMonitor = null;
    // }
    // }
    
    @Override
    protected Boolean performOperation() throws Exception {
        concierge.getOwner().getLoggerHandle().debug("Starting Connect Operation");
        bigIP = concierge.getOwner().getSelectedOrigin();
        
//        while (!bigIP.getIsConnected()) {
//            
//        }

        bigIP.connect();
        
        if (bigIP.getIsConnected()) {
            return true;
            // getiRulesFrom(Bigip bigip) or something like that.
        } else {
            return false;
        }
    }
    
    @Override
    protected void onSuccess(Boolean result) {
        concierge.getNotifier().sendNotification("Connected to BIG-IP [" + bigIP.getName() + "]", TUNotificationType.NOTIFICATION_TYPE_APPEND);
        new FetchiRulesOp(concierge).start();
    }
    
    @Override
    protected void onFailure(Throwable ex) {
        concierge.getNotifier().sendNotification("Failed to connect to BIG-IP [" + bigIP.getName() + "]");
    }
}
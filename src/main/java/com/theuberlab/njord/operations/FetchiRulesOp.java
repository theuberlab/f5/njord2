package com.theuberlab.njord.operations;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import net.sf.swinglib.components.ProgressMonitor;

import com.theuberlab.common.constants.TUNotificationType;
import com.theuberlab.common.util.TheUberlabExceptionHandler;
import com.theuberlab.njord.constants.NjordNodeCategory;
import com.theuberlab.njord.constants.NjordNodeType;
import com.theuberlab.njord.main.NjordConcierge;
import com.theuberlab.njord.model.BigIP;
import com.theuberlab.njord.ui.NjordRuleEditorPanel;
import com.theuberlab.njord.ui.NjordTreeNode;
import com.theuberlab.njord.util.NjordFileLocation;

/**
 * <code>FetchiRulesOp</code> The operation for fetching iRules from a BIG-IP. I should probably have an AbstractiControlOP which this extends but let's hold
 * off until we see if I need it.
 * 
 * @author Aaron Forster
 * 
 * @param <T>
 */
public class FetchiRulesOp extends AbstractiControlOp<List<String>> {
    // private NjordConcierge _concierge;
    // private String _description;
    // private S3Factory _factory; // lazily initialized
    // private S3Bucket _bucket; // lazily initialized
    
    private ProgressMonitor _progressMonitor;
    private BigIP fetchBigIP = null;
    
    public FetchiRulesOp(NjordConcierge concierge) {
        super(concierge, "Fetching list of iRules from server");
    }
    
    // ----------------------------------------------------------------------------
    // Workflow Methods
    // ----------------------------------------------------------------------------
    
    // in super
    // /**
    // * Initiates a wait cursor and then puts this operation on the background
    // * thread. Subclasses may override to provide addition initialization,
    // * but should always delegate to this implementation.
    // */
    // public void start()
    // {
    // setBusyState(true);
    // _concierge.execute(this);
    // }
    
    // /**
    // * Clears the wait cursor / progress monitor.
    // */
    // @Override
    // protected void onComplete()
    // {
    // setBusyState(false);
    // }
    
    //
    // /**
    // * Common exception handler. Turns off wait cursor, logs exception, and
    // * notifies user.
    // * <p>
    // * Note that this is marked as <code>final</code>, to prevent subclasses
    // * from overriding.
    // */
    // @Override
    // protected void onFailure(Throwable ex) {
    // _concierge.getLoggerHandle().debug("request failed [{}]", (Object)ex.getStackTrace());
    // JOptionPane.showMessageDialog(
    // _concierge.getDialogOwner(),
    // "Unable to process this request: " + ex.getMessage()
    // + "\nSee log for more information",
    // "Unable to Execute Operation",
    // JOptionPane.ERROR_MESSAGE);
    // }
    
    /**
     * Called during operation to update the progress monitor status message.
     */
    protected void updateProgressMonitor(String message) {
        _progressMonitor.setStatus(message);
    }
    
    // ----------------------------------------------------------------------------
    // Internals
    // ----------------------------------------------------------------------------
    //
    // /**
    // * Controls the various "wait indicators" -- pass <code>true</code> at
    // * the start of an operation, <code>false</code> at the end.
    // */
    // private void setBusyState(boolean isBusy)
    // {
    // //TODO: Fix this.
    // ((Njord) _concierge.getOwner()).setBusyState(isBusy);
    // if (isBusy)
    // {
    // _progressMonitor = new ProgressMonitor(
    // _concierge.getDialogOwner(),
    // "S3 Operation in Progress",
    // _description,
    // ProgressMonitor.Options.MODAL,
    // ProgressMonitor.Options.CENTER,
    // ProgressMonitor.Options.SHOW_STATUS);
    // _progressMonitor.show();
    // }
    // else if (_progressMonitor != null)
    // {
    // _progressMonitor.dispose();
    // _progressMonitor = null;
    // }
    // }
    
    // Aha, whatever this returns is what gets passed to onSuccess.
    
    @Override
    protected List<String> performOperation() throws Exception {
        concierge.getOwner().getLoggerHandle().debug("Starting Refresh Operation");
        fetchBigIP = concierge.getOwner().getSelectedOrigin();
        List<String> rulesList = null;
        
        if (!fetchBigIP.getIsConnected()) {
            // Connect dialog canceled
            concierge.getNotifier().sendNotification("Throw exception here!!", TUNotificationType.NOTIFICATION_TYPE_APPEND);
            concierge.getNotifier().sendNotification("Failed to connect to BIG-IP [" + fetchBigIP.getName() + "] How is this possible at all?");
            concierge.getNotifier().sendNotification("Throw exception here!!", TUNotificationType.NOTIFICATION_TYPE_APPEND);
            return null;
        }
        
        // Loop escaped, let's gets us some irules
        concierge.getNotifier().sendNotification("Fetching list of iRules from BIG-IP.", TUNotificationType.NOTIFICATION_TYPE_APPEND);
        fetchBigIP.setiRulesNames(getiRuleList(fetchBigIP.getInterface()));
        rulesList = fetchBigIP.getiRulesNames();
        concierge.getOwner().getLoggerHandle().debug("List successful, found [" + rulesList.size() + "] rules");
        
        
        return rulesList;
    }
    
    // After the op finishes finishes fix this.
    @Override
    protected void onSuccess(List<String> results) {
        // Temp for testing
        concierge.getNotifier().sendNotification("Fetching rules Success: ", TUNotificationType.NOTIFICATION_TYPE_APPEND);
        for (String ruleName : results) {
            concierge.getNotifier().sendNotification("Found rule [" + ruleName + "]", TUNotificationType.NOTIFICATION_TYPE_APPEND);
        }
        
        // new BuildiRulesNavtreePortionOp(concierge).start();
        
        // Doing this here for now even though it's dumb
        
        NjordTreeNode tree = concierge.getLastSelectedOrigin();
        NjordTreeNode remoteiRulesCategory = new NjordTreeNode("iRules", NjordNodeType.NODE_TYPE_CATEGORY);
        // NjordTreeNode tree = concierge.getLastSelectedPathComponentType();
        // getPathOrigin
        tree.add(remoteiRulesCategory);
        
        //
        
        NjordTreeNode addRule = null;
        fetchBigIP = concierge.getOwner().getSelectedOrigin();
        
        // fetchBigIP.setiRulesNames(getiRuleList(fetchBigIP.getInterface()));
        //
        if (fetchBigIP.getiRulesNames().isEmpty()) {
            // This is an odd hack. if there are zero rules found on the bigip we create a blank one called "No iRules found on BIG-IP"
            concierge.getNotifier().sendNotification("No valid iRules found on BIGIP!");
            NjordRuleEditorPanel ruleEditor = new NjordRuleEditorPanel(concierge, null, "No iRules Found On BIG-IP");
            NjordTreeNode bogusEmptyRuleNode = new NjordTreeNode(ruleEditor);
            tree.add(bogusEmptyRuleNode);
        } else {
            // This is for debugging purposes
            // int childcount = tree.getChildCount();
            // TODO: DO this better. Something like tree.getPath then check if we're looking for an iRule or a template (or however I do this for plugin
            // method) Then treepath + category.
            // This is no longer going to work if we have multiple categories.
            NjordTreeNode categoryNode = (NjordTreeNode) tree.getFirstChild();
            ArrayList<String> theRules = (ArrayList<String>) fetchBigIP.getiRulesNames();
            
            for (Iterator<String> it = theRules.iterator(); it.hasNext();) {
                String rule = it.next();
                if (!concierge.getPreferences().getShwSysiRules()) {
                    concierge.getOwner().getLoggerHandle().debug("Skipping sys iRules");
                    concierge.getOwner().getLoggerHandle().debug("Checking sys iRules for rule [{}]", rule);
                    if (rule.matches("/Common/_sys_.*")) { // V11 syntax
                        concierge.getOwner().getLoggerHandle().info("Sys iRule, Skipping");
                        // This is ALSO generating an unsupportedOperationException? WTF?
                        it.remove();
                        // This is generating an unsupportedOperationException?
                        // theRules.remove(rule);
                        continue;
                    } else if (rule.matches("_sys_.*")) { // V10 and maybe V9
                        concierge.getOwner().getLoggerHandle().info("Sys iRule, Skipping");
                        it.remove();
                        // theRules.remove(rule);
                        continue;
                    } else {
                        concierge.getOwner().getLoggerHandle().debug("Non-sys iRule, keeping");
                    }
                }
             
                // This is crap but until I get the time to work out a better way to do the GTM iRules as well we're still assuming LTM only.
                NjordFileLocation ruleLocation = new NjordFileLocation(concierge, rule, NjordNodeCategory.NODE_CATEGORY_LOCAL_IRULE, fetchBigIP.getInterface());
                NjordRuleEditorPanel ruleEditor = new NjordRuleEditorPanel(concierge, ruleLocation, rule);
                NjordTreeNode addRuleNode = new NjordTreeNode(ruleEditor);
                categoryNode.add(addRuleNode);
            }
            fetchBigIP.setiRulesNames(theRules);
        }

        
//          NjordRuleEditorPanel ruleEditor = new NjordRuleEditorPanel(this, null, "No iRules Found On BIGIP");
//          NjordTreeNode bogusEmptyRuleNode = new NjordTreeNode(ruleEditor);
//          // addRule = new NjordTreeNode(ruleEditor);
//          tree.add(bogusEmptyRuleNode);
//      } else {
//          // This is for debugging purposes
//          // int childcount = tree.getChildCount();
//          // TODO: DO this better. Something like tree.getPath then check if we're looking for an iRule or a template (or however I do this for plugin
//          // method) Then treepath + category.
//          NjordTreeNode categoryNode = (NjordTreeNode) tree.getFirstChild();
//          // List<String> theRules = thisBigIp.getiRulesNames();
//          ArrayList<String> theRules = (ArrayList<String>) thisBigIp.getiRulesNames();
//          // for (String rule : theRules) {
//          for (Iterator<String> it = theRules.iterator(); it.hasNext();) {
//              String rule = it.next();
//              
//              if (!preferences.getShwSysiRules()) {
//                  log.debug("Checking sys iRules for rule [{}]", rule);
//                  if (rule.matches("/Common/_sys_.*")) { // V11 syntax
//                      log.info("Sys iRule, Skipping");
//                      // This is ALSO generating an unsupportedOperationException? WTF?
//                      it.remove();
//                      // This is generating an unsupportedOperationException?
//                      // theRules.remove(rule);
//                      continue;
//                  } else if (rule.matches("_sys_.*")) { // V10 and maybe V9
//                      log.info("Sys iRule, Skipping");
//                      it.remove();
//                      // theRules.remove(rule);
//                      continue;
//                  } else {
//                      log.debug("Non-sys iRule, keeping");
//                  }
//              }
//              // This is crap but until I get the time to work out a better way to do the GTM iRules as well we're still assuming LTM only.
//              NjordFileLocation ruleLocation = new NjordFileLocation(concierge, rule, NjordNodeCategory.NODE_CATEGORY_LOCAL_IRULE, ic);
//              NjordRuleEditorPanel ruleEditor = new NjordRuleEditorPanel(this, ruleLocation, rule);
//              NjordTreeNode addRuleNode = new NjordTreeNode(ruleEditor);
//              
//              categoryNode.add(addRuleNode);
//          }
//          // Don't forget to write the truncated list back to thisBigIp
//          thisBigIp.setiRulesNames(theRules);
//      }
//      
//  }
//
        
        
    }
    
    /**
     * 
     * getiRuleList returns only a string list of the names of the iRules instead of actual iRule objects
     * 
     * @return The list of iRules from the BIGIP.
     * @throws Exception
     */
    public List<String> getiRuleList(iControl.Interfaces ic) {
        // LTM and GTM are the only two sections that have iRules.
        String[] LTMiRules = null;
//        String[] GTMiRules = null;
        List<String> iRules = null;
        
        try {
            LTMiRules = ic.getLocalLBRule().get_list();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
//        try {
//            GTMiRules = ic.getGlobalLBRule().get_list();
//            // iRules = ic.getGlobalLBRule().get_list();
//        } catch (RemoteException e) {
//            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
//            exceptionHandler.processException();
//        } catch (Exception e) {
//            TheUberlabExceptionHandler exceptionHandler = new TheUberlabExceptionHandler(e);
//            exceptionHandler.processException();
//        }
        
        // This is crap but until I come up with a better way we're still only going to do LTM iRules.
        iRules = Arrays.asList(LTMiRules);
        // TODO: Make this part work. I'm pretty sure if we try and edit a GTM iRule we won't be able to save it. This is because I'm just adding everything so
        // I don't know which is a local and which is a remote.
        // Not sure if this will work. I'll have to be able to determine the rule type when i try and save it.
        // TODO: O.K. I'm NOT going to make fetching of GTM iRules work together with the local iRules. GTM (or "global iRules") will be handled by a separate
        // plugin.
        // iRules.addAll(Arrays.asList(GTMiRules));
        
//        log.info("Available Rules");
//        for (String string : iRules) {
//            log.debug("   " + string);
//        }
        return iRules;
    }
    
}